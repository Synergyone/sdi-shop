<?php namespace SunnyDayInc\Shop\Translation;

use SunnyDayInc\Shop\Contracts\Translator as TranslatorInterface;

use Symfony\Component\Translation\Translator as SymfonyTranslator;
use Symfony\Component\Translation\MessageSelector;
use Symfony\Component\Translation\Loader\PhpFileLoader;

class Translator implements TranslatorInterface
{

    /**
     * The loader implementation.
     *
     * @var \Symfony\Component\Translation\Loader\LoaderInterface
     */
    protected $loader;

    /**
     * The default locale being used by the translator.
     *
     * @var string
     */
    protected $locale;

    /**
     * The default locale path being used by the translator.
     *
     * @var string
     */
    protected $localePath;

    /**
     * The translator object.
     *
     * @var \Symfony\Component\Translation\TranslatorInterface
     */
    protected $trans;
  
    /**
     * Create a new instance.
     *
     * @param  string $locale
     * @param  string $path
     * @return void
     */
    public function __construct($locale = '', $path = '')
    {
        $this->createLoader();

        return $this->setLocale($locale, $path);
    }

    /**
     * {@inheritdoc}
     */
    public function setLocale($locale, $path = '', $domain = 'messages')
    {
        $this->locale = $locale;

        if (! empty($path)) {
            $this->localePath = $path;
        }

        $this->domain = $domain;

        // path to the .php file that we should monitor
        $file = $this->localePath.'/'.$locale.'/'.$this->domain.'.php';

        $fallbackLocale = substr($this->locale, 0, 2);

        $this->trans = $this->createTranslator($this->locale);
        $this->trans->setFallbackLocales([$fallbackLocale]);
        $this->trans->addLoader('php', $this->loader);
        $this->trans->addResource('php', $file, $this->locale, $this->domain);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function trans($text, $parameters = [], $domain = 'messages')
    {
        if (! is_array($parameters)) {
            $domain = $parameters;
            $parameters = [];
        }

        if ($domain !== $this->domain) {
            $this->setLocale($this->locale, $this->localePath, $domain);
        }

        return $this->trans->trans($text, $parameters, $this->domain);
    }

    /**
     * Create the translator object.
     *
     * @param  string $locale
     * @return \Symfony\Component\Translation\TranslatorInterface
     */
    private function createTranslator($locale)
    {
        return new SymfonyTranslator($locale, new MessageSelector());
    }

    /**
     * Get the loader object.
     *
     * @return \Symfony\Component\Translation\Loader\LoaderInterface
     */
    private function createLoader()
    {
        return $this->loader = new PhpFileLoader;
    }

}
