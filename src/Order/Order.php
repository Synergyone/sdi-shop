<?php namespace SunnyDayInc\Shop\Order;

use SunnyDayInc\Shop\Contracts\Order as OrderInterface;
use SunnyDayInc\Shop\Order\Repository\Order as OrderRepository;
use SunnyDayInc\Shop\Models\OrderInterface as OrderModelInterface;

class Order implements OrderInterface
{

    const STATE_CART        = 'cart';
    const STATE_CART_LOCKED = 'cart_locked';
    const STATE_PENDING     = 'pending';
    const STATE_CONFIRMED   = 'confirmed';
    const STATE_HOLDED      = 'holded';
    const STATE_SHIPPED     = 'shipped';
    const STATE_ABANDONED   = 'abandoned';
    const STATE_CANCELLED   = 'cancelled';
    const STATE_RETURNED    = 'returned';
    const STATE_COMPLETED   = 'completed';

    const ENTITY_ORDER       = 'order';
    const ENTITY_INVOICE     = 'invoice';
    const ENTITY_CREDIT_MEMO = 'creditmemo';
    const ENTITY_SHIPMENT    = 'shipment';

    const ADDRESS_TYPE_BILLING  = 'billing';
    const ADDRESS_TYPE_SHIPPING = 'shipping';

    /**
     * The repository instance.
     *
     * @var \SunnyDayInc\Shop\Order\Repository\Order
     */
    protected $repository;

    /**
     * The log instance.
     *
     * @var mixed
     */
    protected $log;

    /**
     * The field formats.
     *
     * @var array
     */
    protected $fieldFormats = [];
  
    /**
     * Create a new instance.
     *
     * @param  \SunnyDayInc\Shop\Order\Repository\Order $repository
     * @return void
     */
    public function __construct(OrderRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    public function setLogger($log)
    {
        $this->log = $log;
    }

    /**
     * {@inheritdoc}
     */
    public function addFieldFormat($field, Array $format)
    {
        $this->fieldFormats[$field] = $format;
    }

    /**
     * {@inheritdoc}
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * {@inheritdoc}
     */
    public function create(Array $data = [])
    {
        return $this->createFactory($data);
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        $order = $this->repository->find($id);

        if ($order instanceOf OrderModelInterface) {
            $factory = $this->createFactory();

            $factory->setOrder($order);

            unset($order);

            return $factory;
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function findByIdentifier($identifier)
    {
        $order = $this->repository->findByIdentifier($identifier);

        if ($order instanceOf OrderModelInterface) {
            $factory = $this->createFactory();

            $factory->setOrder($order);

            unset($order);

            return $factory;
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function findByObject($objectType, $objectId, $id)
    {
        $order = $this->repository->findByObject($objectType, $objectId, $id);

        if ($order instanceOf OrderModelInterface) {
            $factory = $this->createFactory();

            $factory->setOrder($order);

            unset($order);

            return $factory;
        }

        return null;
    }

    /**
     * Create the factory instance.
     *
     * @param  array $data
     * @return \SunnyDayInc\Shop\Order\FactoryInterface
     */
    private function createFactory(Array $data = [])
    {
        $factory = new Factory($this->repository, $this->fieldFormats, $data);

        if (! is_null($this->log)) {
            $factory->setLogger($this->log);
        }

        return $factory;
    }

}
