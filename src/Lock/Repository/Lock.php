<?php namespace SunnyDayInc\Shop\Lock\Repository;

interface Lock
{

    /**
     * Return the Model class name
     *
     * @return string
     */
    public function model();

}
