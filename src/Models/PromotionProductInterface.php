<?php namespace SunnyDayInc\Shop\Models;

use SunnyDayInc\Shop\Contracts\Model;

interface PromotionProductInterface extends Model
{

    /**
     * Get the promotion that owns the product.
     */
    public function promotion();

}
