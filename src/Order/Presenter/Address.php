<?php namespace SunnyDayInc\Shop\Order\Presenter;

use SunnyDayInc\Shop\Presenter\Presenter;
use SunnyDayInc\Shop\Models\OrderAddressInterface;

class Address extends Presenter
{

    /**
     * The Address model.
     *
     * @var Address
     */
    protected $_model;

    /**
     * {@inheritdoc}
     */
    protected $publicData = [
    'id'             => 'id', 
    'location_title' => 'title', 
    'name'           => 'name', 
    'company'        => 'company', 
    'phone'          => 'phone', 
    'mobile_phone'   => 'mobile_phone', 
    'address'        => 'address', 
    'country_code'   => 'country_code', 
    'country_name'   => 'country_name', 
    'state'          => 'state', 
    'city_id'        => 'city_id', 
    'city_name'      => 'city_name', 
    'postcode'       => 'postcode'
    ];

    /**
     * Create the Presenter and store the object we are presenting.
     *
     * @param  OrderAddressInterface $address
     * @param  array                 $fields
     * @return void
     */
    public function __construct(OrderAddressInterface $address, Array $fields = ['*'])
    {
        parent::__construct($address->toArray());

        $this->_model = $address;
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentId()
    {
        return (int) $this->_model->id;
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentCityId()
    {
        return (int) $this->_model->city_id;
    }

}
