<?php namespace SunnyDayInc\Shop\ServiceProvider\Illuminate;

use Illuminate\Support\ServiceProvider;

use SunnyDayInc\Shop\Invoice\Invoice as InvoiceContainer;
use SunnyDayInc\Shop\Invoice\Repository\EloquentInvoice;

class Invoice extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes(
            [
            __DIR__.'/../../config/invoice.php' => config_path('sunnydayinc/shop/invoice.php')
            ], 'config'
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            'SunnyDayInc\Shop\Contracts\Invoice', function ($app) {
                $invoice = new InvoiceContainer(new EloquentInvoice());

                $invoice->setLogger($app['log']);

                $fieldFormats = $app['config']->get('sunnydayinc.shop.invoice.field_formats');

                if (! empty($fieldFormats)) {
                    foreach ($fieldFormats as $field => $format) {
                        $invoice->addFieldFormat($field, $format);
                    }
                }

                unset($fieldFormats);

                return $invoice;
            }
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['SunnyDayInc\Shop\Contracts\Invoice'];
    }
}
