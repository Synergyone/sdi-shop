<?php namespace SunnyDayInc\Shop\ServiceProvider\Illuminate;

use Illuminate\Support\ServiceProvider;

use SunnyDayInc\Shop\Lock\Repository\EloquentLock;
use SunnyDayInc\Shop\Lock\Lock as LockContainer;

use BeatSwitch\Lock\Manager;
use BeatSwitch\Lock\Drivers\ArrayDriver;

class Lock extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes(
            [
            __DIR__.'/../../config/lock.php' => config_path('sunnydayinc/shop/lock.php')
            ], 'config'
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            'SunnyDayInc\Shop\Contracts\Lock', function ($app) {
                $this->registerManager();

                $table = $app['config']->get('sunnydayinc.shop.lock.table');

                return new LockContainer(new EloquentLock($table), $app['lock.manager']);
            }
        );
    }

    /**
     * Register the lock manager instance.
     *
     * @return void
     */
    private function registerManager()
    {
        $this->app['lock.manager'] = $this->app->share(
            function ($app) {
                return new Manager(new ArrayDriver());
            }
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['SunnyDayInc\Shop\Contracts\Lock', 'lock.manager'];
    }
}
