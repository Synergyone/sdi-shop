<?php namespace SunnyDayInc\Shop\Promotion;

use SunnyDayInc\Shop\Contracts\Promotion as PromotionInterface;
use SunnyDayInc\Shop\Promotion\Repository\Promotion as PromotionRepository;
use SunnyDayInc\Shop\Models\PromotionInterface as PromotionModelInterface;

class Promotion implements PromotionInterface
{

    const TYPE_CATALOG = 'catalog';
    const TYPE_CART    = 'cart';

    const ACTION_BY_PERCENT  = 'by_percent';
    const ACTION_BY_FIXED    = 'by_fixed';
    const ACTION_TO_PERCENT  = 'to_percent';
    const ACTION_TO_FIXED    = 'to_fixed';
    const ACTION_BUY_X_GET_Y = 'buy_x_get_y';

    /**
     * The repository instance.
     *
     * @var \SunnyDayInc\Shop\Promotion\Repository\Promotion
     */
    protected $repository;

    /**
     * The log instance.
     *
     * @var \Illuminate\Contracts\Logging\Log
     */
    protected $log;

    /**
     * Available catalog simple actions.
     *
     * @var array
     */
    protected $catalogActions;

    /**
     * Available cart simple actions.
     *
     * @var array
     */
    protected $cartActions;
  
    /**
     * Create a new instance.
     *
     * @param  \SunnyDayInc\Shop\Promotion\Repository\Promotion $repository
     * @return void
     */
    public function __construct(PromotionRepository $repository)
    {
        $this->repository = $repository;

        $this->catalogActions = [
        self::ACTION_BY_PERCENT, 
        self::ACTION_BY_FIXED, 
        self::ACTION_TO_PERCENT, 
        self::ACTION_TO_FIXED
        ];

        $this->cartActions = [
        self::ACTION_BY_PERCENT, 
        self::ACTION_BY_FIXED, 
        self::ACTION_BUY_X_GET_Y
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function setLogger($log)
    {
        $this->log = $log;
    }

    /**
     * {@inheritdoc}
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * Return available catalog actions.
     *
     * @return array
     */
    public function getCatalogActions()
    {
        return $this->catalogActions;
    }

    /**
     * Return available cart actions.
     *
     * @return array
     */
    public function getCartActions()
    {
        return $this->cartActions;
    }

    /**
     * {@inheritdoc}
     */
    public function create(Array $data = [])
    {
        return $this->createFactory($data);
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        $promotion = $this->repository->find($id);

        if ($promotion instanceOf PromotionModelInterface) {
            $factory = $this->createFactory();

            $factory->setPromotion($promotion);

            unset($promotion);

            return $factory;
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function findByObject($objectType, $objectId, $id)
    {
        $promotion = $this->repository->findByObject($objectType, $objectId, $id);

        if ($promotion instanceOf PromotionModelInterface) {
            $factory = $this->createFactory();

            $factory->setPromotion($promotion);

            unset($promotion);

            return $factory;
        }

        return null;
    }

    /**
     * Create the factory instance.
     *
     * @param  array $data
     * @return \SunnyDayInc\Shop\Promotion\FactoryInterface
     */
    private function createFactory(Array $data = [])
    {
        $factory = new Factory($this->repository, $data);

        if (! is_null($this->log)) {
            $factory->setLogger($this->log);
        }

        return $factory;
    }

}
