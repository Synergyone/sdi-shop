<?php namespace SunnyDayInc\Shop\Payment\Formatter;

interface FormatterInterface
{

    /**
     * Return the formatted value.
     *
     * @return string
     */
    public function compile();

}
