<?php namespace SunnyDayInc\Shop\Cart;

use SunnyDayInc\Shop\Support\Collection;
use SunnyDayInc\Shop\Cart\Repository\ProductInterface as ProductRepository;

use SunnyDayInc\Shop\Models\CartInterface;
use SunnyDayInc\Shop\Models\CartItemInterface;
use SunnyDayInc\Shop\Models\PromotionCouponInterface;

interface FactoryInterface
{

    /**
     * Set the logger instance.
     *
     * @param  mixed $log
     * @return self
     */
    public function setLogger($log);

    /**
     * Define product repository instance.
     *
     * @param  \SunnyDayInc\Shop\Cart\Repository\ProductInterface $repository
     * @return void
     */
    public function setProductRepository(ProductRepository $repository);

    /**
     * Set the cart instance.
     *
     * @param  \SunnyDayInc\Shop\Models\CartInterface $cart
     * @return self
     */
    public function setCart(CartInterface $cart);

    /**
     * Return the cart instance.
     *
     * @return \SunnyDayInc\Shop\Models\CartInterface
     */
    public function getCart();

    /**
     * Set promotion coupon instance.
     *
     * @param  \SunnyDayInc\Shop\Models\PromotionCouponInterface $coupon
     * @return self
     */
    public function setCoupon(PromotionCouponInterface $coupon);

    /**
     * Remove promotion coupon.
     *
     * @return self
     */
    public function removeCoupon();

    /**
     * Return the coupon instance.
     *
     * @return \SunnyDayInc\Shop\Models\PromotionCouponInterface
     */
    public function getCoupon();

    /**
     * Set the object owner.
     *
     * @param  string  $objectType
     * @param  integer $objectId
     * @return self
     */
    public function setObjectOwner($objectType, $objectId);

    /**
     * Set the customer Id.
     *
     * @param  integer $id
     * @return self
     */
    public function setCustomerId($id);

    /**
     * Set the address Id.
     *
     * @param  integer $id
     * @return self
     */
    public function setAddressId($id);

    /**
     * Check is having unavilable items.
     *
     * @return boolean
     */
    public function isHavingUnavailableItems();

    /**
     * Return unavilable items.
     *
     * @return \Collection
     */
    public function getUnavailableItems();

    /**
     * Return the items.
     *
     * @return \Collection|\SunnyDayInc\Shop\Models\CartItemInterface[]
     */
    public function getItems();

    /**
     * Get a item by it's ID.
     *
     * @param  string $id
     * @return \SunnyDayInc\Shop\Models\CartItemInterface|null
     * @throws \SunnyDayInc\Shop\Cart\Exceptions\CartNotFoundException
     * @throws \SunnyDayInc\Shop\Cart\Exceptions\CartItemNotFoundException
     */
    public function getItem($id);

    /**
     * Insert a new item.
     *
     * @param  \SunnyDayInc\Shop\Models\CartItemInterface $item
     * @return self
     */
    public function addItem(CartItemInterface $item);

    /**
     * Update a item.
     *
     * @param  string $id
     * @param  array  $data
     * @return self
     * @throws \SunnyDayInc\Shop\Cart\Exceptions\CartItemNotFoundException
     */
    public function updateItem($id, Array $data);

    /**
     * Delete a item.
     *
     * @param  string $id
     * @return self
     * @throws \SunnyDayInc\Shop\Cart\Exceptions\CartItemNotFoundException
     */
    public function deleteItem($id);

    /**
     * Create a new cart item.
     *
     * @return \SunnyDayInc\Shop\Models\CartItemInterface
     */
    public function createItem();

    /**
     * Calculate cart pricing.
     *
     * @return self
     */
    public function calculate();

    /**
     * Save the cart.
     *
     * @return boolean
     */
    public function save();

    /**
     * Delete the cart.
     *
     * @return boolean
     */
    public function delete();

}
