<?php namespace SunnyDayInc\Shop\Models;

use SunnyDayInc\Shop\Promotion\Presenter\Coupon as CouponPresenter;
use DateTime;

trait PromotionCoupon
{

    /**
     * Return the ID.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Return the code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Return the expire at.
     *
     * @return DateTime
     */
    public function getExpireAt()
    {
        return DateTime::createFromFormat('Y-m-d H:i:s', $this->expire_at);
    }

    /**
     * {@inheritdoc}
     */
    public function getPresenter(Array $fields = ['*'])
    {
        return new CouponPresenter($this, $fields);
    }

}
