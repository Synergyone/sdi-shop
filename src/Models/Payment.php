<?php namespace SunnyDayInc\Shop\Models;

use SunnyDayInc\Shop\Payment\Presenter\Payment as PaymentPresenter;

trait Payment
{

    /**
     * {@inheritdoc}
     */
    public function getPresenter(Array $fields = ['*'])
    {
        return new PaymentPresenter($this, $fields);
    }

}
