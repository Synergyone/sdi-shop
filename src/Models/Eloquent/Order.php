<?php namespace SunnyDayInc\Shop\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

use SunnyDayInc\Shop\Models\OrderInterface;
use SunnyDayInc\Shop\Models\PromotionValidationInterface;
use SunnyDayInc\Shop\Models\Order as OrderModel;
use SunnyDayInc\Shop\Contracts\Presentable;

class Order extends Model implements OrderInterface, PromotionValidationInterface, Presentable
{

    use OrderModel;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'order';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'object_type', 
    'object_id', 
    'branch_id', 
    'identifier', 
    'code', 
    'customer_id', 
    'is_new_customer', 
    'promotion_coupon_name', 
    'promotion_coupon_code', 
    'total_qty', 
    'discount_amount', 
    'base_discount_amount', 
    'discount_canceled', 
    'base_discount_canceled', 
    'discount_invoiced', 
    'base_discount_invoiced', 
    'discount_refunded', 
    'base_discount_refunded', 
    'shipping_discount_amount', 
    'base_shipping_discount_amount', 
    'shipping_amount', 
    'base_shipping_amount', 
    'shipping_tax_amount', 
    'base_shipping_tax_amount', 
    'shipping_tax_refunded', 
    'base_shipping_tax_refunded', 
    'shipping_incl_tax', 
    'base_shipping_incl_tax', 
    'shipping_canceled', 
    'base_shipping_canceled', 
    'shipping_invoiced', 
    'base_shipping_invoiced', 
    'shipping_refunded', 
    'base_shipping_refunded', 
    'tax_amount', 
    'base_tax_amount', 
    'tax_canceled', 
    'base_tax_canceled', 
    'tax_invoiced', 
    'base_tax_invoiced', 
    'tax_refunded', 
    'base_tax_refunded', 
    'subtotal', 
    'base_subtotal', 
    'subtotal_incl_tax', 
    'base_subtotal_incl_tax', 
    'subtotal_canceled', 
    'base_subtotal_canceled', 
    'subtotal_invoiced', 
    'base_subtotal_invoiced', 
    'subtotal_refunded', 
    'base_subtotal_refunded', 
    'total_canceled', 
    'base_total_canceled', 
    'total_invoiced', 
    'base_total_invoiced', 
    'total_paid', 
    'base_total_paid', 
    'total', 
    'base_total', 
    'total_refunded', 
    'base_total_refunded', 
    'total_due', 
    'base_total_due', 
    'grand_total', 
    'base_grand_total', 
    'order_currency_code', 
    'base_currency_code', 
    'global_currency_code', 
    'base_to_order_rate', 
    'base_to_global_rate', 
    'customer_is_guest', 
    'customer_balance_amount', 
    'base_customer_balance_amount', 
    'customer_balance_invoiced', 
    'base_customer_balance_invoiced', 
    'customer_balance_refunded', 
    'base_customer_balance_refunded', 
    'customer_balance_total_refunded', 
    'base_customer_balance_total_refunded', 
    'status', 
    'ip_address', 
    'accepted_at', 
    'completed_at', 
    'cancelled_at'
    ];

    /**
     * Return billing address.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function billingAddress()
    {
        return $this->hasOne('SunnyDayInc\Shop\Models\Eloquent\OrderAddress')
            ->where('type', '=', 'billing');
    }

    /**
     * Return items.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany('SunnyDayInc\Shop\Models\Eloquent\OrderItem');
    }

}
