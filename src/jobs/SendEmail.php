<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

use SunnyDayInc\Shop\Mail\TemplateInterface;
use SunnyDayInc\Shop\Contracts\Mailer;

class SendEmail extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * The template instance.
     *
     * @var \SunnyDayInc\Shop\Mail\TemplateInterface
     */
    protected $template;

    /**
     * The options.
     *
     * @var array
     */
    protected $options;

    /**
     * Create a new job instance.
     *
     * @param  \SunnyDayInc\Shop\Mail\TemplateInterface $template
     * @param  array                                    $options
     * @return void
     */
    public function __construct(TemplateInterface $template, Array $options = [])
    {
        $this->template = $template;
        $this->options  = $options;
    }

    /**
     * Execute the job.
     *
     * @param  \SunnyDayInc\Shop\Contracts\Mailer $mailer
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        $options = $this->options;

        $baseItem = new \stdClass();
        $baseItem->name = 'Product A';
        $baseItem->quantity = 2;
        $baseItem->price = 4030494;

        $order = new \stdClass();
        $order->items = [
        clone $baseItem, 
        clone $baseItem, 
        clone $baseItem, 
        clone $baseItem
        ];

        // Define data to each template code handlers
        $this->template->setData('ORDER_ID', 'ORD/3903904/594lkjd3289');
        $this->template->setData('CUSTOMER_NAME', 'Sulaeman Tea');
        $this->template->setData('ORDER_LIST', $order);

        $mailer->send(
            $this->template, function ($message) use ($options) {

                // Define mail subject
                if (isset($options['subject'])) {
                    $message->subject($options['subject']);
                }

                // Define mail sender
                if (isset($options['from'])) {
                    $address = (isset($options['from']['address'])) ? $options['from']['address'] : null;
                    $name    = (isset($options['from']['name'])) ? $options['from']['name'] : null;

                    if (! is_null($address) && ! empty($address)) {
                        $message->from($address, $name);
                    }
                }

                // Define mail receiver
                if (isset($options['to'])) {
                    $address = (isset($options['to']['address'])) ? $options['to']['address'] : null;
                    $name    = (isset($options['to']['name'])) ? $options['to']['name'] : null;

                    if (! is_null($address) && ! empty($address)) {
                        $message->to($address, $name);
                    }
                }

                // Define mail reply target
                if (isset($options['reply_to'])) {
                    $address = (isset($options['reply_to']['address'])) ? $options['reply_to']['address'] : null;
                    $name    = (isset($options['reply_to']['name'])) ? $options['reply_to']['name'] : null;

                    if (! is_null($address) && ! empty($address)) {
                        $message->replyTo($address, $name);
                    }
                }

                // Define mail cc
                if (isset($options['cc'])) {
                    $address = (isset($options['cc']['address'])) ? $options['cc']['address'] : null;
                    $name    = (isset($options['cc']['name'])) ? $options['cc']['name'] : null;

                    if (! is_null($address) && ! empty($address)) {
                        $message->cc($address, $name);
                    }
                }

                // Define mail bcc
                if (isset($options['bcc'])) {
                    $address = (isset($options['bcc']['address'])) ? $options['bcc']['address'] : null;
                    $name    = (isset($options['bcc']['name'])) ? $options['bcc']['name'] : null;

                    if (! is_null($address) && ! empty($address)) {
                        $message->bcc($address, $name);
                    }
                }

                // Define mail attachments
                if (isset($options['attachments'])) {
                    if (! empty($options['attachments'])) {
                        foreach ($options['attachments'] as $item) {
                            if (isset($item['file'])) {
                                $options = [];
              
                                if (isset($item['as'])) {
                                    $options['as'] = $item['as'];
                                }

                                if (isset($item['mime'])) {
                                    $options['mime'] = $item['mime'];
                                }

                                $message->attach($item['file'], $options);
                            }
                        }
                    }
                }
            }
        );
    }
}
