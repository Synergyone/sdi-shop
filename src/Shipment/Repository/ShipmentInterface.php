<?php namespace SunnyDayInc\Shop\Shipment\Repository;

interface ShipmentInterface
{

    /**
     * Return a shipment by it's ID.
     *
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Contracts\Model|null
     */
    public function find($id);

    /**
     * Return shipments.
     *
     * @param  array   $where
     * @param  array   $column
     * @param  integer $page
     * @param  integer $limit
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findBy(
        Array $where = [], 
        Array $column = ['*'], 
        $page = 1, 
        $limit = 10, 
        $order = 'DESC', 
        $orderBy = 'created_at'
    );

    /**
     * Return the current total row in previous query.
     *
     * @return string
     */
    public function getTotal();

    /**
     * Return the shipment tracks by shipment ID.
     *
     * @param  integer $shipmentId
     * @return \Collection|\SunnyDayInc\Shop\Models\ShipmentTrackInterface[]
     */
    public function findTracksByShipment($shipmentId);

    /**
     * Return the Model class name
     *
     * @return string
     */
    public function model();

    /**
     * Return the Model Track class name
     *
     * @return string
     */
    public function modelTrack();

    /**
     * Return the Model Item class name
     *
     * @return string
     */
    public function modelItem();

    /**
     * Return the Model Comment class name
     *
     * @return string
     */
    public function modelComment();

    /**
     * Return the model object creation.
     *
     * @return \SunnyDayInc\Shop\Models\ShipmentInterface
     */
    public function createModel();

    /**
     * Return the model track object creation.
     *
     * @return \SunnyDayInc\Shop\Models\ShipmentTrackInterface
     */
    public function createModelTrack();

    /**
     * Return the model item object creation.
     *
     * @return \SunnyDayInc\Shop\Models\ShipmentItemInterface
     */
    public function createModelItem();

    /**
     * Return the model comment object creation.
     *
     * @return \SunnyDayInc\Shop\Models\ShipmentCommentInterface
     */
    public function createModelComment();

}
