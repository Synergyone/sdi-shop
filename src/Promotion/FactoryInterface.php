<?php namespace SunnyDayInc\Shop\Promotion;

use SunnyDayInc\Shop\Models\PromotionInterface;
use SunnyDayInc\Shop\Models\PromotionCouponInterface;

interface FactoryInterface
{

    /**
     * Set the logger instance.
     *
     * @param  mixed $log
     * @return self
     */
    public function setLogger($log);

    /**
     * Set the promotion instance.
     *
     * @param  \SunnyDayInc\Shop\Models\PromotionInterface $promotion
     * @return self
     */
    public function setPromotion(PromotionInterface $promotion);

    /**
     * Return the promotion instance.
     *
     * @return \SunnyDayInc\Shop\Models\PromotionInterface
     */
    public function getPromotion();

    /**
     * Return the coupons.
     *
     * @return \Collection|\SunnyDayInc\Shop\Models\PromotionCouponInterface[]
     */
    public function getCoupons();

    /**
     * Get a coupon by it's ID.
     *
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Models\PromotionCouponInterface|null
     */
    public function getCoupon($id);

    /**
     * Insert a new coupon.
     *
     * @param  \SunnyDayInc\Shop\Models\PromotionCouponInterface $coupon
     * @return self
     */
    public function addCoupon(PromotionCouponInterface $coupon);

    /**
     * Update a coupon.
     *
     * @param  \SunnyDayInc\Shop\Models\PromotionCouponInterface $coupon
     * @return self
     */
    public function updateCoupon(PromotionCouponInterface $coupon);

    /**
     * Remove a coupon.
     *
     * @param  \SunnyDayInc\Shop\Models\PromotionCouponInterface $coupon
     * @return self
     */
    public function removeCoupon(PromotionCouponInterface $coupon);

    /**
     * Create a new invoice coupon.
     *
     * @return \SunnyDayInc\Shop\Models\PromotionCouponInterface
     */
    public function createCoupon();

    /**
     * Update the promotion.
     *
     * @param  array $data
     * @return self
     */
    public function update(Array $data);

    /**
     * Save the promotion.
     *
     * @return boolean
     */
    public function save();

    /**
     * Delete the promotion.
     *
     * @return boolean
     */
    public function delete();

}
