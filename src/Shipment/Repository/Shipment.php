<?php namespace SunnyDayInc\Shop\Shipment\Repository;

abstract class Shipment implements ShipmentInterface
{

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        return $this->createModel()->find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * {@inheritdoc}
     */
    public function createModel()
    {
        $model = $this->model();

        return new $model;
    }

    /**
     * {@inheritdoc}
     */
    public function createModelTrack()
    {
        $model = $this->modelTrack();

        return new $model;
    }

    /**
     * {@inheritdoc}
     */
    public function createModelItem()
    {
        $model = $this->modelItem();

        return new $model;
    }

    /**
     * {@inheritdoc}
     */
    public function createModelComment()
    {
        $model = $this->modelComment();

        return new $model;
    }

}
