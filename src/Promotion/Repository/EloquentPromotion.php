<?php namespace SunnyDayInc\Shop\Promotion\Repository;

class EloquentPromotion extends Promotion
{

    /**
     * The current total row in previous query.
     *
     * @var integer
     */
    protected $total = 0;

    /**
     * {@inheritdoc}
     */
    public function findAllBy(
        Array $where = [], 
        Array $column = ['*'], 
        $page = 1, 
        $limit = 10, 
        $order = 'DESC', 
        $orderBy = 'created_at'
    ) {
        $where = array_merge(
            [
            'object_type' => '', 
            'object_id'   => 0, 
            'type'        => '', 
            'start_at'    => '', 
            'end_at'      => '', 
            'date_at'     => '', 
            'search'      => '', 
            'is_coupon'   => null, 
            'is_active'   => null
            ], $where
        );

        if (empty($page)) {
            $page = 1;
        }

        $model = $this->createModel();

        $useWhere = false;
        $hasWhere = false;

        $fromSql = '(';
        $fromSql .= 'SELECT `'.$model->getTable().'`.`id`';
        $fromSql .= ' FROM `'.$model->getTable().'`';

        if (! empty($where['object_type'])
            || ! empty($where['object_id'])
            || ! empty($where['type'])
            || ! empty($where['start_at'])
            || ! empty($where['end_at'])
            || ! empty($where['date_at'])
            || ! empty($where['search'])
            || ! is_null($where['is_coupon'])
            || ! is_null($where['is_active'])
        ) {
            $useWhere = true;
        }

        if ($useWhere) {
            $fromSql .= ' WHERE';
        }

        if (! empty($where['object_type'])) {
            $fromSql .= ' `'.$model->getTable().'`.`object_type` = "'.$where['object_type'].'"';

            $hasWhere = true;
        }

        if (! empty($where['object_id'])) {
            if ($hasWhere) {
                $fromSql .= ' AND';
            }

            $fromSql .= ' `'.$model->getTable().'`.`object_id` = '.$where['object_id'];

            $hasWhere = true;
        }

        if (! empty($where['type'])) {
            if ($hasWhere) {
                $fromSql .= ' AND';
            }

            $fromSql .= ' `'.$model->getTable().'`.`type` = "'.$where['type'].'"';

            $hasWhere = true;
        }

        if (! empty($where['start_at']) && ! empty($where['end_at'])) {
            if ($hasWhere) {
                $fromSql .= ' AND';
            }

            $fromSql .= ' (`'.$model->getTable().'`.`from_date` <= "'.$where['start_at'].'"';
            $fromSql .= ' AND `'.$model->getTable().'`.`to_date` >= "'.$where['end_at'].'")';

            $hasWhere = true;
        }

        if (! empty($where['date_at'])) {
            if ($hasWhere) {
                $fromSql .= ' AND';
            }

            $fromSql .= ' (`'.$model->getTable().'`.`from_date` <= "'.$where['date_at'].'"';
            $fromSql .= ' AND `'.$model->getTable().'`.`to_date` >= "'.$where['date_at'].'")';

            $hasWhere = true;
        }

        if (! empty($where['search'])) {
            if ($hasWhere) {
                $fromSql .= ' AND';
            }

            $fromSql .= ' (`'.$model->getTable().'`.`name` LIKE "%'.$where['search'].'%"';
            $fromSql .= ' OR `'.$model->getTable().'`.`description` LIKE "%'.$where['search'].'%")';

            $hasWhere = true;
        }

        if (! is_null($where['is_coupon'])) {
            if ($hasWhere) {
                $fromSql .= ' AND';
            }

            $fromSql .= ' `'.$model->getTable().'`.`is_coupon` = '.$where['is_coupon'];

            $hasWhere = true;
        }

        if (! is_null($where['is_active'])) {
            if ($hasWhere) {
                $fromSql .= ' AND';
            }

            $fromSql .= ' `'.$model->getTable().'`.`is_active` = '.$where['is_active'];

            $hasWhere = true;
        }

        if (! empty($order) && ! empty($orderBy)) {
            $fromSql .= ' ORDER BY `'.$model->getTable().'`.`'.$orderBy.'` '.$order;
        }

        if ($limit > 0) {
            $fromSql .= ' limit '.$limit.' offset '.($page - 1) * $limit;
        }

        $fromSql .= ') o';

        if ($limit > 0) {
            $query = $model->newQuery()->select('id');

            if (! empty($where['object_type'])) {
                $query->where('object_type', '=', $where['object_type']);
            }

            if (! empty($where['object_id'])) {
                $query->where('object_id', '=', $where['object_id']);
            }

            if (! empty($where['type'])) {
                $query->where('type', '=', $where['type']);
            }

            if (! empty($where['start_at']) && ! empty($where['end_at'])) {
                $query->where('from_date', '<=', $where['start_at']);
                $query->where('to_date', '>=', $where['end_at']);
            }

            if (! empty($where['date_at'])) {
                $query->where('from_date', '<=', $where['date_at']);
                $query->where('to_date', '>=', $where['date_at']);
            }

            if (! is_null($where['search'])) {
                $searchText = $where['search'];

                $query->where(
                    function ($qry) use ($searchText) {
                        $qry->where('name', 'LIKE', '%'.$searchText.'%')
                            ->orWhere('description', 'LIKE', '%'.$searchText.'%');
                    }
                );
            }

            if (! is_null($where['is_coupon'])) {
                $query->where('is_coupon', '=', $where['is_coupon']);
            }

            if (! is_null($where['is_active'])) {
                $query->where('is_active', '=', $where['is_active']);
            }

            $this->total = $query->count();
        } else {
            $this->total = 0;
        }

        $query = $model->newQuery()
            ->from(app('db')->raw($fromSql))
            ->join($model->getTable(), $model->getTable().'.id', '=', 'o.id');

        if (! empty($order) && ! empty($orderBy)) {
            $query->orderBy($model->getTable().'.'.$orderBy, $order);
        }

        unset($fromSql);
        unset($model);

        return $query->get($column);
    }

    /**
     * {@inheritdoc}
     */
    public function findCoupon($code, $isActive = true, $objectType = null, $objectId = null, $dateAt = null)
    {
        $model = $this->createModel();
        $modelCoupon = $this->createModelCoupon();

        $query = $modelCoupon->newQuery()->select($modelCoupon->getTable().'.*');

        $query->leftJoin(
            $model->getTable(), 
            $modelCoupon->getTable().'.promotion_id', 
            '=', 
            $model->getTable().'.id'
        );

        $query->where($modelCoupon->getTable().'.code', '=', $code);
        $query->where($model->getTable().'.is_active', '=', $isActive);

        if (! is_null($objectType) && ! is_null($objectId)) {
            $query->where($model->getTable().'.object_type', '=', $objectType);
            $query->where($model->getTable().'.object_id', '=', $objectId);
        }

        if (! is_null($dateAt)) {
            $query->where(
                function ($qry) use ($model, $dateAt) {
                    $qry->where($model->getTable().'.from_date', '<=', $dateAt->format('Y-m-d H:i:s'))
                        ->where($model->getTable().'.to_date', '>=', $dateAt->format('Y-m-d H:i:s'));
                }
            );
        }

        unset($modelCoupon);
        unset($model);

        return $query->first();
    }

    /**
     * {@inheritdoc}
     */
    public function findCouponsByPromotion($invoiceId)
    {
        $query = $this->createModelCoupon()->newQuery();
        $query->where('promotion_id', '=', $invoiceId);

        return $query->get();
    }

    /**
     * {@inheritdoc}
     */
    public function findCouponByPromotionAndId($invoiceId, $id)
    {
        $query = $this->createModelCoupon()->newQuery();
        $query->where('promotion_id', '=', $invoiceId);
        $query->where('id', '=', $id);

        return $query->first();
    }

    /**
     * {@inheritdoc}
     */
    public function model()
    {
        return '\SunnyDayInc\Shop\Models\Eloquent\Promotion';
    }

    /**
     * {@inheritdoc}
     */
    public function modelCoupon()
    {
        return '\SunnyDayInc\Shop\Models\Eloquent\PromotionCoupon';
    }

    /**
     * {@inheritdoc}
     */
    public function modelProduct()
    {
        return '\SunnyDayInc\Shop\Models\Eloquent\PromotionProduct';
    }

}
