<?php namespace SunnyDayInc\Shop\Currency\Repository;

class EloquentCurrency extends Currency
{

    /**
     * {@inheritdoc}
     */
    public function findRatesTo($currency)
    {
        $query = $this->createModel()->newQuery();
        $query->select(['from', 'rate']);
        $query->where('to', '=', $currency);

        return $query->get();
    }

    /**
     * {@inheritdoc}
     */
    public function findFormat($currency)
    {
        $query = $this->createModelFormat()->newQuery();
        $query->select(['symbol', 'decimal', 'thousand_separator', 'decimal_separator']);
        $query->where('code', '=', $currency);

        return $query->first();
    }

    /**
     * {@inheritdoc}
     */
    public function model()
    {
        return '\SunnyDayInc\Shop\Models\Eloquent\ExchangeRate';
    }

    /**
     * {@inheritdoc}
     */
    public function modelFormat()
    {
        return '\SunnyDayInc\Shop\Models\Eloquent\CurrencyFormat';
    }

}
