<?php namespace SunnyDayInc\Shop\Currency\Repository;

interface CurrencyInterface
{

    /**
     * Return the rates to a currency
     *
     * @param  string $currency
     * @return \Collection
     */
    public function findRatesTo($currency);

    /**
     * Return the format for a currency
     *
     * @param  string $currency
     * @return \SunnyDayInc\Shop\Contracts\Model
     */
    public function findFormat($currency);

    /**
     * Return the Model class name
     *
     * @return string
     */
    public function model();

    /**
     * Return the Model format class name
     *
     * @return string
     */
    public function modelFormat();
  
    /**
     * Return the model object creation.
     *
     * @return \SunnyDayInc\Shop\Contracts\Model
     */
    public function createModel();

    /**
     * Return the model format object creation.
     *
     * @return \SunnyDayInc\Shop\Contracts\Model
     */
    public function createModelFormat();

}
