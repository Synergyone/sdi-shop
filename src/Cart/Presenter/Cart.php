<?php namespace SunnyDayInc\Shop\Cart\Presenter;

use SunnyDayInc\Shop\Support\Collection;
use SunnyDayInc\Shop\Contracts\Currency;
use SunnyDayInc\Shop\Promotion\Promotion as PromotionContainer;
use SunnyDayInc\Shop\Promotion\Repository\PromotionInterface as PromotionRepository;
use SunnyDayInc\Shop\Cart\Repository\ProductInterface as ProductRepository;
use SunnyDayInc\Shop\Models\PromotionValidationInterface;
use SunnyDayInc\Shop\Models\PromotionInterface;
use SunnyDayInc\Shop\Presenter\Presenter;
use SunnyDayInc\Shop\Models\CartInterface;

use DateTime;

class Cart extends Presenter implements PromotionValidationInterface
{

    /**
     * The Cart model.
     *
     * @var Cart
     */
    protected $_model;

    /**
     * The account ID.
     *
     * @var integer
     */
    protected $_accountId;

    /**
     * The Promotion Repository.
     *
     * @var \SunnyDayInc\Shop\Promotion\Repository\PromotionInterface
     */
    protected $_promotionRepository;

    /**
     * The Promotion DateTime.
     *
     * @var \DateTime
     */
    protected $_promotionDateAt;

    /**
     * The product repository instance.
     *
     * @var \SunnyDayInc\Shop\Cart\Repository\ProductInterface
     */
    protected $_productRepository;

    /**
     * The Currency Manager.
     *
     * @var \SunnyDayInc\Shop\Contracts\Currency
     */
    protected $_currency;

    /**
     * The currency format.
     *
     * @var string
     */
    protected $_currencyFormat = '%s %a';

    /**
     * The currency code target.
     *
     * @var string
     */
    protected $_targetCurrencyCode;

    /**
     * The minimum total qty.
     *
     * @var float
     */
    protected $_minTotalQty;

    /**
     * The minimum base amount.
     *
     * @var float
     */
    protected $_minBaseAmount;

    /**
     * The delivery time in minute.
     *
     * @var integer
     */
    protected $_deliveryTime;

    /**
     * The delivery cost.
     *
     * @var float
     */
    protected $_deliveryCost;

    /**
     * The items.
     *
     * @var \Collection
     */
    protected $_items;

    /**
     * The unavailable items.
     *
     * @var \Collection
     */
    protected $_unavailableItems;

    /**
     * The coupon error.
     *
     * @var Array
     */
    protected $_couponError;

    /**
     * The applied promotion IDs.
     *
     * @var Array
     */
    protected $_appliedPromotionIds;

    /**
     * The applied promotions.
     *
     * @var Array
     */
    protected $_appliedPromotions;

    /**
     * The subtotal.
     *
     * @var float
     */
    protected $_subtotal;

    /**
     * The discount.
     *
     * @var float
     */
    protected $_discount;

    /**
     * The total.
     *
     * @var float
     */
    protected $_total;

    /**
     * {@inheritdoc}
     */
    protected $publicData = [
    'id'                      => 'id', 
    'customer'                => 'customer', 
    'address'                 => 'address', 
    'qty'                     => 'qty', 
    'items'                   => 'items', 
    'unavailable_items'       => 'unavailable_items', 
    'subtotal'                => 'subtotal', 
    'subtotal_display'        => 'subtotal_display', 
    'delivery_time'           => 'delivery_time', 
    'delivery'                => 'delivery', 
    'delivery_display'        => 'delivery_display', 
    'discount'                => 'discount', 
    'discount_display'        => 'discount_display', 
    'total'                   => 'total', 
    'total_display'           => 'total_display', 
    'min_total_qty'           => 'min_total_qty', 
    'min_base_amount'         => 'min_base_amount', 
    'min_base_amount_display' => 'min_base_amount_display', 
    'coupon'                  => 'coupon', 
    'coupon_error'            => 'coupon_error', 
    'applied_promotions'      => 'applied_promotions', 
    'created_at'              => 'created_at', 
    'updated_at'              => 'updated_at'
    ];

    /**
     * Create the Presenter and store the object we are presenting.
     *
     * @param  CartInterface $cart
     * @param  array         $fields
     * @param  Collection    $unavailableItems
     * @param  array         $items
     * @return void
     */
    public function __construct(CartInterface $cart, Array $fields = ['*'], Collection $unavailableItems, Array $couponError = [])
    {
        parent::__construct($cart->toArray());

        // Select field to publish
        // Remove the rest
        $newPublicData = [];

        if (is_array($fields)) {
            if (trim($fields[0]) != '*') {
                foreach ($this->publicData as $key => $data) {
                    foreach ($fields as $item) {
                        if ($data == $item) {
                            $newPublicData[$key] = $data;
                        }
                    }
                }
            }
        }

        if (! empty($newPublicData)) {
            $this->publicData = $newPublicData;
        }

        $this->_model = $cart;
        $this->_unavailableItems = $unavailableItems;
        $this->_couponError = $couponError;
        $this->_appliedPromotionIds = [];
        $this->_appliedPromotions = [];
    }

    /**
     * Define account ID.
     *
     * @param  integer $id
     * @return void
     */
    public function setAccountId($id)
    {
        $this->_accountId = $id;
    }

    /**
     * Define currency manager.
     *
     * @param  \SunnyDayInc\Shop\Contracts\Currency $currency
     * @param  string                               $format
     * @return void
     */
    public function setCurrencyManager(Currency $currency, $format = '%s %a')
    {
        $this->_currency = $currency;
        $this->_currencyFormat = $format;
    }

    /**
     * Define currency code target.
     *
     * @param  string $code
     * @return void
     */
    public function setCurrencyTarget($code)
    {
        $this->_targetCurrencyCode = $code;
    }

    /**
     * Define minimum total qty.
     *
     * @param  float $qty
     * @return void
     */
    public function setMinTotalQty($qty)
    {
        $this->_minTotalQty = $qty;
    }

    /**
     * Define minimum base amount.
     *
     * @param  float $amount
     * @return void
     */
    public function setMinBaseAmount($amount)
    {
        $this->_minBaseAmount = $amount;
    }

    /**
     * Define delivery time in minute.
     *
     * @param  integer $time
     * @return void
     */
    public function setDeliveryTime($time)
    {
        $this->_deliveryTime = $time;
    }

    /**
     * Define delivery cost.
     *
     * @param  float $cost
     * @return void
     */
    public function setDeliveryCost($cost)
    {
        $this->_deliveryCost = $cost;
    }

    /**
     * Define promotion repository.
     *
     * @param  \SunnyDayInc\Shop\Promotion\Repository\PromotionInterface
     * @param  DateTime                                                  $dateAt
     * @return void
     */
    public function setPromotionRepository(PromotionRepository $repository, $dateAt = '')
    {
        $this->_promotionRepository = $repository;

        if (! empty($dateAt)) {
            $this->_promotionDateAt = $dateAt;
        }
    }

    /**
     * Define product repository instance.
     *
     * @param  \SunnyDayInc\Shop\Cart\Repository\ProductInterface $repository
     * @return void
     */
    public function setProductRepository(ProductRepository $repository)
    {
        $this->_productRepository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    public function getItems()
    {
        return $this->presentItems();
    }

    /**
     * {@inheritdoc}
     */
    public function getBaseSubtotal()
    {
        return $this->presentSubtotal();
    }

    /**
     * {@inheritdoc}
     */
    public function getTotalQty()
    {
        return $this->presentQty();
    }

    /**
     * Format and return the data.
     *
     * @return array
     */
    public function presentCustomer()
    {
        $data = null;

        if (! empty($this->_model->customer_id)) {
            $data = [
            'id' => (int) $this->_model->customer_id
            ];
        }

        if (isset($this->_model->customer_first_name) && isset($this->_model->customer_last_name)) {
            $data['name'] = trim($this->_model->customer_first_name.' '.$this->_model->customer_last_name);
        }

        return $data;
    }

    /**
     * Format and return the data.
     *
     * @return array
     */
    public function presentAddress()
    {
        $data = null;

        if (! empty($this->_model->address_id)) {
            $data = [
            'id' => (int) $this->_model->address_id
            ];
        }

        if (isset($this->_model->address_title)) {
            $data['title'] = $this->_model->address_title;
        }

        return $data;
    }

    /**
     * Format and return the data.
     *
     * @return float
     */
    public function presentQty()
    {
        return (float) $this->_model->total_qty;
    }

    /**
     * Format and return the data.
     *
     * @return \Collection
     */
    public function presentItems()
    {
        if (! is_null($this->_items)) {
            return $this->_items;
        }

        $itemList     = $this->_model->items;
        $this->_items = new Collection();

        if (! $itemList->isEmpty()) {

            // Collect available catalog promotions if available
            $promotions = new Collection();
            if (! is_null($this->_promotionRepository) && ! is_null($this->_promotionDateAt) && ! is_null($this->_accountId)) {
                $promotions = $this->_promotionRepository->findAllBy(
                    [
                    'object_type' => 'account', 
                    'object_id'   => $this->_accountId, 
                    'type'        => PromotionContainer::TYPE_CATALOG, 
                    'date_at'     => $this->_promotionDateAt->format('Y-m-d H:i:s'), 
                    'is_coupon'   => 0, 
                    'is_active'   => 1
                    ]
                );

                $promotions = new Collection($promotions->all());
            }

            foreach ($itemList as $item) {
                $presenter = $item->getPresenter();

                if (! is_null($this->_productRepository)) {
                    $presenter->setProductRepository($this->_productRepository);
                }

                if (! is_null($this->_currency)) {
                    $presenter->setCurrencyManager($this->_currency, $this->_currencyFormat);
                }

                if (! is_null($this->_targetCurrencyCode)) {
                    $presenter->setCurrencyTarget($this->_targetCurrencyCode);
                }

                $presenter->setPromotions($promotions);

                $this->_items->push($presenter);
            }

            unset($promotions);
        }

        unset($itemList);

        return $this->_items;
    }

    /**
     * Format and return the data.
     *
     * @return array
     */
    public function presentUnavailableItems()
    {
        return $this->_unavailableItems;
    }

    /**
     * Format and return the data.
     *
     * @return float
     */
    public function presentSubtotal()
    {
        // Calculate
        $this->calculate();

        return $this->_subtotal;
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentSubtotalDisplay()
    {
        // Collect subtotal
        $this->presentSubtotal();

        if ($this->_currency instanceOf Currency && ! is_null($this->_targetCurrencyCode)) {
            return $this->_currency
                ->convert($this->_subtotal, $this->_targetCurrencyCode, $this->_targetCurrencyCode)
                ->format($this->_currencyFormat);
        }

        return '';
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentDeliveryTime()
    {
        return (int) $this->_deliveryTime;
    }

    /**
     * Format and return the data.
     *
     * @return float
     */
    public function presentDelivery()
    {
        return (float) $this->_deliveryCost;
    }

    /**
     * Format and return the data.
     *
     * @return float
     */
    public function presentDeliveryDisplay()
    {
        if ($this->_currency instanceOf Currency && ! is_null($this->_targetCurrencyCode)) {
            return $this->_currency
                ->convert($this->_deliveryCost, $this->_targetCurrencyCode, $this->_targetCurrencyCode)
                ->format($this->_currencyFormat);
        }

        return '';
    }

    /**
     * Format and return the data.
     *
     * @return float
     */
    public function presentDiscount()
    {
        // Calculate
        $this->calculate();

        return $this->_discount;
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentDiscountDisplay()
    {
        // Collect subtotal
        $this->presentSubtotal();

        if ($this->_currency instanceOf Currency && ! is_null($this->_targetCurrencyCode)) {
            return $this->_currency
                ->convert($this->_discount, $this->_targetCurrencyCode, $this->_targetCurrencyCode)
                ->format($this->_currencyFormat);
        }

        return '';
    }

    /**
     * Format and return the data.
     *
     * @return float
     */
    public function presentTotal()
    {
        // Calculate
        $this->calculate();

        return $this->_total;
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentTotalDisplay()
    {
        // Collect total
        $this->presentTotal();

        if ($this->_currency instanceOf Currency && ! is_null($this->_targetCurrencyCode)) {
            return $this->_currency
                ->convert($this->_total, $this->_targetCurrencyCode, $this->_targetCurrencyCode)
                ->format($this->_currencyFormat);
        }

        return '';
    }

    /**
     * Format and return the data.
     *
     * @return float
     */
    public function presentMinTotalQty()
    {
        return (float) $this->_minTotalQty;
    }

    /**
     * Format and return the data.
     *
     * @return float
     */
    public function presentMinBaseAmount()
    {
        return (float) $this->_minBaseAmount;
    }

    /**
     * Format and return the data.
     *
     * @return float
     */
    public function presentMinBaseAmountDisplay()
    {
        if ($this->_currency instanceOf Currency && ! is_null($this->_targetCurrencyCode)) {
            return $this->_currency
                ->convert($this->_minBaseAmount, $this->_targetCurrencyCode, $this->_targetCurrencyCode)
                ->format($this->_currencyFormat);
        }

        return '';
    }

    /**
     * Format and return the data.
     *
     * @return object
     */
    public function presentCoupon()
    {
        $coupon = $this->_model->coupon;

        if (! is_object($coupon)) {
            return null;
        }

        return $coupon->getPresenter();
    }

    /**
     * Format and return the data.
     *
     * @return Array
     */
    public function presentCouponError()
    {
        return $this->_couponError;
    }

    /**
     * Format and return the data.
     *
     * @return Array
     */
    public function presentAppliedPromotions()
    {
        return $this->_appliedPromotions;
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentCreatedAt()
    {
        if ($this->_model->created_at != '0000-00-00 00:00:00') {
            return (new DateTime($this->_model->created_at))->format('c');
        }

        return null;
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentUpdatedAt()
    {
        if ($this->_model->updated_at != '0000-00-00 00:00:00') {
            return (new DateTime($this->_model->updated_at))->format('c');
        }

        return null;
    }

    /**
     * Calculate.
     *
     * @return void
     */
    private function calculate()
    {
        if (! is_null($this->_subtotal) || ! is_null($this->_discount) || ! is_null($this->_total)) {
            return;
        }

        // Collect all items presenter
        $this->presentItems();

        $this->_subtotal = 0;
        $this->_discount = 0;
        $this->_total    = 0;

        if (! $this->_items->isEmpty()) {
            foreach ($this->_items as $item) {
                $this->_subtotal += $item->subtotal;
                $this->_discount += $item->discount;
                $this->_total    += $item->total;

                // Collect applied promotions
                $appliedPromotions = $item->getAppliedPromotions();

                if (! empty($appliedPromotions)) {
                    foreach ($appliedPromotions as $promotion) {
                        if (! in_array($promotion->getId(), $this->_appliedPromotionIds)) {
                            $this->_appliedPromotionIds[] = $promotion->getId();
                            $this->_appliedPromotions[] = $promotion->getPresenter(
                                [
                                'id', 'name', 'description', 'simple_action', 
                                'discount_amount', 'discount_step', 'is_free_shipping', 
                                'is_coupon'
                                ]
                            );
                        }
                    }
                }

                unset($appliedPromotions);
            }

            // Collect available cart promotions if available
            $promotions = new Collection();
            if (! is_null($this->_promotionRepository) && ! is_null($this->_promotionDateAt) && ! is_null($this->_accountId)) {
                $promotions = $this->_promotionRepository->findAllBy(
                    [
                    'object_type' => 'account', 
                    'object_id'   => $this->_accountId, 
                    'type'        => PromotionContainer::TYPE_CART, 
                    'date_at'     => $this->_promotionDateAt->format('Y-m-d H:i:s'), 
                    'is_coupon'   => 0, 
                    'is_active'   => 1
                    ]
                );

                $promotions = new Collection($promotions->all());
            }

            // Collect coupon promotion if available
            $coupon = $this->_model->coupon;
            if (is_object($coupon)) {
                $promotion = $coupon->promotion;

                // Set new discount amount
                if ($promotion->applyToShipping()) {
                    if ($promotion->isFreeShipping()) {
                        // CODE HERE
                    } else {
                        // CODE HERE
                    }
                } else {
                    $promotions->push($promotion);
                }
            }
            unset($coupon);

            // Check available promotions and update everything needed
            $additionalDiscount = 0;
            if ( ! $promotions->isEmpty()) {
                foreach ($this->_items as $i => $item) {
                    foreach ($promotions as $promotion) {
                        if ($promotion->isItemApplicable($item->collectProduct(), true)) {
                            if ($promotion->isApplicable($this)) {
                                if ($promotion->getSimpleAction() === PromotionContainer::ACTION_BUY_X_GET_Y) {
                                    $item->setAdditionalDiscount($promotion->getDiscount($item->product->original_price, $item->qty));
                                    $this->_items->offsetSet($i, $item);
                                } else {
                                    $additionalDiscount += $promotion->getDiscount($this->_subtotal, $this->_model->total_qty);
                                }

                                if (! in_array($promotion->getId(), $this->_appliedPromotionIds)) {
                                    $this->_appliedPromotionIds[] = $promotion->getId();
                                    $this->_appliedPromotions[] = $promotion->getPresenter(
                                        [
                                        'id', 'name', 'description', 'simple_action', 
                                        'discount_amount', 'discount_step', 'is_free_shipping', 
                                        'is_coupon'
                                        ]
                                    );
                                }
                            }
                        }
                    }
                }

                // Re-calculate whole cart
                $this->_subtotal = 0;
                $this->_discount = 0;
                $this->_total    = 0;
                foreach ($this->_items as $item) {
                    $this->_subtotal += $item->subtotal;
                    $this->_discount += $item->discount;
                    $this->_total    += $item->total;
                }
            }

            if ($this->_total > $additionalDiscount) {
                $this->_total = $this->_total - $additionalDiscount;
            } else {
                $this->_total = $additionalDiscount;
            }

            // Add delivery cost to total if available
            if ( ! is_null($this->_deliveryCost)) {
                $this->_total = $this->_total + $this->_deliveryCost;
            }
        }
    }

}
