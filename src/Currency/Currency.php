<?php namespace SunnyDayInc\Shop\Currency;

use SunnyDayInc\Shop\Contracts\Currency as CurrencyInterface;
use SunnyDayInc\Shop\Currency\Repository\Currency as CurrencyRepository;

class Currency implements CurrencyInterface
{

    /**
     * The repository instance.
     *
     * @var \SunnyDayInc\Shop\Currency\Repository\Currency
     */
    protected $repository;

    /**
     * The available rates.
     *
     * @var Array
     */
    protected $rates = [];

    /**
     * The available currency format.
     *
     * @var Array
     */
    protected $formats = [];

    /**
     * The current currency target.
     *
     * @var string
     */
    protected $targetCode;

    /**
     * The current value to convert.
     *
     * @var float
     */
    protected $value;

    /**
     * Create a new instance.
     *
     * @param  \SunnyDayInc\Shop\Currency\Repository\Currency $repository
     * @return void
     */
    public function __construct(CurrencyRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * {@inheritdoc}
     */
    public function convert($value, $from, $to, $rate = null)
    {
        $this->targetCode = $to;

        if (is_null($rate)) {
            $rate = $this->getRate($from, $to);

            if ($rate instanceOf self) {
                $rate = 1;
            }
        }

        $this->value = $value * $rate;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getRate($from, $to)
    {
        $rate = 1;

        if ($from != $to) {
            if (! isset($this->rates[$to])) {
                $this->collectRates($to); 
            }

            if (! isset($this->rates[$to])) {
                return $this; 
            }

            if (! isset($this->rates[$to][$from])) {
                return $this; 
            }

            $rate = $this->rates[$to][$from];
        }

        return $rate;
    }

    /**
     * {@inheritdoc}
     */
    public function result()
    {
        return $this->value;
    }

    /**
     * {@inheritdoc}
     */
    public function format($format = '%s %a', $decimal = null)
    {
        if (is_null($this->targetCode) || is_null($this->value)) {
            return ''; 
        }

        if (! isset($this->formats[$this->targetCode])) {
            $this->collectFormat(); 
        }

        if (! isset($this->formats[$this->targetCode])) {
            return $this->value; 
        }

        if (empty($format)) {
            $format = '%a'; 
        }

        if (is_null($decimal)) {
            $decimal = $this->formats[$this->targetCode]['decimal']; 
        }

        $formatedValue = number_format(
            $this->value, 
            $decimal, 
            $this->formats[$this->targetCode]['decimal_separator'], 
            $this->formats[$this->targetCode]['thousand_separator']
        );

        $format = str_replace('%s', $this->formats[$this->targetCode]['symbol'], $format);
        $format = str_replace('%c', $this->targetCode, $format);
        $format = str_replace('%a', $formatedValue, $format);

        return $format;
    }

    /**
     * Collect exchange rates to a currency
     *
     * @param  string $to
     * @return void
     */
    private function collectRates($to)
    {
        $rates = $this->repository->findRatesTo($to);

        if (! $rates->isEmpty()) {
            $this->rates[$to] = [];

            foreach ($rates as $rate) {
                $this->rates[$to][$rate->from] = $rate->rate;
            }
        }

        unset($rates);
    }

    /**
     * Collect currency format
     *
     * @return void
     */
    private function collectFormat()
    {
        $format = $this->repository->findFormat($this->targetCode);

        if (is_object($format)) {
            $this->formats[$this->targetCode] = [
            'symbol'             => $format->symbol, 
            'decimal'            => $format->decimal, 
            'thousand_separator' => $format->thousand_separator, 
            'decimal_separator'  => $format->decimal_separator
            ];
        }

        unset($format);
    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return ''.$this->value;
    }

}
