<?php namespace SunnyDayInc\Shop\Models;

use SunnyDayInc\Shop\Support\Collection;
use SunnyDayInc\Shop\Promotion\Promotion as PromotionContainer;
use SunnyDayInc\Shop\Promotion\Rule\RuleInterface;
use SunnyDayInc\Shop\Promotion\Presenter\Catalogue as CataloguePresenter;
use SunnyDayInc\Shop\Promotion\Presenter\Cart as CartPresenter;

trait Promotion
{

    /**
     * {@inheritdoc}
     */
    public function save(array $options = [])
    {
        if ($this->condition instanceOf RuleInterface) {
            $this->conditions_serialized = $this->condition->toJson();
        }

        if ($this->action instanceOf RuleInterface) {
            $this->actions_serialized = $this->action->toJson();
        }

        unset($this->condition);
        unset($this->action);

        return parent::save($options);
    }

    /**
     * {@inheritdoc}
     */
    protected function finishSave(array $options)
    {
        $this->buildCondition();
        $this->buildAction();

        parent::finishSave($options);
    }

    /**
     * {@inheritdoc}
     */
    public function delete()
    {
        $this->deleteCoupons();
        $this->deleteProducts();

        return parent::delete();
    }

    /**
     * Delete products.
     *
     * @return void
     */
    public function deleteProducts()
    {
        $products = $this->products;

        if (! $products->isEmpty()) {
            foreach ($products as $item) {
                $item->delete();
            }
        }

        unset($products);
    }

    /**
     * Delete coupons.
     *
     * @return void
     */
    protected function deleteCoupons()
    {
        $coupons = $this->coupons;

        if (! $coupons->isEmpty()) {
            foreach ($coupons as $item) {
                $item->delete();
            }
        }

        unset($coupons);
    }

    /**
     * Return is existing data.
     *
     * @return boolean
     */
    public function isExist()
    {
        return $this->exists;
    }

    /**
     * Return the ID.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Return the type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Return the name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Return the description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Return the simple action.
     *
     * @return string
     */
    public function getSimpleAction()
    {
        return $this->simple_action;
    }

    /**
     * Return the discount amount.
     *
     * @return float
     */
    public function getDiscountAmount()
    {
        return $this->discount_amount;
    }

    /**
     * Return the discount step.
     *
     * @return float
     */
    public function getDiscountStep()
    {
        return $this->discount_step;
    }

    /**
     * Check if promotion applicable to the order.
     *
     * @param  \SunnyDayInc\Shop\Models\PromotionValidationInterface $order
     * @return boolean
     */
    public function isApplicable(PromotionValidationInterface $order)
    {
        if (! isset($this->condition)) {
            $this->getCondition();
        }

        if (! isset($this->condition)) {
            return false;
        }

        if (! ($this->condition instanceOf RuleInterface)) {
            return false;
        }

        return $this->condition->isApplicable($order);
    }

    /**
     * Check if promotion applicable to the order item.
     *
     * @param  \SunnyDayInc\Shop\Models\PromotionItemValidationInterface $order
     * @param  boolean                                                   $checkFromAction
     * @return boolean
     */
    public function isItemApplicable(PromotionItemValidationInterface $item, $checkFromAction = false)
    {
        if ($checkFromAction) {
      
            if (! isset($this->action)) {
                $this->getAction();
            }

            if (! isset($this->action)) {
                return false;
            }

            if (! ($this->action instanceOf RuleInterface)) {
                return false;
            }

            return $this->action->isApplicable($item);

        } else {

            if (! isset($this->condition)) {
                $this->getCondition();
            }

            if (! isset($this->condition)) {
                return false;
            }

            if (! ($this->condition instanceOf RuleInterface)) {
                return false;
            }

            return $this->condition->isApplicable($item);

        }
    }

    /**
     * Check if promotion is applicable to shipping.
     *
     * @return boolean
     */
    public function applyToShipping()
    {
        return ($this->apply_to_shipping == 1);
    }

    /**
     * Check if promotion is free shipping.
     *
     * @return boolean
     */
    public function isFreeShipping()
    {
        return ($this->free_shipping == 1);
    }

    /**
     * Check if promotion action is buy x get y.
     *
     * @return boolean
     */
    public function isFreeItem()
    {
        return ($this->condition->simple_action == 'buy_x_get_y');
    }

    /**
     * Check if promotion is active.
     *
     * @return boolean
     */
    public function isActive()
    {
        return ($this->is_active);
    }

    /**
     * Return the discount.
     *
     * @param  double $price
     * @param  double $qty
     * @return double|integer
     */
    public function getDiscount($price = 0, $qty = null)
    {
        $priceRule = 0;

        switch ($this->simple_action) {
        case PromotionContainer::ACTION_BY_PERCENT:
            $priceRule = $price * ($this->discount_amount / 100);
            break;

        case PromotionContainer::ACTION_TO_PERCENT:
            $priceRule = $price - ($price * ($this->discount_amount / 100));
            break;

        case PromotionContainer::ACTION_BY_FIXED:
            $priceRule = $price - max(0, $price - $this->discount_amount);
            break;

        case PromotionContainer::ACTION_TO_FIXED:
            $priceRule = $price - min($this->discount_amount, $price);
            break;

        case PromotionContainer::ACTION_BUY_X_GET_Y:
            $x = $this->discount_step;
            $y = $this->discount_amount;

            if (empty($x) || $y > $x || ($qty - $y <= 0)) {
                break;
            }

            $priceRule = $y * $price;
            break;
        }

        return $priceRule;
    }

    /**
     * Return the shipping discount.
     *
     * @param  double $price
     * @return double|integer
     */
    public function getShippingDiscount($price = 0)
    {
        $priceRule = 0;

        switch ($this->simple_action) {
        case PromotionContainer::ACTION_BY_PERCENT:
            $priceRule = $price - ($price * ($this->discount_amount / 100));
            break;

        case PromotionContainer::ACTION_BY_FIXED:
            $priceRule = max(0, $price - $this->discount_amount);
            break;
        }

        return $priceRule;
    }

    /**
     * Should continue next promotion checking.
     *
     * @return boolean
     */
    public function continueFurtherRulesProcessing()
    {
        return ($this->stop_rules_processing === 0);
    }

    /**
     * Return the condition rule instance.
     *
     * @return \SunnyDayInc\Shop\Promotion\Rule\RuleInterface
     */
    public function getCondition()
    {
        if (is_null($this->condition)) {
            $this->buildCondition();
        }

        return $this->condition;
    }

    /**
     * Build object rules from condition JSON string.
     *
     * @return void
     */
    public function buildCondition()
    {
        if (! empty($this->conditions_serialized)) {
            $this->condition = $this->buildRulesFromJson($this->conditions_serialized);
        }
    }

    /**
     * Return the action rule instance.
     *
     * @return \SunnyDayInc\Shop\Promotion\Rule\RuleInterface
     */
    public function getAction()
    {
        if (is_null($this->action)) {
            $this->buildAction();
        }

        return $this->action;
    }

    /**
     * Build object rules from action JSON string.
     *
     * @return void
     */
    public function buildAction()
    {
        if (! empty($this->actions_serialized)) {
            $this->action = $this->buildRulesFromJson($this->actions_serialized);
        }
    }

    /**
     * Build object rules from JSON string.
     *
     * @param  string $json
     * @return \SunnyDayInc\Shop\Promotion\Rule\RuleInterface
     */
    private function buildRulesFromJson($json)
    {
        $rule = json_decode($json);
        $rule = $this->iterateBuildRule($rule);

        return $rule;
    }

    /**
     * Build object rule from stdClass object
     *
     * @param  \stdClass $object
     * @return \SunnyDayInc\Shop\Promotion\Rule\RuleInterface
     */
    private function iterateBuildRule(\stdClass $object)
    {
        $ruleObject = $object->rule;
        $data       = (Array) $object;
        $conditions = [];
        if (isset($object->conditions)) {
            $conditions = $object->conditions;
        }

        unset($data['rule']);
        unset($data['conditions']);

        $rule = new $ruleObject($data);

        if (! empty($conditions)) {
            foreach ($conditions as $condition) {
                $rule->addChild($this->iterateBuildRule($condition));
            }
        }

        unset($conditions);
        unset($data);
        unset($ruleObject);

        return $rule;
    }

    /**
     * {@inheritdoc}
     */
    public function getPresenter(Array $fields = ['*'])
    {
        switch ($this->type) {
        case 'cart':
            return new CartPresenter($this, $fields);
        break;
      
        default:
            return new CataloguePresenter($this, $fields);
        break;
        }
    }

}
