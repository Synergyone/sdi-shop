<?php namespace SunnyDayInc\Shop\Mail\Compiler;

interface HandlerInterface
{

    /**
     * Set a value.
     *
     * @param  mixed $val
     * @return \SunnyDayInc\Shop\Mail\Compiler\HandlerInterface
     */ 
    public function set($val);

    /**
     * Get a value.
     *
     * @param  array      $attributes
     * @param  array|null $matches
     * @param  string     $tag
     * @return mixed
     */ 
    public function get($attributes, $matches, $tag);

}
