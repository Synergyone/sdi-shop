<?php namespace SunnyDayInc\Shop\Promotion\Presenter;

use SunnyDayInc\Shop\Presenter\Presenter;
use SunnyDayInc\Shop\Models\PromotionCouponInterface as CouponContract;
use DateTime;

class Coupon extends Presenter
{

    /**
     * The Coupon model.
     *
     * @var Coupon
     */
    protected $_model;

    /**
     * {@inheritdoc}
     */
    protected $publicData = [
    'id'                 => 'id', 
    'code'               => 'code', 
    'usage_limit'        => 'usage_limit', 
    'usage_per_customer' => 'usage_per_customer', 
    'total_used'         => 'total_used', 
    'expire_at'          => 'expire_at'
    ];

    /**
     * Create the Presenter and store the object we are presenting.
     *
     * @param  CouponContract $coupon
     * @param  array          $fields
     * @return void
     */
    public function __construct(CouponContract $coupon, Array $fields = ['*'])
    {
        parent::__construct($coupon->toArray());

        // Select field to publish
        // Remove the rest
        $newPublicData = [];

        if (is_array($fields)) {
            if (trim($fields[0]) != '*') {
                foreach ($this->publicData as $key => $data) {
                    foreach ($fields as $item) {
                        if ($data == $item) {
                            $newPublicData[$key] = $data;
                        }
                    }
                }
            }
        }

        if (! empty($newPublicData)) {
            $this->publicData = $newPublicData;
        }

        $this->_model = $coupon;
    }

    /**
     * Return the Coupon model.
     *
     * @return CouponContract
     */
    public function getModel()
    {
        return $this->_model;
    }

    /**
     * Return the Promotion model.
     *
     * @return PromotionContract
     */
    public function getPromotion()
    {
        $promotion = $this->_model->promotion;
    
        if (is_object($promotion)) {
            return $promotion;
        }

        return null;
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentId()
    {
        return (int) $this->_model->id;
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentUsageLimit()
    {
        return (int) $this->_model->usage_limit;
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentUsagePerCustomer()
    {
        return (int) $this->_model->usage_per_customer;
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentTotalUsed()
    {
        return (int) $this->_model->total_used;
    }

    /**
     * Format and return the data.
     *
     * @return DateTime
     */
    public function presentExpireAt()
    {
        if (! empty($this->_model->expire_at)) {
            return (new DateTime($this->_model->expire_at))->format('c');
        }

        return null;
    }

}
