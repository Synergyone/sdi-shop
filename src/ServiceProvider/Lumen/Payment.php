<?php namespace SunnyDayInc\Shop\ServiceProvider\Lumen;

use Illuminate\Support\ServiceProvider;

use SunnyDayInc\Shop\Payment\Payment as PaymentContainer;
use SunnyDayInc\Shop\Payment\Repository\EloquentPayment;

class Payment extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->configure('payment');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            'SunnyDayInc\Shop\Contracts\Payment', function ($app) {
                $promotion = new PaymentContainer(new EloquentPayment());

                $promotion->setLogger($app['log']);

                $fieldFormats = $app['config']->get('payment.field_formats');

                if (! empty($fieldFormats)) {
                    foreach ($fieldFormats as $field => $format) {
                        $promotion->addFieldFormat($field, $format);
                    }
                }

                unset($fieldFormats);

                return $promotion;
            }
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['SunnyDayInc\Shop\Contracts\Payment'];
    }
}
