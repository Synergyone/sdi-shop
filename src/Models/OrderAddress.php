<?php namespace SunnyDayInc\Shop\Models;

use SunnyDayInc\Shop\Order\Presenter\Address as AddressPresenter;

trait OrderAddress
{

    /**
     * {@inheritdoc}
     */
    public function getPresenter(Array $fields = ['*'])
    {
        return new AddressPresenter($this, $fields);
    }

}
