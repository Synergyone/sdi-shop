<?php namespace SunnyDayInc\Shop\Order\Presenter;

use SunnyDayInc\Shop\Support\Collection;
use SunnyDayInc\Shop\Contracts\Currency;
use SunnyDayInc\Shop\Presenter\Presenter;
use SunnyDayInc\Shop\Models\OrderInterface;
use DateTime;

class Order extends Presenter
{

    /**
     * The Order model.
     *
     * @var Order
     */
    protected $_model;

    /**
     * The Currency Manager.
     *
     * @var \SunnyDayInc\Shop\Contracts\Currency
     */
    protected $_currency;

    /**
     * The currency format.
     *
     * @var string
     */
    protected $_currencyFormat = '%s %a';

    /**
     * The items.
     *
     * @var \Collection
     */
    protected $_items;

    /**
     * {@inheritdoc}
     */
    protected $publicData = [
    'id'                         => 'id', 
    'branch_id'                  => 'branch_id', 
    'identifier'                 => 'identifier', 
    'code'                       => 'code', 
    'coupon'                     => 'coupon', 
    'customer'                   => 'customer', 
    'is_new_customer'            => 'is_new_customer', 
    'billing_address'            => 'billing_address', 
    'total_qty'                  => 'total_qty', 
    'discount'                   => 'discount', 
    'base_discount'              => 'base_discount', 
    'discount_invoiced'          => 'discount_invoiced', 
    'base_discount_invoiced'     => 'base_discount_invoiced', 
    'shipping_discount'          => 'shipping_discount', 
    'base_shipping_discount'     => 'base_shipping_discount', 
    'shipping'                   => 'shipping', 
    'base_shipping'              => 'base_shipping', 
    'shipping_tax'               => 'shipping_tax', 
    'base_shipping_tax'          => 'base_shipping_tax', 
    'shipping_incl_tax'          => 'shipping_incl_tax', 
    'base_shipping_incl_tax'     => 'base_shipping_incl_tax', 
    'shipping_invoiced'          => 'shipping_invoiced', 
    'base_shipping_invoiced'     => 'base_shipping_invoiced', 
    'tax'                        => 'tax', 
    'base_tax'                   => 'base_tax', 
    'tax_invoiced'               => 'tax_invoiced', 
    'base_tax_invoiced'          => 'base_tax_invoiced', 
    'subtotal'                   => 'subtotal', 
    'base_subtotal'              => 'base_subtotal', 
    'subtotal_incl_tax'          => 'subtotal_incl_tax', 
    'base_subtotal_incl_tax'     => 'base_subtotal_incl_tax', 
    'subtotal_invoiced'          => 'subtotal_invoiced', 
    'base_subtotal_invoiced'     => 'base_subtotal_invoiced', 
    'total_invoiced'             => 'total_invoiced', 
    'base_total_invoiced'        => 'base_total_invoiced', 
    'total_paid'                 => 'total_paid', 
    'base_total_paid'            => 'base_total_paid', 
    'total'                      => 'total', 
    'base_total'                 => 'base_total', 
    'total_due'                  => 'total_due', 
    'base_total_due'             => 'base_total_due', 
    'grand_total'                => 'grand_total', 
    'base_grand_total'           => 'base_grand_total', 
    'order_currency_code'        => 'order_currency_code', 
    'base_currency_code'         => 'base_currency_code', 
    'global_currency_code'       => 'global_currency_code', 
    'status'                     => 'status', 
    'ip_address'                 => 'ip_address', 
    'accepted_at'                => 'accepted_at', 
    'completed_at'               => 'completed_at', 
    'cancelled_at'               => 'cancelled_at', 
    'items'                      => 'items', 
    'created_at'                 => 'created_at', 
    'updated_at'                 => 'updated_at'
    ];

    /**
     * Create the Presenter and store the object we are presenting.
     *
     * @param  OrderInterface $order
     * @param  array          $fields
     * @return void
     */
    public function __construct(OrderInterface $order, Array $fields = ['*'])
    {
        parent::__construct($order->toArray());

        // Select field to publish
        // Remove the rest
        $newPublicData = [];

        if (is_array($fields)) {
            if (trim($fields[0]) != '*') {
                foreach ($this->publicData as $key => $data) {
                    foreach ($fields as $item) {
                        if ($data == $item) {
                            $newPublicData[$key] = $data;
                        }
                    }
                }
            }
        }

        if (! empty($newPublicData)) {
            $this->publicData = $newPublicData;
        }

        $this->_model = $order;
    }

    /**
     * Define currency manager.
     *
     * @param  \SunnyDayInc\Shop\Contracts\Currency $currency
     * @param  string                               $format
     * @return void
     */
    public function setCurrencyManager(Currency $currency, $format = '%s %a')
    {
        $this->_currency = $currency;
        $this->_currencyFormat = $format;
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentId()
    {
        return (int) $this->_model->id;
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentBranchId()
    {
        return (int) $this->_model->branch_id;
    }

    /**
     * Format and return the data.
     *
     * @return array
     */
    public function presentCoupon()
    {
        if (! empty($this->_model->promotion_coupon_name)) {
            return [
            'title' => $this->_model->promotion_coupon_name, 
            'code'  => $this->_model->promotion_coupon_code
            ];
        }

        return null;
    }

    /**
     * Format and return the data.
     *
     * @return array
     */
    public function presentCustomer()
    {
        if (! empty($this->_model->customer_id)) {
            return $this->_model->customer->getPresenter();
        }

        return null;
    }

    /**
     * Format and return the data.
     *
     * @return boolean
     */
    public function presentIsNewCustomer()
    {
        return (bool) $this->_model->is_new_customer;
    }

    /**
     * Format and return the data.
     *
     * @return array
     */
    public function presentBillingAddress()
    {
        $billingAddress = $this->_model->billingAddress;

        if (is_object($billingAddress)) {
            return $billingAddress->getPresenter();
        }

        return null;
    }

    /**
     * Format and return the data.
     *
     * @return float
     */
    public function presentTotalQty()
    {
        return (float) $this->_model->total_qty;
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentDiscount()
    {
        return $this->formatCurrency($this->_model->discount_amount);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBaseDiscount()
    {
        return $this->formatCurrency($this->_model->base_discount_amount);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentDiscountInvoiced()
    {

        return $this->formatCurrency($this->_model->discount_invoiced);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBaseDiscountInvoiced()
    {
        return $this->formatCurrency($this->_model->base_discount_invoiced);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentShippingDiscount()
    {
        return $this->formatCurrency($this->_model->shipping_discount_amount);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBaseShippingDiscount()
    {
        return $this->formatCurrency($this->_model->base_shipping_discount_amount);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentShipping()
    {
        return $this->formatCurrency($this->_model->shipping_amount);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBaseShipping()
    {
        return $this->formatCurrency($this->_model->base_shipping_amount);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentShippingTax()
    {
        return $this->formatCurrency($this->_model->shipping_tax_amount);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBaseShippingTax()
    {
        return $this->formatCurrency($this->_model->base_shipping_tax_amount);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentShippingInclTax()
    {
        return $this->formatCurrency($this->_model->shipping_incl_tax);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBaseShippingInclTax()
    {
        return $this->formatCurrency($this->_model->base_shipping_incl_tax);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentShippingInvoiced()
    {
        return $this->formatCurrency($this->_model->shipping_invoiced);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBaseShippingInvoiced()
    {
        return $this->formatCurrency($this->_model->base_shipping_invoiced);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentTax()
    {
        return $this->formatCurrency($this->_model->tax_amount);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBaseTax()
    {
        return $this->formatCurrency($this->_model->base_tax_amount);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentTaxInvoiced()
    {
        return $this->formatCurrency($this->_model->tax_invoiced);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBaseTaxInvoiced()
    {
        return $this->formatCurrency($this->_model->base_tax_invoiced);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentSubtotal()
    {
        return $this->formatCurrency($this->_model->subtotal);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBaseSubtotal()
    {
        return $this->formatCurrency($this->_model->base_subtotal);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentSubtotalInclTax()
    {
        return $this->formatCurrency($this->_model->subtotal_incl_tax);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBaseSubtotalInclTax()
    {
        return $this->formatCurrency($this->_model->base_subtotal_incl_tax);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentSubtotalInvoiced()
    {
        return $this->formatCurrency($this->_model->subtotal_invoiced);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBaseSubtotalInvoiced()
    {
        return $this->formatCurrency($this->_model->base_subtotal_invoiced);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentTotalInvoiced()
    {
        return $this->formatCurrency($this->_model->total_invoiced);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBaseTotalInvoiced()
    {
        return $this->formatCurrency($this->_model->base_total_invoiced);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentTotalPaid()
    {
        return $this->formatCurrency($this->_model->total_paid);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBaseTotalPaid()
    {
        return $this->formatCurrency($this->_model->base_total_paid);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentTotal()
    {
        return $this->formatCurrency($this->_model->total);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBaseTotal()
    {
        return $this->formatCurrency($this->_model->base_total);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentTotalDue()
    {
        return $this->formatCurrency($this->_model->total_due);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBaseTotalDue()
    {
        return $this->formatCurrency($this->_model->base_total_due);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentGrandTotal()
    {
        return $this->formatCurrency($this->_model->grand_total);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBaseGrandTotal()
    {
        return $this->formatCurrency($this->_model->base_grand_total);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentAcceptedAt()
    {
        if ($this->_model->accepted_at != '0000-00-00 00:00:00') {
            return (new DateTime($this->_model->accepted_at))->format('c');
        }

        return null;
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentCompletedAt()
    {
        if ($this->_model->completed_at != '0000-00-00 00:00:00') {
            return (new DateTime($this->_model->completed_at))->format('c');
        }

        return null;
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentCancelledAt()
    {
        if ($this->_model->cancelled_at != '0000-00-00 00:00:00') {
            return (new DateTime($this->_model->cancelled_at))->format('c');
        }

        return null;
    }

    /**
     * Format and return the data.
     *
     * @return array
     */
    public function presentItems()
    {
        if (! is_null($this->_items)) {
            return $this->_items;
        }

        $itemList     = $this->_model->items;
        $this->_items = new Collection();

        if (! $itemList->isEmpty()) {

            foreach ($itemList as $item) {
                $presenter = $item->getPresenter();

                if (! is_null($this->_currency)) {
                    $presenter->setCurrencyManager($this->_currency, $this->_currencyFormat);
                }

                $this->_items->push($presenter);
            }
        }

        unset($itemList);

        return $this->_items;
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentCreatedAt()
    {
        if ($this->_model->created_at != '0000-00-00 00:00:00') {
            return (new DateTime($this->_model->created_at))->format('c');
        }

        return null;
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentUpdatedAt()
    {
        if ($this->_model->updated_at != '0000-00-00 00:00:00') {
            return (new DateTime($this->_model->updated_at))->format('c');
        }

        return null;
    }

    /**
     * Return the formated currency value.
     *
     * @return string
     */
    private function formatCurrency($value)
    {
        if ($this->_currency instanceOf Currency) {
            return $this->_currency
                ->convert(
                    $value, 
                    $this->_model->base_currency_code, 
                    $this->_model->order_currency_code, 
                    $this->_model->base_to_order_rate
                )->format($this->_currencyFormat);
        }

        return $value;
    }

}
