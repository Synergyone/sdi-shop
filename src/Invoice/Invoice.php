<?php namespace SunnyDayInc\Shop\Invoice;

use SunnyDayInc\Shop\Order\Order;
use SunnyDayInc\Shop\Contracts\Invoice as InvoiceInterface;
use SunnyDayInc\Shop\Invoice\Repository\Invoice as InvoiceRepository;
use SunnyDayInc\Shop\Models\InvoiceInterface as InvoiceModelInterface;
use SunnyDayInc\Shop\Order\FactoryInterface as OrderInterface;

class Invoice implements InvoiceInterface
{

    /**
     * The repository instance.
     *
     * @var \SunnyDayInc\Shop\Invoice\Repository\Invoice
     */
    protected $repository;

    /**
     * The log instance.
     *
     * @var \Illuminate\Contracts\Logging\Log
     */
    protected $log;

    /**
     * The field formats.
     *
     * @var array
     */
    protected $fieldFormats = [];
  
    /**
     * Create a new instance.
     *
     * @param  \SunnyDayInc\Shop\Invoice\Repository\Invoice $repository
     * @return void
     */
    public function __construct(InvoiceRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    public function setLogger($log)
    {
        $this->log = $log;
    }

    /**
     * Insert new field format.
     *
     * @param  string $field
     * @param  array  $format
     * @return void
     */
    public function addFieldFormat($field, Array $format)
    {
        $this->fieldFormats[$field] = $format;
    }

    /**
     * {@inheritdoc}
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * {@inheritdoc}
     */
    public function create(Array $data = [])
    {
        return $this->createFactory($data);
    }

    /**
     * {@inheritdoc}
     */
    public function createFromOrder(OrderInterface $order, Array $options = [])
    {
        $options = array_merge([
        ], $options);

        $billingAddress = $order->getAddress(Order::ADDRESS_TYPE_BILLING);

        // Get the order model
        $model = $order->getOrder();

        // Create a new invoice
        $invoice = $this->createFactory([
            'object_type'              => $model->object_type, 
            'object_id'                => $model->object_id, 
            'branch_id'                => $model->branch_id, 
            'order_id'                 => $model->id, 
            'billing_address_id'       => $billingAddress->id, 
            'shipping_amount'          => $model->shipping_amount, 
            'base_shipping_amount'     => $model->base_shipping_amount, 
            'shipping_tax_amount'      => $model->shipping_tax_amount, 
            'base_shipping_tax_amount' => $model->base_shipping_tax_amount, 
            'shipping_incl_tax'        => $model->shipping_incl_tax, 
            'base_shipping_incl_tax'   => $model->base_shipping_incl_tax, 
            'order_currency_code'      => $model->order_currency_code, 
            'base_currency_code'       => $model->base_currency_code, 
            'global_currency_code'     => $model->global_currency_code, 
            'base_to_order_rate'       => $model->base_to_order_rate, 
            'base_to_global_rate'      => $model->base_to_global_rate
        ]);

        unset($model);
        unset($billingAddress);

        // Get all order items
        $items = $order->getItems();

        // Loop through all order items
        // and create new invoice item
        if ( ! $items->isEmpty()) {
            foreach ($items as $orderItem) {
                $item = $invoice->createItem();

                $item->setOrder($orderItem);

                $item->calculate();

                $invoice->addItem($item);
            }
        }

        unset($items);
        unset($order);

        // Save the invoice
        if ( ! $invoice->save()) {
            return false;
        }

        return $invoice;
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        $invoice = $this->repository->find($id);

        if ($invoice instanceOf InvoiceModelInterface) {
            $factory = $this->createFactory();

            $factory->setInvoice($invoice);

            unset($invoice);

            return $factory;
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function findByObject($objectType, $objectId, $id)
    {
        $invoice = $this->repository->findByObject($objectType, $objectId, $id);

        if ($invoice instanceOf InvoiceModelInterface) {
            $factory = $this->createFactory();

            $factory->setInvoice($invoice);

            unset($invoice);

            return $factory;
        }

        return null;
    }

    /**
     * Create the factory instance.
     *
     * @param  array $data
     * @return \SunnyDayInc\Shop\Invoice\FactoryInterface
     */
    private function createFactory(Array $data = [])
    {
        $factory = new Factory($this->repository, $this->fieldFormats, $data);

        if (! is_null($this->log)) {
            $factory->setLogger($this->log);
        }

        return $factory;
    }

}
