<?php namespace SunnyDayInc\Shop\Support\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \SunnyDayInc\Shop\Invoice\Invoice
 */
class Invoice extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'SunnyDayInc\Shop\Contracts\Invoice';
    }
}
