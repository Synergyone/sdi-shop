<?php namespace SunnyDayInc\Shop\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

use SunnyDayInc\Shop\Models\WalletCreditInterface;
use SunnyDayInc\Shop\Models\WalletCredit as WalletCreditModel;

class WalletCredit extends Model implements WalletCreditInterface
{

    use WalletCreditModel;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wallet_credit';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'wallet_id', 
    'total', 
    'base_total', 
    'currency_code', 
    'base_currency_code', 
    'base_to_credit_rate', 
    'source', 
    'source_value'
    ];

}
