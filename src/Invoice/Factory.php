<?php namespace SunnyDayInc\Shop\Invoice;

use SunnyDayInc\Shop\Invoice\Repository\Invoice as InvoiceRepository;

use SunnyDayInc\Shop\Support\Collection;

use SunnyDayInc\Shop\Models\InvoiceInterface;
use SunnyDayInc\Shop\Models\InvoiceItemInterface;

class Factory implements FactoryInterface
{

    /**
     * The log context.
     *
     * @var string
     */
    const LOG_CONTEXT = 'invoice.factory';

    /**
     * The repository instance.
     *
     * @var \SunnyDayInc\Shop\Invoice\Repository\Invoice
     */
    protected $repository;

    /**
     * The field formats.
     *
     * @var array
     */
    protected $fieldFormats;

    /**
     * The invoice model instance.
     *
     * @var \SunnyDayInc\Shop\Models\InvoiceInterface
     */
    protected $invoice;

    /**
     * The invoice items.
     *
     * @var \Collection|\SunnyDayInc\Shop\Models\InvoiceItemInterface[]
     */
    protected $items;

    /**
     * The invoice comment.
     *
     * @var \SunnyDayInc\Shop\Models\InvoiceCommentInterface
     */
    protected $comment;

    /**
     * Create a new instance.
     *
     * @param  \SunnyDayInc\Shop\Invoice\Repository\Invoice $repository
     * @param  array                                        $fieldFormats
     * @param  array                                        $data
     * @return void
     */
    public function __construct(InvoiceRepository $repository, Array $fieldFormats, Array $data = [])
    {
        $this->repository = $repository;
        $this->fieldFormats = $fieldFormats;

        $this->items = new Collection();
    
        // Create the invoice model and fill the default data
        if (! empty($data)) {
            $this->invoice = $this->repository->createModel();
            $this->invoice->fill($data);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setLogger($log)
    {
        $this->log = $log;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setInvoice(InvoiceInterface $invoice)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * {@inheritdoc}
     */
    public function getItems()
    {
        if (is_null($this->invoice)) {
            return $this->items;
        }

        $invoiceId = (isset($this->invoice->id)) ? $this->invoice->id : 0;

        // Because the invoice is not yet saved
        // we cannot identify any item by it's ID
        if (empty($invoiceId)) {
            return $this->items;
        }

        // Fetch all items from repository
        if ($this->items->isEmpty()) {
            $items = $this->repository->findItemsByInvoice($invoiceId);

            if (! $items->isEmpty()) {
                foreach ($items as $item) {
                    $this->items->put($item->id, $item);
                }
            }

            unset($items);
        }

        return $this->items;
    }

    /**
     * {@inheritdoc}
     */
    public function getItem($id)
    {
        if (is_null($this->invoice)) {
            return null;
        }

        $invoiceId = (isset($this->invoice->id)) ? $this->invoice->id : 0;

        // Because the invoice is not yet saved
        // we cannot identify any item by it's ID
        if (empty($invoiceId)) {
            return null;
        }

        if (! $this->items->isEmpty()) {
            $item = $this->items->get($id);

            if ($item instanceOf InvoiceItemInterface) {
                return $item;
            }
        }

        $item = $this->repository->findItemByInvoiceAndId($invoiceId, $id);

        if (! ($item instanceOf InvoiceItemInterface)) {
            return null;
        }

        $this->items->put($item->id, $item);

        return $item;
    }

    /**
     * {@inheritdoc}
     */
    public function addItem(InvoiceItemInterface $item)
    {
        // Set the invoice ID if available
        if (! empty($this->invoice->id)) {
            $item->invoice_id = $this->invoice->id;
        }

        // Merge if item is same as a existing item
        foreach ($this->items as $key => $existingItem) {
            if ($existingItem->equals($item)) {
                $this->items[$key]->merge($item, false);
                return $this;
            }

            unset($existingItem);
        }

        // Add item to collection
        $this->items->push($item);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function createItem()
    {
        return $this->repository->createModelItem();
    }

    /**
     * {@inheritdoc}
     */
    public function setComment($comment, $notifyCustomer = false, $visibleInFront = false)
    {
        $this->comment = $this->repository->createModelComment();

        // Set the invoice ID if available
        if (! empty($this->invoice->id)) {
            $this->comment->invoice_id = $this->invoice->id;
        } else {
            $this->comment->invoice_id = 0;
        }

        $this->comment->comment              = $comment;
        $this->comment->is_customer_notified = $notifyCustomer;
        $this->comment->is_visible_on_front  = $visibleInFront;

        // Save directly if invoice exist
        if (! empty($this->comment->invoice_id)) {
            $this->comment->save();
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function save()
    {
        $connection = $this->invoice->getConnection();

        // Initiate transaction
        $connection->beginTransaction();

        $isSaved = true;

        // Save the invoice
        // Do field formatters if available only when it's new invoice
        if (! $this->invoice->exists && ! empty($this->fieldFormats)) {
            foreach ($this->fieldFormats as $field => $format) {
                $handler = new $format['formatter']($format['format'], $this->invoice);

                $this->invoice->$field = $handler->compile();

                unset($handler);
            }
        }

        // Calculate invoice pricing
        $this->invoice->calculate($this->items);
    
        try {
            $isSaved = $this->invoice->save();
        } catch (\ErrorException $e) {
            $this->createLog('error', $e);

            $isSaved = false;
        }

        if (! $isSaved) {
            // Rollback the transaction
            $connection->rollBack();

            return false;
        }

        // Get the increment invoice ID
        $invoiceId = $this->invoice->id;

        // Save all items
        if (! $this->saveItems()) {
            // Rollback the transaction
            $connection->rollBack();

            return false;
        }

        // Save comment if available
        if (! is_null($this->comment)) {
            $this->comment->invoice_id = $invoiceId;
      
            try {
                $isSaved = $this->comment->save();
            } catch (\ErrorException $e) {
                $this->createLog('error', $e);

                $isSaved = false;
            }

            if (! $isSaved) {
                // Rollback the transaction
                $connection->rollBack();

                return false;
            }
        }

        // Commit the transaction
        $connection->commit();

        return true;
    }

    /**
     * Save all items.
     *
     * @return boolean
     */
    private function saveItems()
    {
        $isAllSaved = true;

        if ($this->items instanceOf Collection) {
            foreach ($this->items as $key => $item) {
                $isSaved = true;

                // Assign invoice ID
                $this->items[$key]->invoice_id = $this->invoice->id;

                try {
                    $isSaved = $this->items[$key]->save();
                } catch (\ErrorException $e) {
                    $this->createLog('error', $e);

                    $isSaved = false;
                }

                if (! $isSaved) {
                    $isAllSaved = false;
                    break;
                }
            }
        }

        return $isAllSaved;
    }

    /**
     * Return a created presenter.
     *
     * @return \SunnyDayInc\Shop\Presenter\Presenter
     */
    public function getPresenter()
    {
        return $this->invoice->getPresenter();
    }

    /**
     * Create a new log.
     *
     * @param  string $level
     * @param  mixed  $message
     * @param  Array  $data
     * @return void
     */
    private function createLog($level, $message, Array $data = [])
    {
        if (! is_null($this->log)) {
            $data['context'] = self::LOG_CONTEXT;

            if ($message instanceOf \ErrorException) {
                $this->log->$level($message->getCode().': '.$message->getMessage()."\n".$message->getTraceAsString(), $data);
            } else {
                $this->log->$level($message, $data);
            }
        }
    }

}
