<?php namespace SunnyDayInc\Shop\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

use SunnyDayInc\Shop\Models\OrderItemInterface;
use SunnyDayInc\Shop\Models\PromotionItemValidationInterface;
use SunnyDayInc\Shop\Models\OrderItem as OrderItemModel;
use SunnyDayInc\Shop\Contracts\Presentable;

class OrderItem extends Model implements OrderItemInterface, PromotionItemValidationInterface, Presentable
{

    use OrderItemModel;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'order_item';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'order_id', 
    'object_type', 
    'object_id', 
    'sku', 
    'title', 
    'options', 
    'description', 
    'preparation_additional_time', 
    'preparation_total_time', 
    'delivery_time', 
    'delivery_type', 
    'price', 
    'base_price', 
    'original_price', 
    'base_original_price', 
    'price_incl_tax', 
    'base_price_incl_tax', 
    'qty_ordered', 
    'qty_canceled', 
    'qty_invoiced', 
    'qty_refunded', 
    'qty_shipped', 
    'qty_returned', 
    'tax_percent', 
    'tax_amount', 
    'base_tax_amount', 
    'tax_before_discount', 
    'base_tax_before_discount', 
    'tax_invoiced', 
    'base_tax_invoiced', 
    'tax_refunded', 
    'base_tax_refunded', 
    'discount_percent', 
    'discount_amount', 
    'base_discount_amount', 
    'discount_invoiced', 
    'base_discount_invoiced', 
    'discount_refunded', 
    'base_discount_refunded', 
    'amount_refunded', 
    'base_amount_refunded', 
    'total', 
    'base_total', 
    'total_incl_tax', 
    'base_total_incl_tax', 
    'invoiced', 
    'base_invoiced', 
    'order_currency_code', 
    'base_currency_code', 
    'global_currency_code', 
    'base_to_order_rate', 
    'base_to_global_rate', 
    'start_preparation_at', 
    'end_preparation_at'
    ];

}
