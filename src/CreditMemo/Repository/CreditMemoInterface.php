<?php namespace SunnyDayInc\Shop\CreditMemo\Repository;

interface CreditMemoInterface
{

    /**
     * Return a credit memo by it's ID.
     *
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Contracts\Model|null
     */
    public function find($id);

    /**
     * Return credit memos.
     *
     * @param  array   $where
     * @param  array   $column
     * @param  integer $page
     * @param  integer $limit
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findBy(
        Array $where = [], 
        Array $column = ['*'], 
        $page = 1, 
        $limit = 10, 
        $order = 'DESC', 
        $orderBy = 'created_at'
    );

    /**
     * Return the current total row in previous query.
     *
     * @return string
     */
    public function getTotal();

    /**
     * Return the credit memo items by credit memo ID.
     *
     * @param  integer $creditMemoId
     * @return \Collection|\SunnyDayInc\Shop\Models\CreditMemoItemInterface[]
     */
    public function findItemsByCreditMemo($creditMemoId);

    /**
     * Return the credit memo item by credit memo ID and it's ID.
     *
     * @param  integer $creditMemoId
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Models\CreditMemoItemInterface|null
     */
    public function findItemByCreditMemoAndId($creditMemoId, $id);

    /**
     * Return the Model class name
     *
     * @return string
     */
    public function model();

    /**
     * Return the Model Item class name
     *
     * @return string
     */
    public function modelItem();

    /**
     * Return the Model Comment class name
     *
     * @return string
     */
    public function modelComment();

    /**
     * Return the model object creation.
     *
     * @return \SunnyDayInc\Shop\Models\CreditMemoInterface
     */
    public function createModel();

    /**
     * Return the model item object creation.
     *
     * @return \SunnyDayInc\Shop\Models\CreditMemoItemInterface
     */
    public function createModelItem();

    /**
     * Return the model promotion object creation.
     *
     * @return \SunnyDayInc\Shop\Models\CreditMemoCommentInterface
     */
    public function createModelComment();

}
