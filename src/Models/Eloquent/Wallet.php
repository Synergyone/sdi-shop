<?php namespace SunnyDayInc\Shop\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

use SunnyDayInc\Shop\Models\WalletInterface;
use SunnyDayInc\Shop\Models\Wallet as WalletModel;

class Wallet extends Model implements WalletInterface
{

    use WalletModel;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wallet';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'object_type', 
    'object_id', 
    'total', 
    'currency_code'
    ];

}
