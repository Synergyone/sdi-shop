ALTER TABLE `cart`
ADD `object_type` varchar(100) COLLATE 'utf8mb4_unicode_ci' NULL AFTER `id`,
ADD `object_id` bigint(20) unsigned NULL AFTER `object_type`;

ALTER TABLE `cart`
CHANGE `user_id` `user_id` bigint(20) unsigned NULL AFTER `id`,
ADD `customer_id` bigint(20) unsigned NULL AFTER `user_id`,
ADD `address_id` bigint(20) unsigned NULL AFTER `customer_id`;

ALTER TABLE `cart`
ADD INDEX `object_type` (`object_type`);

ALTER TABLE `cart`
ADD INDEX `object_type_object_id` (`object_type`, `object_id`);

ALTER TABLE `cart`
ADD INDEX `customer_id` (`customer_id`),
ADD INDEX `address_id` (`address_id`),
ADD INDEX `promotion_coupon_id` (`promotion_coupon_id`),
ADD INDEX `promotion_coupon_code` (`promotion_coupon_code`);