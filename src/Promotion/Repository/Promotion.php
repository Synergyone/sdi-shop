<?php namespace SunnyDayInc\Shop\Promotion\Repository;

abstract class Promotion implements PromotionInterface
{

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        return $this->createModel()->find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function findByObject($objectType, $objectId, $id)
    {
        return $this->createModel()->newQuery()
            ->where('object_type', '=', $objectType)
            ->where('object_id', '=', $objectId)
            ->where('id', '=', $id)
            ->first();
    }

    /**
     * {@inheritdoc}
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * {@inheritdoc}
     */
    public function createModel()
    {
        $model = $this->model();

        return new $model;
    }

    /**
     * {@inheritdoc}
     */
    public function createModelCoupon()
    {
        $model = $this->modelCoupon();

        return new $model;
    }

    /**
     * {@inheritdoc}
     */
    public function createModelProduct()
    {
        $model = $this->modelProduct();

        return new $model;
    }

}
