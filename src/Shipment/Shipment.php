<?php namespace SunnyDayInc\Shop\Shipment;

use SunnyDayInc\Shop\Contracts\Shipment as ShipmentInterface;
use SunnyDayInc\Shop\Shipment\Repository\Shipment as ShipmentRepository;
use SunnyDayInc\Shop\Models\ShipmentInterface as ShipmentModelInterface;

use Illuminate\Contracts\Logging\Log;

class Shipment implements ShipmentInterface
{

    const STATE_PENDING   = 'pending';
    const STATE_PICKED    = 'picked';
    const STATE_OTW       = 'on_the_way';
    const STATE_DELIVERED = 'delivered';

    /**
     * The repository instance.
     *
     * @var \SunnyDayInc\Shop\Shipment\Repository\Shipment
     */
    protected $repository;

    /**
     * The log instance.
     *
     * @var \Illuminate\Contracts\Logging\Log
     */
    protected $log;

    /**
     * The field formats.
     *
     * @var array
     */
    protected $fieldFormats = [];

    /**
     * Create a new instance.
     *
     * @param  \SunnyDayInc\Shop\Shipment\Repository\Shipment $repository
     * @return void
     */
    public function __construct(ShipmentRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    public function setLogger(Log $log)
    {
        $this->log = $log;
    }

    /**
     * {@inheritdoc}
     */
    public function addFieldFormat($field, Array $format)
    {
        $this->fieldFormats[$field] = $format;
    }

    /**
     * {@inheritdoc}
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * {@inheritdoc}
     */
    public function create(Array $data = [])
    {
        return $this->createFactory($data);
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        $shipment = $this->repository->find($id);

        if ($shipment instanceOf ShipmentModelInterface) {
            $factory = $this->createFactory();

            $factory->setShipment($shipment);

            unset($shipment);

            return $factory;
        }

        return null;
    }

    /**
     * Create the factory instance.
     *
     * @param  array $data
     * @return \SunnyDayInc\Shop\Shipment\FactoryInterface
     */
    private function createFactory(Array $data = [])
    {
        $factory = new Factory($this->repository, $this->fieldFormats, $data);

        if (! is_null($this->log)) {
            $factory->setLogger($this->log);
        }

        return $factory;
    }

}
