<?php namespace SunnyDayInc\Shop\Order\Repository;

interface CustomerInterface
{

    /**
     * Return a customer by the given ID.
     *
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Contracts\Model|null
     */
    public function findCustomer($id);

}
