<?php namespace SunnyDayInc\Shop\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

use SunnyDayInc\Shop\Models\ShipmentTrackInterface;
use SunnyDayInc\Shop\Models\ShipmentTrack as ShipmentTrackItemModel;

class ShipmentTrack extends Model implements ShipmentTrackInterface
{

    use ShipmentTrackItemModel;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shipment_track';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'shipment_id', 
    'order_id', 
    'qty', 
    'title', 
    'description', 
    'track_number', 
    'carrier_code', 
    'picked_at', 
    'received_at'
    ];

    /**
     * {@inheritdoc}
     */
    public function getItems()
    {
        $items = $this->hasMany('\SunnyDayInc\Shop\Models\Eloquent\ShipmentItem', 'shipment_track_id');

        $this->items = $items->get();

        unset($items);

        return $this->items;
    }

}
