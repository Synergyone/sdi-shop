<?php namespace SunnyDayInc\Shop\Models;

interface PromotionValidationInterface
{

    /**
     * Return the item collection.
     *
     * @return \Collection
     */
    public function getItems();

    /**
     * Return the base subtotal.
     *
     * @return float
     */
    public function getBaseSubtotal();

    /**
     * Return the total qty.
     *
     * @return integer
     */
    public function getTotalQty();

}
