<?php namespace SunnyDayInc\Shop\ServiceProvider\Illuminate;

use Illuminate\Support\ServiceProvider;

use SunnyDayInc\Shop\Wallet\Wallet as WalletContainer;
use SunnyDayInc\Shop\Wallet\Repository\EloquentWallet;

class Wallet extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            'SunnyDayInc\Shop\Contracts\Wallet', function ($app) {
                $creditMemo = new WalletContainer(new EloquentWallet());

                $creditMemo->setLogger($app['log']);

                return $creditMemo;
            }
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['SunnyDayInc\Shop\Contracts\Wallet'];
    }
}
