<?php namespace SunnyDayInc\Shop\Models;

use SunnyDayInc\Shop\Contracts\Model;

interface PromotionCouponInterface extends Model
{

    /**
     * Get the promotion that owns the coupon.
     */
    public function promotion();

}
