CREATE TABLE `wallet` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `object_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `object_id` bigint(20) unsigned NOT NULL,
  `total` decimal(30,20) NOT NULL DEFAULT '0.00000000000000000000',
  `currency_code` char(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `object_type` (`object_type`),
  KEY `object_id` (`object_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `wallet_credit` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `wallet_id` bigint(20) unsigned NOT NULL,
  `total` decimal(30,20) unsigned NOT NULL DEFAULT '0.00000000000000000000',
  `base_total` decimal(30,20) unsigned NOT NULL DEFAULT '0.00000000000000000000',
  `currency_code` char(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `base_currency_code` char(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `base_to_credit_rate` decimal(30,20) NOT NULL DEFAULT '0.00000000000000000000',
  `source` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `source_value` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `wallet_id` (`wallet_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `wallet_debit` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `wallet_id` bigint(20) unsigned NOT NULL,
  `total` decimal(30,20) NOT NULL DEFAULT '0.00000000000000000000',
  `base_total` decimal(30,20) NOT NULL DEFAULT '0.00000000000000000000',
  `currency_code` char(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `base_currency_code` char(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `base_to_debit_rate` decimal(30,20) NOT NULL DEFAULT '0.00000000000000000000',
  `status` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target_value` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `requested_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `completed_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `note` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `wallet_id` (`wallet_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;