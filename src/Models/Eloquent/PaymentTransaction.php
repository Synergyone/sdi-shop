<?php namespace SunnyDayInc\Shop\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

use SunnyDayInc\Shop\Models\PaymentTransactionInterface;

class PaymentTransaction extends Model implements PaymentTransactionInterface
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'payment_transaction';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'payment_id', 
    'order_id', 
    'invoice_id', 
    'txn_id', 
    'parent_txn_id', 
    'txn_type', 
    'is_closed', 
    'request', 
    'response'
    ];

}
