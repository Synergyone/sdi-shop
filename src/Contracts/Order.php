<?php namespace SunnyDayInc\Shop\Contracts;

interface Order
{

    /**
     * Set the logger instance.
     *
     * @param  mixed $log
     * @return void
     */
    public function setLogger($log);

    /**
     * Insert new field format.
     *
     * @param  string $field
     * @param  array  $format
     * @return void
     */
    public function addFieldFormat($field, Array $format);

    /**
     * Return the repository instance.
     *
     * @return \SunnyDayInc\Shop\Order\Repository\Order
     */
    public function getRepository();

    /**
     * Create a new order.
     *
     * @param  array $data
     * @return \SunnyDayInc\Shop\Order\FactoryInterface
     */
    public function create(Array $data = []);

    /**
     * Find a order by ID and return the factory instance.
     *
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Order\FactoryInterface
     */
    public function find($id);

    /**
     * Find a order by ID and return the factory instance.
     *
     * @param  string $identifier
     * @return \SunnyDayInc\Shop\Order\FactoryInterface
     */
    public function findByIdentifier($identifier);

    /**
     * Find a order by object type, object ID, and it's ID
     * then return the factory instance.
     *
     * @param  string  $objectType
     * @param  integer $objectId
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Order\FactoryInterface
     */
    public function findByObject($objectType, $objectId, $id);

}
