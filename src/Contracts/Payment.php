<?php namespace SunnyDayInc\Shop\Contracts;

use SunnyDayInc\Shop\Invoice\FactoryInterface as InvoiceInterface;

interface Payment
{

    /**
     * Set the logger instance.
     *
     * @param  mixed $log
     * @return void
     */
    public function setLogger($log);

    /**
     * Insert new field format.
     *
     * @param  string $field
     * @param  array  $format
     * @return void
     */
    public function addFieldFormat($field, Array $format);

    /**
     * Return the repository instance.
     *
     * @return \SunnyDayInc\Shop\Payment\Repository\Payment
     */
    public function getRepository();

    /**
     * Create a new payment from invoice.
     *
     * @param  \SunnyDayInc\Shop\Invoice\FactoryInterface $invoice
     * @param  Array                                      $options
     * @return \SunnyDayInc\Shop\Models\PaymentInterface
     */
    public function createFromInvoice(InvoiceInterface $invoice, Array $options = []);

}
