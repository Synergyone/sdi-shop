<?php namespace SunnyDayInc\Shop\Models;

use SunnyDayInc\Shop\Address\Presenter\Address as AddressPresenter;

trait Address
{

    /**
     * {@inheritdoc}
     */
    public function getPresenter(Array $fields = ['*'])
    {
        return new AddressPresenter($this, $fields);
    }

}
