<?php namespace SunnyDayInc\Shop\Contracts;

use Illuminate\Contracts\Logging\Log;

interface Shipment
{

    /**
     * Set the logger instance.
     *
     * @param  \Illuminate\Contracts\Logging\Log $repository
     * @return void
     */
    public function setLogger(Log $log);

    /**
     * Insert new field format.
     *
     * @param  string $field
     * @param  array  $format
     * @return void
     */
    public function addFieldFormat($field, Array $format);

    /**
     * Return the repository instance.
     *
     * @return \SunnyDayInc\Shop\Shipment\Repository\Shipment
     */
    public function getRepository();

    /**
     * Create a new shipment.
     *
     * @param  array $data
     * @return \SunnyDayInc\Shop\Shipment\FactoryInterface
     */
    public function create(Array $data = []);

    /**
     * Find a shipment by ID and return the factory instance.
     *
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Shipment\FactoryInterface
     */
    public function find($id);

}
