<?php namespace SunnyDayInc\Shop\Models;

use SunnyDayInc\Shop\Support\Uuid;
use SunnyDayInc\Shop\Cart\Repository\ProductInterface as ProductRepository;
use SunnyDayInc\Shop\Cart\Presenter\Item as ItemPresenter;

use SunnyDayInc\Shop\Exceptions\CartItemProductNotFoundException;
use SunnyDayInc\Shop\Exceptions\CartItemProductStockCouldNotSatisfyException;
use SunnyDayInc\Shop\Exceptions\CartItemProductOutOfStockException;
use DateTime;

trait CartItem
{

    /**
     * The product repository instance.
     *
     * @var \SunnyDayInc\Shop\Cart\Repository\ProductInterface
     */
    protected $productRepository;

    /**
     * The current item product.
     *
     * @var mixed
     */
    protected $product;

    /**
     * Define product repository instance.
     *
     * @param  \SunnyDayInc\Shop\Cart\Repository\ProductInterface $repository
     * @return void
     */
    public function setProductRepository(ProductRepository $repository)
    {
        $this->productRepository = $repository;
    }

    /**
     * Checks whether the item given as argument corresponds to
     * the same item. Can be overwritten to enable merge quantities.
     *
     * @param CartItemInterface $item
     *
     * @return bool
     */
    public function equals(CartItemInterface $item)
    {
        $isEqual = ($this->product_id === $item->product_id);

        if ($isEqual) {
            $isEqual = ($this->description == $item->description);
        }

        return $isEqual;
    }

    /**
     * Merge the item given as argument corresponding to the same item.
     *
     * @param  CartItemInterface $item
     * @return self
     */
    public function merge(CartItemInterface $item)
    {
        // Merge the quantity ordered
        $this->qty += $item->qty;

        return $this;
    }

    /**
     * Check the item product is still available.
     *
     * @return boolean
     * @throws SunnyDayInc\Shop\Exceptions\CartItemProductNotFoundException
     * @throws SunnyDayInc\Shop\Exceptions\CartItemProductOutOfStockException
     * @throws SunnyDayInc\Shop\Exceptions\CartItemProductStockCouldNotSatisfyException
     */
    public function checkIsAvailable()
    {
        $product = $this->getProduct();

        if (is_null($product)) {
            throw new CartItemProductNotFoundException('', $product);
        }

        if ($product->getStock() <= 0) {
            throw new CartItemProductOutOfStockException('', $product);
        }

        // Decided to check this at factory
        // if ($product->getStock() < $this->qty) {
        //   throw new CartItemProductStockCouldNotSatisfyException('', $product);
        // }

        unset($product);

        return true;
    }

    /**
     * Collect current item product.
     *
     * @return void
     */
    public function getProduct()
    {
        if (! is_null($this->product)) {
            return $this->product;
        }

        if (! is_null($this->productRepository)) {

            if (! empty($this->branch_id)) {
                $this->product = $this->productRepository->findByBranch($this->branch_id, $this->product_id);
            } else {
                $this->product = $this->productRepository->find($this->product_id);
            }

            return $this->product;

        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function save(array $options = [])
    {
        // Generate the cart ID if not available
        if (! $this->exists) {
            $this->setCreatedAt(new DateTime());
      
            $this->id = Uuid::generate(5, $this->cart_id.'-'.$this->product_id.'-'.$this->created_at->format('Y-m-d H:i:s'), Uuid::NS_DNS)->__toString();
        }

        return parent::save($options);
    }

    /**
     * {@inheritdoc}
     */
    public function getPresenter(Array $fields = ['*'])
    {
        return new ItemPresenter($this, $fields);
    }

}
