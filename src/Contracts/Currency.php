<?php namespace SunnyDayInc\Shop\Contracts;

use Illuminate\Contracts\Logging\Log;

interface Currency
{

    /**
     * Return the repository instance.
     *
     * @return \SunnyDayInc\Shop\Currency\Repository\Currency
     */
    public function getRepository();

    /**
     * Convert currency using exchange rate
     *
     * @param  float  $value
     * @param  string $from
     * @param  string $to
     * @param  float  $rate
     * @return self
     */
    public function convert($value, $from, $to, $rate = null);

    /**
     * Return currency rate using exchange rate
     *
     * @param  string $from
     * @param  string $to
     * @return float
     */
    public function getRate($from, $to);

    /**
     * Return the converted value
     *
     * @return float
     */
    public function result();

    /**
     * Format currency into specified format
     *
     * @param  string  $format
     * @param  integer $decimal
     * @return string
     */
    public function format($format = '%s %a', $decimal = 2);

    /**
     * Return the converted string value.
     *
     * @return string
     */
    public function __toString();

}
