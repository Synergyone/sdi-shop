<?php namespace SunnyDayInc\Shop\CreditMemo;

use SunnyDayInc\Shop\Contracts\CreditMemo as CreditMemoInterface;
use SunnyDayInc\Shop\CreditMemo\Repository\CreditMemo as CreditMemoRepository;
use SunnyDayInc\Shop\Models\CreditMemoInterface as CreditMemoModelInterface;

use Illuminate\Contracts\Logging\Log;

class CreditMemo implements CreditMemoInterface
{

    /**
     * The repository instance.
     *
     * @var \SunnyDayInc\Shop\CreditMemo\Repository\CreditMemo
     */
    protected $repository;

    /**
     * The log instance.
     *
     * @var \Illuminate\Contracts\Logging\Log
     */
    protected $log;

    /**
     * The field formats.
     *
     * @var array
     */
    protected $fieldFormats = [];
  
    /**
     * Create a new instance.
     *
     * @param  \SunnyDayInc\Shop\CreditMemo\Repository\CreditMemo $repository
     * @return void
     */
    public function __construct(CreditMemoRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    public function setLogger(Log $log)
    {
        $this->log = $log;
    }

    /**
     * Insert new field format.
     *
     * @param  string $field
     * @param  array  $format
     * @return void
     */
    public function addFieldFormat($field, Array $format)
    {
        $this->fieldFormats[$field] = $format;
    }

    /**
     * {@inheritdoc}
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * {@inheritdoc}
     */
    public function create(Array $data = [])
    {
        return $this->createFactory($data);
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        $creditMemo = $this->repository->find($id);

        if ($creditMemo instanceOf CreditMemoModelInterface) {
            $factory = $this->createFactory();

            $factory->setCreditMemo($creditMemo);

            unset($creditMemo);

            return $factory;
        }

        return null;
    }

    /**
     * Create the factory instance.
     *
     * @param  array $data
     * @return \SunnyDayInc\Shop\CreditMemo\FactoryInterface
     */
    private function createFactory(Array $data = [])
    {
        $factory = new Factory($this->repository, $this->fieldFormats, $data);

        if (! is_null($this->log)) {
            $factory->setLogger($this->log);
        }

        return $factory;
    }

}
