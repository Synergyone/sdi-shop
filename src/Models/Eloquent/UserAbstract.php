<?php namespace SunnyDayInc\Shop\Models\Eloquent;

use SunnyDayInc\Shop\Contracts\Model as ModelInterface;
use Illuminate\Database\Eloquent\Model;

use BeatSwitch\Lock\Callers\Caller as LockCaller;
use BeatSwitch\Lock\LockAware;

abstract class UserAbstract extends Model implements ModelInterface, LockCaller
{

    use LockAware;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['email', 'password', 'activated'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * The user groups pivot table name.
     *
     * @var string
     */
    protected static $userGroupsPivot = 'users_groups';

    /**
     * The type of caller
     *
     * @return string
     */
    public function getCallerType()
    {
        return 'users';
    }

    /**
     * The unique ID to identify the caller with
     *
     * @return int
     */
    public function getCallerId()
    {
        return $this->getKey();
    }

    /**
     * The caller's roles
     *
     * @return array
     */
    public function getCallerRoles()
    {
        $groups = $this->groups()->get();
        $roles = [];

        if (! $groups->isEmpty()) {
            foreach ($groups as $group) {
                $roles[] = $group->name;
            }
        }

        unset($groups);

        return $roles;
    }

    /**
     * Returns the relationship between user and groups.
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function groups()
    {
        return $this->belongsToMany(
            'SunnyDayInc\Shop\Models\Eloquent\Group', 
            static::$userGroupsPivot
        );
    }

}
