<?php namespace SunnyDayInc\Shop\Order\Formatter;

interface FormatterInterface
{

    /**
     * Return the formatted value.
     *
     * @return string
     */
    public function compile();

}
