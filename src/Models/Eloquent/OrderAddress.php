<?php namespace SunnyDayInc\Shop\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

use SunnyDayInc\Shop\Models\OrderAddressInterface;
use SunnyDayInc\Shop\Models\OrderAddress as OrderAddressModel;
use SunnyDayInc\Shop\Contracts\Presentable;

class OrderAddress extends Model implements OrderAddressInterface, Presentable
{

    use OrderAddressModel;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'order_address';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'order_id', 
    'type', 
    'location_title', 
    'name', 
    'company', 
    'phone', 
    'mobile_phone', 
    'address', 
    'country_code', 
    'country_name', 
    'state', 
    'city_id', 
    'city_name', 
    'postcode', 
    'note'
    ];

}
