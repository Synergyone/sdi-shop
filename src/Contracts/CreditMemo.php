<?php namespace SunnyDayInc\Shop\Contracts;

use Illuminate\Contracts\Logging\Log;

interface CreditMemo
{

    /**
     * Set the logger instance.
     *
     * @param  \Illuminate\Contracts\Logging\Log $repository
     * @return void
     */
    public function setLogger(Log $log);

    /**
     * Return the repository instance.
     *
     * @return \SunnyDayInc\Shop\CreditMemo\Repository\CreditMemo
     */
    public function getRepository();

    /**
     * Create a new credit memo.
     *
     * @param  array $data
     * @return \SunnyDayInc\Shop\CreditMemo\FactoryInterface
     */
    public function create(Array $data = []);

    /**
     * Find a credit memo by ID and return the factory instance.
     *
     * @param  integer $id
     * @return \SunnyDayInc\Shop\CreditMemo\FactoryInterface
     */
    public function find($id);

}
