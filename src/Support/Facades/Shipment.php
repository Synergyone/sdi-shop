<?php namespace SunnyDayInc\Shop\Support\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \SunnyDayInc\Shop\Shipment\Shipment
 */
class Shipment extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'SunnyDayInc\Shop\Contracts\Shipment';
    }
}
