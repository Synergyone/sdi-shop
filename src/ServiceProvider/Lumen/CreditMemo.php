<?php namespace SunnyDayInc\Shop\ServiceProvider\Lumen;

use Illuminate\Support\ServiceProvider;

use SunnyDayInc\Shop\CreditMemo\CreditMemo as CreditMemoContainer;
use SunnyDayInc\Shop\CreditMemo\Repository\EloquentCreditMemo;

class CreditMemo extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->configure('credit_memo');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            'SunnyDayInc\Shop\Contracts\CreditMemo', function ($app) {
                $creditMemo = new CreditMemoContainer(new EloquentCreditMemo());

                $creditMemo->setLogger($app['log']);

                $fieldFormats = $app['config']->get('credit_memo.field_formats');

                if (! empty($fieldFormats)) {
                    foreach ($fieldFormats as $field => $format) {
                        $creditMemo->addFieldFormat($field, $format);
                    }
                }

                unset($fieldFormats);

                return $creditMemo;
            }
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['SunnyDayInc\Shop\Contracts\CreditMemo'];
    }
}
