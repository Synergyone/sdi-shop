<?php namespace SunnyDayInc\Shop\Wallet;

use SunnyDayInc\Shop\Wallet\Repository\Wallet as WalletRepository;

use SunnyDayInc\Shop\Support\Collection;

use SunnyDayInc\Shop\Models\WalletInterface;
use SunnyDayInc\Shop\Models\WalletCreditInterface;
use SunnyDayInc\Shop\Models\WalletDebitInterface;

use Illuminate\Contracts\Logging\Log;

class Factory implements FactoryInterface
{

    /**
     * The log context.
     *
     * @var string
     */
    const LOG_CONTEXT = 'wallet.factory';

    /**
     * The repository instance.
     *
     * @var \SunnyDayInc\Shop\Wallet\Repository\Wallet
     */
    protected $repository;

    /**
     * The wallet model instance.
     *
     * @var \SunnyDayInc\Shop\Models\WalletInterface
     */
    protected $wallet;

    /**
     * The wallet credits.
     *
     * @var \Collection|\SunnyDayInc\Shop\Models\WalletCreditInterface[]
     */
    protected $credits;

    /**
     * The wallet debits.
     *
     * @var \Collection|\SunnyDayInc\Shop\Models\WalletDebitInterface[]
     */
    protected $debits;

    /**
     * Create a new instance.
     *
     * @param  \SunnyDayInc\Shop\Wallet\Repository\Wallet $repository
     * @param  array                                      $data
     * @return void
     */
    public function __construct(WalletRepository $repository, Array $data = [])
    {
        $this->repository = $repository;

        $this->credits = new Collection();
        $this->debits = new Collection();
    
        // Create the wallet model and fill the default data
        if (! empty($data)) {
            $this->wallet = $this->repository->createModel();
            $this->wallet->fill($data);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setLogger(Log $log)
    {
        $this->log = $log;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setWallet(WalletInterface $wallet)
    {
        $this->wallet = $wallet;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getWallet()
    {
        return $this->wallet;
    }

    /**
     * {@inheritdoc}
     */
    public function getCredit($id)
    {
        if (is_null($this->wallet)) {
            return null;
        }

        $walletId = (isset($this->wallet->id)) ? $this->wallet->id : 0;

        // Because the wallet is not yet saved
        // we cannot identify any item by it's ID
        if (empty($walletId)) {
            return null;
        }

        if (! $this->credits->isEmpty()) {
            $item = $this->credits->get($id);

            if ($item instanceOf WalletCreditInterface) {
                return $item;
            }
        }

        $item = $this->repository->findCreditByWalletAndId($walletId, $id);

        if (! ($item instanceOf WalletCreditInterface)) {
            return null;
        }

        $this->credits->put($item->id, $item);

        return $item;
    }

    /**
     * {@inheritdoc}
     */
    public function addCredit(WalletCreditInterface $credit)
    {
        // Set the wallet ID if available
        if (! empty($this->wallet->id)) {
            $credit->wallet_id = $this->wallet->id;
        }

        // Add credit to collection
        $this->credits->push($credit);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function createCredit()
    {
        return $this->repository->createModelCredit();
    }

    /**
     * {@inheritdoc}
     */
    public function getDebit($id)
    {
        if (is_null($this->wallet)) {
            return null;
        }

        $walletId = (isset($this->wallet->id)) ? $this->wallet->id : 0;

        // Because the wallet is not yet saved
        // we cannot identify any item by it's ID
        if (empty($walletId)) {
            return null;
        }

        if (! $this->debits->isEmpty()) {
            $item = $this->debits->get($id);

            if ($item instanceOf WalletDebitInterface) {
                return $item;
            }
        }

        $item = $this->repository->findDebitByWalletAndId($walletId, $id);

        if (! ($item instanceOf WalletDebitInterface)) {
            return null;
        }

        $this->debits->put($item->id, $item);

        return $item;
    }

    /**
     * {@inheritdoc}
     */
    public function addDebit(WalletDebitInterface $debit)
    {
        // Set the wallet ID if available
        if (! empty($this->wallet->id)) {
            $debit->wallet_id = $this->wallet->id;
        }

        // Add debit to collection
        $this->debits->push($debit);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function createDebit()
    {
        return $this->repository->createModelDebit();
    }

    /**
     * {@inheritdoc}
     */
    public function save()
    {
        $connection = $this->wallet->getConnection();

        // Initiate transaction
        $connection->beginTransaction();

        $isSaved = true;

        // Save the wallet
        // Calculate wallet total
        $this->wallet->calculate($this->credits, $this->debits);

        try {
            $isSaved = $this->wallet->save();
        } catch (\ErrorException $e) {
            $this->createLog('error', $e);

            $isSaved = false;
        }

        if (! $isSaved) {
            // Rollback the transaction
            $connection->rollBack();

            return false;
        }

        // Get the increment wallet ID
        $walletId = $this->wallet->id;

        // Save all credits
        if (! $this->saveCredits()) {
            // Rollback the transaction
            $connection->rollBack();

            return false;
        }

        // Save all debits
        if (! $this->saveDebits()) {
            // Rollback the transaction
            $connection->rollBack();

            return false;
        }

        // Commit the transaction
        $connection->commit();

        return true;
    }

    /**
     * Save all credits.
     *
     * @return boolean
     */
    private function saveCredits()
    {
        $isAllSaved = true;

        if ($this->credits instanceOf Collection) {
            foreach ($this->credits as $key => $item) {
                $isSaved = true;

                // Assign wallet ID
                $this->credits[$key]->wallet_id = $this->wallet->id;

                try {
                    $isSaved = $this->credits[$key]->save();
                } catch (\ErrorException $e) {
                    $this->createLog('error', $e);

                    $isSaved = false;
                }

                if (! $isSaved) {
                    $isAllSaved = false;
                    break;
                }
            }
        }

        return $isAllSaved;
    }

    /**
     * Save all debits.
     *
     * @return boolean
     */
    private function saveDebits()
    {
        $isAllSaved = true;

        if ($this->debits instanceOf Collection) {
            foreach ($this->debits as $key => $item) {
                $isSaved = true;

                // Assign wallet ID
                $this->debits[$key]->wallet_id = $this->wallet->id;

                try {
                    $isSaved = $this->debits[$key]->save();
                } catch (\ErrorException $e) {
                    $this->createLog('error', $e);

                    $isSaved = false;
                }

                if (! $isSaved) {
                    $isAllSaved = false;
                    break;
                }
            }
        }

        return $isAllSaved;
    }

    /**
     * Create a new log.
     *
     * @param  string $level
     * @param  mixed  $message
     * @param  Array  $data
     * @return void
     */
    private function createLog($level, $message, Array $data = [])
    {
        if (! is_null($this->log)) {
            $data['context'] = self::LOG_CONTEXT;

            if ($message instanceOf \ErrorException) {
                $this->log->$level($message->getCode().': '.$message->getMessage()."\n".$message->getTraceAsString(), $data);
            } else {
                $this->log->$level($message, $data);
            }
        }
    }

}
