<?php namespace SunnyDayInc\Shop\Models;

trait Shipment
{

    /**
     * Set the shipment status.
     *
     * @param  string $status
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

}
