<?php

namespace SunnyDayInc\Shop\Exceptions;

class CartItemProductOutOfStockException extends CartItemProductException
{

    /**
     * The exception type
     */
    public $errorType = 'out_of_stock';

}
