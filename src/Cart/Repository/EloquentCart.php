<?php namespace SunnyDayInc\Shop\Cart\Repository;

class EloquentCart extends Cart
{

    /**
     * The current total row in previous query.
     *
     * @var integer
     */
    protected $total = 0;

    /**
     * {@inheritdoc}
     */
    public function findBy(
        Array $where = [], 
        Array $column = ['*'], 
        $page = 1, 
        $limit = 10, 
        $order = 'DESC', 
        $orderBy = 'created_at'
    ) {
        $where = array_merge(
            [
            'object_type' => '', 
            'object_id'   => 0, 
            'search'      => '', 
            'status'      => [], 
            'date_start'  => null, 
            'date_end'    => null
            ], $where
        );

        if (empty($page)) {
            $page = 1;
        }

        $model = $this->createModel();

        if (! is_null($this->modelCustomer)) {
            $modelCustomer = $this->createModelCustomer();
        }

        if (! is_null($this->modelAddress)) {
            $modelAddress = $this->createModelAddress();
        }

        $useWhere = false;
        $hasWhere = false;

        $fromSql = '(';
        $fromSql .= 'SELECT `'.$model->getTable().'`.`id`';

        if (! is_null($this->modelCustomer)) {
            $fromSql .= ', `'.$modelCustomer->getTable().'`.`first_name` as `customer_first_name`';
            $fromSql .= ', `'.$modelCustomer->getTable().'`.`last_name` as `customer_last_name`';
        }

        if (! is_null($this->modelAddress)) {
            $fromSql .= ', `'.$modelAddress->getTable().'`.`title` as `address_title`';
        }

        $fromSql .= ' FROM `'.$model->getTable().'`';

        if (! empty($where['object_type'])
            || ! empty($where['object_id'])
            || ! empty($where['search'])
            || ! empty($where['status'])
            || ! empty($where['date_start'])
            || ! empty($where['date_end'])
        ) {
            $useWhere = true;
        }

        if (! is_null($this->modelCustomer)) {
            $fromSql .= ' LEFT JOIN `'.$modelCustomer->getTable().'`';
            $fromSql .= ' ON `'.$model->getTable().'`.`customer_id` = `'.$modelCustomer->getTable().'`.`id`';
        }

        if (! is_null($this->modelAddress)) {
            $fromSql .= ' LEFT JOIN `'.$modelAddress->getTable().'`';
            $fromSql .= ' ON `'.$model->getTable().'`.`address_id` = `'.$modelAddress->getTable().'`.`id`';
        }

        if ($useWhere) {
            $fromSql .= ' WHERE';
        }

        if (! empty($where['object_type'])) {
            $fromSql .= ' `'.$model->getTable().'`.`object_type` = "'.$where['object_type'].'"';

            $hasWhere = true;
        }

        if (! empty($where['object_id'])) {
            if ($hasWhere) {
                $fromSql .= ' AND';
            }

            $fromSql .= ' `'.$model->getTable().'`.`object_id` = '.$where['object_id'];

            $hasWhere = true;
        }

        if (! empty($where['search'])) {
            if (! is_null($this->modelCustomer) || ! is_null($this->modelAddress)) {
                if ($hasWhere) {
                    $fromSql .= ' AND';
                }

                $fromSql .= ' (';

                $isSearchHasOr = false;

                if (! is_null($this->modelCustomer)) {
                    $fromSql .= '`'.$modelCustomer->getTable().'`.`first_name` LIKE "%'.$where['search'].'%"';
                    $fromSql .= ' OR `'.$modelCustomer->getTable().'`.`last_name` LIKE "%'.$where['search'].'%"';

                    $isSearchHasOr = true;
                }

                if (! is_null($this->modelAddress)) {
                    if ($isSearchHasOr) {
                        $fromSql .= ' OR ';
                    }

                    $fromSql .= '`'.$modelAddress->getTable().'`.`title` LIKE "%'.$where['search'].'%"';
                    $fromSql .= ' OR `'.$modelAddress->getTable().'`.`company` LIKE "%'.$where['search'].'%"';
                    $fromSql .= ' OR `'.$modelAddress->getTable().'`.`phone` LIKE "%'.$where['search'].'%"';
                    $fromSql .= ' OR `'.$modelAddress->getTable().'`.`mobile_phone` LIKE "%'.$where['search'].'%"';
                    $fromSql .= ' OR `'.$modelAddress->getTable().'`.`address` LIKE "%'.$where['search'].'%"';

                    $isSearchHasOr = true;
                }

                $fromSql .= ')';

                $hasWhere = true;
            }
        }

        if (! empty($where['status'])) {
            if ($hasWhere) {
                $fromSql .= ' AND';
            }

            $fromSql .= ' `'.$model->getTable().'`.`status` IN ("'.implode('", "', $where['status']).'")';

            $hasWhere = true;
        }

        if (! is_null($where['date_start'])) {
            if ($hasWhere) {
                $fromSql .= ' AND';
            }

            $fromSql .= ' `'.$model->getTable().'`.`created_at` >= "'.$where['date_start']->format('Y-m-d H:i:s').'"';

            $hasWhere = true;
        }

        if (! is_null($where['date_end'])) {
            if ($hasWhere) {
                $fromSql .= ' AND';
            }

            $fromSql .= ' `'.$model->getTable().'`.`created_at` <= "'.$where['date_end']->format('Y-m-d H:i:s').'"';

            $hasWhere = true;
        }

        if (! empty($order) && ! empty($orderBy)) {
            $fromSql .= ' ORDER BY `'.$model->getTable().'`.`'.$orderBy.'` '.$order;
        }

        if ($limit > 0) {
            $fromSql .= ' limit '.$limit.' offset '.($page - 1) * $limit;
        }

        $fromSql .= ') o';
  
        if ($limit > 0) {
            $query = $model->newQuery()->select('id');

            if (! empty($where['search'])) {
                if (! is_null($this->modelCustomer)) {
                    $query->leftJoin($modelCustomer->getTable(), $model->getTable().'.customer_id', '=', $modelCustomer->getTable().'.id');
                }

                if (! is_null($this->modelAddress)) {
                    $query->leftJoin($modelAddress->getTable(), $model->getTable().'.address_id', '=', $modelAddress->getTable().'.id');
                }
            }

            if (! empty($where['object_type'])) {
                $query->where($model->getTable().'.object_type', '=', $where['object_type']);
            }

            if (! empty($where['object_id'])) {
                $query->where($model->getTable().'.object_id', '=', $where['object_id']);
            }

            if (! empty($where['search'])) {
                if (! is_null($this->modelCustomer)) {
                    $searchText = $where['search'];

                    $query->where(
                        function ($qry) use ($modelCustomer, $searchText) {
                            $qry->where($modelCustomer->getTable().'.first_name', 'LIKE', '%'.$searchText.'%')
                                ->orWhere($modelCustomer->getTable().'.last_name', 'LIKE', '%'.$searchText.'%');
                        }
                    );
                }

                if (! is_null($this->modelAddress)) {
                    $searchText = $where['search'];

                    $query->where(
                        function ($qry) use ($modelAddress, $searchText) {
                            $qry->where($modelAddress->getTable().'.title', 'LIKE', '%'.$searchText.'%')
                                ->orWhere($modelAddress->getTable().'.company', 'LIKE', '%'.$searchText.'%')
                                ->orWhere($modelAddress->getTable().'.phone', 'LIKE', '%'.$searchText.'%')
                                ->orWhere($modelAddress->getTable().'.mobile_phone', 'LIKE', '%'.$searchText.'%')
                                ->orWhere($modelAddress->getTable().'.address', 'LIKE', '%'.$searchText.'%');
                        }
                    );
                }
            }

            if (! empty($where['status'])) {
                $query->whereIn($model->getTable().'.status', $where['status']);
            }

            if (! empty($where['date_start'])) {
                $query->where($model->getTable().'.created_at', '>=', $where['date_start']->format('Y-m-d H:i:s'));
            }

            if (! empty($where['date_end'])) {
                $query->where($model->getTable().'.created_at', '<=', $where['date_end']->format('Y-m-d H:i:s'));
            }

            $this->total = $query->count();
        } else {
            $this->total = 0;
        }

        $query = $model->newQuery()
            ->from(app('db')->raw($fromSql))
            ->join($model->getTable(), $model->getTable().'.id', '=', 'o.id');

        if (! empty($order) && ! empty($orderBy)) {
            $query->orderBy($model->getTable().'.'.$orderBy, $order);
        }

        unset($fromSql);
        unset($model);

        return $query->get($column);
    }

    /**
     * {@inheritdoc}
     */
    public function findItemsByCart($cartId)
    {
        $query = $this->createModelItem()->newQuery();
        $query->where('cart_id', '=', $cartId);

        return $query->get();
    }

    /**
     * {@inheritdoc}
     */
    public function findItemByCartAndId($cartId, $id)
    {
        $query = $this->createModelItem()->newQuery();
        $query->where('cart_id', '=', $cartId);
        $query->where('id', '=', $id);

        return $query->first();
    }

    /**
     * {@inheritdoc}
     */
    public function model()
    {
        return '\SunnyDayInc\Shop\Models\Eloquent\Cart';
    }

    /**
     * {@inheritdoc}
     */
    public function modelItem()
    {
        return '\SunnyDayInc\Shop\Models\Eloquent\CartItem';
    }

}
