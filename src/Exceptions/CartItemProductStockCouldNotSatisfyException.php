<?php

namespace SunnyDayInc\Shop\Exceptions;

class CartItemProductStockCouldNotSatisfyException extends CartItemProductException
{

    /**
     * The exception type
     */
    public $errorType = 'stock_could_not_satisfy';

}
