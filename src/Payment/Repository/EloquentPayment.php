<?php namespace SunnyDayInc\Shop\Payment\Repository;

class EloquentPayment extends Payment
{

    /**
     * The current total row in previous query.
     *
     * @var integer
     */
    protected $total = 0;

    /**
     * {@inheritdoc}
     */
    public function model()
    {
        return '\SunnyDayInc\Shop\Models\Eloquent\Payment';
    }

    /**
     * {@inheritdoc}
     */
    public function modelTransaction()
    {
        return '\SunnyDayInc\Shop\Models\Eloquent\PaymentTransaction';
    }

    /**
     * {@inheritdoc}
     */
    public function modelCreditCard()
    {
        return '\SunnyDayInc\Shop\Models\Eloquent\PaymentCreditCard';
    }

}
