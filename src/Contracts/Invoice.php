<?php namespace SunnyDayInc\Shop\Contracts;

use SunnyDayInc\Shop\Order\FactoryInterface as OrderInterface;

interface Invoice
{

    /**
     * Set the logger instance.
     *
     * @param  mixed $log
     * @return void
     */
    public function setLogger($log);

    /**
     * Return the repository instance.
     *
     * @return \SunnyDayInc\Shop\Invoice\Repository\Invoice
     */
    public function getRepository();

    /**
     * Create a new invoice.
     *
     * @param  array $data
     * @return \SunnyDayInc\Shop\Invoice\FactoryInterface
     */
    public function create(Array $data = []);

    /**
     * Create a new invoice from order.
     *
     * @param  \SunnyDayInc\Shop\Order\FactoryInterface $order
     * @param  Array                                    $options
     * @return \SunnyDayInc\Shop\Invoice\FactoryInterface
     */
    public function createFromOrder(OrderInterface $order, Array $options = []);

    /**
     * Find a invoice by ID and return the factory instance.
     *
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Invoice\FactoryInterface
     */
    public function find($id);

    /**
     * Find a invoice by object type, object ID, and it's ID
     * then return the factory instance.
     *
     * @param  string  $objectType
     * @param  integer $objectId
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Invoice\FactoryInterface
     */
    public function findByObject($objectType, $objectId, $id);

}
