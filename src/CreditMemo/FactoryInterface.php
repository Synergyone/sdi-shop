<?php namespace SunnyDayInc\Shop\CreditMemo;

use SunnyDayInc\Shop\Models\CreditMemoInterface;
use SunnyDayInc\Shop\Models\CreditMemoItemInterface;

use Illuminate\Contracts\Logging\Log;

interface FactoryInterface
{

    /**
     * Set the logger instance.
     *
     * @param  \Illuminate\Contracts\Logging\Log $log
     * @return self
     */
    public function setLogger(Log $log);

    /**
     * Set the credit memo instance.
     *
     * @param  \SunnyDayInc\Shop\Models\CreditMemoInterface $creditMemo
     * @return self
     */
    public function setCreditMemo(CreditMemoInterface $creditMemo);

    /**
     * Return the credit memo instance.
     *
     * @return \SunnyDayInc\Shop\Models\CreditMemoInterface
     */
    public function getCreditMemo();

    /**
     * Return the items.
     *
     * @return \Collection|\SunnyDayInc\Shop\Models\CreditMemoItemInterface[]
     */
    public function getItems();

    /**
     * Get a item by it's ID.
     *
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Models\CreditMemoItemInterface|null
     */
    public function getItem($id);

    /**
     * Insert a new item.
     *
     * @param  \SunnyDayInc\Shop\Models\CreditMemoItemInterface $item
     * @return self
     */
    public function addItem(CreditMemoItemInterface $item);

    /**
     * Create a new credit memo item.
     *
     * @return \SunnyDayInc\Shop\Models\CreditMemoItemInterface
     */
    public function createItem();

    /**
     * Set comment.
     *
     * @param  string  $comment
     * @param  boolean $notifyCustomer
     * @param  boolean $visibleInFront
     * @return self
     */
    public function setComment($comment, $notifyCustomer = false, $visibleInFront = false);

    /**
     * Save the credit memo.
     *
     * @return boolean
     */
    public function save();

}
