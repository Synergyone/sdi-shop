<?php namespace SunnyDayInc\Shop\Models;

use SunnyDayInc\Shop\Support\Collection;

use SunnyDayInc\Shop\Order\Repository\CustomerInterface as CustomerRepositoryContract;
use SunnyDayInc\Shop\Promotion\Promotion as PromotionContainer;
use SunnyDayInc\Shop\Models\PromotionInterface;
use SunnyDayInc\Shop\Order\Presenter\Order as OrderPresenter;

trait Order
{

    /**
     * The customer repository instance.
     *
     * @var \SunnyDayInc\Shop\Order\Repository\CustomerInterface
     */
    protected $customerRepo;

    /**
     * The order promotions.
     *
     * @var \Collection|\SunnyDayInc\Shop\Models\PromotionInterface[]
     */
    protected $promotions;

    /**
     * Set the customer repository.
     *
     * @param  \SunnyDayInc\Shop\Order\Repository\CustomerInterface $repository
     * @return self
     */
    public function setCustomerRepository(CustomerRepositoryContract $repository)
    {
        $this->customerRepo = $repository;
    }

    /**
     * {@inheritdoc}
     */
    public function getItems()
    {
        return new Collection($this->items->all());
    }

    /**
     * {@inheritdoc}
     */
    public function getBaseSubtotal()
    {
        $this->calculate();

        return $this->base_subtotal;
    }

    /**
     * {@inheritdoc}
     */
    public function getTotalQty()
    {
        $this->calculate();

        return $this->total_qty;
    }

    /**
     * Insert a new promotion.
     *
     * @param  \SunnyDayInc\Shop\Models\PromotionInterface               $promo
     * @param  \Collection|\SunnyDayInc\Shop\Models\OrderItemInterface[]
     * @return boolean
     */
    public function addPromotion(PromotionInterface $promo, Collection $items)
    {
        // Only accept cart promotion
        if ($promo->type !== PromotionContainer::TYPE_CART) {
            return false;
        }

        // Check is promotion applicable
        if (! $promo->isApplicable($this, $items)) {
            return false;
        }

        // Set new discount amount
        if ($promo->applyToShipping()) {
            if ($promo->isFreeShipping()) {
                $this->base_shipping_amount     = 0;
                $this->base_shipping_tax_amount = 0;

                $this->shipping_incl_tax        = 0;
                $this->base_shipping_incl_tax   = 0;
            } else {
                $this->shipping_discount_amount      = $promo->getCartDiscount($this->shipping_amount);
                $this->base_shipping_discount_amount = $promo->getCartDiscount($this->base_shipping_amount);

                $this->shipping_tax_amount          -= $promo->getCartDiscount($this->shipping_tax_amount);
                $this->base_shipping_tax_amount     -= $promo->getCartDiscount($this->base_shipping_tax_amount);

                $this->shipping_amount              -= $this->shipping_discount_amount;
                $this->base_shipping_amount         -= $this->base_shipping_discount_amount;

                $this->shipping_incl_tax            -= $this->shipping_amount + $this->shipping_tax_amount;
                $this->base_shipping_incl_tax       -= $this->base_shipping_amount + $this->base_shipping_tax_amount;
            }
        } else {
            $discountAmount     = $promo->getCartDiscount($this->subtotal);
            $baseDiscountAmount = $promo->getCartDiscount($this->base_subtotal);

            $this->discount_amount      += $discountAmount;
            $this->base_discount_amount += $baseDiscountAmount;

            $this->total                -= $discountAmount;
            $this->base_total           -= $baseDiscountAmount;

            $this->grand_total          -= $discountAmount;
            $this->base_grand_total     -= $baseDiscountAmount;
        }

        if (is_null($this->promotions)) {
            $this->promotions = new Collection();
        }

        $this->promotions->push($promo);

        return true;
    }

    /**
     * Return existing promotions.
     *
     * @return \Collection|\SunnyDayInc\Shop\Models\PromotionInterface[]|null
     */
    public function getPromotions()
    {
        return $this->promotions;
    }

    /**
     * Calculate.
     *
     * @param  \Collection $items
     * @return self
     */
    public function calculate(Collection $items)
    {
        $this->total_qty              = 0;

        $this->discount_amount        = 0;
        $this->base_discount_amount   = 0;

        $this->tax_amount             = 0;
        $this->base_tax_amount        = 0;

        $this->subtotal               = 0;
        $this->base_subtotal          = 0;

        $this->subtotal_incl_tax      = 0;
        $this->base_subtotal_incl_tax = 0;

        if (! $items->isEmpty()) {
            foreach ($items as $item) {

                $this->total_qty              += $item->qty_ordered;

                $this->subtotal               += $item->total + $item->discount_amount;
                $this->base_subtotal          += $item->base_total + $item->base_discount_amount;

                $this->subtotal_incl_tax      += $item->total_incl_tax + $item->discount_amount;
                $this->base_subtotal_incl_tax += $item->base_total_incl_tax + $item->base_discount_amount;

                $this->discount_amount        += $item->discount_amount;
                $this->base_discount_amount   += $item->base_discount_amount;

                $this->tax_amount             += $item->tax_amount;
                $this->base_tax_amount        += $item->base_tax_amount;

            }
        }

        $this->shipping_amount        = $this->base_shipping_amount * $this->base_to_order_rate;
        $this->shipping_tax_amount    = $this->base_shipping_tax_amount * $this->base_to_order_rate;

        $this->shipping_incl_tax      = $this->shipping_amount + $this->shipping_tax_amount;
        $this->base_shipping_incl_tax = $this->base_shipping_amount + $this->base_shipping_tax_amount;

        $this->total                  = ($this->subtotal - $this->discount_amount) + $this->shipping_amount;
        $this->base_total             = ($this->base_subtotal - $this->base_discount_amount) + $this->base_shipping_amount;

        $this->grand_total            = $this->subtotal_incl_tax + $this->shipping_amount;
        $this->base_grand_total       = $this->base_subtotal_incl_tax + $this->base_shipping_amount;

        // $this->discount_canceled      = ;
        // $this->base_discount_canceled = ;
        // $this->discount_invoiced      = ;
        // $this->base_discount_invoiced = ;
        // $this->discount_refunded      = ;
        // $this->base_discount_refunded = ;

        // $this->shipping_tax_refunded      = ;
        // $this->base_shipping_tax_refunded = ;

        // $this->shipping_canceled      = ;
        // $this->base_shipping_canceled = ;
        // $this->shipping_invoiced      = ;
        // $this->base_shipping_invoiced = ;
        // $this->shipping_refunded      = ;
        // $this->base_shipping_refunded = ;

        // $this->tax_canceled           = ;
        // $this->base_tax_canceled      = ;
        // $this->tax_invoiced           = ;
        // $this->base_tax_invoiced      = ;
        // $this->tax_refunded           = ;
        // $this->base_tax_refunded      = ;

        // $this->subtotal_canceled      = ;
        // $this->base_subtotal_canceled = ;
        // $this->subtotal_invoiced      = ;
        // $this->base_subtotal_invoiced = ;
        // $this->subtotal_refunded      = ;
        // $this->base_subtotal_refunded = ;
        // $this->total_canceled         = ;
        // $this->base_total_canceled    = ;
        // $this->total_invoiced         = ;
        // $this->base_total_invoiced    = ;

        // $this->total_paid             = ;
        // $this->base_total_paid        = ;

        // $this->total_refunded         = ;
        // $this->base_total_refunded    = ;
        // $this->total_due              = ;
        // $this->base_total_due         = ;
    }

    /**
     * {@inheritdoc}
     */
    public function getPresenter(Array $fields = ['*'])
    {
        // Collect the customer if available
        if (! empty($this->customer_id) && ! is_null($this->customerRepo)) {
            $this->customer = $this->customerRepo->findCustomer($this->customer_id);
        }

        return new OrderPresenter($this, $fields);
    }

}
