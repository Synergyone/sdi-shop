<?php namespace SunnyDayInc\Shop\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

use SunnyDayInc\Shop\Models\PromotionProductInterface;
use SunnyDayInc\Shop\Models\PromotionProduct as PromotionProductModel;

class PromotionProduct extends Model implements PromotionProductInterface
{

    use PromotionProductModel;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'promotion_product';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'promotion_id', 
    'from_time', 
    'to_time', 
    'product_id', 
    'action_operator', 
    'action_amount', 
    'sort_order'
    ];

    /**
     * Return product promotion.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function promotion()
    {
        return $this->belongsTo('SunnyDayInc\Shop\Models\Eloquent\Promotion');
    }

}
