<?php namespace SunnyDayInc\Shop\Promotion\Rule\Condition;

use SunnyDayInc\Shop\Support\Collection;
use SunnyDayInc\Shop\Promotion\Rule\Rule;

class Cart extends Rule
{

    const ATTR_BASE_SUBTOTAL = 'base_subtotal';
    const ATTR_TOTAL_QTY     = 'total_qty';
    const ATTR_POSTCODE      = 'postcode';
    const ATTR_CITY_ID       = 'city_id';
    const ATTR_COUNTRY_ID    = 'country_id';

    /**
     * Create a new instance.
     *
     * @param  array $data
     * @return void
     */
    public function __construct(Array $data)
    {
        parent::__construct($data);

        $this->attributes = [
        self::ATTR_BASE_SUBTOTAL, 
        self::ATTR_TOTAL_QTY, 
        self::ATTR_POSTCODE, 
        self::ATTR_CITY_ID, 
        self::ATTR_COUNTRY_ID
        ];
        $this->operatorTypeByAttribute = [
        self::ATTR_BASE_SUBTOTAL => 'numeric', 
        self::ATTR_TOTAL_QTY     => 'numeric', 
        self::ATTR_POSTCODE      => 'string', 
        self::ATTR_CITY_ID       => 'select', 
        self::ATTR_COUNTRY_ID    => 'select'
        ];
    }

    /**
     * Check if applicable.
     *
     * @param  mixed $item
     * @return boolean
     */
    public function isApplicable($item)
    {
        $isApplicable = false;

        // Validate based on child rules result
        switch ($this->attribute) {
        case self::ATTR_BASE_SUBTOTAL:
        
            $isApplicable = $this->validateValueByOperator($item->getBaseSubtotal());

            break;
      
        case self::ATTR_TOTAL_QTY:
        
            $isApplicable = $this->validateValueByOperator($item->getTotalQty());

            break;

        case self::ATTR_POSTCODE:
        
            $isApplicable = $this->validateValueByOperator('');

            break;

        case self::ATTR_CITY_ID:
        
            $isApplicable = $this->validateValueByOperator(null);

            break;

        case self::ATTR_COUNTRY_ID:
        
            $isApplicable = $this->validateValueByOperator(null);

            break;
        }

        return $isApplicable;
    }

}
