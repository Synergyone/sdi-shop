<?php namespace SunnyDayInc\Shop\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

use SunnyDayInc\Shop\Models\PaymentInterface;
use SunnyDayInc\Shop\Models\Payment as PaymentModel;
use SunnyDayInc\Shop\Contracts\Presentable;

class Payment extends Model implements PaymentInterface, Presentable
{

    use PaymentModel;
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'payment';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'object_type', 
    'object_id', 
    'order_id', 
    'invoice_id', 
    'identifier', 
    'code', 
    'shipping_amount', 
    'base_shipping_amount', 
    'amount_authorized', 
    'base_amount_authorized', 
    'amount_unique_authorized', 
    'amount_paid', 
    'base_amount_paid', 
    'shipping_refunded', 
    'base_shipping_refunded', 
    'amount_refunded', 
    'base_amount_refunded', 
    'amount_canceled', 
    'base_amount_canceled', 
    'method', 
    'source', 
    'sender_source_title', 
    'sender_name', 
    'sender_account', 
    'receiver_source_title', 
    'receiver_name', 
    'receiver_account', 
    'note', 
    'status'
    ];

}
