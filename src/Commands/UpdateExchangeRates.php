<?php namespace App\Console\Commands;

use Illuminate\Console\Command;

use SunnyDayInc\Shop\Models\Eloquent\ExchangeRate;

class UpdateExchangeRates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'currency:updateExchangeRates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Exchange Rates';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $availableRates = $this->getRates();

        if (! empty($availableRates)) {
            $model = new ExchangeRate;

            $existingRates = $model->newQuery()
                ->where('from', '=', 'USD')->get();

            $rates = array();

            if (! empty($existingRates)) {
                foreach ($existingRates as $item) {
                    $rates['USD_'.$item->to] = $item;
                }
            }

            unset($existingRates);

            $currentDate = date('Y-m-d H:i:s');

            foreach ($availableRates as $currency => $rate) {
                $currencyKey   = 'USD_'.$currency;
                $otherwiseRate = null;

                if (isset($rates[$currencyKey])) {
                    $rateItem = $model->newQuery()->find($rates[$currencyKey]->id);
                    $rateItem->setAttribute('rate', $rate);

                    $otherwiseRate = $model->newQuery()
                        ->where('from', '=', $rateItem->to)
                        ->where('to', '=', 'USD')
                        ->first();

                    if ($otherwiseRate) {
                        $otherwiseRate->setAttribute('rate', 1 / $rate);
                        $otherwiseRate->save();
                    }
                } else {
                    $rateItem = new ExchangeRate;
                    $rateItem->fill(
                        array(
                        'from'       => 'USD',
                        'to'         => $currency,
                        'rate'       => $rate,
                        'created_at' => $currentDate
                        )
                    );
                }

                $rateItem->save();

                if (is_null($otherwiseRate)) {
                    $otherwiseRate = new ExchangeRate;
                    $otherwiseRate->fill(
                        array(
                        'from'       => $currency,
                        'to'         => 'USD',
                        'rate'       => 1 / $rate,
                        'created_at' => $currentDate
                        )
                    );
                    $otherwiseRate->save();
                }
            }

            $this->currencyPermutation(config('sunnydayinc.shop.currency.permutation'));

            unset($rates);
            unset($model);
        }

        unset($availableRates);
    }

    /**
     * Create currency permutation
     *
     * @param  array $currencyCodes
     * @return array
     */
    private function currencyPermutation($currencyCodes = array())
    {
        $model = new ExchangeRate;

        foreach ($currencyCodes as $code1) {
            $theRate = $model->newQuery()
                ->where('from', '=', $code1)
                ->where('to', '=', 'USD')
                ->first();

            if (is_object($theRate)) {
                foreach ($currencyCodes as $code2) {
                    if ($code1 != $code2) {
                        $anotherRate = $model->newQuery()
                            ->where('from', '=', $code1)
                            ->where('to', '=', $code2)
                            ->first();

                        $targetRate = $model->newQuery()
                            ->where('from', '=', 'USD')
                            ->where('to', '=', $code2)
                            ->first();
            
                        if (is_object($targetRate)) {
                            if (is_object($anotherRate)) {
                                $anotherRate->setAttribute('rate', $theRate->rate * $targetRate->rate);
                            } else {
                                $anotherRate = new ExchangeRate;
                                $anotherRate->fill(
                                    array(
                                    'from' => $code1,
                                    'to'   => $code2,
                                    'rate' => $theRate->rate * $targetRate->rate
                                    )
                                );
                            }

                            $anotherRate->save();
                        }

                        unset($targetRate);
                        unset($anotherRate);
                    }
                }
            }

            unset($theRate);
        }

        unset($model);
    }

    /**
     * Get current exchange rates from open exchange rates API
     *
     * @return array
     */
    private function getRates()
    {
        $rates = array();

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Porter (curl)');
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 15);
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($curl, CURLOPT_URL, 'http://openexchangerates.org/api/latest.json?app_id='.config('sunnydayinc.shop.currency.openexchangerate.app_id'));
    
        $response = curl_exec($curl);

        if (! curl_errno($curl) and ! empty($response)) {
            $response = json_decode($response);

            if (isset($response->rates)) {
                $rates = $response->rates;
            }
        }

        unset($response);

        curl_close($curl);

        return $rates;
    }
}
