<?php namespace SunnyDayInc\Shop\CreditMemo\Repository;

abstract class CreditMemo implements CreditMemoInterface
{

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        return $this->createModel()->find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * {@inheritdoc}
     */
    public function createModel()
    {
        $model = $this->model();

        return new $model;
    }

    /**
     * {@inheritdoc}
     */
    public function createModelItem()
    {
        $model = $this->modelItem();

        return new $model;
    }

    /**
     * {@inheritdoc}
     */
    public function createModelComment()
    {
        $model = $this->modelComment();

        return new $model;
    }

}
