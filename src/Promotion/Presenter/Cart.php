<?php namespace SunnyDayInc\Shop\Promotion\Presenter;

class Cart extends Promotion
{

    /**
     * {@inheritdoc}
     */
    protected $publicData = [
    'id'                    => 'id', 
    'name'                  => 'name', 
    'description'           => 'description', 
    'from_date'             => 'from_date', 
    'to_date'               => 'to_date', 
    'simple_action'         => 'simple_action', 
    'discount_amount'       => 'discount_amount', 
    'discount_step'         => 'discount_step', 
    'free_shipping'         => 'is_free_shipping', 
    'apply_to_shipping'     => 'apply_to_shipping', 
    'uses_per_customer'     => 'uses_per_customer', 
    'uses_per_coupon'       => 'uses_per_coupon', 
    'is_coupon'             => 'is_coupon', 
    'sort_order'            => 'sort_order', 
    'stop_rules_processing' => 'stop_rules_processing', 
    'total_used'            => 'total_used', 
    'is_active'             => 'is_active', 
    'rule'                  => 'rule', 
    'action'                => 'action'
    ];

}
