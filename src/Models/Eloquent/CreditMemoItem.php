<?php namespace SunnyDayInc\Shop\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

use SunnyDayInc\Shop\Models\CreditMemoItemInterface;
use SunnyDayInc\Shop\Models\CreditMemoItem as CreditMemoItemModel;

class CreditMemoItem extends Model implements CreditMemoItemInterface
{

    use CreditMemoItemModel;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'credit_memo_item';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'credit_memo_id', 
    'order_item_id', 
    'object_type', 
    'object_id', 
    'sku', 
    'title', 
    'options', 
    'description', 
    'qty', 
    'price', 
    'base_price', 
    'price_incl_tax', 
    'base_price_incl_tax', 
    'discount_amount', 
    'base_discount_amount', 
    'tax_amount', 
    'base_tax_amount', 
    'total', 
    'base_total', 
    'total_incl_tax', 
    'base_total_incl_tax', 
    'currency_code'
    ];

}
