<?php namespace SunnyDayInc\Shop\Address\Repository;

abstract class Address implements AddressInterface
{

    /**
     * {@inheritdoc}
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * {@inheritdoc}
     */
    public function createModel()
    {
        $model = $this->model();

        return new $model;
    }

}
