<?php namespace SunnyDayInc\Shop\Mail\Compiler;

use SunnyDayInc\Shop\Mail\Compiler\HandlerInterface;
use SunnyDayInc\Shop\Support\Shortcode;

class Compiler
{

    /**
     * The data handlers.
     *
     * @var array
     */
    protected $handlers = [];

    /**
     * Register a handler.
     *
     * @param  string                                           $handle
     * @param  \SunnyDayInc\Shop\Mail\Compiler\HandlerInterface $handler
     * @return \SunnyDayInc\Shop\Mail\Compiler
     */
    public function registerHandler($handle, HandlerInterface $handler)
    {
        $this->handlers[$handle] = $handler;

        return $this;
    }

    /**
     * Return specific data compiler.
     *
     * @param  string $handle
     * @return \SunnyDayInc\Shop\Mail\Compiler\HandlerInterface
     */
    public function getHandler($handle)
    {
        if (isset($this->handlers[$handle])) {
            return $this->handlers[$handle];
        }

        return null;
    }

    /**
     * Do compile data.
     *
     * @param  string $data
     * @return string
     */
    public function compile($data)
    {
        if (empty($this->handlers)) {
            return $data;
        }

        return Shortcode::compile($this->handlers, $data);
    }

}
