<?php namespace SunnyDayInc\Shop\Presenter;

use SunnyDayInc\Shop\Support\Collection;

use SunnyDayInc\Shop\Contracts\Currency;

class CollectionPresenter extends Collection
{

    /**
     * The request fields.
     *
     * @var Array
     */
    protected $_fields;

    /**
     * The Currency Manager.
     *
     * @var \SunnyDayInc\Shop\Contracts\Currency
     */
    protected $_currency;

    /**
     * The currency format.
     *
     * @var string
     */
    protected $_currencyFormat = '%s %a';

    /**
     * The currency code target.
     *
     * @var string
     */
    protected $_targetCurrencyCode;

    /**
     * Create the Presenter and store the object we are presenting.
     *
     * @param  array $data
     * @param  array $fields
     * @return void
     */
    public function __construct(Array $data, Array $fields = ['*'])
    {
        parent::__construct($data);

        $this->_fields = $fields;
    }

    /**
     * Define currency manager.
     *
     * @param  \SunnyDayInc\Shop\Contracts\Currency $currency
     * @param  string                               $format
     * @return void
     */
    public function setCurrencyManager(Currency $currency, $format = '%s %a')
    {
        $this->_currency = $currency;
        $this->_currencyFormat = $format;
    }

    /**
     * Define currency code target.
     *
     * @param  string $code
     * @return void
     */
    public function setCurrencyTarget($code)
    {
        $this->_targetCurrencyCode = $code;
    }

    /**
     * Get the collection of items as a plain array.
     *
     * @return array
     */
    public function toArray()
    {
        $fields = $this->_fields;

        $currency = null;
        if (! is_null($this->_currency)) {
            $currency = $this->_currency;
        }

        $currencyFormat = null;
        if (! is_null($this->_currencyFormat)) {
            $currencyFormat = $this->_currencyFormat;
        }

        $targetCurrencyCode = null;
        if (! is_null($this->_targetCurrencyCode)) {
            $targetCurrencyCode = $this->_targetCurrencyCode;
        }

        return array_map(
            function ($value) use ($fields, $currency, $currencyFormat, $targetCurrencyCode) {
                if ($value instanceof Arrayable) {
                    return $value->toArray();
                }

                $presenter = $value->getPresenter($fields);

                if (! is_null($currency)) {
                    $presenter->setCurrencyManager($currency, $currencyFormat);
                }

                if (! is_null($targetCurrencyCode)) {
                    $presenter->setCurrencyTarget($targetCurrencyCode);
                }

                return $presenter;
            }, $this->items
        );
    }

}
