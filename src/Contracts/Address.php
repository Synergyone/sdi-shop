<?php namespace SunnyDayInc\Shop\Contracts;

interface Address
{

    /**
     * Return the repository instance.
     *
     * @return \SunnyDayInc\Shop\Address\Repository\Address
     */
    public function getRepository();

    /**
     * Create a new address.
     *
     * @param  array $data
     * @return \SunnyDayInc\Shop\Contracts\Model|null
     */
    public function create(Array $data);

}
