<?php namespace SunnyDayInc\Shop\Contracts;

interface Presentable
{

    /**
     * Return a created presenter.
     *
     * @param  array $fields
     * @return \SunnyDayInc\Shop\Presenter\Presenter
     */
    public function getPresenter(Array $fields = ['*']);
  
}