<?php namespace SunnyDayInc\Shop\Cart\Repository;

abstract class Cart implements CartInterface
{

    /**
     * The Customer Model.
     *
     * @var string
     */
    protected $modelCustomer;

    /**
     * The Address Model.
     *
     * @var string
     */
    protected $modelAddress;

    /**
     * {@inheritdoc}
     */
    public function setModelCustomer($model)
    {
        $this->modelCustomer = $model;
    }

    /**
     * {@inheritdoc}
     */
    public function setModelAddress($model)
    {
        $this->modelAddress = $model;
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        return $this->createModel()->find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function findByUser($id)
    {
        return $this->createModel()->newQuery()
            ->where('user_id', '=', $id)
            ->first();
    }

    /**
     * {@inheritdoc}
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * {@inheritdoc}
     */
    public function createModel()
    {
        $model = $this->model();

        return new $model;
    }

    /**
     * {@inheritdoc}
     */
    public function createModelItem()
    {
        $model = $this->modelItem();

        return new $model;
    }

    /**
     * {@inheritdoc}
     */
    public function createModelCustomer()
    {
        $model = $this->modelCustomer;

        return new $model;
    }

    /**
     * {@inheritdoc}
     */
    public function createModelAddress()
    {
        $model = $this->modelAddress;

        return new $model;
    }

}
