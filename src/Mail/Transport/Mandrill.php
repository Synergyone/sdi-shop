<?php namespace SunnyDayInc\Shop\Mail\Transport;

use Mandrill as MandrillClient;
use Swift_Mime_Message;

use SunnyDayInc\Shop\Mail\MailError;
use Mandrill_Error;

class Mandrill extends Transport
{
    /**
     * Mandrill client instance.
     *
     * @var \Mandrill
     */
    protected $client;

    /**
     * Create a new Mandrill transport instance.
     *
     * @param  string $key
     * @return void
     */
    public function __construct($key)
    {
        $this->client = $this->getClient($key);
    }

    /**
     * {@inheritdoc}
     */
    public function send(Swift_Mime_Message $message, &$failedRecipients = null)
    {
        $this->beforeSendPerformed($message);

        try {
            $this->client->messages->sendRaw($message->toString());
        } catch (Mandrill_Error $e) {
            throw new MailError($e->getMessage());
        }

        return true;
    }

    /**
     * Get the Mandrill client instance.
     *
     * @param  string $key
     * @return \Mandrill
     */
    private function getClient($key)
    {
        return new MandrillClient($key);
    }
}
