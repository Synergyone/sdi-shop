<?php namespace SunnyDayInc\Shop\CreditMemo\Formatter;

interface FormatterInterface
{

    /**
     * Return the formatted value.
     *
     * @return string
     */
    public function compile();

}
