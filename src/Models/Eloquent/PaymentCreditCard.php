<?php namespace SunnyDayInc\Shop\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

use SunnyDayInc\Shop\Models\PaymentCreditCardInterface;

class PaymentCreditCard extends Model implements PaymentCreditCardInterface
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'payment_cc';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'payment_id', 
    'cc_trans_id', 
    'cc_type', 
    'cc_owner', 
    'cc_exp_month', 
    'cc_exp_year', 
    'cc_last4', 
    'cc_number_enc', 
    'cc_debug_request_body', 
    'cc_secure_verify', 
    'cc_approval', 
    'cc_status_description', 
    'cc_debug_response_serialized', 
    'cc_cid_status', 
    'cc_status', 
    'cc_avs_status'
    ];

}
