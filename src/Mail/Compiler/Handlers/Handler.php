<?php namespace SunnyDayInc\Shop\Mail\Compiler\Handlers;

use SunnyDayInc\Shop\Mail\Compiler\HandlerInterface;

class Handler implements HandlerInterface
{
 
    /**
     * The value.
     *
     * @var mixed
     */
    protected $val;

    /**
     * {@inheritdoc}
     */
    public function set($val)
    {
        $this->val = $val;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function get($attributes, $matches, $tag)
    {
        return $this->val;
    }

}
