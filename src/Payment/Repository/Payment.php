<?php namespace SunnyDayInc\Shop\Payment\Repository;

abstract class Payment implements PaymentInterface
{

    /**
     * {@inheritdoc}
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * {@inheritdoc}
     */
    public function createModel()
    {
        $model = $this->model();

        return new $model;
    }

    /**
     * {@inheritdoc}
     */
    public function createModelTransaction()
    {
        $model = $this->modelTransaction();

        return new $model;
    }

    /**
     * {@inheritdoc}
     */
    public function createModelCreditCard()
    {
        $model = $this->modelCreditCard();

        return new $model;
    }

}
