<?php namespace SunnyDayInc\Shop\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

use SunnyDayInc\Shop\Models\CreditMemoCommentInterface;

class CreditMemoComment extends Model implements CreditMemoCommentInterface
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'credit_memo_comment';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'credit_memo_id', 
    'comment', 
    'is_customer_notified', 
    'is_visible_on_front'
    ];

}
