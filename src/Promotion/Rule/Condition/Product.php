<?php namespace SunnyDayInc\Shop\Promotion\Rule\Condition;

use SunnyDayInc\Shop\Promotion\Rule\Rule;

class Product extends Rule
{

    const ATTR_CATEGORY_IDS = 'category_ids';
    const ATTR_SKU          = 'sku';

    /**
     * Create a new instance.
     *
     * @param  array $data
     * @return void
     */
    public function __construct(Array $data)
    {
        parent::__construct($data);

        $this->attributes = [self::ATTR_CATEGORY_IDS, self::ATTR_SKU];
        $this->operatorTypeByAttribute = [
        self::ATTR_CATEGORY_IDS => 'category', 
        self::ATTR_SKU          => 'category'
        ];
        $this->arrayInputTypes[] = 'category';
    }

    /**
     * Check if applicable.
     *
     * @param  mixed $item
     * @return boolean
     */
    public function isApplicable($item)
    {
        $isApplicable = false;

        // Validate based on child rules result
        switch ($this->attribute) {
        case self::ATTR_CATEGORY_IDS:

            $isApplicable = $this->validateValueByOperator($item->getCategoryIds());

            break;
      
        case self::ATTR_SKU:

            $isApplicable = $this->validateValueByOperator([$item->getSku()]);

            break;
        }

        return $isApplicable;
    }

    /**
     * Return product IDs available in the rule.
     *
     * @return array
     */
    public function getProductIds()
    {
        return [];
    }

}
