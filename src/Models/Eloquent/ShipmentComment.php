<?php namespace SunnyDayInc\Shop\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

use SunnyDayInc\Shop\Models\ShipmentCommentInterface;

class ShipmentComment extends Model implements ShipmentCommentInterface
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shipment_comment';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'shipment_id', 
    'comment', 
    'is_customer_notified', 
    'is_visible_on_front'
    ];

}
