<?php namespace SunnyDayInc\Shop\ServiceProvider\Lumen;

use Illuminate\Support\ServiceProvider;

use SunnyDayInc\Shop\Translation\Translator;

class Translation extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->configure('locale');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            'SunnyDayInc\Shop\Contracts\Translator', function ($app) {
                $code = $app['config']->get('locale.code');
                $path = $app['config']->get('locale.path');

                return new Translator($code, $path);
            }
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['SunnyDayInc\Shop\Contracts\Translator'];
    }
}
