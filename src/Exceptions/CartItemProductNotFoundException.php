<?php

namespace SunnyDayInc\Shop\Exceptions;

class CartItemProductNotFoundException extends CartItemProductException
{

    /**
     * The exception type
     */
    public $errorType = 'not_found';

}
