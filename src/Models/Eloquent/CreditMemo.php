<?php namespace SunnyDayInc\Shop\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

use SunnyDayInc\Shop\Models\CreditMemoInterface;
use SunnyDayInc\Shop\Models\CreditMemo as CreditMemoModel;

class CreditMemo extends Model implements CreditMemoInterface
{

    use CreditMemoModel;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'credit_memo';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'object_type', 
    'object_id', 
    'branch_id', 
    'order_id', 
    'invoice_id', 
    'billing_address_id', 
    'identifier', 
    'code', 
    'adjustment_positive', 
    'base_adjustment_positive', 
    'adjustment_negative', 
    'base_adjustment_negative', 
    'discount_amount', 
    'base_discount_amount', 
    'tax_amount', 
    'base_tax_amount', 
    'shipping_amount', 
    'base_shipping_amount', 
    'shipping_tax_amount', 
    'base_shipping_tax_amount', 
    'shipping_incl_tax', 
    'base_shipping_incl_tax', 
    'subtotal', 
    'base_subtotal', 
    'subtotal_incl_tax', 
    'base_subtotal_incl_tax', 
    'grand_total', 
    'base_grand_total', 
    'customer_balance_amount', 
    'base_customer_balance_amount', 
    'status', 
    'order_currency_code', 
    'base_currency_code', 
    'global_currency_code', 
    'base_to_order_rate', 
    'base_to_global_rate', 
    'note'
    ];

}
