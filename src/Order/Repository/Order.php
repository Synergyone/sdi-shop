<?php namespace SunnyDayInc\Shop\Order\Repository;

abstract class Order implements OrderInterface
{

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        return $this->createModel()->find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function findByIdentifier($identifier)
    {
        return $this->createModel()->newQuery()
            ->where('identifier', '=', $identifier)
            ->first();
    }

    /**
     * {@inheritdoc}
     */
    public function findByObject($objectType, $objectId, $id)
    {
        return $this->createModel()->newQuery()
            ->where('object_type', '=', $objectType)
            ->where('object_id', '=', $objectId)
            ->where('id', '=', $id)
            ->first();
    }

    /**
     * {@inheritdoc}
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * {@inheritdoc}
     */
    public function createModel()
    {
        $model = $this->model();

        return new $model;
    }

    /**
     * {@inheritdoc}
     */
    public function createModelItem()
    {
        $model = $this->modelItem();

        return new $model;
    }

    /**
     * {@inheritdoc}
     */
    public function createModelAddress()
    {
        $model = $this->modelAddress();

        return new $model;
    }

    /**
     * {@inheritdoc}
     */
    public function createModelStatusHistory()
    {
        $model = $this->modelStatusHistory();

        return new $model;
    }

}
