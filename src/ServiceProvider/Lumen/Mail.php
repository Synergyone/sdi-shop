<?php namespace SunnyDayInc\Shop\ServiceProvider\Lumen;

use Illuminate\Support\ServiceProvider;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Mail\Transport\LogTransport;

use SunnyDayInc\Shop\Mail\Mailer;
use SunnyDayInc\Shop\Mail\Transport\Mandrill;
use SunnyDayInc\Shop\Mail\Repository\EloquentTemplate;
use SunnyDayInc\Shop\Mail\Loader\Database;
use SunnyDayInc\Shop\Mail\Compiler\Manager;

use Swift_Mailer;
use Swift_SmtpTransport;
use Swift_MailTransport;
use Swift_SendmailTransport;

class Mail extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->configure('mail');
        $this->app->configure('services');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            'SunnyDayInc\Shop\Contracts\Mailer', function ($app) {
                $this->registerSwiftMailer();
                $this->registerTemplateLoader();
      
                $mailer = new Mailer($app['mailer.template.loader'],  $app['swift.mailer']);

                // If a "from" address is set, we will set it on the mailer so that all mail
                // messages sent by the applications will utilize the same "from" address
                // on each one, which makes the developer's life a lot more convenient.
                $from = $app['config']->get('mail.from');

                if (is_array($from) && isset($from['address'])) {
                    $mailer->alwaysFrom($from['address'], $from['name']);
                }

                return $mailer;
            }
        );
    }

    /**
     * Register the Swift Mailer instance.
     *
     * @return void
     */
    public function registerSwiftMailer()
    {
        $this->registerTransport();

        // Once we have the transporter registered, we will register the actual Swift
        // mailer instance, passing in the transport instances, which allows us to
        // override this transporter instances during app start-up if necessary.
        $this->app['swift.mailer'] = $this->app->share(
            function ($app) {
                return new Swift_Mailer($app['swift.transport']);
            }
        );
    }

    /**
     * Register the Swift Transport instance.
     *
     * @return void
     */
    protected function registerTransport()
    {
        $this->app['swift.transport'] = $this->app->share(
            function ($app) {
                $config = $app['config']->get('mail');

                $transport = null;

                switch ($config['driver']) {
                case 'mandrill':
                    $key = $app['config']->get('services.mandrill.secret');
                    $transport = new Mandrill($key);
                    break;

                case 'smtp':
                    // The Swift SMTP transport instance will allow us to use any SMTP backend
                    // for delivering mail such as Sendgrid, Amazon SES, or a custom server
                    // a developer has available. We will just pass this configured host.
                    $transport = Swift_SmtpTransport::newInstance(
                        $config['host'], $config['port']
                    );

                    if (isset($config['encryption'])) {
                        $transport->setEncryption($config['encryption']);
                    }

                    // Once we have the transport we will check for the presence of a username
                    // and password. If we have it we will set the credentials on the Swift
                    // transporter instance so that we'll properly authenticate delivery.
                    if (isset($config['username'])) {
                        $transport->setUsername($config['username']);

                        $transport->setPassword($config['password']);
                    }
                    break;

                case 'mail':
                    $transport = Swift_MailTransport::newInstance();
                    break;

                case 'sendmail':
                    $transport = Swift_SendmailTransport::newInstance($config['sendmail']);
                    break;

                case 'log':
                    $transport = new LogTransport($this->app->make('Psr\Log\LoggerInterface'));
                    break;
                }

                unset($config);

                return $transport;
            }
        );
    }

    /**
     * Register the template loader.
     *
     * @return void
     */
    public function registerTemplateLoader()
    {
        $this->app['mailer.template.loader'] = $this->app->share(
            function ($app) {
                $this->registerTemplateCompiler();

                $config = $app['config']->get('mail.template');

                switch ($config['loader']) {
                case 'database':
                    $repository = new EloquentTemplate($config['table']);
                    $loader = new Database($repository);
                    break;
                }

                $loader->setCompiler($app['mailer.template.compiler']);

                return $loader;
            }
        );
    }

    /**
     * Register the template compiler.
     *
     * @return void
     */
    public function registerTemplateCompiler()
    {
        $this->app['mailer.template.compiler'] = $this->app->share(
            function ($app) {
                $compiler = new Manager();

                // Register defined handlers
                $shortcodes = $app['config']->get('mail.template.shortcodes');

                if (! empty($shortcodes)) {
                    foreach ($shortcodes as $handle => $handler) {
                        $compiler->registerHandler($handle, $handler);
                    }
                }

                return $compiler;
            }
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
        'SunnyDayInc\Shop\Contracts\Mailer', 
        'swift.mailer', 
        'swift.transport', 
        'mailer.template.loader', 
        'mailer.template.compiler'
        ];
    }
}
