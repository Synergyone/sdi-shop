<?php namespace SunnyDayInc\Shop\Lock\Repository;

class EloquentLock implements Lock
{

    /**
     * The model object.
     *
     * @var \SunnyDayInc\Shop\Contracts\Model
     */
    protected $model;

    /**
     * Create a new instance.
     *
     * @param  string $tableName
     * @return void
     */
    public function __construct($tableName)
    {
        $this->createModel();

        $this->model->setTable($tableName);
    }

    /**
     * Return all.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all()
    {
        return $this->model->all();
    }

    /**
     * {@inheritdoc}
     */
    public function model()
    {
        return '\SunnyDayInc\Shop\Models\Eloquent\Group';
    }

    /**
     * Return the model object creation.
     *
     * @return \SunnyDayInc\Shop\Models\Eloquent\Group
     */
    private function createModel()
    {
        if (! is_null($this->model)) {
            return $this->model;
        }

        $model = $this->model();

        return $this->model = new $model;
    }

}
