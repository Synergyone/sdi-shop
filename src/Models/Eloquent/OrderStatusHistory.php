<?php namespace SunnyDayInc\Shop\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

use SunnyDayInc\Shop\Models\OrderStatusHistoryInterface;

class OrderStatusHistory extends Model implements OrderStatusHistoryInterface
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'order_status_history';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'order_id', 
    'entity_type', 
    'comment', 
    'status', 
    'is_customer_notified', 
    'is_visible_on_front'
    ];

}
