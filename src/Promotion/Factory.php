<?php namespace SunnyDayInc\Shop\Promotion;

use SunnyDayInc\Shop\Promotion\Repository\Promotion as PromotionRepository;
use SunnyDayInc\Shop\Promotion\Rule\RuleInterface;

use SunnyDayInc\Shop\Support\Collection;

use SunnyDayInc\Shop\Models\PromotionInterface;
use SunnyDayInc\Shop\Models\PromotionCouponInterface;

class Factory implements FactoryInterface
{

    /**
     * The log context.
     *
     * @var string
     */
    const LOG_CONTEXT = 'promotion.factory';

    /**
     * The repository instance.
     *
     * @var \SunnyDayInc\Shop\Promotion\Repository\Promotion
     */
    protected $repository;

    /**
     * The promotion model instance.
     *
     * @var \SunnyDayInc\Shop\Models\PromotionInterface
     */
    protected $promotion;

    /**
     * The promotion coupons.
     *
     * @var \Collection|\SunnyDayInc\Shop\Models\PromotionCouponInterface[]
     */
    protected $coupons;

    /**
     * The promotion coupons to remove.
     *
     * @var \Collection|\SunnyDayInc\Shop\Models\PromotionCouponInterface[]
     */
    protected $removeCoupons;

    /**
     * The promotion products.
     *
     * @var \Collection|\SunnyDayInc\Shop\Models\PromotionProductInterface[]
     */
    protected $products;

    /**
     * Create a new instance.
     *
     * @param  \SunnyDayInc\Shop\Promotion\Repository\Promotion $repository
     * @param  array                                            $data
     * @return void
     */
    public function __construct(PromotionRepository $repository, Array $data = [])
    {
        $this->repository = $repository;
    
        $this->coupons = new Collection();
        $this->removeCoupons = new Collection();
        $this->products = new Collection();
    
        // Create the promotion model and fill the default data
        if (! empty($data)) {
            $this->promotion = $this->repository->createModel();
            $this->promotion->fill($data);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setLogger($log)
    {
        $this->log = $log;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setPromotion(PromotionInterface $promotion)
    {
        $this->promotion = $promotion;

        $this->promotion->buildCondition();
        $this->promotion->buildAction();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPromotion()
    {
        return $this->promotion;
    }

    /**
     * Set the condition.
     *
     * @param  \SunnyDayInc\Shop\Promotion\Rule\RuleInterface $condition
     * @return void
     */
    public function setCondition(RuleInterface $condition)
    {
        $this->promotion->condition = $condition;
    }

    /**
     * Get the condition.
     *
     * @return \SunnyDayInc\Shop\Promotion\Rule\RuleInterface|null
     */
    public function getCondition()
    {
        return $this->promotion->condition;
    }

    /**
     * Set the action.
     *
     * @param  \SunnyDayInc\Shop\Promotion\Rule\RuleInterface $action
     * @return void
     */
    public function setAction(RuleInterface $action)
    {
        $this->promotion->action = $action;
    }

    /**
     * Get the action.
     *
     * @return \SunnyDayInc\Shop\Promotion\Rule\RuleInterface|null
     */
    public function getAction()
    {
        return $this->promotion->action;
    }

    /**
     * {@inheritdoc}
     */
    public function getCoupons()
    {
        if (is_null($this->promotion)) {
            return $this->coupons;
        }

        $promotionId = (isset($this->promotion->id)) ? $this->promotion->id : 0;

        // Because the promotion is not yet saved
        // we cannot identify any item by it's ID
        if (empty($promotionId)) {
            return $this->coupons;
        }

        // Fetch all coupons from repository
        if ($this->coupons->isEmpty()) {
            $coupons = $this->repository->findCouponsByPromotion($promotionId);

            if (! $coupons->isEmpty()) {
                foreach ($coupons as $item) {
                    $this->coupons->put($item->id, $item);
                }
            }

            unset($coupons);
        }

        return $this->coupons;
    }

    /**
     * {@inheritdoc}
     */
    public function getCoupon($id)
    {
        if (is_null($this->promotion)) {
            return null;
        }

        $promotionId = (isset($this->promotion->id)) ? $this->promotion->id : 0;

        // Because the promotion is not yet saved
        // we cannot identify any coupon by it's ID
        if (empty($promotionId)) {
            return null;
        }

        if (! $this->coupons->isEmpty()) {
            $item = $this->coupons->get($id);

            if ($item instanceOf PromotionCouponInterface) {
                return $item;
            }
        }

        $item = $this->repository->findCouponByPromotionAndId($promotionId, $id);

        if (! ($item instanceOf PromotionCouponInterface)) {
            return null;
        }

        $this->coupons->put($item->id, $item);

        return $item;
    }

    /**
     * {@inheritdoc}
     */
    public function addCoupon(PromotionCouponInterface $item)
    {
        // Set the promotion ID if available
        if (! empty($this->promotion->id)) {
            $item->promotion_id = $this->promotion->id;
        }

        // Add item to collection
        $this->coupons->push($item);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function updateCoupon(PromotionCouponInterface $item)
    {
        if (empty($this->promotion->id)) {
            return $this;
        }

        if (! $this->coupons->isEmpty()) {
            foreach ($this->coupons as $index => $coupon) {
                if ($coupon->id == $item->id) {
                    $this->coupons[$index] = $item;
                    break;
                }
            }
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function removeCoupon(PromotionCouponInterface $item)
    {
        if (empty($this->promotion->id)) {
            return $this;
        }

        if (! $this->coupons->isEmpty()) {
            foreach ($this->coupons as $index => $coupon) {
                if ($coupon->id == $item->id) {
                    $this->removeCoupons->push($item);
                    unset($this->coupons[$index]);
                    break;
                }
            }
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function createCoupon()
    {
        return $this->repository->createModelCoupon();
    }

    /**
     * {@inheritdoc}
     */
    public function update(Array $data)
    {
        if (! empty($data)) {
            foreach ($data as $key => $value) {
                $this->promotion->$key = $value;
            }
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function save()
    {
        $connection = $this->promotion->getConnection();

        // Initiate transaction
        $connection->beginTransaction();

        $isSaved = true;

        // Aggregate the condition products for catalogue promo if available
        if ($this->promotion->isActive() && $this->promotion->getType() == Promotion::TYPE_CATALOG && $this->promotion->condition instanceOf RuleInterface) {
            $this->aggregateProductRule($this->promotion->condition);
        }

        // Save the promotion
        try {
            $isSaved = $this->promotion->save();
        } catch (\ErrorException $e) {
            $this->createLog('error', $e);

            $isSaved = false;
        }

        if (! $isSaved) {
            // Rollback the transaction
            $connection->rollBack();

            return false;
        }

        // Get the increment promotion ID
        $promotionId = $this->promotion->id;

        // Save all coupons
        if (! $this->saveCoupons()) {
            // Rollback the transaction
            $connection->rollBack();

            return false;
        }

        // Remove all coupons
        if (! $this->removeCoupons()) {
            // Rollback the transaction
            $connection->rollBack();

            return false;
        }

        // Save all product rules
        if (! $this->saveProductRules()) {
            // Rollback the transaction
            $connection->rollBack();

            return false;
        }

        // Remove all product rules if current promotion is not active
        if (! $this->promotion->isActive() && $this->promotion->isExist()) {
            $this->removeProductRules();
        }

        // Commit the transaction
        $connection->commit();

        return true;
    }

    /**
     * Save all coupons.
     *
     * @return boolean
     */
    private function saveCoupons()
    {
        $isAllSaved = true;

        if ($this->coupons instanceOf Collection) {
            foreach ($this->coupons as $key => $item) {
                $isSaved = true;

                // Assign promotion ID
                $this->coupons[$key]->promotion_id = $this->promotion->id;

                try {
                    $isSaved = $this->coupons[$key]->save();
                } catch (\ErrorException $e) {
                    $this->createLog('error', $e);

                    $isSaved = false;
                }

                if (! $isSaved) {
                    $isAllSaved = false;
                    break;
                }
            }
        }

        return $isAllSaved;
    }

    /**
     * Remove all coupons.
     *
     * @return boolean
     */
    private function removeCoupons()
    {
        $isAllRemoved = true;

        if ($this->removeCoupons instanceOf Collection) {
            foreach ($this->removeCoupons as $item) {
                if (! $item->delete()) {
                    $isAllRemoved = false;
                    break;
                }
            }
        }

        return $isAllRemoved;
    }

    /**
     * Save all product rules.
     *
     * @return boolean
     */
    private function saveProductRules()
    {
        $isAllSaved = true;

        if ($this->products instanceOf Collection) {
            foreach ($this->products as $key => $item) {
                $isSaved = true;

                // Assign promotion ID
                $this->products[$key]->promotion_id = $this->promotion->id;

                try {
                    $isSaved = $this->products[$key]->save();
                } catch (\ErrorException $e) {
                    $this->createLog('error', $e);

                    $isSaved = false;
                }

                if (! $isSaved) {
                    $isAllSaved = false;
                    break;
                }
            }
        }

        return $isAllSaved;
    }

    /**
     * Remove all product rules.
     *
     * @return void
     */
    private function removeProductRules()
    {
        $this->promotion->deleteProducts();
    }

    /**
     * {@inheritdoc}
     */
    public function delete()
    {
        return $this->promotion->delete();
    }

    /**
     * Return a created presenter.
     *
     * @return \SunnyDayInc\Shop\Presenter\Presenter
     */
    public function getPresenter()
    {
        return $this->promotion->getPresenter();
    }

    /**
     * Aggregate product rule.
     *
     * @param  \SunnyDayInc\Shop\Promotion\Rule\RuleInterface $action
     * @return void
     */
    private function aggregateProductRule(RuleInterface $rule)
    {
        if ($rule instanceOf \SunnyDayInc\Shop\Promotion\Rule\Condition\Product) {
            $productIds = $rule->getProductIds();

            if (! empty($productIds)) {
                foreach ($productIds as $productId) {
                    $item = $this->repository->createModelProduct();

                    $item->from_time       = strtotime($this->promotion->from_date);
                    $item->to_time         = strtotime($this->promotion->to_date);
                    $item->product_id      = $productId;
                    $item->action_operator = $this->promotion->simple_action;
                    $item->action_amount   = $this->promotion->discount_amount;
                    $item->sort_order      = $this->promotion->sort_order;

                    // Set the promotion ID if available
                    if (! empty($this->promotion->id)) {
                        $item->promotion_id = $this->promotion->id;
                    }

                    // Add item to collection
                    $this->products->push($item);
                }
            }
        }

        if ($rule->hasChildren()) {
            $children = $rule->getChildren();

            foreach ($children as $child) {
                $this->aggregateProductRule($child);
            }

            unset($children);
        }
    }

    /**
     * Create a new log.
     *
     * @param  string $level
     * @param  mixed  $message
     * @param  Array  $data
     * @return void
     */
    private function createLog($level, $message, Array $data = [])
    {
        if (! is_null($this->log)) {
            $data['context'] = self::LOG_CONTEXT;

            if ($message instanceOf \ErrorException) {
                $this->log->$level($message->getCode().': '.$message->getMessage()."\n".$message->getTraceAsString(), $data);
            } else {
                $this->log->$level($message, $data);
            }
        }
    }

}
