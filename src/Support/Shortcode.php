<?php namespace SunnyDayInc\Shop\Support;

class Shortcode
{

    /**
     * Do compile data.
     *
     * @param  array  $shortcodes
     * @param  string $data
     * @return string
     */
    public static function compile(Array $shortcodes, $data)
    {
        if (empty($shortcodes)) {
            return $data;
        }

        $pattern = self::getShortcodeRegex(array_keys($shortcodes));
        $data    = preg_replace_callback(
            "/$pattern/s", function ($matches) use ($shortcodes) {
                return Shortcode::doShortcodeTag($shortcodes, $matches);
            }, $data
        );
    
        return $data;
    }

    /**
     * Execute each shortcode handler.
     *
     * @param  array $shortcodes
     * @param  array $matches    Regular expression match array
     * @return mixed False on failure.
     */
    public static function doShortcodeTag(Array $shortcodes, Array $matches)
    {
        // allow [[foo]] syntax for escaping a tag
        if ($matches[1] == '[' && $matches[6] == ']' ) {
            return substr($matches[0], 1, -1);
        }

        $tag  = $matches[2];
        $attr = self::parseShortcodeAttributes($matches[3]);

        if (isset($matches[5]) ) {
            // enclosing tag - extra parameter
            if (is_array($shortcodes[$tag])) {
                return $matches[1].call_user_func($shortcodes[$tag], $attr, $matches[5], $tag).$matches[6];
            } else {
                return $matches[1].$shortcodes[$tag]->get($attr, $matches[5], $tag).$matches[6];
            }
        } else {
            // self-closing tag
            if (is_array($shortcodes[$tag])) {
                return $matches[1].call_user_func($shortcodes[$tag], $attr, null, $tag).$matches[6];
            } else {
                return $matches[1].$shortcodes[$tag]->get($attr, null, $tag).$matches[6];
            }
        }
    }

    /**
     * Retrieve all attributes from the shortcodes tag.
     *
     * The attributes list has the attribute name as the key and the value of the
     * attribute as the value in the key/value pair. This allows for easier
     * retrieval of the attributes, since all attributes have to be known.
     *
     * @param  string $text
     * @return array List of attributes and their value.
     */
    public static function parseShortcodeAttributes($text)
    {
        $atts    = array();
        $pattern = '/(\w+)\s*=\s*"([^"]*)"(?:\s|$)|(\w+)\s*=\s*\'([^\']*)\'(?:\s|$)|(\w+)\s*=\s*([^\s\'"]+)(?:\s|$)|"([^"]*)"(?:\s|$)|(\S+)(?:\s|$)/';
        $text    = preg_replace("/[\x{00a0}\x{200b}]+/u", " ", $text);

        if (preg_match_all($pattern, $text, $match, PREG_SET_ORDER)) {
            foreach ($match as $m) {
                if (! empty($m[1])) {
                    $atts[strtolower($m[1])] = stripcslashes($m[2]); 
                }
                elseif (! empty($m[3]))
                $atts[strtolower($m[3])] = stripcslashes($m[4]);
                elseif (! empty($m[5]))
                $atts[strtolower($m[5])] = stripcslashes($m[6]);
                elseif (isset($m[7]) && strlen($m[7]))
                $atts[] = stripcslashes($m[7]);
                elseif (isset($m[8]))
                $atts[] = stripcslashes($m[8]);
            }
        } else {
            $atts = ltrim($text);
        }

        return $atts;
    }

    /**
     * Retrieve the shortcode regular expression for searching.
     *
     * The regular expression combines the shortcode tags in the regular expression
     * in a regex class.
     *
     * The regular expression contains 6 different sub matches to help with parsing.
     *
     * 1 - An extra [ to allow for escaping shortcodes with double [[]]
     * 2 - The shortcode name
     * 3 - The shortcode argument list
     * 4 - The self closing /
     * 5 - The content of a shortcode when it wraps some content.
     * 6 - An extra ] to allow for escaping shortcodes with double [[]]
     *
     * @return string The shortcode search regular expression
     */
    public static function getShortcodeRegex($tags)
    {
        $tagRegexp = join('|', array_map('preg_quote', $tags));

        return
        '\\['                              // Opening bracket
        . '(\\[?)'                           // 1: Optional second opening bracket for escaping shortcodes: [[tag]]
        . "($tagRegexp)"                     // 2: Shortcode name
        . '(?![\\w-])'                       // Not followed by word character or hyphen
        . '('                                // 3: Unroll the loop: Inside the opening shortcode tag
        .     '[^\\]\\/]*'                   // Not a closing bracket or forward slash
        .     '(?:'
        .         '\\/(?!\\])'               // A forward slash not followed by a closing bracket
        .         '[^\\]\\/]*'               // Not a closing bracket or forward slash
        .     ')*?'
        . ')'
        . '(?:'
        .     '(\\/)'                        // 4: Self closing tag ...
        .     '\\]'                          // ... and closing bracket
        . '|'
        .     '\\]'                          // Closing bracket
        .     '(?:'
        .         '('                        // 5: Unroll the loop: Optionally, anything between the opening and closing shortcode tags
        .             '[^\\[]*+'             // Not an opening bracket
        .             '(?:'
        .                 '\\[(?!\\/\\2\\])' // An opening bracket not followed by the closing shortcode tag
        .                 '[^\\[]*+'         // Not an opening bracket
        .             ')*+'
        .         ')'
        .         '\\[\\/\\2\\]'             // Closing shortcode tag
        .     ')?'
        . ')'
        . '(\\]?)';                          // 6: Optional second closing brocket for escaping shortcodes: [[tag]]
    }

}
