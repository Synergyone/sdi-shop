<?php namespace SunnyDayInc\Shop\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

use SunnyDayInc\Shop\Models\ShipmentInterface;
use SunnyDayInc\Shop\Models\Shipment as ShipmentModel;

class Shipment extends Model implements ShipmentInterface
{

    use ShipmentModel;
  
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shipment';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'order_id', 
    'account_id', 
    'branch_id', 
    'customer_id', 
    'shipping_address_id', 
    'billing_address_id', 
    'identifier', 
    'code', 
    'qty', 
    'status'
    ];

}
