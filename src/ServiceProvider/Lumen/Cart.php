<?php namespace SunnyDayInc\Shop\ServiceProvider\Lumen;

use Illuminate\Support\ServiceProvider;

use SunnyDayInc\Shop\Cart\Cart as CartContainer;
use SunnyDayInc\Shop\Cart\Repository\EloquentCart;

class Cart extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerRepository();

        $this->app->singleton(
            'SunnyDayInc\Shop\Contracts\Cart', function ($app) {
                return new CartContainer($app['SunnyDayInc\Shop\Cart\Repository\CartInterface']);
            }
        );
    }

    /**
     * Register the repository instance.
     *
     * @return void
     */
    private function registerRepository()
    {
        $this->app->singleton(
            'SunnyDayInc\Shop\Cart\Repository\CartInterface', function ($app) {
                return new EloquentCart();
            }
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['SunnyDayInc\Shop\Cart\Repository\CartInterface', 'SunnyDayInc\Shop\Contracts\Cart'];
    }
}
