<?php namespace SunnyDayInc\Shop\Order\Repository;

interface OrderInterface
{

    /**
     * Return a order by it's ID.
     *
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Contracts\Model|null
     */
    public function find($id);

    /**
     * Return a order by it's identifier.
     *
     * @param  string $identifier
     * @return \SunnyDayInc\Shop\Contracts\Model|null
     */
    public function findByIdentifier($identifier);

    /**
     * Return a order by object type, object ID, and it's ID.
     *
     * @param  string  $objectType
     * @param  integer $objectId
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Contracts\Model|null
     */
    public function findByObject($objectType, $objectId, $id);

    /**
     * Return orders.
     *
     * @param  array   $where
     * @param  array   $column
     * @param  integer $page
     * @param  integer $limit
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findBy(
        Array $where = [], 
        Array $column = ['*'], 
        $page = 1, 
        $limit = 10, 
        $order = 'DESC', 
        $orderBy = 'created_at'
    );

    /**
     * Return the current total row in previous query.
     *
     * @return string
     */
    public function getTotal();

    /**
     * Return the order items by order ID.
     *
     * @param  integer $orderId
     * @return \Collection|\SunnyDayInc\Shop\Models\OrderItemInterface[]
     */
    public function findItemsByOrder($orderId);

    /**
     * Return the order item by order ID and it's ID.
     *
     * @param  integer $orderId
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Models\OrderItemInterface|null
     */
    public function findItemByOrderAndId($orderId, $id);

    /**
     * Return the address by order ID and type.
     *
     * @param  integer $orderId
     * @param  string  $type
     * @return \SunnyDayInc\Shop\Models\OrderAddressInterface|null
     */
    public function findAddressByOrderAndType($orderId, $type);

    /**
     * Return the Model class name
     *
     * @return string
     */
    public function model();

    /**
     * Return the Model Item class name
     *
     * @return string
     */
    public function modelItem();

    /**
     * Return the Model Address class name
     *
     * @return string
     */
    public function modelAddress();

    /**
     * Return the Model Status History class name
     *
     * @return string
     */
    public function modelStatusHistory();

    /**
     * Return the model object creation.
     *
     * @return \SunnyDayInc\Shop\Models\OrderInterface
     */
    public function createModel();

    /**
     * Return the model item object creation.
     *
     * @return \SunnyDayInc\Shop\Models\OrderItemInterface
     */
    public function createModelItem();

    /**
     * Return the model address object creation.
     *
     * @return \SunnyDayInc\Shop\Models\OrderAddressInterface
     */
    public function createModelAddress();

    /**
     * Return the model status history object creation.
     *
     * @return \SunnyDayInc\Shop\Models\OrderStatusHistoryInterface
     */
    public function createModelStatusHistory();

}
