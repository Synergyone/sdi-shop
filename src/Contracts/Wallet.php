<?php namespace SunnyDayInc\Shop\Contracts;

use Illuminate\Contracts\Logging\Log;

interface Wallet
{

    /**
     * Set the logger instance.
     *
     * @param  \Illuminate\Contracts\Logging\Log $repository
     * @return void
     */
    public function setLogger(Log $log);

    /**
     * Return the repository instance.
     *
     * @return \SunnyDayInc\Shop\Wallet\Repository\Wallet
     */
    public function getRepository();

    /**
     * Create a new wallet.
     *
     * @param  array $data
     * @return \SunnyDayInc\Shop\Wallet\FactoryInterface
     */
    public function create(Array $data = []);

    /**
     * Find a wallet by ID and return the factory instance.
     *
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Wallet\FactoryInterface
     */
    public function find($id, $objectType = '', $objectId = '');

}
