<?php namespace SunnyDayInc\Shop\Cart\Presenter;

use SunnyDayInc\Shop\Support\Collection;
use SunnyDayInc\Shop\Contracts\Currency;
use SunnyDayInc\Shop\Promotion\Promotion as PromotionContainer;
use SunnyDayInc\Shop\Cart\Repository\ProductInterface as ProductRepository;
use SunnyDayInc\Shop\Presenter\Presenter;
use SunnyDayInc\Shop\Models\CartItemInterface;

class Item extends Presenter
{

    /**
     * The Cart model.
     *
     * @var Cart
     */
    protected $_model;

    /**
     * The product repository instance.
     *
     * @var \SunnyDayInc\Shop\Cart\Repository\ProductInterface
     */
    protected $_productRepository;

    /**
     * The Currency Manager.
     *
     * @var \SunnyDayInc\Shop\Contracts\Currency
     */
    protected $_currency;

    /**
     * The currency format.
     *
     * @var string
     */
    protected $_currencyFormat = '%s %a';

    /**
     * The currency code target.
     *
     * @var string
     */
    protected $_targetCurrencyCode;

    /**
     * The available promotions.
     *
     * @var \Collection
     */
    protected $_promotions;

    /**
     * The applied promotions.
     *
     * @var Array
     */
    protected $_appliedPromotions;

    /**
     * The current item product.
     *
     * @var mixed
     */
    protected $_product;

    /**
     * The subtotal.
     *
     * @var float
     */
    protected $_subtotal;

    /**
     * The discount.
     *
     * @var float
     */
    protected $_discount;

    /**
     * The discount percentage.
     *
     * @var float
     */
    protected $_discountPercentage;

    /**
     * The total.
     *
     * @var float
     */
    protected $_total;

    /**
     * {@inheritdoc}
     */
    protected $publicData = [
    'id'                  => 'id',
    'object_type'         => 'object_type', 
    'object_id'           => 'object_id', 
    'branch_id'           => 'branch_id', 
    'product'             => 'product', 
    'qty'                 => 'qty', 
    'subtotal'            => 'subtotal', 
    'subtotal_display'    => 'subtotal_display', 
    'discount'            => 'discount', 
    'discount_display'    => 'discount_display', 
    'discount_percentage' => 'discount_percentage', 
    'total'               => 'total', 
    'total_display'       => 'total_display', 
    'description'         => 'description'
    ];

    /**
     * Create the Presenter and store the object we are presenting.
     *
     * @param  CartItemInterface $item
     * @return void
     */
    public function __construct(CartItemInterface $item)
    {
        parent::__construct($item->toArray());

        $this->_model = $item;
        $this->_appliedPromotions = [];
    }

    /**
     * Define currency manager.
     *
     * @param  \SunnyDayInc\Shop\Contracts\Currency $currency
     * @param  string                               $format
     * @return self
     */
    public function setCurrencyManager(Currency $currency, $format = '%s %a')
    {
        $this->_currency = $currency;
        $this->_currencyFormat = $format;

        return $this;
    }

    /**
     * Define currency code target.
     *
     * @param  string $code
     * @return self
     */
    public function setCurrencyTarget($code)
    {
        $this->_targetCurrencyCode = $code;

        return $this;
    }

    /**
     * Define product repository instance.
     *
     * @param  \SunnyDayInc\Shop\Cart\Repository\ProductInterface $repository
     * @return void
     */
    public function setProductRepository(ProductRepository $repository)
    {
        $this->_productRepository = $repository;
    }

    /**
     * Define available promotions.
     *
     * @param  \Collection
     * @return void
     */
    public function setPromotions(Collection $promotions)
    {
        $this->_promotions = $promotions;
    }

    /**
     * Return the applied promotions.
     *
     * @return \Collection
     */
    public function getAppliedPromotions()
    {
        return $this->_appliedPromotions;
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentObjectId()
    {
        return (int) $this->_model->object_id;
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentBranchId()
    {
        return (int) $this->_model->branch_id;
    }

    /**
     * Format and return the data.
     *
     * @return array|null
     */
    public function presentProduct()
    {
        $presenter = null;

        $this->collectProduct();

        if (! is_null($this->_product)) {

            $presenter = $this->_product->getPresenter(
                [
                'id', 'slug', 'sku', 'title', 
                'original_price', 'original_price_display', 'price', 'price_display', 
                'discount', 'discount_display', 'discount_percentage', 
                'photo_url'
                ]
            );

            if (! is_null($this->_currency)) {
                $presenter->setCurrencyManager($this->_currency, $this->_currencyFormat);
            }

            if (! is_null($this->_targetCurrencyCode)) {
                $presenter->setCurrencyTarget($this->_targetCurrencyCode);
            }

        }

        return $presenter;
    }

    /**
     * Format and return the data.
     *
     * @return float
     */
    public function presentQty()
    {
        return (float) $this->_model->qty;
    }

    /**
     * Format and return the data.
     *
     * @return float
     */
    public function presentSubtotal()
    {
        $this->calculate();

        return (float) $this->_subtotal;
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentSubtotalDisplay()
    {
        if ($this->_currency instanceOf Currency && ! is_null($this->_targetCurrencyCode)) {
            return $this->_currency
                ->convert($this->_subtotal, $this->_product->currency, $this->_targetCurrencyCode)
                ->format($this->_currencyFormat);
        }

        return '';
    }

    /**
     * Format and return the data.
     *
     * @return float
     */
    public function presentDiscount()
    {
        $this->calculate();

        return (float) $this->_discount;
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentDiscountDisplay()
    {
        if ($this->_currency instanceOf Currency && ! is_null($this->_targetCurrencyCode)) {
            return $this->_currency
                ->convert($this->_discount, $this->_product->currency, $this->_targetCurrencyCode)
                ->format($this->_currencyFormat);
        }

        return '';
    }

    /**
     * Format and return the data.
     *
     * @return float
     */
    public function presentDiscountPercentage()
    {
        $this->calculate();

        return (float) $this->_discountPercentage;
    }

    /**
     * Format and return the data.
     *
     * @return float
     */
    public function presentTotal()
    {
        $this->calculate();

        return (float) $this->_total;
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentTotalDisplay()
    {
        if ($this->_currency instanceOf Currency && ! is_null($this->_targetCurrencyCode)) {
            return $this->_currency
                ->convert($this->_total, $this->_product->currency, $this->_targetCurrencyCode)
                ->format($this->_currencyFormat);
        }

        return '';
    }

    /**
     * Collect current item product.
     *
     * @return void
     */
    public function collectProduct()
    {
        if (! is_null($this->_product)) {
            return $this->_product;
        }

        if (! is_null($this->_productRepository) && ! empty($this->_model->product_id)) {

            if (! empty($this->_model->branch_id)) {
                $this->_product = $this->_productRepository->findByBranch($this->_model->branch_id, $this->_model->product_id);
            } else {
                $this->_product = $this->_productRepository->find($this->_model->product_id);
            }

            if ($this->_promotions instanceOf Collection) {
                if (! $this->_promotions->isEmpty()) {
                    foreach ($this->_promotions as $promotion) {
                        if ($promotion->getType() === PromotionContainer::TYPE_CATALOG) {
                            if ($this->_product->addPromotion($promotion, $this->qty)) {
                                $this->_appliedPromotions[] = $promotion;
                            }
                        }
                    }
                }
            }

        }
    }

    /**
     * Define additional discount amount.
     *
     * @param  float $amount
     * @return void
     */
    public function setAdditionalDiscount($amount)
    {
        $this->_discount += $amount;
        if ($this->_subtotal > $this->_discount) {
            $this->_total = $this->_subtotal - $this->_discount;
        } else {
            $this->_total = 0;
        }

        $this->_discountPercentage = 100;
        if (! empty($this->_total)) {
            $this->_discountPercentage = 100 - (($this->_total / $this->_subtotal) * 100);
        }
    }

    /**
     * Calculate.
     *
     * @return void
     */
    private function calculate()
    {
        if (! is_null($this->_subtotal)
            || ! is_null($this->_discount) 
            || ! is_null($this->_discountPercentage) 
            || ! is_null($this->_total)
        ) {
            return;
        }

        $product = $this->presentProduct();

        $this->_subtotal = $this->_model->qty * $this->product->original_price;
        $this->_discount = $this->_model->qty * $this->product->discount;
    
        if ($this->_promotions instanceOf Collection) {
            if (! $this->_promotions->isEmpty()) {
                foreach ($this->_promotions as $promotion) {
                    if ($promotion->getType() === PromotionContainer::TYPE_CART) {
                        if ($promotion->isItemApplicable($this->_product, true)) {
                            $this->_discount += $promotion->getDiscount($this->product->original_price, $this->_model->qty);
                            $this->_appliedPromotions[] = $promotion;
                        }
                    }
                }
            }
        }

        unset($product);

        $this->_total = 0;
        if ($this->_subtotal > $this->_discount) {
            $this->_total = $this->_subtotal - $this->_discount;
        }

        $this->_discountPercentage = 100;
        if (! empty($this->_total)) {
            $this->_discountPercentage = 100 - (($this->_total / $this->_subtotal) * 100);
        }
    }

}
