<?php namespace SunnyDayInc\Shop\Payment;

use SunnyDayInc\Shop\Models\PaymentInterface;
use SunnyDayInc\Shop\Models\PaymentTransactionInterface;

interface FactoryInterface
{

    /**
     * Set the logger instance.
     *
     * @param  mixed $log
     * @return self
     */
    public function setLogger($log);

    /**
     * Set the payment instance.
     *
     * @param  \SunnyDayInc\Shop\Models\PaymentInterface $payment
     * @return self
     */
    public function setPayment(PaymentInterface $payment);

    /**
     * Return the payment instance.
     *
     * @return \SunnyDayInc\Shop\Models\PaymentInterface
     */
    public function getPayment();

    /**
     * Return the transactions.
     *
     * @return \Collection|\SunnyDayInc\Shop\Models\PaymentTransactionInterface[]
     */
    public function getTransactions();

    /**
     * Get a transaction by it's ID.
     *
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Models\PaymentTransactionInterface|null
     */
    public function getTransaction($id);

    /**
     * Insert a new transaction.
     *
     * @param  \SunnyDayInc\Shop\Models\PaymentTransactionInterface $transaction
     * @return self
     */
    public function addTransaction(PaymentTransactionInterface $transaction);

    /**
     * Create a new payment transaction.
     *
     * @return \SunnyDayInc\Shop\Models\PaymentTransactionInterface
     */
    public function createTransaction();

    /**
     * Save the payment.
     *
     * @return boolean
     */
    public function save();

}
