<?php namespace SunnyDayInc\Shop\Contracts;

interface Translator
{

    /**
     * Set current locale.
     *
     * @param  string $locale
     * @param  string $path
     * @param  string $domain
     * @return self
     */
    public function setLocale($locale, $path = '', $domain = 'messages');

    /**
     * Translate a text.
     *
     * @param  string $text
     * @param  mixed  $parameters
     * @param  string $domain
     * @return string
     */
    public function trans($text, $parameters = [], $domain = 'messages');

}
