<?php namespace SunnyDayInc\Shop\Promotion\Repository;

interface PromotionInterface
{

    /**
     * Return a promotion by it's ID.
     *
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Contracts\Model|null
     */
    public function find($id);

    /**
     * Return a promotion by object type, object ID, and it's ID.
     *
     * @param  string  $objectType
     * @param  integer $objectId
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Contracts\Model|null
     */
    public function findByObject($objectType, $objectId, $id);

    /**
     * Return promotions.
     *
     * @param  array   $where
     * @param  array   $column
     * @param  integer $page
     * @param  integer $limit
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAllBy(
        Array $where = [], 
        Array $column = ['*'], 
        $page = 1, 
        $limit = 10, 
        $order = 'DESC', 
        $orderBy = 'created_at'
    );

    /**
     * Return a promotion by coupon.
     *
     * @param  string    $code
     * @param  boolean   $isActive
     * @param  string    $objectType
     * @param  integer   $objectId
     * @param  \DateTime $dateAt
     * @return \SunnyDayInc\Shop\Contracts\Model|null
     */
    public function findCoupon($code, $isActive = true, $objectType = null, $objectId = null, $dateAt = null);

    /**
     * Return the current total row in previous query.
     *
     * @return string
     */
    public function getTotal();

    /**
     * Return the promotion items by promotion ID.
     *
     * @param  integer $promotionId
     * @return \Collection|\SunnyDayInc\Shop\Models\PromotionCouponInterface[]
     */
    public function findCouponsByPromotion($promotionId);

    /**
     * Return the promotion item by promotion ID and it's ID.
     *
     * @param  integer $promotionId
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Models\PromotionCouponInterface|null
     */
    public function findCouponByPromotionAndId($promotionId, $id);

    /**
     * Return the Model class name
     *
     * @return string
     */
    public function model();

    /**
     * Return the Model Coupon class name
     *
     * @return string
     */
    public function modelCoupon();

    /**
     * Return the Model Product class name
     *
     * @return string
     */
    public function modelProduct();

    /**
     * Return the model object creation.
     *
     * @return \SunnyDayInc\Shop\Models\PromotionInterface
     */
    public function createModel();

    /**
     * Return the model coupon object creation.
     *
     * @return \SunnyDayInc\Shop\Models\PromotionCouponInterface
     */
    public function createModelCoupon();

    /**
     * Return the model product object creation.
     *
     * @return \SunnyDayInc\Shop\Models\PromotionProductInterface
     */
    public function createModelProduct();

}
