<?php namespace SunnyDayInc\Shop\Mail\Loader;

use SunnyDayInc\Shop\Mail\Repository\Template;

class Database extends Loader
{

    /**
     * The repository instance.
     *
     * @var \SunnyDayInc\Shop\Mail\Repository\Template
     */
    protected $repository;

    /**
     * The fields reference.
     *
     * @var array
     */
    protected $fields = [
    'owner_id'      => 'account_id', 
    'name'          => 'type', 
    'language_code' => 'language_code', 
    'reply_address' => 'reply_address', 
    'subject'       => 'subject', 
    'content_text'  => 'content_text', 
    'content_html'  => 'content_html'
    ];

    /**
     * Create a new instance.
     *
     * @param  \SunnyDayInc\Shop\Mail\Repository\Template $repository
     * @return void
     */
    public function __construct(Template $repository)
    {
        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    public function find($name, $languageCode = 'id_ID', $ownerId = 0)
    {
        // Get template
        if (! empty($ownerId)) {
            $template = $this->repository->getByOwnerAndNameAndLanguage($ownerId, $name, $languageCode);
        } else {
            $template = $this->repository->getByNameAndLanguage($name, $languageCode);
        }

        if (! is_object($template)) {
            return null;
        }

        // Get the array data
        $template = $this->repository->getTheArray($template);

        // Build then return the template object
        return $this->buildTemplate(
            [
            'language_code' => $template[$this->fields['language_code']], 
            'reply_address' => $template[$this->fields['reply_address']], 
            'subject'       => $template[$this->fields['subject']], 
            'content_text'  => $template[$this->fields['content_text']], 
            'content_html'  => $template[$this->fields['content_html']]
            ]
        );
    }

}
