<?php namespace SunnyDayInc\Shop\Models;

use SunnyDayInc\Shop\Models\OrderItemInterface;
use SunnyDayInc\Shop\Models\InvoiceItemInterface;

trait InvoiceItem
{

    /**
     * Checks whether the item given as argument corresponds to
     * the same item. Can be overwritten to enable merge quantities.
     *
     * @param InvoiceItemInterface $item
     *
     * @return bool
     */
    public function equals(InvoiceItemInterface $item)
    {
        $isEqual = ($this->object_type === $item->object_type);

        if ($isEqual) {
            $isEqual = ($this->object_id === $item->object_id);
        }

        if ($isEqual) {
            $isEqual = ($this->options === $item->options);
        }

        if ($isEqual) {
            $isEqual = ($this->description === $item->description);
        }

        return $isEqual;
    }

    /**
     * Merge the item given as argument corresponding to the same item.
     *
     * @param  InvoiceItemInterface $item
     * @return self
     */
    public function merge(InvoiceItemInterface $item)
    {
        // Merge the quantity ordered
        $this->setQty($this->qty + $item->qty);

        return $this;
    }

    /**
     * Set the order item going to use.
     *
     * @param  \SunnyDayInc\Shop\Models\OrderItemInterface $order
     * @return self
     */
    public function setOrder(OrderItemInterface $order)
    {
        $this->order_item_id        = $order->id;
        $this->object_type          = $order->object_type;
        $this->object_id            = $order->object_id;
        $this->sku                  = $order->sku;
        $this->title                = $order->title;
        $this->options              = $order->options;
        $this->description          = $order->description;
        $this->qty                  = $order->qty_ordered;

        $this->price                = $order->original_price;
        $this->base_price           = $order->base_original_price;

        $this->price_incl_tax       = $this->price + (($this->price * $order->tax_percent) / 100);
        $this->base_price_incl_tax  = $this->base_price + (($this->base_price * $order->tax_percent) / 100);

        $this->discount_amount      = $order->discount_amount;
        $this->base_discount_amount = $order->base_discount_amount;
    
        $this->currency_code        = $order->order_currency_code;

        return $this;
    }

    /**
     * Set the item quantity.
     *
     * @param  integer $qty
     * @return self
     */
    public function setQty($qty)
    {
        $this->qty = $qty;

        $this->calculate();

        return $this;
    }

    /**
     * Calculate.
     *
     * @return self
     */
    public function calculate()
    {
        $this->total                = ($this->price - $this->discount_amount) * $this->qty;
        $this->base_total           = ($this->base_price - $this->base_discount_amount) * $this->qty;
        // $this->total                = $this->price * $this->qty;
        // $this->base_total           = $this->base_price * $this->qty;

        $this->tax_amount           = ($this->price_incl_tax - $this->price) * $this->qty;
        $this->base_tax_amount      = ($this->base_price_incl_tax - $this->base_price) * $this->qty;

        $this->total_incl_tax       = $this->total + $this->tax_amount;
        $this->base_total_incl_tax  = $this->base_total + $this->base_tax_amount;

        return $this;
    }

}
