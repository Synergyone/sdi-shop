<?php namespace SunnyDayInc\Shop\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

use SunnyDayInc\Shop\Models\InvoiceInterface;
use SunnyDayInc\Shop\Models\Invoice as InvoiceModel;

class Invoice extends Model implements InvoiceInterface
{

    use InvoiceModel;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'invoice';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'object_type', 
    'object_id', 
    'branch_id', 
    'order_id', 
    'billing_address_id', 
    'identifier', 
    'code', 
    'qty', 
    'discount_amount', 
    'base_discount_amount', 
    'tax_amount', 
    'base_tax_amount', 
    'shipping_amount', 
    'base_shipping_amount', 
    'shipping_tax_amount', 
    'base_shipping_tax_amount', 
    'shipping_incl_tax', 
    'base_shipping_incl_tax', 
    'subtotal', 
    'base_subtotal', 
    'subtotal_incl_tax', 
    'base_subtotal_incl_tax', 
    'grand_total', 
    'base_grand_total', 
    'grand_total_unique', 
    'grand_total_paid', 
    'base_grand_total_paid', 
    'base_total_refunded', 
    'customer_balance_amount', 
    'base_customer_balance_amount', 
    'status', 
    'order_currency_code', 
    'base_currency_code', 
    'global_currency_code', 
    'base_to_order_rate', 
    'base_to_global_rate', 
    'note', 
    'due_at', 
    'paid_at', 
    'cancelled_at'
    ];

}
