<?php namespace SunnyDayInc\Shop\Wallet\Repository;

class EloquentWallet extends Wallet
{

    /**
     * The current total row in previous query.
     *
     * @var integer
     */
    protected $total = 0;

    /**
     * {@inheritdoc}
     */
    public function findBy(
        Array $where = [], 
        Array $column = ['*'], 
        $page = 1, 
        $limit = 10, 
        $order = 'DESC', 
        $orderBy = 'created_at'
    ) {
        $where = array_merge(
            [
            'object_type' => '', 
            'object_id'   => 0, 
            'type'        => '', 
            'is_default'  => null, 
            'is_active'   => null
            ], $where
        );

        if (empty($page)) {
            $page = 1;
        }

        $useWhere = false;
        $hasWhere = false;

        $fromSql = '(';
        $fromSql .= 'SELECT `'.$this->model->getTable().'`.`id`';
        $fromSql .= ' FROM `'.$this->model->getTable().'`';

        if (! empty($where['object_type'])
            || ! empty($where['object_id'])
            || ! empty($where['type'])
            || ! is_null($where['is_default'])
            || ! is_null($where['is_active'])
        ) {
            $useWhere = true;
        }

        if ($useWhere) {
            $fromSql .= ' WHERE';
        }

    

        if (! empty($order) && ! empty($orderBy)) {
            $fromSql .= ' ORDER BY `'.$this->model->getTable().'`.`'.$orderBy.'` '.$order;
        }

        if ($limit > 0) {
            $fromSql .= ' limit '.$limit.' offset '.($page - 1) * $limit;
        }

        $fromSql .= ') o';
  
        if ($limit > 0) {
            $query = $this->model->newQuery()->select('id');

      

            $this->total = $query->count();
        } else {
            $this->total = 0;
        }

        $query = $this->model->newQuery()
            ->from(\DB::raw($fromSql))
            ->join($this->model->getTable(), $this->model->getTable().'.id', '=', 'o.id');

        if (! empty($order) && ! empty($orderBy)) {
            $query->orderBy($this->model->getTable().'.'.$orderBy, $order);
        }

        unset($fromSql);
        unset($model);

        return $query->get($column);
    }

    /**
     * {@inheritdoc}
     */
    public function findCreditByWalletAndId($walletId, $id)
    {
        $query = $this->createModelCredit()->newQuery();
        $query->where('wallet_id', '=', $walletId);
        $query->where('id', '=', $id);

        return $query->first();
    }

    /**
     * {@inheritdoc}
     */
    public function findDebitByWalletAndId($walletId, $id)
    {
        $query = $this->createModelDebit()->newQuery();
        $query->where('wallet_id', '=', $walletId);
        $query->where('id', '=', $id);

        return $query->first();
    }

    /**
     * {@inheritdoc}
     */
    public function model()
    {
        return '\SunnyDayInc\Shop\Models\Eloquent\Wallet';
    }

    /**
     * {@inheritdoc}
     */
    public function modelCredit()
    {
        return '\SunnyDayInc\Shop\Models\Eloquent\WalletCredit';
    }

    /**
     * {@inheritdoc}
     */
    public function modelDebit()
    {
        return '\SunnyDayInc\Shop\Models\Eloquent\WalletDebit';
    }

}
