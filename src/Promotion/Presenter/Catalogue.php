<?php namespace SunnyDayInc\Shop\Promotion\Presenter;

class Catalogue extends Promotion
{

    /**
     * {@inheritdoc}
     */
    protected $publicData = [
    'id'                    => 'id', 
    'name'                  => 'name', 
    'description'           => 'description', 
    'from_date'             => 'from_date', 
    'to_date'               => 'to_date', 
    'simple_action'         => 'simple_action', 
    'discount_amount'       => 'discount_amount', 
    'sort_order'            => 'sort_order', 
    'stop_rules_processing' => 'stop_rules_processing', 
    'total_used'            => 'total_used', 
    'is_active'             => 'is_active', 
    'rule'                  => 'rule'
    ];

}
