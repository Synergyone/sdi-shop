<?php namespace SunnyDayInc\Shop\Order\Presenter;

use SunnyDayInc\Shop\Support\Collection;
use SunnyDayInc\Shop\Contracts\Currency;
use SunnyDayInc\Shop\Presenter\Presenter;
use SunnyDayInc\Shop\Models\OrderItemInterface;
use DateTime;

class Item extends Presenter
{

    /**
     * The Item model.
     *
     * @var Item
     */
    protected $_model;

    /**
     * The Currency Manager.
     *
     * @var \SunnyDayInc\Shop\Contracts\Currency
     */
    protected $_currency;

    /**
     * The currency format.
     *
     * @var string
     */
    protected $_currencyFormat = '%s %a';

    /**
     * {@inheritdoc}
     */
    protected $publicData = [
    'id'                          => 'id', 
    'sku'                         => 'sku', 
    'title'                       => 'title', 
    'options'                     => 'options', 
    'description'                 => 'description', 
    'preparation_additional_time' => 'preparation_additional_time', 
    'preparation_total_time'      => 'preparation_total_time', 
    'delivery_time'               => 'delivery_time', 
    'delivery_type'               => 'delivery_type', 
    'price'                       => 'price', 
    'base_price'                  => 'base_price', 
    'original_price'              => 'original_price', 
    'base_original_price'         => 'base_original_price', 
    'price_incl_tax'              => 'price_incl_tax', 
    'base_price_incl_tax'         => 'base_price_incl_tax', 
    'qty_ordered'                 => 'qty_ordered', 
    'qty_canceled'                => 'qty_canceled', 
    'qty_invoiced'                => 'qty_invoiced', 
    'qty_refunded'                => 'qty_refunded', 
    'qty_shipped'                 => 'qty_shipped', 
    'qty_returned'                => 'qty_returned', 
    'tax_percent'                 => 'tax_percent', 
    'tax'                         => 'tax', 
    'base_tax'                    => 'base_tax', 
    'tax_before_discount'         => 'tax_before_discount', 
    'base_tax_before_discount'    => 'base_tax_before_discount', 
    'tax_invoiced'                => 'tax_invoiced', 
    'base_tax_invoiced'           => 'base_tax_invoiced', 
    'tax_refunded'                => 'tax_refunded', 
    'base_tax_refunded'           => 'base_tax_refunded', 
    'discount_percent'            => 'discount_percent', 
    'discount'                    => 'discount', 
    'base_discount'               => 'base_discount', 
    'discount_invoiced'           => 'discount_invoiced', 
    'base_discount_invoiced'      => 'base_discount_invoiced', 
    'discount_refunded'           => 'discount_refunded', 
    'base_discount_refunded'      => 'base_discount_refunded', 
    'refunded'                    => 'refunded', 
    'base_refunded'               => 'base_refunded', 
    'subtotal'                    => 'subtotal', 
    'base_subtotal'               => 'base_subtotal', 
    'subtotal_incl_tax'           => 'subtotal_incl_tax', 
    'base_subtotal_incl_tax'      => 'base_subtotal_incl_tax', 
    'total'                       => 'total', 
    'base_total'                  => 'base_total', 
    'total_incl_tax'              => 'total_incl_tax', 
    'base_total_incl_tax'         => 'base_total_incl_tax', 
    'invoiced'                    => 'invoiced', 
    'base_invoiced'               => 'base_invoiced', 
    'order_currency_code'         => 'order_currency_code', 
    'base_currency_code'          => 'base_currency_code', 
    'global_currency_code'        => 'global_currency_code', 
    'start_preparation_at'        => 'start_preparation_at', 
    'end_preparation_at'          => 'end_preparation_at', 
    'created_at'                  => 'created_at', 
    'updated_at'                  => 'updated_at'
    ];

    /**
     * Create the Presenter and store the object we are presenting.
     *
     * @param  OrderItemInterface $item
     * @param  array              $fields
     * @return void
     */
    public function __construct(OrderItemInterface $item, Array $fields = ['*'])
    {
        parent::__construct($item->toArray());

        // Select field to publish
        // Remove the rest
        $newPublicData = [];

        if (is_array($fields)) {
            if (trim($fields[0]) != '*') {
                foreach ($this->publicData as $key => $data) {
                    foreach ($fields as $item) {
                        if ($data == $item) {
                            $newPublicData[$key] = $data;
                        }
                    }
                }
            }
        }

        if (! empty($newPublicData)) {
            $this->publicData = $newPublicData;
        }

        $this->_model = $item;
    }

    /**
     * Define currency manager.
     *
     * @param  \SunnyDayInc\Shop\Contracts\Currency $currency
     * @param  string                               $format
     * @return void
     */
    public function setCurrencyManager(Currency $currency, $format = '%s %a')
    {
        $this->_currency = $currency;
        $this->_currencyFormat = $format;
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentId()
    {
        return (int) $this->_model->id;
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentPreparationAdditionalTime()
    {
        return (int) $this->_model->preparation_additional_time;
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentPreparationTotalTime()
    {
        return (int) $this->_model->preparation_total_time;
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentDeliveryTime()
    {
        return (int) $this->_model->delivery_time;
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentPrice()
    {
        return $this->formatCurrency($this->_model->price);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBasePrice()
    {
        return $this->formatCurrency($this->_model->base_price);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentOriginalPrice()
    {
        return $this->formatCurrency($this->_model->original_price);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBaseOriginalPrice()
    {
        return $this->formatCurrency($this->_model->base_original_price);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentPriceInclTax()
    {
        return $this->formatCurrency($this->_model->price_incl_tax);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBasePriceInclTax()
    {
        return $this->formatCurrency($this->_model->base_price_incl_tax);
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentQtyOrdered()
    {
        return (int) $this->_model->qty_ordered;
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentQtyCanceled()
    {
        return (int) $this->_model->qty_canceled;
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentQtyInvoiced()
    {
        return (int) $this->_model->qty_invoiced;
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentQtyRefunded()
    {
        return (int) $this->_model->qty_refunded;
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentQtyShipped()
    {
        return (int) $this->_model->qty_shipped;
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentQtyReturned()
    {
        return (int) $this->_model->qty_returned;
    }

    /**
     * Format and return the data.
     *
     * @return float
     */
    public function presentTaxPercent()
    {
        return (float) $this->_model->tax_percent;
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentTax()
    {
        return $this->formatCurrency($this->_model->tax_amount);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBaseTax()
    {
        return $this->formatCurrency($this->_model->base_tax_amount);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentTaxBeforeDiscount()
    {
        return $this->formatCurrency($this->_model->tax_before_discount);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBaseTaxBeforeDiscount()
    {
        return $this->formatCurrency($this->_model->base_tax_before_discount);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentTaxInvoiced()
    {
        return $this->formatCurrency($this->_model->tax_invoiced);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBaseTaxInvoiced()
    {
        return $this->formatCurrency($this->_model->base_tax_invoiced);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentTaxRefunded()
    {
        return $this->formatCurrency($this->_model->tax_refunded);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBaseTaxRefunded()
    {
        return $this->formatCurrency($this->_model->base_tax_refunded);
    }

    /**
     * Format and return the data.
     *
     * @return float
     */
    public function presentDiscountPercent()
    {
        return (float) $this->_model->discount_percent;
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentDiscount()
    {
        return $this->formatCurrency($this->_model->discount_amount);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBaseDiscount()
    {
        return $this->formatCurrency($this->_model->base_discount_amount);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentDiscountInvoiced()
    {
        return $this->formatCurrency($this->_model->discount_invoiced);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBaseDiscountInvoiced()
    {
        return $this->formatCurrency($this->_model->base_discount_invoiced);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentDiscountRefunded()
    {
        return $this->formatCurrency($this->_model->discount_refunded);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentRefunded()
    {
        return $this->formatCurrency($this->_model->amount_refunded);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBaseRefunded()
    {
        return $this->formatCurrency($this->_model->base_amount_refunded);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentSubtotal()
    {
        return $this->formatCurrency($this->_model->total + $this->_model->discount_amount);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBaseSubtotal()
    {
        return $this->formatCurrency($this->_model->base_total + $this->_model->base_discount_amount);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentSubtotalInclTax()
    {
        return $this->formatCurrency($this->_model->total_incl_tax + $this->_model->discount_amount);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBaseSubtotalInclTax()
    {
        return $this->formatCurrency($this->_model->base_total_incl_tax + $this->_model->base_discount_amount);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentTotal()
    {
        return $this->formatCurrency($this->_model->total);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBaseTotal()
    {
        return $this->formatCurrency($this->_model->base_total);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentTotalInclTax()
    {
        return $this->formatCurrency($this->_model->total_incl_tax);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBaseTotalInclTax()
    {
        return $this->formatCurrency($this->_model->base_total_incl_tax);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentInvoiced()
    {
        return $this->formatCurrency($this->_model->invoiced);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentBaseInvoiced()
    {
        return $this->formatCurrency($this->_model->base_invoiced);
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentStartPreparationAt()
    {
        if ($this->_model->start_preparation_at != '0000-00-00 00:00:00') {
            return (new DateTime($this->_model->start_preparation_at))->format('c');
        }

        return null;
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentEndPreparationAt()
    {
        if ($this->_model->end_preparation_at != '0000-00-00 00:00:00') {
            return (new DateTime($this->_model->end_preparation_at))->format('c');
        }

        return null;
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentCreatedAt()
    {
        if ($this->_model->created_at != '0000-00-00 00:00:00') {
            return (new DateTime($this->_model->created_at))->format('c');
        }

        return null;
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentUpdatedAt()
    {
        if ($this->_model->updated_at != '0000-00-00 00:00:00') {
            return (new DateTime($this->_model->updated_at))->format('c');
        }

        return null;
    }

    /**
     * Return the formated currency value.
     *
     * @return string
     */
    private function formatCurrency($value)
    {
        if ($this->_currency instanceOf Currency) {
            return $this->_currency
                ->convert(
                    $value, 
                    $this->_model->base_currency_code, 
                    $this->_model->order_currency_code, 
                    $this->_model->base_to_order_rate
                )->format($this->_currencyFormat);
        }

        return $value;
    }

}
