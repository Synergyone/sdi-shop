<?php namespace SunnyDayInc\Shop\Mail\Compiler\Handlers;

class OrderList extends Handler
{
 
    /**
     * {@inheritdoc}
     */
    public function get($attributes, $matches, $tag)
    {
        return $this->val->code;
    }

}
