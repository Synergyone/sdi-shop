<?php namespace SunnyDayInc\Shop\Models;

interface PromotionItemValidationInterface
{

    /**
     * Return the item SKU.
     *
     * @return string
     */
    public function getSku();

    /**
     * Return the category IDs if available.
     *
     * @return array
     */
    public function getCategoryIds();

}
