<?php namespace SunnyDayInc\Shop\Contracts;

interface Model
{

    /**
     * Set the table associated with the model.
     *
     * @param  string $table
     * @return void
     */
    public function setTable($table);

}
