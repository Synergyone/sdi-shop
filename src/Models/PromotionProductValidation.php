<?php namespace SunnyDayInc\Shop\Models;

use SunnyDayInc\Shop\Support\Collection;
use SunnyDayInc\Shop\Promotion\Promotion as PromotionContainer;
use SunnyDayInc\Shop\Models\PromotionInterface;

trait PromotionProductValidation
{

    /**
     * The discount amount.
     *
     * @var float
     */
    public $discount_amount = 0;

    /**
     * The discount percentage.
     *
     * @var float
     */
    public $discount_percent = 0;

    /**
     * The order promotions.
     *
     * @var \Collection|\SunnyDayInc\Shop\Models\PromotionInterface[]
     */
    protected $promotions;

    /**
     * {@inheritdoc}
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * {@inheritdoc}
     */
    public function getCategoryIds()
    {
        $ids = [];
        $list = $this->categoryIds;

        if (! empty($list)) {
            foreach ($list as $item) {
                $ids[] = $item->category_id;
            }
        }

        unset($list);

        return $ids;
    }

    /**
     * Insert a new promotion.
     *
     * @param  \SunnyDayInc\Shop\Models\PromotionInterface $promo
     * @param  float                                       $qty
     * @return boolean
     */
    public function addPromotion(PromotionInterface $promo, $qty = null)
    {
        if (is_null($this->promotions)) {
            $this->promotions = new Collection();
        }

        if ($this->promotions->offsetExists($promo->getId())) {
            return false;
        }

        // Check is promotion applicable
        if ($promo->type === PromotionContainer::TYPE_CART) {
            if (! $promo->isItemApplicable($this, true)) {
                return false;
            }
        }

        if ($promo->type === PromotionContainer::TYPE_CATALOG) {
            if (! $promo->isItemApplicable($this)) {
                return false;
            }
        }

        if (! isset($this->original_price)) {
            $this->original_price = $this->price;
        }
        
        // Set new discount amount
        $this->discount_amount += $promo->getDiscount($this->original_price, $qty);

        // Re-calculate pricing
        $this->calculate();

        $this->promotions->put($promo->getId(), $promo);

        return true;
    }

    /**
     * Return existing promotions.
     *
     * @return \Collection|\SunnyDayInc\Shop\Models\PromotionInterface[]|null
     */
    public function getPromotions()
    {
        return $this->promotions;
    }

    /**
     * Calculate.
     *
     * @return self
     */
    public function calculate()
    {
        if ($this->original_price < $this->discount_amount) {
            $this->discount_amount = $this->original_price;
        }

        $this->price = $this->original_price - $this->discount_amount;

        $this->discount_percent = 100;
        if (! empty($this->price)) {
            $this->discount_percent = 100 - (($this->price / $this->original_price) * 100);
        }

        return $this;
    }

}
