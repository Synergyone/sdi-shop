<?php namespace SunnyDayInc\Shop\ServiceProvider\Illuminate;

use Illuminate\Support\ServiceProvider;

use SunnyDayInc\Shop\Promotion\Promotion as PromotionContainer;
use SunnyDayInc\Shop\Promotion\Repository\EloquentPromotion;

class Promotion extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerRepository();
    
        $this->app->singleton(
            'SunnyDayInc\Shop\Contracts\Promotion', function ($app) {
                $promotion = new PromotionContainer($app['SunnyDayInc\Shop\Promotion\Repository\PromotionInterface']);

                $promotion->setLogger($app['log']);

                return $promotion;
            }
        );
    }

    /**
     * Register the repository instance.
     *
     * @return void
     */
    private function registerRepository()
    {
        $this->app->singleton(
            'SunnyDayInc\Shop\Promotion\Repository\PromotionInterface', function ($app) {
                return new EloquentPromotion();
            }
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
        'SunnyDayInc\Shop\Promotion\Repository\PromotionInterface', 
        'SunnyDayInc\Shop\Contracts\Promotion'
        ];
    }
}
