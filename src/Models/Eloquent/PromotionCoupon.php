<?php namespace SunnyDayInc\Shop\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

use SunnyDayInc\Shop\Models\PromotionCouponInterface;
use SunnyDayInc\Shop\Models\PromotionCoupon as PromotionCouponModel;
use SunnyDayInc\Shop\Contracts\Presentable;

class PromotionCoupon extends Model implements PromotionCouponInterface, Presentable
{

    use PromotionCouponModel;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'promotion_coupon';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'promotion_id', 
    'code', 
    'usage_limit', 
    'usage_per_customer', 
    'total_used', 
    'expire_at'
    ];

    /**
     * {@inheritdoc}
     */
    public function promotion()
    {
        return $this->belongsTo('\SunnyDayInc\Shop\Models\Eloquent\Promotion', 'promotion_id');
    }

}
