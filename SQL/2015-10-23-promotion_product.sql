CREATE TABLE `promotion_product` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `promotion_id` bigint(20) unsigned NOT NULL,
  `from_time` int(10) unsigned NOT NULL DEFAULT '0',
  `to_time` int(10) unsigned NOT NULL DEFAULT '0',
  `product_id` bigint(20) unsigned NOT NULL,
  `action_operator` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT 'to_fixed',
  `action_amount` decimal(30,20) NOT NULL DEFAULT '0.00000000000000000000',
  `sort_order` int(10) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `promotion_id` (`promotion_id`),
  KEY `from_time` (`from_time`),
  KEY `to_time` (`to_time`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;