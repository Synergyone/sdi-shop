<?php namespace SunnyDayInc\Shop\Address;

use SunnyDayInc\Shop\Contracts\Address as AddressInterface;
use SunnyDayInc\Shop\Address\Repository\Address as AddressRepository;

class Address implements AddressInterface
{

    /**
     * The address instance.
     *
     * @var \SunnyDayInc\Shop\Address\Repository\Address
     */
    protected $repository;
  
    /**
     * Create a new instance.
     *
     * @param  \SunnyDayInc\Shop\Address\Repository\Address $repository
     * @return void
     */
    public function __construct(AddressRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * {@inheritdoc}
     */
    public function create(Array $data)
    {
        return $this->repository->create($data);
    }

}
