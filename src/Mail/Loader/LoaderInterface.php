<?php namespace SunnyDayInc\Shop\Mail\Loader;

use SunnyDayInc\Shop\Mail\Compiler\Manager;

interface LoaderInterface
{

    /**
     * Register the compiler manager.
     *
     * @param  \SunnyDayInc\Shop\Mail\Compiler\Manager $compiler
     * @return void
     */
    public function setCompiler(Manager $compiler);

    /**
     * Find a template by it's name.
     *
     * @param  string  $name
     * @param  string  $languageCode
     * @param  integer $ownerId
     * @return \SunnyDayInc\Shop\Mail\TemplateInterface
     */
    public function find($name, $languageCode = 'id_ID', $ownerId = 0);

}
