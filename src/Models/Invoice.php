<?php namespace SunnyDayInc\Shop\Models;

use SunnyDayInc\Shop\Support\Collection;
use SunnyDayInc\Shop\Invoice\Presenter\Invoice as InvoicePresenter;

trait Invoice
{

    /**
     * Calculate.
     *
     * @param  \Collection $items
     * @return self
     */
    public function calculate(Collection $items)
    {
        if (! $items->isEmpty()) {
            foreach ($items as $item) {

                $this->qty                    += $item->qty;

                $this->discount_amount        += $item->discount_amount;
                $this->base_discount_amount   += $item->base_discount_amount;
        
                $this->tax_amount             += $item->tax_amount;
                $this->base_tax_amount        += $item->base_tax_amount;

                $this->subtotal               += $item->total;
                $this->base_subtotal          += $item->base_total;

                $this->subtotal_incl_tax      += $item->total_incl_tax;
                $this->base_subtotal_incl_tax += $item->base_total_incl_tax;

            }
        }

        $this->grand_total              = $this->subtotal_incl_tax + $this->shipping_incl_tax + $this->grand_total_unique;
        $this->base_grand_total         = $this->base_subtotal_incl_tax + $this->base_shipping_incl_tax + $this->grand_total_unique;
        $this->grand_total_unique       = $this->grand_total;

        $this->grand_total_paid         = 0;
        $this->base_grand_total_paid    = 0;
        $this->base_total_refunded      = 0;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPresenter(Array $fields = ['*'])
    {
        return new InvoicePresenter($this, $fields);
    }

}
