<?php namespace SunnyDayInc\Shop\Mail\Repository;

use SunnyDayInc\Shop\Models\Eloquent\MailTemplate;

interface Template
{

    /**
     * Return template by it's name, and language code.
     *
     * @param  string $name
     * @param  string $language
     * @return \SunnyDayInc\Shop\Contracts\Model
     */
    public function getByNameAndLanguage($name, $language);

    /**
     * Return template by it's owner ID, name, and language code.
     *
     * @param  integer $ownerId
     * @param  string  $name
     * @param  string  $language
     * @return \SunnyDayInc\Shop\Contracts\Model
     */
    public function getByOwnerAndNameAndLanguage($ownerId, $name, $language);

    /**
     * Return the data array from model object.
     *
     * @param  \SunnyDayInc\Shop\Contracts\Model $template
     * @return array
     */
    public function getTheArray(MailTemplate $template);

    /**
     * Return the Model class name
     *
     * @return string
     */
    public function model();

}
