<?php namespace SunnyDayInc\Shop\Lock;

use SunnyDayInc\Shop\Contracts\Lock as LockInterface;
use SunnyDayInc\Shop\Lock\Repository\Lock as Repository;

use BeatSwitch\Lock\Manager;
use \BeatSwitch\Lock\Callers\Caller;

class Lock implements LockInterface
{

    /**
     * The repository instance.
     *
     * @var \SunnyDayInc\Shop\Lock\Repository\Lock
     */
    protected $repository;

    /**
     * The manager instance.
     *
     * @var \BeatSwitch\Lock\Manager
     */
    protected $manager;

    /**
     * Create a new instance.
     *
     * @param  \SunnyDayInc\Shop\Lock\Repository\Lock $repository
     * @param  \BeatSwitch\Lock\Manager               $manager
     * @return void
     */
    public function __construct(Repository $repository, Manager $manager)
    {
        $this->repository = $repository;
        $this->manager = $manager;

        $this->setRoles();
    }

    /**
     * {@inheritdoc}
     */
    public function setLock(Caller $caller)
    {
        // Get the lock instance for the authed user.
        $lock = $this->manager->caller($caller);

        // Enable the LockAware trait on the user.
        $caller->setLock($lock);

        unset($lock);

        return $caller;
    }

    /**
     * {@inheritdoc}
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * Set roles.
     *
     * @return void
     */
    private function setRoles()
    {
        $groups = $this->repository->all();

        if (! $groups->isEmpty()) {
            foreach ($groups as $group) {
                $this->manager->setRole($group->name);

                $permissions = json_decode($group->permissions);

                if ($permissions) {
                    if (isset($permissions->allow)) {
                        if (! empty($permissions->allow) and is_array($permissions->allow)) {
                            foreach ($permissions->allow as $role) {
                                $this->manager->role($group->name)->allow($role);
                            }
                        }
                    }

                    if (isset($permissions->deny)) {
                        if (! empty($permissions->deny) and is_array($permissions->deny)) {
                            foreach ($permissions->deny as $role) {
                                $this->manager->role($group->name)->deny($role);
                            }
                        }
                    }
                }

                unset($permissions);
                unset($group);
            }
        }

        unset($groups);
    }

}
