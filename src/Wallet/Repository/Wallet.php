<?php namespace SunnyDayInc\Shop\Wallet\Repository;

abstract class Wallet implements WalletInterface
{

    /**
     * {@inheritdoc}
     */
    public function find($id, $objectType = '', $objectId = '')
    {
        $query = $this->createModel();
        $query->where('id', '=', $id);

        if (! empty($objectType)) {
            $query->where('object_type', '=', $objectType);
        }

        if (! empty($objectId)) {
            $query->where('object_id', '=', $objectId);
        }

        return $query->first();
    }

    /**
     * {@inheritdoc}
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * {@inheritdoc}
     */
    public function createModel()
    {
        $model = $this->model();

        return new $model;
    }

    /**
     * {@inheritdoc}
     */
    public function createModelCredit()
    {
        $model = $this->modelCredit();

        return new $model;
    }

    /**
     * {@inheritdoc}
     */
    public function createModelDebit()
    {
        $model = $this->modelDebit();

        return new $model;
    }

}
