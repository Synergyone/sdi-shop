<?php namespace SunnyDayInc\Shop\Cart;

use SunnyDayInc\Shop\Contracts\Cart as CartInterface;
use SunnyDayInc\Shop\Cart\Repository\Cart as CartRepository;
use SunnyDayInc\Shop\Models\CartInterface as CartModelInterface;

class Cart implements CartInterface
{

    /**
     * The repository instance.
     *
     * @var \SunnyDayInc\Shop\Cart\Repository\Cart
     */
    protected $repository;

    /**
     * The log instance.
     *
     * @var \Illuminate\Contracts\Logging\Log
     */
    protected $log;

    /**
     * Create a new instance.
     *
     * @param  \SunnyDayInc\Shop\Cart\Repository\Cart $repository
     * @return void
     */
    public function __construct(CartRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    public function setLogger($log)
    {
        $this->log = $log;
    }

    /**
     * {@inheritdoc}
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * {@inheritdoc}
     */
    public function create(Array $data = [])
    {
        return $this->createFactory($data);
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        $cart = $this->repository->find($id);

        if ($cart instanceOf CartModelInterface) {
            $factory = $this->createFactory();

            $factory->setCart($cart);

            unset($cart);

            return $factory;
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function findByUser($id)
    {
        $cart = $this->repository->findByUser($id);

        if ($cart instanceOf CartModelInterface) {
            $factory = $this->createFactory();

            $factory->setCart($cart);

            unset($cart);

            return $factory;
        }

        return null;
    }

    /**
     * Create the factory instance.
     *
     * @param  array $data
     * @return \SunnyDayInc\Shop\Cart\FactoryInterface
     */
    private function createFactory(Array $data = [])
    {
        $factory = new Factory($this->repository, $data);

        if (! is_null($this->log)) {
            $factory->setLogger($this->log);
        }

        return $factory;
    }

}
