<?php namespace SunnyDayInc\Shop\Mail;

use SunnyDayInc\Shop\Contracts\Mailer as MailerInterface;
use SunnyDayInc\Shop\Mail\Loader\LoaderInterface;
use SunnyDayInc\Shop\Mail\TemplateInterface;

use Closure;
use Swift_Mailer;
use Swift_Mime_Message;
use Swift_Message;

class Mailer implements MailerInterface
{

    /**
     * The Swift Mailer instance.
     *
     * @var \Swift_Mailer
     */
    protected $swift;

    /**
     * The template loader instance.
     *
     * @var \SunnyDayIncSunnyDayInc\Shop\Mail\Loader\LoaderInterface
     */
    protected $templateLoader;

    /**
     * The transport instance.
     *
     * @var \SunnyDayInc\Shop\Mail\Transport\Transport
     */
    protected $transport;

    /**
     * The global from address and name.
     *
     * @var array
     */
    protected $from;

    /**
     * Create a new instance.
     *
     * @param  \SunnyDayInc\Shop\Mail\Loader\LoaderInterface $transport
     * @param  \Swift_Mailer                                 $swift
     * @return void
     */
    public function __construct(LoaderInterface $templateLoader, Swift_Mailer $swift)
    {
        $this->templateLoader = $templateLoader;
        $this->swift = $swift;
    }

    /**
     * {@inheritdoc}
     */
    public function alwaysFrom($address, $name = null)
    {
        $this->from = compact('address', 'name');
    }

    /**
     * {@inheritdoc}
     */
    public function send(TemplateInterface $template, Closure $callback)
    {
        $message = $this->createMessage();

        $this->addContent($message, $template);

        $this->callMessageBuilder($callback, $message);

        $message = $message->getSwiftMessage();

        return $this->sendSwiftMessage($message);
    }

    /**
     * Create a new message instance.
     *
     * @return \SunnyDayInc\Shop\Mail\Message
     */
    protected function createMessage()
    {
        $message = new Message(new Swift_Message);

        // If a global from address has been specified we will set it on every message
        // instances so the developer does not have to repeat themselves every time
        // they create a new message. We will just go ahead and push the address.
        if (! empty($this->from['address'])) {
            $message->from($this->from['address'], $this->from['name']);
        }

        return $message;
    }

    /**
     * Add the content to a given message.
     *
     * @param  \SunnyDayInc\Shop\Mail\Message           $message
     * @param  \SunnyDayInc\Shop\Mail\TemplateInterface $template
     * @return void
     */
    protected function addContent(Message $message, TemplateInterface $template)
    {
        $message->subject($template->getSubject());

        if ($template->hasContentHtml()) {
            $message->setBody($template->getContentHtml(), 'text/html');
        }

        if ($template->hasContentText()) {
            $message->addPart($template->getContentText(), 'text/plain');
        }
    }

    /**
     * Call the provided message builder.
     *
     * @param  \Closure                       $callback
     * @param  \SunnyDayInc\Shop\Mail\Message $message
     * @return mixed
     */
    protected function callMessageBuilder($callback, Message $message)
    {
        return call_user_func($callback, $message);
    }

    /**
     * Send a Swift Message instance.
     *
     * @param  \Swift_Mime_Message $message
     * @return void
     */
    protected function sendSwiftMessage(Swift_Mime_Message $message)
    {
        return $this->swift->send($message, $this->failedRecipients);
    }

    /**
     * {@inheritdoc}
     */
    public function getTemplate($name)
    {
        if (is_null($this->templateLoader)) {
            throw new \Exception(
                'Mail template loader does not exist.'
            );
        }

        // Find the template required
        $template = $this->templateLoader->find($name);

        if (is_null($template)) {
            throw new \Exception(
                'Could not found ['.$name.'] template using ['.get_class($this->templateLoader).'] loader.'
            );
        }

        return $template;
    }

}
