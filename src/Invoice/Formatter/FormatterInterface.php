<?php namespace SunnyDayInc\Shop\Invoice\Formatter;

interface FormatterInterface
{

    /**
     * Return the formatted value.
     *
     * @return string
     */
    public function compile();

}
