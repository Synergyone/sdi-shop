<?php namespace SunnyDayInc\Shop\Models;

use SunnyDayInc\Shop\Support\Collection;

trait Wallet
{

    /**
     * Calculate.
     *
     * @param  \Collection $credits
     * @param  \Collection $debits
     * @return self
     */
    public function calculate(Collection $credits, Collection $debits)
    {
        if (! $credits->isEmpty()) {
            foreach ($credits as $item) {

                $this->total += $item->total;

            }
        }

        if (! $debits->isEmpty()) {
            foreach ($debits as $item) {

                $this->total -= $item->total;

            }
        }

        return $this;
    }

}
