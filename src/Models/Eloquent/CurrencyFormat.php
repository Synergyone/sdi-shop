<?php namespace SunnyDayInc\Shop\Models\Eloquent;

use SunnyDayInc\Shop\Contracts\Model as ModelInterface;
use Illuminate\Database\Eloquent\Model;

class CurrencyFormat extends Model implements ModelInterface
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'currency_format';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'code', 
    'decimal', 
    'format', 
    'thousand_separator', 
    'decimal_separator'
    ];

}
