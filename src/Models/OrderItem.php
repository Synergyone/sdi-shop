<?php namespace SunnyDayInc\Shop\Models;

use SunnyDayInc\Shop\Support\Collection;

use SunnyDayInc\Shop\Promotion\Promotion as PromotionContainer;
use SunnyDayInc\Shop\Models\PromotionInterface;
use SunnyDayInc\Shop\Order\Presenter\Item as ItemPresenter;

trait OrderItem
{

    /**
     * The order promotions.
     *
     * @var \Collection|\SunnyDayInc\Shop\Models\PromotionInterface[]
     */
    protected $promotions;

    /**
     * Insert a new promotion.
     *
     * @param  \SunnyDayInc\Shop\Models\PromotionInterface $promo
     * @return boolean
     */
    public function addPromotion(PromotionInterface $promo)
    {
        if (is_null($this->promotions)) {
            $this->promotions = new Collection();
        }

        if ($this->promotions->offsetExists($promo->getId())) {
            return false;
        }

        // Check is promotion applicable
        if ($promo->type === PromotionContainer::TYPE_CART) {
            if (! $promo->isItemApplicable($this, true)) {
                return false;
            }
        }

        if ($promo->type === PromotionContainer::TYPE_CATALOG) {
            if (! $promo->isItemApplicable($this)) {
                return false;
            }
        }

        // Set new discount amount
        $this->base_discount_amount += $promo->getDiscount($this->base_original_price);

        // Re-calculate pricing
        $this->calculate();

        $this->promotions->put($promo->getId(), $promo);

        return true;
    }

    /**
     * Return existing promotions.
     *
     * @return \Collection|\SunnyDayInc\Shop\Models\PromotionInterface[]|null
     */
    public function getPromotions()
    {
        return $this->promotions;
    }

    /**
     * Checks whether the item given as argument corresponds to
     * the same item. Can be overwritten to enable merge quantities.
     *
     * @param OrderItemInterface $item
     *
     * @return bool
     */
    public function equals(OrderItemInterface $item)
    {
        $isEqual = ($this->object_type === $item->object_type);

        if ($isEqual) {
            $isEqual = ($this->object_id === $item->object_id);
        }

        if ($isEqual) {
            $isEqual = ($this->options === $item->options);
        }

        if ($isEqual) {
            $isEqual = ($this->description === $item->description);
        }

        return $isEqual;
    }

    /**
     * Merge the item given as argument corresponding to the same item.
     *
     * @param  OrderItemInterface $item
     * @return self
     */
    public function merge(OrderItemInterface $item)
    {
        // Merge the quantity ordered
        $this->qty_ordered += $item->qty_ordered;

        // Merge the promotion(s) if available
        $promotions = $item->getPromotions();

        if ($promotions instanceof Collection) {
            if (! ($this->promotions instanceof Collection)) {
                $this->promotions = $promotions;
            } else {
                foreach ($promotions as $newPromo) {
                    $isExist = false;

                    foreach ($this->promotions as $existingPromo) {
                        if ($newPromo === $existingPromo) {
                            $isExist = true;
                            break;
                        }
                    }

                    if (! $isExist) {
                        $this->addPromotion($newPromo);
                    }
                }
            }
        }

        unset($promotions);

        // Re-calculate
        $this->calculate();

        return $this;
    }

    /**
     * Calculate.
     *
     * @return self
     */
    public function calculate()
    {
        $this->original_price = $this->base_original_price;
        if ($this->base_to_order_rate > 1) {
            $this->original_price = $this->base_original_price * $this->base_to_order_rate;
        }

        $this->price = $this->base_price;
        if ($this->base_to_order_rate > 1) {
            $this->price = $this->base_price * $this->base_to_order_rate;
        }

        $this->discount_amount = $this->base_discount_amount;
        if ($this->base_to_order_rate > 1) {
            $this->discount_amount = $this->base_discount_amount * $this->base_to_order_rate;
        }

        // $this->price                    = $this->original_price - $this->discount_amount;
        // $this->base_price               = $this->base_original_price - $this->base_discount_amount;
    
        // $this->discount_percent         = 100 - (($this->base_price / $this->base_original_price) * 100);

        $this->price_incl_tax      = $this->price + (($this->price * $this->tax_percent) / 100);
        $this->base_price_incl_tax = $this->base_price + (($this->base_price * $this->tax_percent) / 100);

        // $this->total                    = $this->price * $this->qty_ordered;
        // $this->base_total               = $this->base_price * $this->qty_ordered;
        $this->total = $this->base_total;
        if ($this->base_to_order_rate > 1) {
            $this->total = $this->base_total * $this->base_to_order_rate;
        }
    
        $this->tax_before_discount      = (($this->original_price * $this->tax_percent) / 100) * $this->qty_ordered;
        $this->base_tax_before_discount = (($this->base_original_price * $this->tax_percent) / 100) * $this->qty_ordered;

        $this->tax_amount               = ($this->total * $this->tax_percent) / 100;
        $this->base_tax_amount          = ($this->base_total * $this->tax_percent) / 100;

        $this->total_incl_tax           = $this->total + $this->tax_amount;
        $this->base_total_incl_tax      = $this->base_total + $this->base_tax_amount;
    
        // $this->qty_canceled
        // $this->qty_invoiced
        // $this->qty_refunded
        // $this->qty_shipped
        // $this->qty_returned
    
        // $this->tax_invoiced
        // $this->base_tax_invoiced
        // $this->tax_refunded
        // $this->base_tax_refunded
    
        // $this->discount_invoiced
        // $this->base_discount_invoiced
        // $this->discount_refunded
        // $this->base_discount_refunded
        // $this->amount_refunded
        // $this->base_amount_refunded

        // $this->invoiced                 = ;
        // $this->base_invoiced            = ;

        return $this;
    }

    /**
     * Return the item SKU.
     *
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Return the product category IDs if available.
     *
     * @return array
     */
    public function getCategoryIds()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getPresenter(Array $fields = ['*'])
    {
        return new ItemPresenter($this, $fields);
    }

}
