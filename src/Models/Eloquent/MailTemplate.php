<?php namespace SunnyDayInc\Shop\Models\Eloquent;

use SunnyDayInc\Shop\Contracts\Model as ModelInterface;
use Illuminate\Database\Eloquent\Model;

class MailTemplate extends Model implements ModelInterface
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'email_templates';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'account_id', 
    'type', 
    'language_code', 
    'reply_address', 
    'reply_address_gs', 
    'subject', 
    'content_text', 
    'content_html'
    ];

}
