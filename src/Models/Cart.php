<?php namespace SunnyDayInc\Shop\Models;

use SunnyDayInc\Shop\Support\Uuid;
use SunnyDayInc\Shop\Support\Collection;
use SunnyDayInc\Shop\Cart\Presenter\Cart as CartPresenter;

trait Cart
{

    /**
     * The unavailable items.
     *
     * @var \Collection
     */
    protected $unavailableItems;

    /**
     * The coupon error.
     *
     * @var array
     */
    protected $couponError;

    /**
     * {@inheritdoc}
     */
    public function save(array $options = [])
    {
        // Generate the cart ID if not available
        if (! $this->exists) {
            if (! isset($this->user_id)) {
                $userId = uniqid();
            } else {
                $userId = $this->user_id;
            }

            $this->id = Uuid::generate(5, $userId, Uuid::NS_DNS)->__toString();
        }

        return parent::save($options);
    }

    /**
     * Set unavilable items.
     *
     * @param  \Collection $items
     * @return self
     */
    public function setUnavailableItems(Collection $items)
    {
        $this->unavailableItems = $items;

        return $this;
    }

    /**
     * Set coupon error.
     *
     * @param  Array $error
     * @return self
     */
    public function setCouponError(Array $error)
    {
        $this->setRelation('coupon', null);

        $this->couponError = $error;

        return $this;
    }

    /**
     * Calculate.
     *
     * @param  \Collection $items
     * @return self
     */
    public function calculate(Collection $items)
    {
        $this->total_qty = 0;

        if (! $items->isEmpty()) {
            foreach ($items as $item) {
                $this->total_qty += $item->qty;
            }
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPresenter(Array $fields = ['*'])
    {
        if (! ($this->unavailableItems instanceOf Collection)) {
            $this->unavailableItems = new Collection();
        }

        if (! is_array($this->couponError)) {
            $this->couponError = [];
        }

        return new CartPresenter($this, $fields, $this->unavailableItems, $this->couponError);
    }

}
