<?php

return [

  /*
  |--------------------------------------------------------------------------
  | Fields Format
  |--------------------------------------------------------------------------
  |
  */

  'field_formats' => [
    'identifier' => [
      'format'    => '[MERCHANT_CODE].[DATE format="Ymd"].[RAND_STRING num="true" uc="true" lc="true" length="8"]', 
      'formatter' => \SunnyDayInc\Shop\Invoice\Formatter\Identifier::class
    ], 
    'code' => [
      'format'    => 'INV/[MERCHANT_CODE]/[DATE format="Ymd"]/[RAND_STRING num="true" length="8"]', 
      'formatter' => \SunnyDayInc\Shop\Invoice\Formatter\Code::class
    ]
  ]

];
