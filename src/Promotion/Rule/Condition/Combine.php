<?php namespace SunnyDayInc\Shop\Promotion\Rule\Condition;

use SunnyDayInc\Shop\Promotion\Rule\Rule;

class Combine extends Rule
{

    const AGGREGATOR_ALL = 'all';
    const AGGREGATOR_ANY = 'any';

    /**
     * Available aggregators.
     *
     * @var array
     */
    protected $aggregators;

    /**
     * Create a new instance.
     *
     * @param  array $data
     * @return void
     */
    public function __construct(Array $data)
    {
        parent::__construct($data);

        $this->aggregators = [self::AGGREGATOR_ALL, self::AGGREGATOR_ANY];
    }

    /**
     * Return available aggregators.
     *
     * @return array
     */
    public function getAggregators()
    {
        return $this->aggregators;
    }

    /**
     * Check if applicable.
     *
     * @param  mixed       $item
     * @param  \Collection $items
     * @return boolean
     */
    public function isApplicable($item, $items = [])
    {
        $isApplicable      = false;
        $totalRules        = 0;
        $totalValidRules   = 0;
        $totalInValidRules = 0;

        // Validate child rules
        if (isset($this->conditions)) {
            if (! $this->conditions->isEmpty()) {
                $totalRules = $this->conditions->count();

                foreach ($this->conditions as $rule) {
          
                    if ($rule->isApplicable($item, $items)) {
                        $totalValidRules += 1;
                    } else {
                        $totalInValidRules += 1;
                    }

                    unset($rule);
          
                }
            }
        }

        // Validate based on child rules result
        switch ($this->aggregator) {
        case self::AGGREGATOR_ALL:
        
            switch ($this->value) {
            case 1:
                $isApplicable = $totalValidRules === $totalRules;
                break;
          
            default:
                $isApplicable = $totalInValidRules === $totalRules;
                break;
            }

            break;
      
        case self::AGGREGATOR_ANY:
        
            switch ($this->value) {
            case 1:
                $isApplicable = $totalValidRules > 0;
                break;
          
            default:
                $isApplicable = $totalInValidRules > 0;
                break;
            }

            break;
        }

        unset($totalRules);
        unset($totalValidRules);
        unset($totalInValidRules);

        return $isApplicable;
    }

}
