<?php namespace SunnyDayInc\Shop\Support;

class Str
{

    /**
     * Generate a random string.
     *
     * @param
     * len: int number char to return
     * num: boolean, to include numeric
     * uc: boolean, to include uppercase char
     * lc: boolean, to include lowercase char
     * oc: boolean, to include others char => !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~
     * @return string
     */
    public static function random($len = 50, $num = true, $uc = true, $lc = true, $oc = false)
    {
        if (!$len || $len < 1 || $len > 100) {
            $len = 100; 
        }

        $s = '';
        $i = 0;
        do {
            switch(mt_rand(1, 4)) {
            // get number - ASCII characters (0:48 through 9:57)
            case 1:
                if ($num) {
                    $s .= chr(mt_rand(48, 57));
                    $i++;
                }
                break;
            // get uppercase letter - ASCII characters (a:65 through z:90)
            case 2:
                if ($uc) {
                    $s .= chr(mt_rand(65, 90));
                    $i++;
                }
                break;
            // get lowercase letter - ASCII characters (A:97 through Z:122)
            case 3:
                if ($lc) {
                    $s .= chr(mt_rand(97, 122));
                    $i++;
                }
                break;
            // get other characters - ASCII characters
            // !"#$%&'()*+,-./ :;<=>?@ [\]^_` {|}~
            // (33-47, 58-64, 91-96, 123-126)
            case 4:
                if ($oc) {
                    switch(mt_rand(1, 4)) {
                    case 1:
                        $s .= "&#" . mt_rand(33, 47) . ";";
                        $i++;
                        break;
                    case 2:
                        $s .= "&#" . mt_rand(58, 64) . ";";
                        $i++;
                        break;
                    case 3:
                        $s .= "&#" . mt_rand(91, 96) . ";";
                        $i++;
                        break;
                    case 4:
                        $s .= "&#" . mt_rand(123, 126) . ";";
                        $i++;
                        break;
                    }
                }
                break;
            }
        } while ($i < $len);

        return $s;
    }

}
