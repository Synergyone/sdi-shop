<?php namespace SunnyDayInc\Shop\Promotion\Rule;

use JsonSerializable;
use SunnyDayInc\Shop\Contracts\Arrayable;
use SunnyDayInc\Shop\Contracts\Jsonable;
use SunnyDayInc\Shop\Support\Collection;

abstract class Rule implements RuleInterface, Arrayable, Jsonable, JsonSerializable
{

    const OP_EQUAL                 = '==';
    const OP_NOT_EQUAL             = '!=';
    const OP_EQUAL_OR_GREATER_THAN = '>=';
    const OP_EQUAL_OR_LESS_THAN    = '<=';
    const OP_GREATER_THAN          = '>';
    const OP_LESS_THAN             = '<';
    const OP_CONTAINS              = '{}';
    const OP_NOT_CONTAINS          = '!{}';
    const OP_IS_ONE_OF             = '()';
    const OP_IS_NOT_ONE_OF         = '!()';

    /**
     * Defines which operators will be available for this condition
     *
     * @var string
     */
    protected $inputType;

    /**
     * The attribute.
     *
     * @var string
     */
    protected $attribute;

    /**
     * The attribute.
     *
     * @var string
     */
    protected $operator;

    /**
     * The attribute.
     *
     * @var mixed
     */
    protected $value;

    /**
     * The attribute.
     *
     * @var boolean
     */
    protected $isValueProcessed = false;

    /**
     * The attribute.
     *
     * @var string
     */
    protected $aggregator;

    /**
     * Available attributes.
     *
     * @var array
     */
    protected $attributes;

    /**
     * Available operators.
     *
     * @var array
     */
    protected $operators;

    /**
     * Operator type by attribute.
     *
     * @var array
     */
    protected $operatorTypeByAttribute;

    /**
     * The attribute.
     *
     * @var \Collection|\SunnyDayInc\Shop\Promotion\Repository\Rule\RuleInterface[]
     */
    protected $conditions;

    protected $possibleOperatorInputByType = [
    'string'      => [self::OP_EQUAL, self::OP_NOT_EQUAL, self::OP_EQUAL_OR_GREATER_THAN, self::OP_GREATER_THAN, self::OP_EQUAL_OR_LESS_THAN, self::OP_LESS_THAN, self::OP_CONTAINS, self::OP_NOT_CONTAINS, self::OP_IS_ONE_OF, self::OP_IS_NOT_ONE_OF], 
    'numeric'     => [self::OP_EQUAL, self::OP_NOT_EQUAL, self::OP_EQUAL_OR_GREATER_THAN, self::OP_GREATER_THAN, self::OP_EQUAL_OR_LESS_THAN, self::OP_LESS_THAN, self::OP_IS_ONE_OF, self::OP_IS_NOT_ONE_OF], 
    'date'        => [self::OP_EQUAL, self::OP_EQUAL_OR_GREATER_THAN, self::OP_EQUAL_OR_LESS_THAN], 
    'select'      => [self::OP_EQUAL, self::OP_NOT_EQUAL], 
    'boolean'     => [self::OP_EQUAL, self::OP_NOT_EQUAL], 
    'multiselect' => [self::OP_CONTAINS, self::OP_NOT_CONTAINS, self::OP_IS_ONE_OF, self::OP_IS_NOT_ONE_OF], 
    'grid'        => [self::OP_IS_ONE_OF, self::OP_IS_NOT_ONE_OF], 
    'category'    => [self::OP_EQUAL, self::OP_NOT_EQUAL, self::OP_CONTAINS, self::OP_NOT_CONTAINS, self::OP_IS_ONE_OF, self::OP_IS_NOT_ONE_OF]
    ];

    /**
     * List of input types for values which should be array
     * 
     * @var array
     */
    protected $arrayInputTypes = ['multiselect', 'grid'];

    /**
     * Create a new instance.
     *
     * @param  array $data
     * @return void
     */
    public function __construct(Array $data)
    {
        $this->conditions = new Collection();

        if (! empty($data)) {
            foreach ($data as $key => $value) {
                $this->$key = $value;
            }
        }
    }

    /**
     * Return available attributes.
     *
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Return available operators.
     *
     * @return array
     */
    public function getOperators()
    {
        $type = $this->operatorTypeByAttribute[$this->attribute];
        return $this->possibleOperatorInputByType[$type];
    }

    /**
     * Insert a child rule.
     *
     * @param  \SunnyDayInc\Shop\Promotion\Rule\RuleInterface $rule
     * @return void
     */
    public function addChild(RuleInterface $rule)
    {
        $this->conditions->push($rule);
    }

    /**
     * Check if having child rules.
     *
     * @return boolean
     */
    public function hasChildren()
    {
        return (!$this->conditions->isEmpty());
    }

    /**
     * Return the child rules.
     *
     * @return \Collection
     */
    public function getChildren()
    {
        return $this->conditions;
    }

    /**
     * Retrieve input type
     *
     * @return string
     */
    public function getInputType()
    {
        return $this->operatorTypeByAttribute[$this->attribute];
    }

    /**
     * Get the vars into a plain array.
     *
     * @return array
     */
    public function toArray()
    {
        $data = [
        'rule' => get_class($this)
        ];

        if (! is_null($this->attribute)) {
            $data['attribute'] = $this->attribute;
        }

        if (! is_null($this->operator)) {
            $data['operator'] = $this->operator;
        }

        if (! is_null($this->value)) {
            $data['value'] = $this->value;
        }

        if (! is_null($this->isValueProcessed)) {
            $data['isValueProcessed'] = $this->isValueProcessed;
        }

        if (! is_null($this->aggregator)) {
            $data['aggregator'] = $this->aggregator;
        }

        if (! $this->conditions->isEmpty()) {
            $data['conditions'] = [];
      
            foreach ($this->conditions as $condition) {
                $data['conditions'][] = $condition->jsonSerialize();
            }
        }

        return $data;
    }

    /**
     * Convert the object into something JSON serializable.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }

    /**
     * Get the vars as JSON.
     *
     * @param  int $options
     * @return string
     */
    public function toJson($options = 0)
    {
        return json_encode($this->toArray(), $options);
    }

    /**
     * Convert the collection to its string representation.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->toJson();
    }

    /**
     * Validate value by operator.
     *
     * @param  mixed $data
     * @return boolean
     */
    protected function validateValueByOperator($data)
    {
        $parsedValue = $this->getValueParsed();

        // if operator requires array and it is not, or on opposite, return false
        if ($this->isArrayOperatorType() xor is_array($parsedValue)) {
            return false;
        }

        $isValid = false;

        switch ($this->operator) {
        case self::OP_EQUAL: case self::OP_NOT_EQUAL:
                if (is_array($parsedValue)) {
                    if (is_array($data)) {
                        $isValid = array_intersect($parsedValue, $data);
                        $isValid = !empty($isValid);
                    } else {
                        return false;
                    }
                } else {
                    if (is_array($data)) {
                        $isValid = count($data) == 1 && array_shift($data) == $parsedValue;
                    } else {
                        $isValid = $this->compareValues($data, $parsedValue);
                    }
                }
            break;

        case self::OP_EQUAL_OR_LESS_THAN: case self::OP_GREATER_THAN:
                if (!is_scalar($data)) {
                    return false;
                } else {
                    $isValid = $data <= $parsedValue;
                }
            break;

        case self::OP_EQUAL_OR_GREATER_THAN: case self::OP_LESS_THAN:
                if (!is_scalar($data)) {
                    return false;
                } else {
                    $isValid = $data >= $parsedValue;
                }
            break;

        case self::OP_CONTAINS: case self::OP_NOT_CONTAINS:
                if (is_scalar($data) && is_array($parsedValue)) {
                    foreach ($parsedValue as $item) {
                        if (stripos($data, $item) !== false) {
                            $isValid = true;
                            break;
                        }
                    }
                } elseif (is_array($parsedValue)) {
                    if (is_array($data)) {
                        $isValid = array_intersect($parsedValue, $data);
                        $isValid = !empty($isValid);
                    } else {
                        return false;
                    }
                } else {
                    if (is_array($data)) {
                        $isValid = in_array($parsedValue, $data);
                    } else {
                        $isValid = $this->compareValues($data, $parsedValue, false);
                    }
                }
            break;

        case self::OP_IS_ONE_OF: case self::OP_IS_NOT_ONE_OF:
                if (is_array($data)) {
                    $isValid = count(array_intersect($data, (array)$parsedValue)) > 0;
                } else {
                    $parsedValue = (array)$parsedValue;
                    foreach ($parsedValue as $item) {
                        if ($this->compareValues($data, $item)) {
                            $isValid = true;
                            break;
                        }
                    }
                }
            break;
        }

        if (self::OP_NOT_EQUAL == $this->operator 
            || self::OP_GREATER_THAN == $this->operator 
            || self::OP_LESS_THAN == $this->operator 
            || self::OP_NOT_CONTAINS == $this->operator 
            || self::OP_IS_NOT_ONE_OF == $this->operator
        ) {
            $isValid = !$isValid;
        }

        return $isValid;
    }

    /**
     * Case and type insensitive comparison of values
     *
     * @param  string|int|float $data
     * @param  string|int|float $parsedValue
     * @return bool
     */
    protected function compareValues($data, $parsedValue, $strict = true)
    {
        if ($strict && is_numeric($data) && is_numeric($parsedValue)) {
            return $data == $parsedValue;
        } else {
            $validatePattern = preg_quote($data, '~');
            if ($strict) {
                $validatePattern = '^' . $validatePattern . '$';
            }

            return (bool)preg_match('~' . $validatePattern . '~iu', $parsedValue);
        }
    }

    /**
     * Retrieve parsed value
     *
     * @return array|string|int|float
     */
    public function getValueParsed()
    {
        if (! isset($this->valueParsed)) {
            $value = $this->value;
            if ($this->isArrayOperatorType() && is_string($value)) {
                $value = preg_split('#\s*[,;]\s*#', $value, null, PREG_SPLIT_NO_EMPTY);
            }
            $this->valueParsed = $value;
        }

        return $this->valueParsed;
    }

    /**
     * Check if value should be array
     *
     * Depends on operator input type
     *
     * @return bool
     */
    public function isArrayOperatorType()
    {
        return $this->operator === self::OP_IS_ONE_OF
        || $this->operator === self::OP_IS_NOT_ONE_OF
        || in_array($this->getInputType(), $this->arrayInputTypes);
    }

}
