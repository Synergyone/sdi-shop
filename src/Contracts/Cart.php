<?php namespace SunnyDayInc\Shop\Contracts;

interface Cart
{

    /**
     * Set the logger instance.
     *
     * @param  mixed $log
     * @return void
     */
    public function setLogger($log);

    /**
     * Return the repository instance.
     *
     * @return \SunnyDayInc\Shop\Cart\Repository\Cart
     */
    public function getRepository();

    /**
     * Create a new cart.
     *
     * @param  array $data
     * @return \SunnyDayInc\Shop\Cart\FactoryInterface
     */
    public function create(Array $data = []);

    /**
     * Find a cart by ID and return the factory instance.
     *
     * @param  string $id
     * @return \SunnyDayInc\Shop\Cart\FactoryInterface
     */
    public function find($id);

    /**
     * Find a cart by user ID and return the factory instance.
     *
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Cart\FactoryInterface
     */
    public function findByUser($id);

}
