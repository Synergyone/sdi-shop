<?php namespace App\Http\Middleware;

use Closure;

class LockCannotMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @param  string                   $permission
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        // Get a user instance
        // Only example, should be replaced with other solution
        $user = \App\User::find(1);

        if (is_object($user)) {
            // Enable the LockAware trait on the user.
            $user = app()->make('lock')->setLock($user);

            if (! $user->cannot($permission)) {
                abort(404);
            }
        }

        return $next($request);
    }

}
