<?php namespace SunnyDayInc\Shop\Contracts;

use \BeatSwitch\Lock\Callers\Caller;

interface Lock
{

    /**
     * Creates a new Lock instance for the given caller
     *
     * @param  \BeatSwitch\Lock\Callers\Caller $caller
     * @return \BeatSwitch\Lock\Callers\CallerLock
     */
    public function setLock(Caller $caller);

    /**
     * Return the manager instance.
     *
     * @return \BeatSwitch\Lock\Manager
     */
    public function getManager();

}
