<?php namespace SunnyDayInc\Shop\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

use SunnyDayInc\Shop\Models\CartItemInterface;
use SunnyDayInc\Shop\Models\CartItem as CartItemModel;
use SunnyDayInc\Shop\Contracts\Presentable;

class CartItem extends Model implements CartItemInterface, Presentable
{

    use CartItemModel;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cart_item';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'id', 
    'cart_id', 
    'object_type', 
    'object_id', 
    'branch_id', 
    'product_id', 
    'qty', 
    'comment'
    ];

}
