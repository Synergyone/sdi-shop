<?php namespace SunnyDayInc\Shop\Order\Formatter;

use SunnyDayInc\Shop\Support\Shortcode;
use SunnyDayInc\Shop\Support\Str;
use SunnyDayInc\Shop\Models\OrderInterface;
use DateTime;

abstract class Formatter implements FormatterInterface
{

    /**
     * The available shortcodes.
     *
     * @var array
     */
    protected $shortcodes = [
    'MERCHANT_CODE' => 'merchantCodeHandler', 
    'DATE'          => 'dateHandler', 
    'RAND_STRING'   => 'randomStringHandler'
    ];

    /**
     * The requested format.
     *
     * @var string
     */
    protected $format;

    /**
     * The order instance.
     *
     * @var \SunnyDayInc\Shop\Models\OrderInterface
     */
    protected $order;

    /**
     * Create a new instance.
     *
     * @param  string                                  $format
     * @param  \SunnyDayInc\Shop\Models\OrderInterface $order
     * @return void
     */
    public function __construct($format, OrderInterface $order)
    {
        $this->format = $format;
        $this->order = $order;

        foreach ($this->shortcodes as $key => $item) {
            $this->shortcodes[$key] = [&$this, $item];
        }
    }

    /**
     * {@inheritdoc}
     */
    public function compile()
    {
        return Shortcode::compile($this->shortcodes, $this->format);
    }

    /**
     * Return the formatted merchant code.
     *
     * @return string
     */
    public function merchantCodeHandler($attributes, $matches, $tag)
    {
        return $this->order->object_type.':'.$this->order->object_id;
    }

    /**
     * Return the formatted date.
     *
     * @return string
     */
    public function dateHandler($attributes, $matches, $tag)
    {
        $attributes = array_merge(
            [
            'format' => 'y-m-d'
            ], $attributes
        );

        return (new DateTime())->format($attributes['format']);
    }

    /**
     * Return the random string.
     *
     * @return string
     */
    public function randomStringHandler($attributes, $matches, $tag)
    {
        $attributes = array_merge(
            [
            'num'    => 'false', 
            'uc'     => 'false', 
            'lc'     => 'false', 
            'oc'     => 'false', 
            'length' => 8
            ], $attributes
        );

        $attributes['num'] = ($attributes['num'] === 'true') ? true : false;
        $attributes['uc'] = ($attributes['uc'] === 'true') ? true : false;
        $attributes['lc'] = ($attributes['lc'] === 'true') ? true : false;
        $attributes['oc'] = ($attributes['oc'] === 'true') ? true : false;

        return Str::random(
            $attributes['length'], 
            $attributes['num'], 
            $attributes['uc'], 
            $attributes['lc'], 
            $attributes['oc']
        );
    }

}
