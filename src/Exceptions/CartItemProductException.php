<?php

namespace SunnyDayInc\Shop\Exceptions;

use SunnyDayInc\Shop\Models\CartItemProductInterface;

class CartItemProductException extends SunnyDayIncException
{

    /**
     * The exception type
     */
    public $errorType = 'not_found';

    /**
     * The product instance
     *
     * @var \SunnyDayInc\Shop\Models\CartItemProductInterface
     */
    public $product;

    /**
     * Throw a new exception
     *
     * @param string                                            $msg     Exception Message
     * @param \SunnyDayInc\Shop\Models\CartItemProductInterface $product
     */
    public function __construct($msg, CartItemProductInterface $product)
    {
        parent::__construct($msg);

        $this->product = $product;
    }

    /**
     * Return the error type
     *
     * @return string
     */
    public function getErrorType()
    {
        return $this->errorType;
    }

    /**
     * Return the product instance
     *
     * @return \SunnyDayInc\Shop\Models\CartItemProductInterface
     */
    public function getProduct()
    {
        return $this->product;
    }

}
