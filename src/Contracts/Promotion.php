<?php namespace SunnyDayInc\Shop\Contracts;

interface Promotion
{

    /**
     * Set the logger instance.
     *
     * @param  mixed $log
     * @return void
     */
    public function setLogger($log);

    /**
     * Return the repository instance.
     *
     * @return \SunnyDayInc\Shop\Promotion\Repository\Promotion
     */
    public function getRepository();

    /**
     * Return available catalog actions.
     *
     * @return array
     */
    public function getCatalogActions();

    /**
     * Return available cart actions.
     *
     * @return array
     */
    public function getCartActions();

    /**
     * Create a new promotion.
     *
     * @param  array $data
     * @return \SunnyDayInc\Shop\Promotion\FactoryInterface
     */
    public function create(Array $data = []);

    /**
     * Find a promotion by ID and return the factory instance.
     *
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Promotion\FactoryInterface
     */
    public function find($id);

    /**
     * Find a promotion by object type, object ID, and it's ID
     * then return the factory instance.
     *
     * @param  string  $objectType
     * @param  integer $objectId
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Promotion\FactoryInterface
     */
    public function findByObject($objectType, $objectId, $id);

}
