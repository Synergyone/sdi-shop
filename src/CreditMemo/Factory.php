<?php namespace SunnyDayInc\Shop\CreditMemo;

use SunnyDayInc\Shop\CreditMemo\Repository\CreditMemo as CreditMemoRepository;

use SunnyDayInc\Shop\Support\Collection;

use SunnyDayInc\Shop\Models\CreditMemoInterface;
use SunnyDayInc\Shop\Models\CreditMemoItemInterface;

use Illuminate\Contracts\Logging\Log;

class Factory implements FactoryInterface
{

    /**
     * The log context.
     *
     * @var string
     */
    const LOG_CONTEXT = 'creditmemo.factory';

    /**
     * The repository instance.
     *
     * @var \SunnyDayInc\Shop\CreditMemo\Repository\CreditMemo
     */
    protected $repository;

    /**
     * The field formats.
     *
     * @var array
     */
    protected $fieldFormats;

    /**
     * The credit memo model instance.
     *
     * @var \SunnyDayInc\Shop\Models\CreditMemoInterface
     */
    protected $creditMemo;

    /**
     * The credit memo items.
     *
     * @var \Collection|\SunnyDayInc\Shop\Models\CreditMemoItemInterface[]
     */
    protected $items;

    /**
     * The credit memo comment.
     *
     * @var \SunnyDayInc\Shop\Models\CreditMemoCommentInterface
     */
    protected $comment;

    /**
     * Create a new instance.
     *
     * @param  \SunnyDayInc\Shop\CreditMemo\Repository\CreditMemo $repository
     * @param  array                                              $fieldFormats
     * @param  array                                              $data
     * @return void
     */
    public function __construct(CreditMemoRepository $repository, Array $fieldFormats, Array $data = [])
    {
        $this->repository = $repository;
        $this->fieldFormats = $fieldFormats;

        $this->items = new Collection();
    
        // Create the credit memo model and fill the default data
        if (! empty($data)) {
            $this->creditMemo = $this->repository->createModel();
            $this->creditMemo->fill($data);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setLogger(Log $log)
    {
        $this->log = $log;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setCreditMemo(CreditMemoInterface $creditMemo)
    {
        $this->creditMemo = $creditMemo;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCreditMemo()
    {
        return $this->creditMemo;
    }

    /**
     * {@inheritdoc}
     */
    public function getItems()
    {
        if (is_null($this->creditMemo)) {
            return $this->items;
        }

        $creditMemoId = (isset($this->creditMemo->id)) ? $this->creditMemo->id : 0;

        // Because the credit memo is not yet saved
        // we cannot identify any item by it's ID
        if (empty($creditMemoId)) {
            return $this->items;
        }

        // Fetch all items from repository
        if ($this->items->isEmpty()) {
            $items = $this->repository->findItemsByCreditMemo($creditMemoId);

            if (! $items->isEmpty()) {
                foreach ($items as $item) {
                    $this->items->put($item->id, $item);
                }
            }

            unset($items);
        }

        return $this->items;
    }

    /**
     * {@inheritdoc}
     */
    public function getItem($id)
    {
        if (is_null($this->creditMemo)) {
            return null;
        }

        $creditMemoId = (isset($this->creditMemo->id)) ? $this->creditMemo->id : 0;

        // Because the credit memo is not yet saved
        // we cannot identify any item by it's ID
        if (empty($creditMemoId)) {
            return null;
        }

        if (! $this->items->isEmpty()) {
            $item = $this->items->get($id);

            if ($item instanceOf CreditMemoItemInterface) {
                return $item;
            }
        }

        $item = $this->repository->findItemByCreditMemoAndId($creditMemoId, $id);

        if (! ($item instanceOf CreditMemoItemInterface)) {
            return null;
        }

        $this->items->put($item->id, $item);

        return $item;
    }

    /**
     * {@inheritdoc}
     */
    public function addItem(CreditMemoItemInterface $item)
    {
        // Set the credit memo ID if available
        if (! empty($this->creditMemo->id)) {
            $item->credit_memo_id = $this->creditMemo->id;
        }

        // Add item to collection
        $this->items->push($item);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function createItem()
    {
        return $this->repository->createModelItem();
    }

    /**
     * {@inheritdoc}
     */
    public function setComment($comment, $notifyCustomer = false, $visibleInFront = false)
    {
        $this->comment = $this->repository->createModelComment();

        // Set the credit memo ID if available
        if (! empty($this->creditMemo->id)) {
            $this->comment->credit_memo_id = $this->creditMemo->id;
        } else {
            $this->comment->credit_memo_id = 0;
        }

        $this->comment->comment              = $comment;
        $this->comment->is_customer_notified = $notifyCustomer;
        $this->comment->is_visible_on_front  = $visibleInFront;

        // Save directly if credit memo exist
        if (! empty($this->comment->credit_memo_id)) {
            $this->comment->save();
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function save()
    {
        $connection = $this->creditMemo->getConnection();

        // Initiate transaction
        $connection->beginTransaction();

        $isSaved = true;

        // Save the credit memo
        // Do field formatters if available only when it's new credit memo
        if (! $this->creditMemo->exists && ! empty($this->fieldFormats)) {
            foreach ($this->fieldFormats as $field => $format) {
                $handler = new $format['formatter']($format['format'], $this->creditMemo);

                $this->creditMemo->$field = $handler->compile();

                unset($handler);
            }
        }

        // Calculate credit memo pricing
        $this->creditMemo->calculate($this->items);
    
        try {
            $isSaved = $this->creditMemo->save();
        } catch (\ErrorException $e) {
            $this->createLog('error', $e);

            $isSaved = false;
        }

        if (! $isSaved) {
            // Rollback the transaction
            $connection->rollBack();

            return false;
        }

        // Get the increment credit memo ID
        $creditMemoId = $this->creditMemo->id;

        // Save all items
        if (! $this->saveItems()) {
            // Rollback the transaction
            $connection->rollBack();

            return false;
        }

        // Save comment if available
        if (! is_null($this->comment)) {
            $this->comment->credit_memo_id = $creditMemoId;
      
            try {
                $isSaved = $this->comment->save();
            } catch (\ErrorException $e) {
                $this->createLog('error', $e);

                $isSaved = false;
            }

            if (! $isSaved) {
                // Rollback the transaction
                $connection->rollBack();

                return false;
            }
        }

        // Commit the transaction
        $connection->commit();

        return true;
    }

    /**
     * Save all items.
     *
     * @return boolean
     */
    private function saveItems()
    {
        $isAllSaved = true;

        if ($this->items instanceOf Collection) {
            foreach ($this->items as $key => $item) {
                $isSaved = true;

                // Assign credit memo ID
                $this->items[$key]->credit_memo_id = $this->creditMemo->id;

                try {
                    $isSaved = $this->items[$key]->save();
                } catch (\ErrorException $e) {
                    $this->createLog('error', $e);

                    $isSaved = false;
                }

                if (! $isSaved) {
                    $isAllSaved = false;
                    break;
                }
            }
        }

        return $isAllSaved;
    }

    /**
     * Create a new log.
     *
     * @param  string $level
     * @param  mixed  $message
     * @param  Array  $data
     * @return void
     */
    private function createLog($level, $message, Array $data = [])
    {
        if (! is_null($this->log)) {
            $data['context'] = self::LOG_CONTEXT;

            if ($message instanceOf \ErrorException) {
                $this->log->$level($message->getCode().': '.$message->getMessage()."\n".$message->getTraceAsString(), $data);
            } else {
                $this->log->$level($message, $data);
            }
        }
    }

}
