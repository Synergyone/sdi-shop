<?php namespace SunnyDayInc\Shop\Contracts;

use SunnyDayInc\Shop\Mail\TemplateInterface;
use Closure;

interface Mailer
{

    /**
     * Set the global from address and name.
     *
     * @param  string      $address
     * @param  string|null $name
     * @return void
     */
    public function alwaysFrom($address, $name = null);

    /**
     * Send a new message.
     *
     * @param  \SunnyDayInc\Shop\Mail\TemplateInterface $template
     * @param  \Closure                                 $callback
     * @return mixed
     */
    public function send(TemplateInterface $template, Closure $callback);

    /**
     * Return the requested email template.
     *
     * @param  string $name
     * @return \SunnyDayInc\Shop\Mail\TemplateInterface
     * @throws \Exception
     */
    public function getTemplate($name);

}
