<?php namespace SunnyDayInc\Shop\Address\Repository;

class EloquentAddress extends Address
{

    /**
     * The model object.
     *
     * @var \SunnyDayInc\Shop\Contracts\Model
     */
    protected $model;

    /**
     * The current total row in previous query.
     *
     * @var integer
     */
    protected $total = 0;

    /**
     * Create a new instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->createModel();
    }

    /**
     * {@inheritdoc}
     */
    public function create(Array $data)
    {
        $model = $this->model->newInstance();
    
        $model->fill($data);

        if ($model->save()) {
            return $model->fresh();
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        return $this->model->find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function findSimilar(Array $where)
    {
        $query = $this->model->newQuery();

        foreach ($where as $key => $value) {
            $query->where($key, '=', $value);
        }

        return $query->first();
    }

    /**
     * {@inheritdoc}
     */
    public function findAllBy(
        Array $where = [], 
        Array $column = ['*'], 
        $page = 1, 
        $limit = 10, 
        $order = 'DESC', 
        $orderBy = 'created_at'
    ) {
        $where = array_merge(
            [
            'object_type' => '', 
            'object_id'   => 0, 
            'type'        => '', 
            'search'      => '', 
            'is_default'  => null, 
            'is_active'   => null
            ], $where
        );

        if (empty($page)) {
            $page = 1;
        }

        $useWhere = false;
        $hasWhere = false;

        $fromSql = '(';
        $fromSql .= 'SELECT `'.$this->model->getTable().'`.`id`';
        $fromSql .= ' FROM `'.$this->model->getTable().'`';

        if (! empty($where['object_type'])
            || ! empty($where['object_id'])
            || ! empty($where['type'])
            || ! empty($where['search'])
            || ! is_null($where['is_default'])
            || ! is_null($where['is_active'])
        ) {
            $useWhere = true;
        }

        if ($useWhere) {
            $fromSql .= ' WHERE';
        }

        if (! empty($where['object_type'])) {
            $fromSql .= ' `'.$this->model->getTable().'`.`object_type` = "'.$where['object_type'].'"';

            $hasWhere = true;
        }

        if (! empty($where['object_id'])) {
            if ($hasWhere) {
                $fromSql .= ' AND';
            }

            $fromSql .= ' `'.$this->model->getTable().'`.`object_id` = '.$where['object_id'];

            $hasWhere = true;
        }

        if (! empty($where['type'])) {
            if ($hasWhere) {
                $fromSql .= ' AND';
            }

            $fromSql .= ' `'.$this->model->getTable().'`.`type` = "'.$where['type'].'"';

            $hasWhere = true;
        }

        if (! empty($where['search'])) {
            if ($hasWhere) {
                $fromSql .= ' AND';
            }

            $fromSql .= ' (`'.$this->model->getTable().'`.`title` = "'.$where['search'].'"';
            $fromSql .= ' OR `'.$this->model->getTable().'`.`company` = "'.$where['search'].'"';
            $fromSql .= ' OR `'.$this->model->getTable().'`.`phone` = "'.$where['search'].'"';
            $fromSql .= ' OR `'.$this->model->getTable().'`.`mobile_phone` = "'.$where['search'].'"';
            $fromSql .= ' OR `'.$this->model->getTable().'`.`address` = "'.$where['search'].'")';

            $hasWhere = true;
        }

        if (! is_null($where['is_default'])) {
            if ($hasWhere) {
                $fromSql .= ' AND';
            }

            $fromSql .= ' `'.$this->model->getTable().'`.`is_default` = '.$where['is_default'];

            $hasWhere = true;
        }

        if (! is_null($where['is_active'])) {
            if ($hasWhere) {
                $fromSql .= ' AND';
            }

            $fromSql .= ' `'.$this->model->getTable().'`.`is_active` = '.$where['is_active'];

            $hasWhere = true;
        }

        if (! empty($order) && ! empty($orderBy)) {
            $fromSql .= ' ORDER BY `'.$this->model->getTable().'`.`'.$orderBy.'` '.$order;
        }

        if ($limit > 0) {
            $fromSql .= ' limit '.$limit.' offset '.($page - 1) * $limit;
        }

        $fromSql .= ') o';
  
        if ($limit > 0) {
            $query = $this->model->newQuery()->select('id');

            if (! empty($where['object_type'])) {
                $query->where('object_type', '=', $where['object_type']);
            }

            if (! empty($where['object_id'])) {
                $query->where('object_id', '=', $where['object_id']);
            }

            if (! empty($where['type'])) {
                $query->where('type', '=', $where['type']);
            }

            if (! empty($where['search'])) {
                $searchText = $where['search'];

                $query->where(
                    function ($qry) use ($searchText) {
                        $qry->where('title', 'LIKE', '%'.$searchText.'%')
                            ->orWhere('company', 'LIKE', '%'.$searchText.'%')
                            ->orWhere('phone', 'LIKE', '%'.$searchText.'%')
                            ->orWhere('mobile_phone', 'LIKE', '%'.$searchText.'%')
                            ->orWhere('address', 'LIKE', '%'.$searchText.'%');
                    }
                );
            }

            if (! is_null($where['is_default'])) {
                $query->where('is_default', '=', $where['is_default']);
            }

            if (! is_null($where['is_active'])) {
                $query->where('is_active', '=', $where['is_active']);
            }

            $this->total = $query->count();
        } else {
            $this->total = 0;
        }

        $query = $this->model->newQuery()
            ->from(app('db')->raw($fromSql))
            ->join($this->model->getTable(), $this->model->getTable().'.id', '=', 'o.id');

        if (! empty($order) && ! empty($orderBy)) {
            $query->orderBy($this->model->getTable().'.'.$orderBy, $order);
        }

        unset($fromSql);
        unset($model);

        return $query->get($column);
    }

    /**
     * {@inheritdoc}
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * {@inheritdoc}
     */
    public function model()
    {
        return '\SunnyDayInc\Shop\Models\Eloquent\Address';
    }

    /**
     * Return the model object creation.
     *
     * @return \SunnyDayInc\Shop\Contracts\Model
     */
    public function createModel()
    {
        if (! is_null($this->model)) {
            return $this->model;
        }

        $model = $this->model();

        return $this->model = new $model;
    }

}
