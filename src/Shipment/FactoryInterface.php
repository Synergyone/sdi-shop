<?php namespace SunnyDayInc\Shop\Shipment;

use SunnyDayInc\Shop\Models\ShipmentInterface;
use SunnyDayInc\Shop\Models\ShipmentTrackInterface;

use Illuminate\Contracts\Logging\Log;

interface FactoryInterface
{

    /**
     * Set the logger instance.
     *
     * @param  \Illuminate\Contracts\Logging\Log $log
     * @return self
     */
    public function setLogger(Log $log);

    /**
     * Set the shipment instance.
     *
     * @param  \SunnyDayInc\Shop\Models\ShipmentInterface $shipment
     * @return self
     */
    public function setShipment(ShipmentInterface $shipment);

    /**
     * Return the shipment instance.
     *
     * @return \SunnyDayInc\Shop\Models\ShipmentInterface
     */
    public function getShipment();

    /**
     * Return the tracks.
     *
     * @return \Collection|\SunnyDayInc\Shop\Models\ShipmentTrackInterface[]
     */
    public function getTracks();

    /**
     * Insert a new track.
     *
     * @param  \SunnyDayInc\Shop\Models\ShipmentTrackInterface $track
     * @return self
     */
    public function addTrack(ShipmentTrackInterface $track);

    /**
     * Create a new shipment track.
     *
     * @return \SunnyDayInc\Shop\Models\ShipmentTrackInterface
     */
    public function createTrack();

    /**
     * Create a new shipment item.
     *
     * @return \SunnyDayInc\Shop\Models\ShipmentItemInterface
     */
    public function createItem();

    /**
     * Set comment.
     *
     * @param  string  $comment
     * @param  boolean $notifyCustomer
     * @param  boolean $visibleInFront
     * @return self
     */
    public function setComment($comment, $notifyCustomer = false, $visibleInFront = false);

    /**
     * Save the shipment.
     *
     * @return boolean
     */
    public function save();

}
