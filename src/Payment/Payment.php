<?php namespace SunnyDayInc\Shop\Payment;

use SunnyDayInc\Shop\Contracts\Payment as PaymentInterface;
use SunnyDayInc\Shop\Payment\Repository\Payment as PaymentRepository;
use SunnyDayInc\Shop\Invoice\FactoryInterface as InvoiceInterface;

class Payment implements PaymentInterface
{

    const STATE_PENDING   = 'pending';
    const STATE_CONFIRMED = 'confirmed';
    const STATE_HOLDED    = 'holded';
    const STATE_ABANDONED = 'abandoned';
    const STATE_CANCELLED = 'cancelled';
    const STATE_RETURNED  = 'returned';
    const STATE_COMPLETED = 'completed';

    /**
     * The repository instance.
     *
     * @var \SunnyDayInc\Shop\Payment\Repository\Payment
     */
    protected $repository;

    /**
     * The log instance.
     *
     * @var mixed
     */
    protected $log;

    /**
     * The field formats.
     *
     * @var array
     */
    protected $fieldFormats = [];

    /**
     * Create a new instance.
     *
     * @param  \SunnyDayInc\Shop\Payment\Repository\Payment $repository
     * @return void
     */
    public function __construct(PaymentRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    public function setLogger($log)
    {
        $this->log = $log;
    }

    /**
     * {@inheritdoc}
     */
    public function addFieldFormat($field, Array $format)
    {
        $this->fieldFormats[$field] = $format;
    }

    /**
     * {@inheritdoc}
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * {@inheritdoc}
     */
    public function createFromInvoice(InvoiceInterface $invoice, Array $options = [])
    {
        $options = array_merge([
            'method' => 'bank_transfer', 
            'note'   => '', 
            'status' => self::STATE_PENDING
        ], $options);

        // Get the invoice model
        $model = $invoice->getInvoice();

        // Create a new payment
        $payment = $this->createFactory([
            'object_type'              => $model->object_type, 
            'object_id'                => $model->object_id, 
            'order_id'                 => $model->order_id, 
            'invoice_id'               => $model->id, 
            'shipping_amount'          => $model->shipping_amount, 
            'base_shipping_amount'     => $model->base_shipping_amount, 
            'amount_authorized'        => $model->grand_total, 
            'base_amount_authorized'   => $model->base_grand_total, 
            'amount_unique_authorized' => $model->grand_total, 
            'method'                   => $options['method'], 
            'note'                     => $options['note'], 
            'status'                   => $options['status']
        ]);

        unset($model);

        // Save the payment
        if ( ! $payment->save()) {
            return false;
        }

        return $payment;
    }

    /**
     * Create the factory instance.
     *
     * @param  array $data
     * @return \SunnyDayInc\Shop\Payment\FactoryInterface
     */
    private function createFactory(Array $data = [])
    {
        $factory = new Factory($this->repository, $this->fieldFormats, $data);

        if (! is_null($this->log)) {
            $factory->setLogger($this->log);
        }

        return $factory;
    }

}
