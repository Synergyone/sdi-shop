<?php namespace SunnyDayInc\Shop\Shipment\Repository;

class EloquentShipment extends Shipment
{

    /**
     * The current total row in previous query.
     *
     * @var integer
     */
    protected $total = 0;

    /**
     * {@inheritdoc}
     */
    public function findBy(
        Array $where = [], 
        Array $column = ['*'], 
        $page = 1, 
        $limit = 10, 
        $order = 'DESC', 
        $orderBy = 'created_at'
    ) {
        $where = array_merge(
            [
            'object_type' => '', 
            'object_id'   => 0, 
            'type'        => '', 
            'date'        => '', 
            'is_coupon'   => null, 
            'is_active'   => null
            ], $where
        );

        if (empty($page)) {
            $page = 1;
        }

        $model = $this->createModel();

        $useWhere = false;
        $hasWhere = false;

        $fromSql = '(';
        $fromSql .= 'SELECT `'.$model->getTable().'`.`id`';
        $fromSql .= ' FROM `'.$model->getTable().'`';

        if (! empty($where['object_type'])
            || ! empty($where['object_id'])
            || ! empty($where['type'])
            || ! empty($where['date'])
            || ! is_null($where['is_coupon'])
            || ! is_null($where['is_active'])
        ) {
            $useWhere = true;
        }

        if ($useWhere) {
            $fromSql .= ' WHERE';
        }

        if (! empty($where['object_type'])) {
            $fromSql .= ' `'.$model->getTable().'`.`object_type` = "'.$where['object_type'].'"';

            $hasWhere = true;
        }

        if (! empty($where['object_id'])) {
            if ($hasWhere) {
                $fromSql .= ' AND';
            }

            $fromSql .= ' `'.$model->getTable().'`.`object_id` = '.$where['object_id'];

            $hasWhere = true;
        }

        if (! empty($where['type'])) {
            if ($hasWhere) {
                $fromSql .= ' AND';
            }

            $fromSql .= ' `'.$model->getTable().'`.`type` = "'.$where['type'].'"';

            $hasWhere = true;
        }

        if (! empty($where['date'])) {
            if ($hasWhere) {
                $fromSql .= ' AND';
            }

            $fromSql .= ' (`'.$model->getTable().'`.`from_date` <= "'.$where['date'].'"';
            $fromSql .= ' AND `'.$model->getTable().'`.`to_date` >= "'.$where['date'].'")';

            $hasWhere = true;
        }

        if (! is_null($where['is_coupon'])) {
            if ($hasWhere) {
                $fromSql .= ' AND';
            }

            $fromSql .= ' `'.$model->getTable().'`.`is_coupon` = '.$where['is_coupon'];

            $hasWhere = true;
        }

        if (! is_null($where['is_active'])) {
            if ($hasWhere) {
                $fromSql .= ' AND';
            }

            $fromSql .= ' `'.$model->getTable().'`.`is_active` = '.$where['is_active'];

            $hasWhere = true;
        }

        if (! empty($order) && ! empty($orderBy)) {
            $fromSql .= ' ORDER BY `'.$model->getTable().'`.`'.$orderBy.'` '.$order;
        }

        if ($limit > 0) {
            $fromSql .= ' limit '.$limit.' offset '.($page - 1) * $limit;
        }

        $fromSql .= ') o';

        if ($limit > 0) {
            $query = $model->newQuery()->select('id');

            if (! empty($where['object_type'])) {
                $query->where('object_type', '=', $where['object_type']);
            }

            if (! empty($where['object_id'])) {
                $query->where('object_id', '=', $where['object_id']);
            }

            if (! empty($where['type'])) {
                $query->where('type', '=', $where['type']);
            }

            if (! empty($where['date'])) {
                $query->where('from_date', '<=', $where['date']);
                $query->where('from_date', '>=', $where['date']);
            }

            if (! is_null($where['is_coupon'])) {
                $query->where('is_coupon', '=', $where['is_coupon']);
            }

            if (! is_null($where['is_active'])) {
                $query->where('is_active', '=', $where['is_active']);
            }

            $this->total = $query->count();
        } else {
            $this->total = 0;
        }

        $query = $model->newQuery()
            ->from(\DB::raw($fromSql))
            ->join($model->getTable(), $model->getTable().'.id', '=', 'o.id');

        if (! empty($order) && ! empty($orderBy)) {
            $query->orderBy($model->getTable().'.'.$orderBy, $order);
        }

        unset($fromSql);
        unset($model);

        return $query->get($column);
    }

    /**
     * {@inheritdoc}
     */
    public function findTracksByShipment($shipmentId)
    {
        $query = $this->createModelTrack()->newQuery();
        $query->where('shipment_id', '=', $shipmentId);

        return $query->get();
    }

    /**
     * {@inheritdoc}
     */
    public function model()
    {
        return '\SunnyDayInc\Shop\Models\Eloquent\Shipment';
    }

    /**
     * {@inheritdoc}
     */
    public function modelTrack()
    {
        return '\SunnyDayInc\Shop\Models\Eloquent\ShipmentTrack';
    }

    /**
     * {@inheritdoc}
     */
    public function modelItem()
    {
        return '\SunnyDayInc\Shop\Models\Eloquent\ShipmentItem';
    }

    /**
     * {@inheritdoc}
     */
    public function modelComment()
    {
        return '\SunnyDayInc\Shop\Models\Eloquent\ShipmentComment';
    }

}
