<?php namespace SunnyDayInc\Shop\Mail;

interface TemplateInterface
{

    /**
     * Set the language code.
     *
     * @param  string $code
     * @return \SunnyDayInc\Shop\Mail\TemplateInterface
     */
    public function setLanguageCode($code);

    /**
     * Set the reply address.
     *
     * @param  string $address
     * @return \SunnyDayInc\Shop\Mail\TemplateInterface
     */
    public function setReplyAddress($address);

    /**
     * Set the subject.
     *
     * @param  string $subject
     * @return \SunnyDayInc\Shop\Mail\TemplateInterface
     */
    public function setSubject($subject);

    /**
     * Set the text content.
     *
     * @param  string $content
     * @return \SunnyDayInc\Shop\Mail\TemplateInterface
     */
    public function setContentText($content);

    /**
     * Set the html content.
     *
     * @param  string $content
     * @return \SunnyDayInc\Shop\Mail\TemplateInterface
     */
    public function setContentHtml($content);

    /**
     * Get the language code.
     *
     * @return string
     */
    public function getLanguageCode();

    /**
     * Get the reply address.
     *
     * @return string
     */
    public function getReplyAddress();

    /**
     * Get the subject.
     *
     * @return string
     */
    public function getSubject();

    /**
     * Get the text content.
     *
     * @return string
     */
    public function getContentText();

    /**
     * Get the html content.
     *
     * @return string
     */
    public function getContentHtml();

    /**
     * Check if text content is not empty.
     *
     * @return boolean
     */
    public function hasContentText();

    /**
     * Check if text html is not empty.
     *
     * @return boolean
     */
    public function hasContentHtml();

}
