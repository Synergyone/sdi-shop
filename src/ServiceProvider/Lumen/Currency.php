<?php namespace SunnyDayInc\Shop\ServiceProvider\Lumen;

use Illuminate\Support\ServiceProvider;

use SunnyDayInc\Shop\Currency\Currency as CurrencyContainer;
use SunnyDayInc\Shop\Currency\Repository\EloquentCurrency;

class Currency extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->configure('currency');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            'SunnyDayInc\Shop\Contracts\Currency', function ($app) {
                return new CurrencyContainer(new EloquentCurrency());
            }
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['SunnyDayInc\Shop\Contracts\Currency'];
    }
}
