<?php namespace SunnyDayInc\Shop\Shipment;

use SunnyDayInc\Shop\Shipment\Repository\Shipment as ShipmentRepository;

use SunnyDayInc\Shop\Support\Collection;

use SunnyDayInc\Shop\Models\ShipmentInterface;
use SunnyDayInc\Shop\Models\ShipmentTrackInterface;

use Illuminate\Contracts\Logging\Log;

class Factory implements FactoryInterface
{

    /**
     * The log context.
     *
     * @var string
     */
    const LOG_CONTEXT = 'shipment.factory';

    /**
     * The repository instance.
     *
     * @var \SunnyDayInc\Shop\Shipment\Repository\Shipment
     */
    protected $repository;

    /**
     * The field formats.
     *
     * @var array
     */
    protected $fieldFormats;

    /**
     * The shipment model instance.
     *
     * @var \SunnyDayInc\Shop\Models\ShipmentInterface
     */
    protected $shipment;

    /**
     * The shipment tracks.
     *
     * @var \Collection|\SunnyDayInc\Shop\Models\ShipmentTrackInterface[]
     */
    protected $tracks;

    /**
     * The shipment comment.
     *
     * @var \SunnyDayInc\Shop\Models\ShipmentCommentInterface
     */
    protected $comment;

    /**
     * Create a new instance.
     *
     * @param  \SunnyDayInc\Shop\Shipment\Repository\Shipment $repository
     * @param  array                                          $fieldFormats
     * @param  array                                          $data
     * @return void
     */
    public function __construct(ShipmentRepository $repository, Array $fieldFormats, Array $data = [])
    {
        $this->repository = $repository;
        $this->fieldFormats = $fieldFormats;

        $this->tracks = new Collection();
    
        // Create the shipment model and fill the default data
        if (! empty($data)) {
            $this->shipment = $this->repository->createModel();
            $this->shipment->fill($data);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setLogger(Log $log)
    {
        $this->log = $log;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setShipment(ShipmentInterface $shipment)
    {
        $this->shipment = $shipment;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getShipment()
    {
        return $this->shipment;
    }

    /**
     * {@inheritdoc}
     */
    public function getTracks()
    {
        if (is_null($this->shipment)) {
            return $this->tracks;
        }

        $shipmentId = (isset($this->shipment->id)) ? $this->shipment->id : 0;

        // Because the invoice is not yet saved
        // we cannot identify any track by it's ID
        if (empty($shipmentId)) {
            return $this->tracks;
        }

        // Fetch all tracks from repository
        if ($this->tracks->isEmpty()) {
            $tracks = $this->repository->findTracksByShipment($shipmentId);

            if (! $tracks->isEmpty()) {
                foreach ($tracks as $track) {
                    $this->tracks->push($track);
                }
            }

            unset($tracks);
        }

        return $this->tracks;
    }

    /**
     * {@inheritdoc}
     */
    public function addTrack(ShipmentTrackInterface $track)
    {
        // Set the invoice ID if available
        if (! empty($this->shipment->id)) {
            $track->shipment_id = $this->shipment->id;
        }

        // Add track to collection
        $this->tracks->push($track);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function createTrack()
    {
        return $this->repository->createModelTrack();
    }

    /**
     * {@inheritdoc}
     */
    public function createItem()
    {
        return $this->repository->createModelItem();
    }

    /**
     * {@inheritdoc}
     */
    public function setComment($comment, $notifyCustomer = false, $visibleInFront = false)
    {
        $this->comment = $this->repository->createModelComment();

        // Set the shipment ID if available
        if (! empty($this->shipment->id)) {
            $this->comment->shipment_id = $this->shipment->id;
        } else {
            $this->comment->shipment_id = 0;
        }

        $this->comment->comment              = $comment;
        $this->comment->is_customer_notified = $notifyCustomer;
        $this->comment->is_visible_on_front  = $visibleInFront;

        // Save directly if shipment exist
        if (! empty($this->comment->shipment_id)) {
            $this->comment->save();
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function save()
    {
        $connection = $this->shipment->getConnection();

        // Initiate transaction
        $connection->beginTransaction();

        $isSaved = true;

        // Save the shipment
        // Do field formatters if available only when it's new shipment
        if (! $this->shipment->exists && ! empty($this->fieldFormats)) {
            foreach ($this->fieldFormats as $field => $format) {
                $handler = new $format['formatter']($format['format'], $this->shipment);

                $this->shipment->$field = $handler->compile();

                unset($handler);
            }
        }

        try {
            $isSaved = $this->shipment->save();
        } catch (\ErrorException $e) {
            $this->createLog('error', $e);

            $isSaved = false;
        }

        if (! $isSaved) {
            // Rollback the transaction
            $connection->rollBack();

            return false;
        }

        // Get the increment shipment ID
        $shipmentId = $this->shipment->id;

        // Save all tracks
        if (! $this->saveTracks()) {
            // Rollback the transaction
            $connection->rollBack();

            return false;
        }

        // Set total qty based on track items
        // and re-save the shipment
        $this->shipment->qty = $this->getTotalItemsQty();

        try {
            $isSaved = $this->shipment->save();
        } catch (\ErrorException $e) {
            $this->createLog('error', $e);

            $isSaved = false;
        }

        if (! $isSaved) {
            // Rollback the transaction
            $connection->rollBack();

            return false;
        }

        // Save comment if available
        if (! is_null($this->comment)) {
            $this->comment->shipment_id = $shipmentId;
      
            try {
                $isSaved = $this->comment->save();
            } catch (\ErrorException $e) {
                $this->createLog('error', $e);

                $isSaved = false;
            }

            if (! $isSaved) {
                // Rollback the transaction
                $connection->rollBack();

                return false;
            }
        }

        // Commit the transaction
        $connection->commit();

        return true;
    }

    /**
     * Save all tracks.
     *
     * @return boolean
     */
    private function saveTracks()
    {
        $isAllSaved = true;

        if ($this->tracks instanceOf Collection) {
            foreach ($this->tracks as $key => $item) {
                $isSaved = true;

                // Assign shipment ID
                $this->tracks[$key]->shipment_id = $this->shipment->id;

                try {
                    $isSaved = $this->tracks[$key]->save();
                } catch (\ErrorException $e) {
                    $this->createLog('error', $e);

                    $isSaved = false;
                }

                if (! $isSaved) {
                    $isAllSaved = false;
                    break;
                }
            }
        }

        return $isAllSaved;
    }

    /**
     * Return total items quantity.
     *
     * @return integer
     */
    private function getTotalItemsQty()
    {
        $qty = 0;

        if ($this->tracks instanceOf Collection) {
            foreach ($this->tracks as $item) {
                $qty += $item->qty;
            }
        }

        return $qty;
    }

    /**
     * Create a new log.
     *
     * @param  string $level
     * @param  mixed  $message
     * @param  Array  $data
     * @return void
     */
    private function createLog($level, $message, Array $data = [])
    {
        if (! is_null($this->log)) {
            $data['context'] = self::LOG_CONTEXT;

            if ($message instanceOf \ErrorException) {
                $this->log->$level($message->getCode().': '.$message->getMessage()."\n".$message->getTraceAsString(), $data);
            } else {
                $this->log->$level($message, $data);
            }
        }
    }

}
