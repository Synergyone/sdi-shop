<?php namespace SunnyDayInc\Shop\ServiceProvider\Illuminate;

use Illuminate\Support\ServiceProvider;

use SunnyDayInc\Shop\Shipment\Shipment as ShipmentContainer;
use SunnyDayInc\Shop\Shipment\Repository\EloquentShipment;

class Shipment extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes(
            [
            __DIR__.'/../../config/shipment.php' => config_path('sunnydayinc/shop/shipment.php')
            ], 'config'
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            'SunnyDayInc\Shop\Contracts\Shipment', function ($app) {
                $promotion = new ShipmentContainer(new EloquentShipment());

                $promotion->setLogger($app['log']);

                $fieldFormats = $app['config']->get('sunnydayinc.shop.shipment.field_formats');

                if (! empty($fieldFormats)) {
                    foreach ($fieldFormats as $field => $format) {
                        $promotion->addFieldFormat($field, $format);
                    }
                }

                unset($fieldFormats);

                return $promotion;
            }
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['SunnyDayInc\Shop\Contracts\Shipment'];
    }
}
