<?php namespace SunnyDayInc\Shop\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

use SunnyDayInc\Shop\Models\AddressInterface;
use SunnyDayInc\Shop\Models\Address as AddressModel;
use SunnyDayInc\Shop\Contracts\Presentable;

class Address extends Model implements AddressInterface, Presentable
{

    use AddressModel;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'address';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'object_type', 
    'object_id', 
    'user_id', 
    'type', 
    'title', 
    'company', 
    'phone', 
    'mobile_phone', 
    'address', 
    'country_code', 
    'country_name', 
    'state', 
    'city_id', 
    'city_name', 
    'postcode', 
    'is_default', 
    'is_active'
    ];

}
