<?php namespace SunnyDayInc\Shop\Invoice\Repository;

abstract class Invoice implements InvoiceInterface
{

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        return $this->createModel()->find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function findByObject($objectType, $objectId, $id)
    {
        return $this->createModel()->newQuery()
            ->where('object_type', '=', $objectType)
            ->where('object_id', '=', $objectId)
            ->where('id', '=', $id)
            ->first();
    }

    /**
     * {@inheritdoc}
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * {@inheritdoc}
     */
    public function createModel()
    {
        $model = $this->model();

        return new $model;
    }

    /**
     * {@inheritdoc}
     */
    public function createModelItem()
    {
        $model = $this->modelItem();

        return new $model;
    }

    /**
     * {@inheritdoc}
     */
    public function createModelComment()
    {
        $model = $this->modelComment();

        return new $model;
    }

}
