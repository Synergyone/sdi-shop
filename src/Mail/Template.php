<?php namespace SunnyDayInc\Shop\Mail;

use SunnyDayInc\Shop\Mail\Compiler\Compiler;
use SunnyDayInc\Shop\Mail\Compiler\Manager;

class Template implements TemplateInterface
{
  
    /**
     * The compiler manager instance.
     *
     * @var \SunnyDayInc\Shop\Mail\Compiler\Manager
     */
    protected $compilerManager;

    /**
     * The compiler instance.
     *
     * @var \SunnyDayInc\Shop\Mail\Compiler\Compiler
     */
    protected $compiler;

    /**
     * The language code.
     *
     * @var string
     */
    protected $languageCode;

    /**
     * The reply address.
     *
     * @var string
     */
    protected $replyAddress;

    /**
     * The subject.
     *
     * @var string
     */
    protected $subject;

    /**
     * The Text content.
     *
     * @var string
     */
    protected $contentText;

    /**
     * The HTML content.
     *
     * @var string
     */
    protected $contentHtml;

    /**
     * Create a new instance.
     *
     * @param  \SunnyDayInc\Shop\Mail\Compiler\Manager $compiler
     * @param  array                                   $data
     * @return void
     */
    public function __construct(Manager $compiler, Array $data = [])
    {
        $this->compilerManager = $compiler;

        if (isset($data['language_code'])) {
            $this->languageCode = $data['language_code']; 
        }

        if (isset($data['reply_address'])) {
            $this->replyAddress = $data['reply_address']; 
        }

        if (isset($data['subject'])) {
            $this->subject = $data['subject']; 
        }

        if (isset($data['content_text'])) {
            $this->contentText = $data['content_text']; 
        }

        if (isset($data['content_html'])) {
            $this->contentHtml = $data['content_html']; 
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setLanguageCode($code)
    {
        $this->languageCode = $code;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setReplyAddress($address)
    {
        $this->replyAddress = $address;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setContentText($content)
    {
        $this->contentText = $content;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setContentHtml($content)
    {
        $this->contentHtml = $content;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getLanguageCode()
    {
        return $this->languageCode;
    }

    /**
     * {@inheritdoc}
     */
    public function getReplyAddress()
    {
        return $this->replyAddress;
    }

    /**
     * {@inheritdoc}
     */
    public function getSubject()
    {
        return $this->compile($this->subject);
    }

    /**
     * {@inheritdoc}
     */
    public function getContentText()
    {
        return $this->compile($this->contentText);
    }

    /**
     * {@inheritdoc}
     */
    public function getContentHtml()
    {
        return $this->compile($this->contentHtml);
    }

    /**
     * {@inheritdoc}
     */
    public function hasContentText()
    {
        return ( ! empty($this->contentText));
    }

    /**
     * {@inheritdoc}
     */
    public function hasContentHtml()
    {
        return ( ! empty($this->contentHtml));
    }

    /**
     * Assign specific data to compiler.
     *
     * @param  string $handle
     * @param  mixed  $data
     * @return \SunnyDayInc\Shop\Mail\TemplateInterface
     */
    public function setData($handle, $data)
    {
        // Get specific handler, then set the data
        $handler = $this->compilerManager->getHandler($handle)->set($data);

        // Register handler to compiler
        $this->getCompiler()->registerHandler($handle, $handler);

        return $this;
    }

    /**
     * Do compile template data.
     *
     * @param  string $data
     * @return string
     */
    public function compile($data)
    {
        if (empty($data)) {
            return $data;
        }

        return $this->getCompiler()->compile($data);
    }

    /**
     * Return compiler instance.
     *
     * @return \SunnyDayInc\Shop\Mail\Compiler\Compiler
     */
    private function getCompiler()
    {
        if (is_null($this->compiler)) {
            $this->compiler = new Compiler();
        }

        return $this->compiler;
    }

}
