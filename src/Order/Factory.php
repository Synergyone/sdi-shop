<?php namespace SunnyDayInc\Shop\Order;

use SunnyDayInc\Shop\Order\Repository\Order as OrderRepository;

use SunnyDayInc\Shop\Support\Collection;

use SunnyDayInc\Shop\Order\Repository\CustomerInterface as CustomerRepositoryContract;
use SunnyDayInc\Shop\Models\OrderInterface;
use SunnyDayInc\Shop\Models\OrderItemInterface;
use SunnyDayInc\Shop\Models\OrderAddressInterface;
use SunnyDayInc\Shop\Models\PromotionInterface;

class Factory implements FactoryInterface
{

    /**
     * The log context.
     *
     * @var string
     */
    const LOG_CONTEXT = 'order.factory';

    /**
     * The repository instance.
     *
     * @var \SunnyDayInc\Shop\Order\Repository\Order
     */
    protected $repository;

    /**
     * The field formats.
     *
     * @var array
     */
    protected $fieldFormats;

    /**
     * The log instance.
     *
     * @var mixed
     */
    protected $log;

    /**
     * The order model instance.
     *
     * @var \SunnyDayInc\Shop\Models\OrderInterface
     */
    protected $order;

    /**
     * The order items.
     *
     * @var \Collection|\SunnyDayInc\Shop\Models\OrderItemInterface[]
     */
    protected $items;

    /**
     * The order addresses.
     *
     * @var \Collection|\SunnyDayInc\Shop\Models\OrderAddressInterface[]
     */
    protected $addresses;

    /**
     * The order status.
     *
     * @var \SunnyDayInc\Shop\Models\OrderStatusHistoryInterface
     */
    protected $status;

    /**
     * Is order calculated.
     *
     * @var boolean
     */
    protected $isCalculated = false;

    /**
     * Create a new instance.
     *
     * @param  \SunnyDayInc\Shop\Order\Repository\Order $repository
     * @param  array                                    $fieldFormats
     * @param  array                                    $data
     * @return void
     */
    public function __construct(OrderRepository $repository, Array $fieldFormats, Array $data = [])
    {
        $this->repository = $repository;
        $this->fieldFormats = $fieldFormats;

        $this->items = new Collection();
        $this->addresses = new Collection();

        // Create the order model and fill the default data
        if (! empty($data)) {
            $this->order = $this->repository->createModel();
            $this->order->fill($data);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setLogger($log)
    {
        $this->log = $log;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setOrder(OrderInterface $order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * {@inheritdoc}
     */
    public function setCustomerRepository(CustomerRepositoryContract $repository)
    {
        $this->order->setCustomerRepository($repository);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getItems()
    {
        if (is_null($this->order)) {
            return $this->items;
        }

        // Fetch all items from repository
        if ($this->items->isEmpty()) {
            $items = $this->order->items;

            if (! $items->isEmpty()) {
                foreach ($items as $item) {
                    $this->items->put($item->id, $item);
                }
            }

            unset($items);
        }

        return $this->items;
    }

    /**
     * {@inheritdoc}
     */
    public function getItem($id)
    {
        if (is_null($this->order)) {
            return null;
        }

        $orderId = (isset($this->order->id)) ? $this->order->id : 0;

        // Because the order is not yet saved
        // we cannot identify any item by it's ID
        if (empty($orderId)) {
            return null;
        }

        if (! $this->items->isEmpty()) {
            $item = $this->items->get($id);

            if ($item instanceOf OrderItemInterface) {
                return $item;
            }
        }

        $item = $this->repository->findItemByOrderAndId($orderId, $id);

        if (! ($item instanceOf OrderItemInterface)) {
            return null;
        }

        $this->items->put($item->id, $item);

        return $item;
    }

    /**
     * {@inheritdoc}
     */
    public function addItem(OrderItemInterface $item)
    {
        // Set the order ID if available
        if (! empty($this->order->id)) {
            $item->order_id = $this->order->id;
        }

        // Merge if item is same as a existing item
        foreach ($this->items as $key => $existingItem) {
            if ($existingItem->equals($item)) {
                $this->items[$key]->merge($item, false);
                return $this;
            }

            unset($existingItem);
        }

        $item->order_currency_code    = $this->order->order_currency_code;
        $item->base_currency_code     = $this->order->base_currency_code;
        $item->global_currency_code   = $this->order->global_currency_code;
        $item->base_to_order_rate     = $this->order->base_to_order_rate;
        $item->base_to_global_rate    = $this->order->base_to_global_rate;

        // Calculate pricing
        $item->calculate();

        // Add item to collection
        $this->items->push($item);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function createItem()
    {
        return $this->repository->createModelItem();
    }

    /**
     * {@inheritdoc}
     */
    public function addItemPromotions(Collection $promotions)
    {
        // Try insert the promotion to items
        if ($this->items->isEmpty()) {
            $this->getItems();
        }

        if (! $this->items->isEmpty()) {
            foreach ($this->items as $key => $item) {

                foreach ($promotions as $promo) {

                    // Insert promotion, check if applicable
                    if ($this->items[$key]->addPromotion($promo)) {
            
                        // Stop further promo insertion if needed
                        if (! $promo->continueFurtherRulesProcessing()) {
                            break;
                        }

                    }

                }

            }
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function addPromotion(PromotionInterface $promo)
    {
        // Calculate whole order
        $this->calculate();

        $isApplicable = false;

        // Insert promotion, check if applicable
        if ($this->order->addPromotion($promo, $this->items)) {
            $isApplicable = true;
        }

        return $isApplicable;
    }

    /**
     * {@inheritdoc}
     */
    public function getAddress($type)
    {
        if (is_null($this->order)) {
            return null;
        }

        $orderId = (isset($this->order->id)) ? $this->order->id : 0;

        // Because the order is not yet saved
        // get from the collection directly if available
        if (empty($orderId)) {
            $addresses = $this->addresses->filter(
                function ($item) use ($type) {
                    return ($item->type === $type);
                }
            );

            if (! $addresses->isEmpty()) {
                return $addresses->first();
            }

            unset($addresses);

            return null;
        }

        if (! $this->addresses->isEmpty()) {
            $addresses = $this->addresses->filter(
                function ($item) use ($orderId, $type) {
                    return ($item->order_id === $orderId && $item->type === $type);
                }
            );

            if (! $addresses->isEmpty()) {
                return $addresses->first();
            }

            unset($addresses);
        }

        $address = $this->repository->findAddressByOrderAndType($orderId, $type);

        if (! ($address instanceOf OrderAddressInterface)) {
            return null;
        }

        $this->addresses->push($address);

        return $address;
    }

    /**
     * {@inheritdoc}
     */
    public function addAddress(OrderAddressInterface $address)
    {
        // Set the order ID if available
        if (! empty($this->order->id)) {
            $address->order_id = $this->order->id;
        }

        $this->addresses->push($address);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function createAddress()
    {
        return $this->repository->createModelAddress();
    }

    /**
     * {@inheritdoc}
     */
    public function setStatus($status, $entity, $comment = '', 
        $notifyCustomer = false, $visibleInFront = false
    ) {
        $this->status = $this->repository->createModelStatusHistory();

        // Set the order ID if available
        if (! empty($this->order->id)) {
            $this->status->order_id = $this->order->id;
        } else {
            $this->status->order_id = 0;
        }

        $this->status->entity_type          = $entity;
        $this->status->comment              = $comment;
        $this->status->status               = $status;
        $this->status->is_customer_notified = $notifyCustomer;
        $this->status->is_visible_on_front  = $visibleInFront;

        // Set order status
        $this->order->status = $status;

        // Save directly if order exist
        if (! empty($this->status->order_id)) {
            $this->status->save();

            $this->order->save();
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function calculate()
    {
        // Calculate order pricing
        if (! $this->isCalculated) {
            $this->order->calculate($this->items);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function save()
    {
        $connection = $this->order->getConnection();

        // Initiate transaction
        $connection->beginTransaction();

        $isSaved = true;

        // Save the order
        // Do field formatters if available only when it's new order
        if (! $this->order->exists && ! empty($this->fieldFormats)) {
            foreach ($this->fieldFormats as $field => $format) {
                $handler = new $format['formatter']($format['format'], $this->order);

                $this->order->$field = $handler->compile();

                unset($handler);
            }
        }
    
        try {
            $isSaved = $this->order->save();
        } catch (\ErrorException $e) {
            $this->createLog('error', $e);

            $isSaved = false;
        }

        if (! $isSaved) {
            // Rollback the transaction
            $connection->rollBack();

            return false;
        }

        // Get the increment order ID
        $orderId = $this->order->id;

        // Save all items
        if (! $this->saveItems()) {
            // Rollback the transaction
            $connection->rollBack();

            return false;
        }

        // Save all addresses
        if (! $this->saveAddresses()) {
            // Rollback the transaction
            $connection->rollBack();

            return false;
        }

        // Save status if available
        if (! is_null($this->status)) {
            $this->status->order_id = $orderId;
      
            try {
                $isSaved = $this->status->save();
            } catch (\ErrorException $e) {
                $this->createLog('error', $e);

                $isSaved = false;
            }

            if (! $isSaved) {
                // Rollback the transaction
                $connection->rollBack();

                return false;
            }
        }

        // Commit the transaction
        $connection->commit();

        return true;
    }

    /**
     * Return a created presenter.
     *
     * @return \SunnyDayInc\Shop\Presenter\Presenter
     */
    public function getPresenter()
    {
        return $this->order->getPresenter();
    }

    /**
     * Save all items.
     *
     * @return boolean
     */
    private function saveItems()
    {
        $isAllSaved = true;

        if (! $this->items->isEmpty()) {

            foreach ($this->items as $key => $item) {
                $isSaved = true;

                // Assign order ID
                $this->items[$key]->order_id = $this->order->id;

                try {
                    $isSaved = $this->items[$key]->save();
                } catch (\ErrorException $e) {
                    $this->createLog('error', $e);

                    $isSaved = false;
                }

                if (! $isSaved) {
                    $isAllSaved = false;
                    break;
                }
            }

        }

        return $isAllSaved;
    }

    /**
     * Save all addresses.
     *
     * @return boolean
     */
    private function saveAddresses()
    {
        $isAllSaved = true;

        if (! $this->addresses->isEmpty()) {
            foreach ($this->addresses as $key => $address) {
                $isSaved = true;

                // Assign order ID
                $this->addresses[$key]->order_id = $this->order->id;

                try {
                    $isSaved = $this->addresses[$key]->save();
                } catch (\ErrorException $e) {
                    $this->createLog('error', $e);

                    $isSaved = false;
                }

                if (! $isSaved) {
                    $isAllSaved = false;
                    break;
                }
            }
        }

        return $isAllSaved;
    }

    /**
     * Create a new log.
     *
     * @param  string $level
     * @param  mixed  $message
     * @param  Array  $data
     * @return void
     */
    private function createLog($level, $message, Array $data = [])
    {
        if (! is_null($this->log)) {
            $data['context'] = self::LOG_CONTEXT;

            if ($message instanceOf \ErrorException) {
                $this->log->$level($message->getCode().': '.$message->getMessage()."\n".$message->getTraceAsString(), $data);
            } else {
                $this->log->$level($message, $data);
            }
        }
    }

}
