<?php namespace SunnyDayInc\Shop\Wallet;

use SunnyDayInc\Shop\Models\WalletInterface;
use SunnyDayInc\Shop\Models\WalletCreditInterface;
use SunnyDayInc\Shop\Models\WalletDebitInterface;

use Illuminate\Contracts\Logging\Log;

interface FactoryInterface
{

    /**
     * Set the logger instance.
     *
     * @param  \Illuminate\Contracts\Logging\Log $log
     * @return self
     */
    public function setLogger(Log $log);

    /**
     * Set the wallet instance.
     *
     * @param  \SunnyDayInc\Shop\Models\WalletInterface $wallet
     * @return self
     */
    public function setWallet(WalletInterface $wallet);

    /**
     * Return the wallet instance.
     *
     * @return \SunnyDayInc\Shop\Models\WalletInterface
     */
    public function getWallet();

    /**
     * Get a credit by it's ID.
     *
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Models\WalletCreditInterface|null
     */
    public function getCredit($id);

    /**
     * Insert a new credit.
     *
     * @param  \SunnyDayInc\Shop\Models\WalletCreditInterface $credit
     * @return self
     */
    public function addCredit(WalletCreditInterface $credit);

    /**
     * Create a new wallet credit.
     *
     * @return \SunnyDayInc\Shop\Models\WalletCreditInterface
     */
    public function createCredit();

    /**
     * Get a debit by it's ID.
     *
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Models\WalletDebitInterface|null
     */
    public function getDebit($id);

    /**
     * Insert a new debit.
     *
     * @param  \SunnyDayInc\Shop\Models\WalletDebitInterface $debit
     * @return self
     */
    public function addDebit(WalletDebitInterface $debit);

    /**
     * Create a new wallet debit.
     *
     * @return \SunnyDayInc\Shop\Models\WalletDebitInterface
     */
    public function createDebit();

    /**
     * Save the wallet.
     *
     * @return boolean
     */
    public function save();

}
