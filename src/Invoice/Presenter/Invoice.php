<?php namespace SunnyDayInc\Shop\Invoice\Presenter;

use SunnyDayInc\Shop\Support\Collection;
use SunnyDayInc\Shop\Contracts\Currency;
use SunnyDayInc\Shop\Presenter\Presenter;
use SunnyDayInc\Shop\Models\InvoiceInterface;
use DateTime;

class Invoice extends Presenter
{

    /**
     * The Invoice model.
     *
     * @var Invoice
     */
    protected $_model;

    /**
     * The Currency Manager.
     *
     * @var \SunnyDayInc\Shop\Contracts\Currency
     */
    protected $_currency;

    /**
     * The currency format.
     *
     * @var string
     */
    protected $_currencyFormat = '%s %a';

    /**
     * The items.
     *
     * @var \Collection
     */
    protected $_items;

    /**
     * {@inheritdoc}
     */
    protected $publicData = [
        'id'                         => 'id', 
        'created_at'                 => 'created_at', 
        'updated_at'                 => 'updated_at'
    ];

    /**
     * Create the Presenter and store the object we are presenting.
     *
     * @param  InvoiceInterface $invoice
     * @param  array            $fields
     * @return void
     */
    public function __construct(InvoiceInterface $invoice, Array $fields = ['*'])
    {
        parent::__construct($invoice->toArray());

        // Select field to publish
        // Remove the rest
        $newPublicData = [];

        if (is_array($fields)) {
            if (trim($fields[0]) != '*') {
                foreach ($this->publicData as $key => $data) {
                    foreach ($fields as $item) {
                        if ($data == $item) {
                            $newPublicData[$key] = $data;
                        }
                    }
                }
            }
        }

        if (! empty($newPublicData)) {
            $this->publicData = $newPublicData;
        }

        $this->_model = $invoice;
    }

    /**
     * Define currency manager.
     *
     * @param  \SunnyDayInc\Shop\Contracts\Currency $currency
     * @param  string                               $format
     * @return void
     */
    public function setCurrencyManager(Currency $currency, $format = '%s %a')
    {
        $this->_currency = $currency;
        $this->_currencyFormat = $format;
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentId()
    {
        return (int) $this->_model->id;
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentCreatedAt()
    {
        if ($this->_model->created_at != '0000-00-00 00:00:00') {
            return (new DateTime($this->_model->created_at))->format('c');
        }

        return null;
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentUpdatedAt()
    {
        if ($this->_model->updated_at != '0000-00-00 00:00:00') {
            return (new DateTime($this->_model->updated_at))->format('c');
        }

        return null;
    }

    /**
     * Return the formated currency value.
     *
     * @return string
     */
    private function formatCurrency($value)
    {
        if ($this->_currency instanceOf Currency) {
            return $this->_currency
                ->convert(
                    $value, 
                    $this->_model->base_currency_code, 
                    $this->_model->order_currency_code, 
                    $this->_model->base_to_order_rate
                )->format($this->_currencyFormat);
        }

        return $value;
    }

}
