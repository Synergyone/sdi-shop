<?php namespace SunnyDayInc\Shop\Order\Repository;

class EloquentOrder extends Order
{

    /**
     * The current total row in previous query.
     *
     * @var integer
     */
    protected $total = 0;

    /**
     * {@inheritdoc}
     */
    public function findBy(
        Array $where = [], 
        Array $column = ['*'], 
        $page = 1, 
        $limit = 10, 
        $order = 'DESC', 
        $orderBy = 'created_at'
    ) {
        $where = array_merge(
            [
            'object_type' => '', 
            'object_id'   => 0, 
            'type'        => '', 
            'search'      => '', 
            'status'      => [], 
            'date_start'  => null, 
            'date_end'    => null
            ], $where
        );

        if (empty($page)) {
            $page = 1;
        }

        if (! is_array($where['status'])) {
            $where['status'] = [$where['status']];
        }

        $model = $this->createModel();
        $addressModel = $this->createModelAddress();

        $useWhere = false;
        $hasWhere = false;

        $fromSql = '(';
        $fromSql .= 'SELECT';

        if (! empty($where['search'])) {
            $fromSql .= ' DISTINCT';
        }

        $fromSql .= ' `'.$model->getTable().'`.`id`';
        $fromSql .= ' FROM `'.$model->getTable().'`';

        if (! empty($where['search'])) {
            $fromSql .= ' LEFT JOIN `'.$addressModel->getTable().'`';
            $fromSql .= ' ON `'.$model->getTable().'`.`id` = `'.$addressModel->getTable().'`.`order_id`';
        }

        if (! empty($where['object_type'])
            || ! empty($where['object_id'])
            || ! empty($where['type'])
            || ! empty($where['search'])
            || ! empty($where['status'])
            || ! empty($where['date_start'])
            || ! empty($where['date_end'])
        ) {
            $useWhere = true;
        }

        if ($useWhere) {
            $fromSql .= ' WHERE';
        }

        if (! empty($where['object_type'])) {
            $fromSql .= ' `'.$model->getTable().'`.`object_type` = "'.$where['object_type'].'"';

            $hasWhere = true;
        }

        if (! empty($where['object_id'])) {
            if ($hasWhere) {
                $fromSql .= ' AND';
            }

            $fromSql .= ' `'.$model->getTable().'`.`object_id` = '.$where['object_id'];

            $hasWhere = true;
        }

        if (! empty($where['type'])) {
            if ($hasWhere) {
                $fromSql .= ' AND';
            }

            $fromSql .= ' `'.$model->getTable().'`.`type` = "'.$where['type'].'"';

            $hasWhere = true;
        }

        if (! empty($where['search'])) {
            if ($hasWhere) {
                $fromSql .= ' AND';
            }

            $fromSql .= ' (`'.$model->getTable().'`.`code` LIKE "%'.$where['search'].'%"';
            $fromSql .= ' OR `'.$addressModel->getTable().'`.`location_title` LIKE "%'.$where['search'].'%"';
            $fromSql .= ' OR `'.$addressModel->getTable().'`.`name` LIKE "%'.$where['search'].'%"';
            $fromSql .= ' OR `'.$addressModel->getTable().'`.`company` LIKE "%'.$where['search'].'%"';
            $fromSql .= ' OR `'.$addressModel->getTable().'`.`phone` LIKE "%'.$where['search'].'%"';
            $fromSql .= ' OR `'.$addressModel->getTable().'`.`mobile_phone` LIKE "%'.$where['search'].'%"';
            $fromSql .= ' OR `'.$addressModel->getTable().'`.`address` LIKE "%'.$where['search'].'%")';

            $hasWhere = true;
        }

        if (! empty($where['status'])) {
            if ($hasWhere) {
                $fromSql .= ' AND';
            }

            $fromSql .= ' `'.$model->getTable().'`.`status` IN ("'.implode('", "', $where['status']).'")';

            $hasWhere = true;
        }

        if (! is_null($where['date_start'])) {
            if ($hasWhere) {
                $fromSql .= ' AND';
            }

            $fromSql .= ' `'.$model->getTable().'`.`created_at` >= "'.$where['date_start']->format('Y-m-d H:i:s').'"';

            $hasWhere = true;
        }

        if (! is_null($where['date_end'])) {
            if ($hasWhere) {
                $fromSql .= ' AND';
            }

            $fromSql .= ' `'.$model->getTable().'`.`created_at` <= "'.$where['date_end']->format('Y-m-d H:i:s').'"';

            $hasWhere = true;
        }

        if (! empty($order) && ! empty($orderBy)) {
            $fromSql .= ' ORDER BY `'.$model->getTable().'`.`'.$orderBy.'` '.$order;
        }

        if ($limit > 0) {
            $fromSql .= ' limit '.$limit.' offset '.($page - 1) * $limit;
        }

        $fromSql .= ') o';

        if ($limit > 0) {
            $query = $model->newQuery();

            if (! empty($where['search'])) {
                $query->leftJoin($addressModel->getTable(), $model->getTable().'.id', '=', $addressModel->getTable().'.order_id');
            }

            $query->select('id');

            if (! empty($where['object_type'])) {
                $query->where($model->getTable().'.object_type', '=', $where['object_type']);
            }

            if (! empty($where['object_id'])) {
                $query->where($model->getTable().'.object_id', '=', $where['object_id']);
            }

            if (! empty($where['type'])) {
                $query->where($model->getTable().'.type', '=', $where['type']);
            }

            if (! empty($where['search'])) {
                $searchText = $where['search'];

                $query->where(
                    function ($qry) use ($model, $addressModel, $searchText) {
                        $qry->where($model->getTable().'.code', 'LIKE', '%'.$searchText.'%')
                            ->orWhere($addressModel->getTable().'.location_title', 'LIKE', '%'.$searchText.'%')
                            ->orWhere($addressModel->getTable().'.name', 'LIKE', '%'.$searchText.'%')
                            ->orWhere($addressModel->getTable().'.company', 'LIKE', '%'.$searchText.'%')
                            ->orWhere($addressModel->getTable().'.phone', 'LIKE', '%'.$searchText.'%')
                            ->orWhere($addressModel->getTable().'.mobile_phone', 'LIKE', '%'.$searchText.'%')
                            ->orWhere($addressModel->getTable().'.address', 'LIKE', '%'.$searchText.'%');
                    }
                );
            }

            if (! empty($where['status'])) {
                $query->whereIn($model->getTable().'.status', $where['status']);
            }

            if (! empty($where['date_start'])) {
                $query->where($model->getTable().'.created_at', '>=', $where['date_start']->format('Y-m-d H:i:s'));
            }

            if (! empty($where['date_end'])) {
                $query->where($model->getTable().'.created_at', '<=', $where['date_end']->format('Y-m-d H:i:s'));
            }

            $this->total = $query->count();
        } else {
            $this->total = 0;
        }

        $query = $model->newQuery()
            ->from(app('db')->raw($fromSql))
            ->join($model->getTable(), $model->getTable().'.id', '=', 'o.id');

        if (! empty($order) && ! empty($orderBy)) {
            $query->orderBy($model->getTable().'.'.$orderBy, $order);
        }

        unset($fromSql);
        unset($model);

        return $query->get($column);
    }

    /**
     * {@inheritdoc}
     */
    public function findItemsByOrder($orderId)
    {
        $query = $this->createModelItem()->newQuery();
        $query->where('order_id', '=', $orderId);

        return $query->get();
    }

    /**
     * {@inheritdoc}
     */
    public function findItemByOrderAndId($orderId, $id)
    {
        $query = $this->createModelItem()->newQuery();
        $query->where('order_id', '=', $orderId);
        $query->where('id', '=', $id);

        return $query->first();
    }

    /**
     * {@inheritdoc}
     */
    public function findAddressByOrderAndType($orderId, $type)
    {
        $query = $this->createModelAddress()->newQuery();
        $query->where('order_id', '=', $orderId);
        $query->where('type', '=', $type);

        return $query->first();
    }

    /**
     * {@inheritdoc}
     */
    public function model()
    {
        return '\SunnyDayInc\Shop\Models\Eloquent\Order';
    }

    /**
     * {@inheritdoc}
     */
    public function modelItem()
    {
        return '\SunnyDayInc\Shop\Models\Eloquent\OrderItem';
    }

    /**
     * {@inheritdoc}
     */
    public function modelAddress()
    {
        return '\SunnyDayInc\Shop\Models\Eloquent\OrderAddress';
    }

    /**
     * {@inheritdoc}
     */
    public function modelStatusHistory()
    {
        return '\SunnyDayInc\Shop\Models\Eloquent\OrderStatusHistory';
    }

}
