<?php namespace SunnyDayInc\Shop\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

use SunnyDayInc\Shop\Models\InvoiceCommentInterface;

class InvoiceComment extends Model implements InvoiceCommentInterface
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'invoice_comment';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'invoice_id', 
    'comment', 
    'is_customer_notified', 
    'is_visible_on_front'
    ];

}
