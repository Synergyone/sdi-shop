<?php namespace SunnyDayInc\Shop\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

use SunnyDayInc\Shop\Models\PromotionInterface;
use SunnyDayInc\Shop\Models\Promotion as PromotionModel;
use SunnyDayInc\Shop\Contracts\Presentable;

class Promotion extends Model implements PromotionInterface, Presentable
{

    use PromotionModel;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'promotion';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'object_type', 
    'object_id', 
    'type', 
    'name', 
    'description', 
    'from_date', 
    'to_date', 
    'conditions_serialized', 
    'actions_serialized', 
    'simple_action', 
    'discount_amount', 
    'discount_step', 
    'free_shipping', 
    'apply_to_shipping', 
    'uses_per_customer', 
    'uses_per_coupon', 
    'is_coupon', 
    'sort_order', 
    'stop_rules_processing', 
    'is_active', 
    'total_used'
    ];

    /**
     * Return promotion products.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany('SunnyDayInc\Shop\Models\Eloquent\PromotionProduct');
    }

    /**
     * Return promotion coupons.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function coupons()
    {
        return $this->hasMany('SunnyDayInc\Shop\Models\Eloquent\PromotionCoupon');
    }

}
