<?php namespace SunnyDayInc\Shop\Address\Repository;

interface AddressInterface
{

    /**
     * Create a new address.
     *
     * @return \SunnyDayInc\Shop\Models\AddressInterface|null
     */
    public function create(Array $data);

    /**
     * Return a address by it's ID.
     *
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Models\AddressInterface|null
     */
    public function find($id);

    /**
     * Return a address.
     *
     * @param  Array $where
     * @return \SunnyDayInc\Shop\Models\AddressInterface|null
     */
    public function findSimilar(Array $where);

    /**
     * Return addresses.
     *
     * @param  array   $where
     * @param  array   $column
     * @param  integer $page
     * @param  integer $limit
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAllBy(
        Array $where = [], 
        Array $column = ['*'], 
        $page = 1, 
        $limit = 10, 
        $order = 'DESC', 
        $orderBy = 'created_at'
    );

    /**
     * Return the current total row in previous query.
     *
     * @return string
     */
    public function getTotal();

    /**
     * Return the Model class name
     *
     * @return string
     */
    public function model();

    /**
     * Return the model object creation.
     *
     * @return \SunnyDayInc\Shop\Models\AddressInterface
     */
    public function createModel();

}
