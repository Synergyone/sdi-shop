<?php namespace SunnyDayInc\Shop\Models\Eloquent;

use SunnyDayInc\Shop\Contracts\Model as ModelInterface;
use Illuminate\Database\Eloquent\Model;

class ExchangeRate extends Model implements ModelInterface
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'exchange_rate';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'from', 
    'to', 
    'rate'
    ];

}
