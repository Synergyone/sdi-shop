<?php namespace SunnyDayInc\Shop\Mail\Loader;

use SunnyDayInc\Shop\Mail\Compiler\Manager;
use SunnyDayInc\Shop\Mail\Template;

abstract class Loader implements LoaderInterface
{

    /**
     * The compiler manager instance.
     *
     * @var \SunnyDayInc\Shop\Mail\Compiler\Manager
     */
    protected $compiler;

    /**
     * {@inheritdoc}
     */
    public function setCompiler(Manager $compiler)
    {
        $this->compiler = $compiler;
    }

    /**
     * Return the template object.
     *
     * @param  array $data
     * @return \SunnyDayInc\Shop\Mail\TemplateInterface
     */
    protected function buildTemplate(Array $data)
    {
        return $this->getTemplate($data);
    }

    /**
     * Return the template object.
     *
     * @param  array $data
     * @return \SunnyDayInc\Shop\Mail\TemplateInterface
     */
    private function getTemplate($data)
    {
        return new Template($this->compiler, $data);
    }

}
