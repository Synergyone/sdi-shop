<?php namespace SunnyDayInc\Shop\Cart;

use SunnyDayInc\Shop\Cart\Repository\Cart as CartRepository;
use SunnyDayInc\Shop\Cart\Repository\ProductInterface as ProductRepository;

use SunnyDayInc\Shop\Support\Collection;

use SunnyDayInc\Shop\Models\CartInterface;
use SunnyDayInc\Shop\Models\CartItemInterface;
use SunnyDayInc\Shop\Models\PromotionCouponInterface;

use DateTime;

use SunnyDayInc\Shop\Exceptions\CartItemNotFoundException;
use SunnyDayInc\Shop\Exceptions\CartItemProductNotFoundException;
use SunnyDayInc\Shop\Exceptions\CartItemProductStockCouldNotSatisfyException;
use SunnyDayInc\Shop\Exceptions\CartItemProductOutOfStockException;

class Factory implements FactoryInterface
{

    /**
     * The log context.
     *
     * @var string
     */
    const LOG_CONTEXT = 'cart.factory';

    /**
     * The repository instance.
     *
     * @var \SunnyDayInc\Shop\Cart\Repository\Cart
     */
    protected $repository;

    /**
     * The product repository instance.
     *
     * @var \SunnyDayInc\Shop\Cart\Repository\ProductInterface
     */
    protected $productRepository;

    /**
     * The cart model instance.
     *
     * @var \SunnyDayInc\Shop\Models\CartInterface
     */
    protected $cart;

    /**
     * The coupon model instance.
     *
     * @var \SunnyDayInc\Shop\Models\PromotionCouponInterface
     */
    protected $coupon;

    /**
     * The cart items.
     *
     * @var \Collection|\SunnyDayInc\Shop\Models\CartItemInterface[]
     */
    protected $items;

    /**
     * The cart items to remove.
     *
     * @var \Collection|\SunnyDayInc\Shop\Models\CartItemInterface[]
     */
    protected $deleteItems;

    /**
     * The unavailable items.
     *
     * @var \Collection
     */
    protected $unavailableItems;

    /**
     * The coupon error.
     *
     * @var Array
     */
    protected $couponError;

    /**
     * Is cart calculated.
     *
     * @var boolean
     */
    protected $isCalculated = false;

    /**
     * Is existing items from repository loaded.
     *
     * @var boolean
     */
    protected $isExistingItemsLoaded = false;

    /**
     * Create a new instance.
     *
     * @param  \SunnyDayInc\Shop\Cart\Repository\Cart $repository
     * @param  array                                  $data
     * @return void
     */
    public function __construct(CartRepository $repository, Array $data = [])
    {
        $this->repository = $repository;
    
        $this->items = new Collection();
        $this->deleteItems = new Collection();
        $this->unavailableItems = new Collection();
    
        // Create the cart model and fill the default data
        $this->cart = $this->repository->createModel();
    
        if (! empty($data)) {
            $this->cart->fill($data);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setLogger($log)
    {
        $this->log = $log;

        return $this;
    }

    /**
     * Define product repository instance.
     *
     * @param  \SunnyDayInc\Shop\Cart\Repository\ProductInterface $repository
     * @return void
     */
    public function setProductRepository(ProductRepository $repository)
    {
        $this->productRepository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    public function setCart(CartInterface $cart)
    {
        $this->cart = $cart;

        $this->checkCouponStillApplicable();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * {@inheritdoc}
     */
    public function setCoupon(PromotionCouponInterface $coupon)
    {
        $this->coupon = $coupon;

        $this->checkCouponStillApplicable();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function removeCoupon()
    {
        $this->coupon = null;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCoupon()
    {
        return $this->coupon;
    }

    /**
     * {@inheritdoc}
     */
    public function setObjectOwner($objectType, $objectId)
    {
        if (! is_null($this->cart)) {
            $this->cart->object_type = $objectType;
            $this->cart->object_id = $objectId;
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setCustomerId($id)
    {
        if (! is_null($this->cart)) {
            $this->cart->customer_id = $id;
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setAddressId($id)
    {
        if (! is_null($this->cart)) {
            $this->cart->address_id = $id;
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function isHavingUnavailableItems()
    {
        return (! $this->unavailableItems->isEmpty());
    }

    /**
     * {@inheritdoc}
     */
    public function getUnavailableItems()
    {
        return $this->unavailableItems;
    }

    /**
     * {@inheritdoc}
     */
    public function getItems()
    {
        if ($this->isExistingItemsLoaded) {
            return $this->items;
        }

        if (is_null($this->cart)) {
            return $this->items;
        }

        $cartId = (isset($this->cart->id)) ? $this->cart->id : 0;

        // Because the cart is not yet saved
        // we cannot identify any item by it's ID
        if (empty($cartId)) {
            return $this->items;
        }

        // Fetch all items from repository
        if ($this->items->isEmpty()) {
            $items = $this->repository->findItemsByCart($cartId);

            if (! $items->isEmpty()) {
                foreach ($items as $item) {
                    $this->_addItem($item);
                }
            }

            unset($items);

            $this->isExistingItemsLoaded = true;
        }

        return $this->items;
    }

    /**
     * {@inheritdoc}
     */
    public function getItem($id)
    {
        if (is_null($this->cart)) {
            throw new CartNotFoundException('Cart not found');
        }

        $cartId = (isset($this->cart->id)) ? $this->cart->id : 0;

        // Because the cart is not yet saved
        // we cannot identify any item by it's ID
        if (empty($cartId)) {
            throw new CartNotFoundException('Cart not yet saved');
        }

        if (! $this->items->isEmpty()) {
            $item = $this->items->get($id);

            if ($item instanceOf CartItemInterface) {
                return $item;
            }
        }

        $item = $this->repository->findItemByCartAndId($cartId, $id);

        if (! ($item instanceOf CartItemInterface)) {
            throw new CartItemNotFoundException('Cart Item not found');
        }

        $this->items->put($item->id, $item);

        return $item;
    }

    /**
     * {@inheritdoc}
     */
    public function addItem(CartItemInterface $item)
    {
        // Make sure all existing items loaded
        $this->getItems();

        // Continue adding item
        return $this->_addItem($item);
    }

    /**
     * Insert a new item.
     *
     * @param  \SunnyDayInc\Shop\Models\CartItemInterface $item
     * @return self
     */
    private function _addItem(CartItemInterface $item)
    {
        // Set the cart ID if available
        if (! empty($this->cart->id) && ! isset($item->cart_id)) {
            $item->cart_id = $this->cart->id;
        }

        // Do not continue adding if already exist in the collection
        // Based on the current item is having ID or not
        if (isset($item->id)) {
            if (isset($this->items[$item->id])) {
                return $this;
            }
        }

        // Merge if item is same as a existing item
        foreach ($this->items as $key => $existingItem) {
            if ($existingItem->equals($item)) {
                $this->items[$key]->merge($item, false);
                return $this;
            }

            unset($existingItem);
        }

        // Add item to collection
        if (isset($item->id)) {
            $this->items->put($item->id, $item);
        } else {
            $this->items->push($item);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function updateItem($id, Array $data)
    {
        // Make sure all existing items loaded
        $this->getItems();

        // Continue updating item
        $item = $this->getItem($id);

        foreach ($data as $key => $value) {
            $item->$key = $value;
        }

        $this->items->put($id, $item);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteItem($id)
    {
        if ($this->deleteItems->offsetExists($id)) {
            return $this;
        }

        // Make sure all existing items loaded
        $this->getItems();

        // Continue updating item
        $item = $this->getItem($id);

        // Insert to items collection need to delete
        $this->deleteItems->push($item);

        // Remove from items collection
        $this->items->forget($id);

        unset($item);

        return $this;
    }

    /**
     * Check unvailable item(s).
     *
     * @return void
     */
    public function checkUnavailableItems()
    {
        // Make sure all existing items loaded
        $this->getItems();

        if (! $this->items->isEmpty()) {

            $productStocks = [];
            $itemProductHighestQty = [];

            foreach ($this->items as $key => $item) {
                $isAvailable = false;
                $isProductAvailable = true;

                if (! is_null($this->productRepository)) {
                    $this->items[$key]->setProductRepository($this->productRepository);
                }

                try {
                    $this->items[$key]->checkIsAvailable();

                    $isAvailable = true;
                } catch (CartItemProductNotFoundException $e) {
                    $isProductAvailable = false;
                } catch (CartItemProductOutOfStockException $e) {
                } catch (CartItemProductStockCouldNotSatisfyException $e) {
                }

                if ($isProductAvailable) {
                    $productId = $this->items[$key]->getProduct()->getId();

                    if (! isset($productStocks[$productId])) {
                        $itemProductHighestQty[$productId] = [
                        'ids'   => [], 
                        'title' => $this->items[$key]->getProduct()->getTitle(), 
                        'qty'   => 0
                        ];

                        $productStocks[$productId] = $this->items[$key]->getProduct()->getStock();
                    }

                    $itemProductHighestQty[$productId]['ids'][] = $item->id;
                    $itemProductHighestQty[$productId]['qty'] += $this->items[$key]->qty;
                }

                if (! $isAvailable && ! is_null($e)) {
                    $this->unavailableItems->push(
                        [
                        'ids'        => [$item->id], 
                        'product_id' => (int) $e->getProduct()->getId(), 
                        'title'      => $e->getProduct()->getTitle(), 
                        'stock'      => (float) number_format($e->getProduct()->getStock(), 2), 
                        'reason'     => $e->getErrorType()
                        ]
                    );
                }
        
            }

            // Validate similar product stock
            if (! empty($productStocks) && ! empty($itemProductHighestQty)) {
                foreach ($itemProductHighestQty as $productId => $qty) {
                    if ($productStocks[$productId] < $qty['qty']) {
                        $this->unavailableItems->push(
                            [
                            'ids'        => $qty['ids'], 
                            'product_id' => $productId, 
                            'title'      => $qty['title'], 
                            'stock'      => (float) number_format($productStocks[$productId], 2), 
                            'reason'     => 'stock_could_not_satisfy'
                            ]
                        );
                    }
                }
            }

        }
    }

    /**
     * {@inheritdoc}
     */
    public function createItem()
    {
        return $this->repository->createModelItem();
    }

    /**
     * {@inheritdoc}
     */
    public function calculate()
    {
        // Calculate cart pricing
        if (! $this->isCalculated) {
            $this->cart->calculate($this->items);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function save()
    {
        $connection = $this->cart->getConnection();

        // Initiate transaction
        $connection->beginTransaction();

        $isSaved = true;

        // Save the cart
        // Set the coupon if available
        if (! is_null($this->coupon)) {
            $this->cart->promotion_coupon_id = $this->coupon->getId();
            $this->cart->promotion_coupon_code = $this->coupon->getCode();
        } else {
            $this->cart->promotion_coupon_id = null;
            $this->cart->promotion_coupon_code = null;
        }

        try {
            $isSaved = $this->cart->save();
        } catch (\ErrorException $e) {
            $this->createLog('error', $e);

            $isSaved = false;
        }

        if (! $isSaved) {
            // Rollback the transaction
            $connection->rollBack();

            return false;
        }

        // Delete required items
        if (! $this->deleteRequiredItems()) {
            // Rollback the transaction
            $connection->rollBack();

            return false;
        }

        // Save all items
        if (! $this->saveItems()) {
            // Rollback the transaction
            $connection->rollBack();

            return false;
        }

        // Commit the transaction
        $connection->commit();

        return true;
    }

    /**
     * Save all items.
     *
     * @return boolean
     */
    private function saveItems()
    {
        $isAllSaved = true;

        if (! $this->items->isEmpty()) {

            foreach ($this->items as $key => $item) {
                $isSaved = true;

                // Assign cart ID
                $this->items[$key]->cart_id = $this->cart->id;

                try {
                    $isSaved = $this->items[$key]->save();
                } catch (\ErrorException $e) {
                    $this->createLog('error', $e);

                    $isSaved = false;
                }

                if (! $isSaved) {
                    $isAllSaved = false;
                    break;
                }
            }

        }

        return $isAllSaved;
    }

    /**
     * Delete required items.
     *
     * @return boolean
     */
    private function deleteRequiredItems()
    {
        $isAllDeleted = true;

        if (! $this->deleteItems->isEmpty()) {

            foreach ($this->deleteItems as $key => $item) {
                $isDeleted = true;

                try {
                    $isDeleted = $this->deleteItems[$key]->delete();

                    $this->deleteItems->forget($key);
                } catch (\ErrorException $e) {
                    $this->createLog('error', $e);

                    $isDeleted = false;
                }

                if (! $isDeleted) {
                    $isAllDeleted = false;
                    break;
                }
            }

        }

        return $isAllDeleted;
    }

    /**
     * {@inheritdoc}
     */
    public function delete()
    {
        return $this->cart->delete();
    }

    /**
     * Return a created presenter.
     *
     * @return \SunnyDayInc\Shop\Presenter\Presenter
     */
    public function getPresenter()
    {
        $this->cart->setUnavailableItems($this->unavailableItems);

        if (is_array($this->couponError)) {
            $this->cart->setCouponError($this->couponError);
        }

        return $this->cart->getPresenter();
    }

    /**
     * Check if available coupon is still applicable.
     *
     * @return boolean
     */
    private function checkCouponStillApplicable()
    {
        if (is_null($this->coupon)) {
            if (! empty($this->cart->promotion_coupon_id)) {
                $this->coupon = $this->cart->coupon;
            }
        }

        if (is_null($this->coupon)) {
            return false;
        }

        $promotionCouponId = $this->cart->promotion_coupon_id;
        $promotionCouponCode = $this->cart->promotion_coupon_code;

        if (is_null($this->coupon) && ! empty($promotionCouponId)) {
            $this->coupon = null;

            if ($this->cart->exists) {
                $this->save();
            }

            $this->couponError = [
            'id'   => $promotionCouponId, 
            'code' => $promotionCouponCode, 
            'type' => 'coupon_missing'
            ];

            return false;
        }

        if (time() >= $this->coupon->getExpireAt()->getTimestamp()) {
            $this->coupon = null;
      
            if ($this->cart->exists) {
                $this->save();
            }

            $this->couponError = [
            'id'   => $promotionCouponId, 
            'code' => $promotionCouponCode, 
            'type' => 'coupon_expired'
            ];

            return false;
        }

        $promotion = $this->coupon->promotion;

        if (is_null($promotion)) {
            $this->coupon = null;
      
            if ($this->cart->exists) {
                $this->save();
            }

            $this->couponError = [
            'id'   => $promotionCouponId, 
            'code' => $promotionCouponCode, 
            'type' => 'promotion_missing'
            ];

            return false;
        }

        if (! $promotion->isActive()) {
            $this->coupon = null;
      
            if ($this->cart->exists) {
                $this->save();
            }

            $this->couponError = [
            'id'   => $promotionCouponId, 
            'code' => $promotionCouponCode, 
            'type' => 'promotion_missing'
            ];

            return false;
        }

        if (! $promotion->isApplicable($this->cart->getPresenter())) {
            $this->coupon = null;
      
            if ($this->cart->exists) {
                $this->save();
            }

            $this->couponError = [
            'id'   => $promotionCouponId, 
            'code' => $promotionCouponCode, 
            'type' => 'promotion_not_applicable'
            ];

            return false;
        }

        return true;
    }

    /**
     * Create a new log.
     *
     * @param  string $level
     * @param  mixed  $message
     * @param  Array  $data
     * @return void
     */
    private function createLog($level, $message, Array $data = [])
    {
        if (! is_null($this->log)) {
            $data['context'] = self::LOG_CONTEXT;

            if ($message instanceOf \ErrorException) {
                $this->log->$level($message->getCode().': '.$message->getMessage()."\n".$message->getTraceAsString(), $data);
            } else {
                $this->log->$level($message, $data);
            }
        }
    }

}
