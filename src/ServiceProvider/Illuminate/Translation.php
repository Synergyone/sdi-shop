<?php namespace SunnyDayInc\Shop\ServiceProvider\Illuminate;

use Illuminate\Support\ServiceProvider;

use SunnyDayInc\Shop\Translation\Translator;

class Translation extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes(
            [
            __DIR__.'/../../config/locale.php' => config_path('sunnydayinc/shop/locale.php')
            ], 'config'
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            'SunnyDayInc\Shop\Contracts\Translator', function ($app) {
                $code = $app['config']->get('sunnydayinc.shop.locale.code');
                $path = $app['config']->get('sunnydayinc.shop.locale.path');
      
                return new Translator($code, $path);
            }
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['SunnyDayInc\Shop\Contracts\Translator'];
    }
}
