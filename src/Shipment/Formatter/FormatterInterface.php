<?php namespace SunnyDayInc\Shop\Shipment\Formatter;

interface FormatterInterface
{

    /**
     * Return the formatted value.
     *
     * @return string
     */
    public function compile();

}
