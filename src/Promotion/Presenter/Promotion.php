<?php namespace SunnyDayInc\Shop\Promotion\Presenter;

use SunnyDayInc\Shop\Presenter\Presenter;
use SunnyDayInc\Shop\Models\PromotionInterface as PromotionContract;
use SunnyDayInc\Shop\Promotion\Rule\RuleInterface as RuleContract;
use DateTime;

class Promotion extends Presenter
{

    /**
     * The Promotion model.
     *
     * @var Promotion
     */
    protected $_model;

    /**
     * Create the Presenter and store the object we are presenting.
     *
     * @param  PromotionContract $promo
     * @param  array             $fields
     * @return void
     */
    public function __construct(PromotionContract $promo, Array $fields = ['*'])
    {
        parent::__construct($promo->toArray());

        // Select field to publish
        // Remove the rest
        $newPublicData = [];

        if (is_array($fields)) {
            if (trim($fields[0]) != '*') {
                foreach ($this->publicData as $key => $data) {
                    foreach ($fields as $item) {
                        if ($data == $item) {
                            $newPublicData[$key] = $data;
                        }
                    }
                }
            }
        }

        if (! empty($newPublicData)) {
            $this->publicData = $newPublicData;
        }

        $this->_model = $promo;
    }

    /**
     * Return the Promotion model.
     *
     * @return PromotionContract
     */
    public function getModel()
    {
        return $this->_model;
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentId()
    {
        return (int) $this->_model->id;
    }

    /**
     * Format and return the data.
     *
     * @return DateTime
     */
    public function presentFromDate()
    {
        if (! empty($this->_model->from_date)) {
            return (new DateTime($this->_model->from_date))->format('c');
        }

        return null;
    }

    /**
     * Format and return the data.
     *
     * @return DateTime
     */
    public function presentToDate()
    {
        if (! empty($this->_model->to_date)) {
            return (new DateTime($this->_model->to_date))->format('c');
        }

        return null;
    }

    /**
     * Format and return the data.
     *
     * @return float
     */
    public function presentDiscountAmount()
    {
        return (float) $this->_model->discount_amount;
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentDiscountStep()
    {
        return (float) $this->_model->discount_step;
    }

    /**
     * Format and return the data.
     *
     * @return boolean
     */
    public function presentFreeShipping()
    {
        return (bool) $this->_model->free_shipping;
    }

    /**
     * Format and return the data.
     *
     * @return boolean
     */
    public function presentApplyToShipping()
    {
        return (bool) $this->_model->apply_to_shipping;
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentUsesPerCustomer()
    {
        return (int) $this->_model->uses_per_customer;
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentUsesPerCoupon()
    {
        return (int) $this->_model->uses_per_coupon;
    }

    /**
     * Format and return the data.
     *
     * @return boolean
     */
    public function presentIsCoupon()
    {
        return (bool) $this->_model->is_coupon;
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentSortOrder()
    {
        return (int) $this->_model->sort_order;
    }

    /**
     * Format and return the data.
     *
     * @return boolean
     */
    public function presentStopRulesProcessing()
    {
        return (bool) $this->_model->stop_rules_processing;
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentTotalUsed()
    {
        return (int) $this->_model->total_used;
    }

    /**
     * Format and return the data.
     *
     * @return boolean
     */
    public function presentIsActive()
    {
        return (bool) $this->_model->is_active;
    }

    /**
     * Format and return the data.
     *
     * @return array
     */
    public function presentRule()
    {
        $condition = $this->_model->getCondition();

        if (is_object($condition)) {
            $condition = $this->iterateBuildRule($condition->toArray());

            return $condition;
        }

        return [];
    }

    /**
     * Format and return the data.
     *
     * @return array
     */
    public function presentAction()
    {
        $action = $this->_model->getAction();

        if (! empty($action) && ! is_null($action)) {
            $action = $this->iterateBuildRule($action->toArray());
        }

        return $action;
    }

    /**
     * Build condition presenter
     *
     * @param  array $arrRule
     * @return array
     */
    protected function iterateBuildRule(Array $arrRule)
    {
        $arrClass = explode('\\', $arrRule['rule']);
    
        $rule = [
        'type' => strtolower(end($arrClass))
        ];

        if (isset($arrRule['attribute'])) {
            $rule['attribute'] = $arrRule['attribute'];
        }

        if (isset($arrRule['operator'])) {
            $rule['operator'] = $arrRule['operator'];
        }

        if (isset($arrRule['aggregator'])) {
            $rule['operator'] = $arrRule['aggregator'];
        }

        if (isset($arrRule['value'])) {
            $rule['value'] = $arrRule['value'];
        }

        if (isset($arrRule['conditions'])) {
            $rule['conditions'] = [];

            if (! empty($arrRule['conditions'])) {
                foreach ($arrRule['conditions'] as $condition) {
                    $rule['conditions'][] = $this->iterateBuildRule($condition);
                }
            }
        }

        return $rule;
    }

}
