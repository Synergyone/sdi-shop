<?php namespace SunnyDayInc\Shop\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

use SunnyDayInc\Shop\Models\WalletDebitInterface;
use SunnyDayInc\Shop\Models\WalletDebit as WalletDebitModel;

class WalletDebit extends Model implements WalletDebitInterface
{

    use WalletDebitModel;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wallet_debit';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'wallet_id', 
    'total', 
    'base_total', 
    'currency_code', 
    'base_currency_code', 
    'base_to_debit_rate', 
    'status', 
    'target', 
    'target_value', 
    'requested_at', 
    'requested_timezone', 
    'completed_at', 
    'completed_timezone', 
    'note'
    ];

}
