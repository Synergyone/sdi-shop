<?php namespace SunnyDayInc\Shop\Cart\Repository;

interface CartInterface
{

    /**
     * Define the Customer Model class name
     *
     * @param  string $model
     * @return void
     */
    public function setModelCustomer($model);

    /**
     * Define the Address Model class name
     *
     * @param  string $model
     * @return void
     */
    public function setModelAddress($model);

    /**
     * Return a cart by it's ID.
     *
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Contracts\Model|null
     */
    public function find($id);

    /**
     * Return a cart by user ID.
     *
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Contracts\Model|null
     */
    public function findByUser($id);

    /**
     * Return carts.
     *
     * @param  array   $where
     * @param  array   $column
     * @param  integer $page
     * @param  integer $limit
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findBy(
        Array $where = [], 
        Array $column = ['*'], 
        $page = 1, 
        $limit = 10, 
        $cart = 'DESC', 
        $cartBy = 'created_at'
    );

    /**
     * Return the current total row in previous query.
     *
     * @return string
     */
    public function getTotal();

    /**
     * Return the cart items by cart ID.
     *
     * @param  integer $cartId
     * @return \Collection|\SunnyDayInc\Shop\Models\CartItemInterface[]
     */
    public function findItemsByCart($cartId);

    /**
     * Return the cart item by cart ID and it's ID.
     *
     * @param  integer $cartId
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Models\CartItemInterface|null
     */
    public function findItemByCartAndId($cartId, $id);

    /**
     * Return the Model class name
     *
     * @return string
     */
    public function model();

    /**
     * Return the Model Item class name
     *
     * @return string
     */
    public function modelItem();

    /**
     * Return the model object creation.
     *
     * @return \SunnyDayInc\Shop\Models\CartInterface
     */
    public function createModel();

    /**
     * Return the model item object creation.
     *
     * @return \SunnyDayInc\Shop\Models\CartItemInterface
     */
    public function createModelItem();

    /**
     * Return the customer model object creation.
     *
     * @return \SunnyDayInc\Shop\Contracts\Model
     */
    public function createModelCustomer();

    /**
     * Return the address model object creation.
     *
     * @return \SunnyDayInc\Shop\Contracts\Model
     */
    public function createModelAddress();

}
