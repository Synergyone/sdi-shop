<?php namespace SunnyDayInc\Shop\Mail\Repository;

use SunnyDayInc\Shop\Models\Eloquent\MailTemplate;

class EloquentTemplate implements Template
{

    /**
     * The model object.
     *
     * @var \SunnyDayInc\Shop\Contracts\Model
     */
    protected $model;

    /**
     * The fields reference.
     *
     * @var array
     */
    protected $fields = [
    'owner_id'      => 'account_id', 
    'name'          => 'type', 
    'language_code' => 'language_code', 
    'reply_address' => 'reply_address', 
    'subject'       => 'subject', 
    'content_text'  => 'content_text', 
    'content_html'  => 'content_html'
    ];

    /**
     * Create a new instance.
     *
     * @param  string $tableName
     * @return void
     */
    public function __construct($tableName)
    {
        $this->createModel();

        $this->model->setTable($tableName);
    }

    /**
     * {@inheritdoc}
     */
    public function getByNameAndLanguage($name, $language)
    {
        $query = $this->model->newQuery();

        $query->where($this->fields['name'], '=', $name);
        $query->where($this->fields['language_code'], '=', $language);

        return $query->first();
    }

    /**
     * {@inheritdoc}
     */
    public function getByOwnerAndNameAndLanguage($ownerId, $name, $language)
    {
        $query = $this->model->newQuery();

        if (! empty($ownerId)) {
            $query->where($this->fields['owner_id'], '=', $ownerId);
        }

        $query->where($this->fields['name'], '=', $name);
        $query->where($this->fields['language_code'], '=', $language);

        return $query->first();
    }

    /**
     * {@inheritdoc}
     */
    public function getTheArray(MailTemplate $template)
    {
        return $template->toArray();
    }

    /**
     * {@inheritdoc}
     */
    public function model()
    {
        return '\SunnyDayInc\Shop\Models\Eloquent\MailTemplate';
    }

    /**
     * Return the model object creation.
     *
     * @return \SunnyDayInc\Shop\Models\Eloquent\MailTemplate
     */
    private function createModel()
    {
        if (! is_null($this->model)) {
            return $this->model;
        }

        $model = $this->model();

        return $this->model = new $model;
    }

}
