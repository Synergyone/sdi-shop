<?php namespace SunnyDayInc\Shop\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

use SunnyDayInc\Shop\Models\CartInterface;
use SunnyDayInc\Shop\Models\Cart as CartModel;
use SunnyDayInc\Shop\Contracts\Presentable;

class Cart extends Model implements CartInterface, Presentable
{

    use CartModel;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cart';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'id', 
    'object_type', 
    'object_id', 
    'user_id', 
    'customer_id', 
    'address_id', 
    'promotion_coupon_id', 
    'promotion_coupon_code', 
    'total_qty'
    ];

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * {@inheritdoc}
     */
    protected function finishSave(array $options)
    {
        parent::finishSave($options);
    }

    /**
     * {@inheritdoc}
     */
    public function delete()
    {
        $this->deleteItems();

        return parent::delete();
    }

    /**
     * Delete items.
     *
     * @return void
     */
    protected function deleteItems()
    {
        CartItem::where('cart_id', '=', $this->id)->delete();
    }

    /**
     * Return items.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany('SunnyDayInc\Shop\Models\Eloquent\CartItem');
    }

    /**
     * Return coupon.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function coupon()
    {
        return $this->hasOne('SunnyDayInc\Shop\Models\Eloquent\PromotionCoupon', 'id', 'promotion_coupon_id');
    }

}
