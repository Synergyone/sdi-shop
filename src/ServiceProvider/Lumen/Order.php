<?php namespace SunnyDayInc\Shop\ServiceProvider\Lumen;

use Illuminate\Support\ServiceProvider;

use SunnyDayInc\Shop\Order\Order as OrderContainer;
use SunnyDayInc\Shop\Order\Repository\EloquentOrder;

class Order extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->configure('order');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerRepository();

        $this->app->singleton(
            'SunnyDayInc\Shop\Contracts\Order', function ($app) {
                $order = new OrderContainer($app['SunnyDayInc\Shop\Order\Repository\OrderInterface']);

                $order->setLogger($app['log']);

                $fieldFormats = $app['config']->get('order.field_formats');

                if (! empty($fieldFormats)) {
                    foreach ($fieldFormats as $field => $format) {
                        $order->addFieldFormat($field, $format);
                    }
                }

                unset($fieldFormats);

                return $order;
            }
        );
    }

    /**
     * Register the repository instance.
     *
     * @return void
     */
    private function registerRepository()
    {
        $this->app->singleton(
            'SunnyDayInc\Shop\Order\Repository\OrderInterface', function ($app) {
                return new EloquentOrder();
            }
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['SunnyDayInc\Shop\Order\Repository\OrderInterface', 'SunnyDayInc\Shop\Contracts\Order'];
    }
}
