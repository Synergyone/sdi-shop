<?php namespace SunnyDayInc\Shop\Models;

use SunnyDayInc\Shop\Support\Collection;
use DateTime;

trait ShipmentTrack
{

    /**
     * The shipment items.
     *
     * @var \Collection|\SunnyDayInc\Shop\Models\ShipmentItemInterface[]
     */
    protected $items;

    /**
     * Finish processing on a successful save operation.
     *
     * @param  array $options
     * @return void
     */
    protected function finishSave(array $options)
    {
        if (! $this->saveItems()) {
            return false;
        }

        return parent::finishSave($options);
    }

    /**
     * Insert a new item.
     *
     * @param  \SunnyDayInc\Shop\Models\ShipmentItemInterface $item
     * @return self
     */
    public function addItem(ShipmentItemInterface $item)
    {
        if (! ($this->items instanceOf Collection)) {
            $this->items = new Collection();
        }

        if (isset($this->id)) {
            $item->shipment_track_id = $this->id;
        }

        $this->items->push($item);

        $this->qty += $item->qty;

        return $this;
    }

    /**
     * Set the shipment picked at.
     *
     * @param  DateTime $time
     * @return self
     */
    public function setPickedAt(DateTime $time)
    {
        $this->picked_at = $time;
    
        return $this;
    }

    /**
     * Set the shipment received at.
     *
     * @param  DateTime $time
     * @return self
     */
    public function setReceivedAt(DateTime $time)
    {
        $this->received_at = $time;
    
        return $this;
    }

    /**
     * Save all items.
     *
     * @return boolean
     */
    private function saveItems()
    {
        $isAllSaved = true;

        if ($this->items instanceOf Collection) {
            foreach ($this->items as $key => $item) {
                $isSaved = true;

                // Assign shipment ID
                $this->items[$key]->shipment_track_id = $this->id;

                try {
                    $isSaved = $this->items[$key]->save();
                } catch (\ErrorException $e) {
                    $isSaved = false;
                }

                if (! $isSaved) {
                    $isAllSaved = false;
                    break;
                }
            }
        }

        return $isAllSaved;
    }

}
