<?php namespace SunnyDayInc\Shop\Models;

use SunnyDayInc\Shop\Contracts\Model;

interface ShipmentTrackInterface extends Model
{

    /**
     * Get all items that owned by the track.
     */
    public function getItems();

}
