<?php namespace SunnyDayInc\Shop\Invoice\Repository;

interface InvoiceInterface
{

    /**
     * Return a invoice by it's ID.
     *
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Contracts\Model|null
     */
    public function find($id);

    /**
     * Return invoices.
     *
     * @param  array   $where
     * @param  array   $column
     * @param  integer $page
     * @param  integer $limit
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findBy(
        Array $where = [], 
        Array $column = ['*'], 
        $page = 1, 
        $limit = 10, 
        $order = 'DESC', 
        $orderBy = 'created_at'
    );

    /**
     * Return the current total row in previous query.
     *
     * @return string
     */
    public function getTotal();

    /**
     * Return the invoice items by invoice ID.
     *
     * @param  integer $invoiceId
     * @return \Collection|\SunnyDayInc\Shop\Models\InvoiceItemInterface[]
     */
    public function findItemsByInvoice($invoiceId);

    /**
     * Return the invoice item by invoice ID and it's ID.
     *
     * @param  integer $invoiceId
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Models\InvoiceItemInterface|null
     */
    public function findItemByInvoiceAndId($invoiceId, $id);

    /**
     * Return the Model class name
     *
     * @return string
     */
    public function model();

    /**
     * Return the Model Item class name
     *
     * @return string
     */
    public function modelItem();

    /**
     * Return the Model Comment class name
     *
     * @return string
     */
    public function modelComment();

    /**
     * Return the model object creation.
     *
     * @return \SunnyDayInc\Shop\Models\InvoiceInterface
     */
    public function createModel();

    /**
     * Return the model item object creation.
     *
     * @return \SunnyDayInc\Shop\Models\InvoiceItemInterface
     */
    public function createModelItem();

    /**
     * Return the model comment object creation.
     *
     * @return \SunnyDayInc\Shop\Models\InvoiceCommentInterface
     */
    public function createModelComment();

}
