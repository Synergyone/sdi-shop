<?php namespace SunnyDayInc\Shop\Models;

use SunnyDayInc\Shop\Models\InvoiceItemInterface;

trait CreditMemoItem
{

    /**
     * Set the item going to use.
     *
     * @param  \SunnyDayInc\Shop\Models\InvoiceItemInterface $item
     * @return self
     */
    public function setItem(InvoiceItemInterface $item)
    {
        $this->order_item_id        = $item->id;
        $this->object_type          = $item->object_type;
        $this->object_id            = $item->object_id;
        $this->sku                  = $item->sku;
        $this->title                = $item->title;
        $this->options              = $item->options;
        $this->description          = $item->description;
        $this->qty                  = $item->qty;

        $this->price                = $item->price;
        $this->base_price           = $item->base_price;

        $this->price_incl_tax       = $this->price + (($this->price * $item->tax_percent) / 100);
        $this->base_price_incl_tax  = $this->base_price + (($this->base_price * $item->tax_percent) / 100);

        $this->discount_amount      = $item->discount_amount;
        $this->base_discount_amount = $item->base_discount_amount;
    
        $this->currency_code        = $item->currency_code;

        return $this;
    }

    /**
     * Set the item quantity.
     *
     * @param  integer $qty
     * @return self
     */
    public function setQty($qty)
    {
        $this->qty = $qty;

        $this->calculate();

        return $this;
    }

    /**
     * Calculate.
     *
     * @return self
     */
    public function calculate()
    {
        $this->total                = ($this->price - $this->discount_amount) * $this->qty;
        $this->base_total           = ($this->base_price - $this->base_discount_amount) * $this->qty;

        $this->tax_amount           = ($this->price_incl_tax - $this->price) * $this->qty;
        $this->base_tax_amount      = ($this->base_price_incl_tax - $this->base_price) * $this->qty;

        $this->total_incl_tax       = $this->total + $this->tax_amount;
        $this->base_total_incl_tax  = $this->base_total + $this->base_tax_amount;

        return $this;
    }

}
