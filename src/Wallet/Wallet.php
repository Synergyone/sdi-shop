<?php namespace SunnyDayInc\Shop\Wallet;

use SunnyDayInc\Shop\Contracts\Wallet as WalletInterface;
use SunnyDayInc\Shop\Wallet\Repository\Wallet as WalletRepository;
use SunnyDayInc\Shop\Models\WalletInterface as WalletModelInterface;

use Illuminate\Contracts\Logging\Log;

class Wallet implements WalletInterface
{

    const DEBIT_STATE_PENDING   = 'pending';
    const DEBIT_STATE_CONFIRMED = 'confirmed';
    const DEBIT_STATE_HOLDED    = 'holded';
    const DEBIT_STATE_CANCELLED = 'cancelled';
    const DEBIT_STATE_COMPLETED = 'completed';

    /**
     * The repository instance.
     *
     * @var \SunnyDayInc\Shop\Wallet\Repository\Wallet
     */
    protected $repository;

    /**
     * The log instance.
     *
     * @var \Illuminate\Contracts\Logging\Log
     */
    protected $log;
  
    /**
     * Create a new instance.
     *
     * @param  \SunnyDayInc\Shop\Wallet\Repository\Wallet $repository
     * @return void
     */
    public function __construct(WalletRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    public function setLogger(Log $log)
    {
        $this->log = $log;
    }

    /**
     * {@inheritdoc}
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * {@inheritdoc}
     */
    public function create(Array $data = [])
    {
        return $this->createFactory($data);
    }

    /**
     * {@inheritdoc}
     */
    public function find($id, $objectType = '', $objectId = '')
    {
        $wallet = $this->repository->find($id, $objectType, $objectId);

        if ($wallet instanceOf WalletModelInterface) {
            $factory = $this->createFactory();

            $factory->setWallet($wallet);

            unset($wallet);

            return $factory;
        }

        return null;
    }

    /**
     * Create the factory instance.
     *
     * @param  array $data
     * @return \SunnyDayInc\Shop\Wallet\FactoryInterface
     */
    private function createFactory(Array $data = [])
    {
        $factory = new Factory($this->repository, $data);

        if (! is_null($this->log)) {
            $factory->setLogger($this->log);
        }

        return $factory;
    }

}
