<?php namespace SunnyDayInc\Shop\Payment\Presenter;

use SunnyDayInc\Shop\Presenter\Presenter;
use SunnyDayInc\Shop\Models\PaymentInterface;
use DateTime;

class Payment extends Presenter
{

    /**
     * The Payment model.
     *
     * @var Payment
     */
    protected $_model;

    /**
     * The transactions.
     *
     * @var \Collection
     */
    protected $_transactions;

    /**
     * {@inheritdoc}
     */
    protected $publicData = [
    'id'                         => 'id', 
    'created_at'                 => 'created_at', 
    'updated_at'                 => 'updated_at'
    ];

    /**
     * Create the Presenter and store the object we are presenting.
     *
     * @param  PaymentInterface $payment
     * @param  array            $fields
     * @return void
     */
    public function __construct(PaymentInterface $payment, Array $fields = ['*'])
    {
        parent::__construct($payment->toArray());

        // Select field to publish
        // Remove the rest
        $newPublicData = [];

        if (is_array($fields)) {
            if (trim($fields[0]) != '*') {
                foreach ($this->publicData as $key => $data) {
                    foreach ($fields as $item) {
                        if ($data == $item) {
                            $newPublicData[$key] = $data;
                        }
                    }
                }
            }
        }

        if (! empty($newPublicData)) {
            $this->publicData = $newPublicData;
        }

        $this->_model = $payment;
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentId()
    {
        return (int) $this->_model->id;
    }

    /**
     * Format and return the data.
     *
     * @return array
     */
    public function presentItems()
    {
        if (! is_null($this->_transactions)) {
            return $this->_transactions;
        }

        $itemList            = $this->_model->transactions;
        $this->_transactions = new Collection();

        if (! $itemList->isEmpty()) {

            foreach ($itemList as $item) {
                $this->_transactions->push($presenter);
            }
        }

        unset($itemList);

        return $this->_transactions;
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentCreatedAt()
    {
        if ($this->_model->created_at != '0000-00-00 00:00:00') {
            return (new DateTime($this->_model->created_at))->format('c');
        }

        return null;
    }

    /**
     * Format and return the data.
     *
     * @return string
     */
    public function presentUpdatedAt()
    {
        if ($this->_model->updated_at != '0000-00-00 00:00:00') {
            return (new DateTime($this->_model->updated_at))->format('c');
        }

        return null;
    }

}
