<?php namespace SunnyDayInc\Shop\Payment\Repository;

interface PaymentInterface
{

    /**
     * Return the current total row in previous query.
     *
     * @return string
     */
    public function getTotal();

    /**
     * Return the Model class name
     *
     * @return string
     */
    public function model();

    /**
     * Return the Model Transaction class name
     *
     * @return string
     */
    public function modelTransaction();

    /**
     * Return the Model Credit Card class name
     *
     * @return string
     */
    public function modelCreditCard();

    /**
     * Return the model object creation.
     *
     * @return \SunnyDayInc\Shop\Models\PaymentInterface
     */
    public function createModel();

    /**
     * Return the model transaction object creation.
     *
     * @return \SunnyDayInc\Shop\Models\PaymentTransactionInterface
     */
    public function createModelTransaction();

    /**
     * Return the model credit card object creation.
     *
     * @return \SunnyDayInc\Shop\Models\PaymentCreditCardInterface
     */
    public function createModelCreditCard();

}
