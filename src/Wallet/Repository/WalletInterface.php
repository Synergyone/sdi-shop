<?php namespace SunnyDayInc\Shop\Wallet\Repository;

interface WalletInterface
{

    /**
     * Return a wallet by it's ID.
     *
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Contracts\Model|null
     */
    public function find($id, $objectType = '', $objectId = '');

    /**
     * Return wallets.
     *
     * @param  array   $where
     * @param  array   $column
     * @param  integer $page
     * @param  integer $limit
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findBy(
        Array $where = [], 
        Array $column = ['*'], 
        $page = 1, 
        $limit = 10, 
        $order = 'DESC', 
        $orderBy = 'created_at'
    );

    /**
     * Return the current total row in previous query.
     *
     * @return string
     */
    public function getTotal();

    /**
     * Return the wallet credit by wallet ID and it's ID.
     *
     * @param  integer $walletId
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Models\WalletCreditInterface|null
     */
    public function findCreditByWalletAndId($walletId, $id);

    /**
     * Return the wallet debit by wallet ID and it's ID.
     *
     * @param  integer $walletId
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Models\WalletDebitInterface|null
     */
    public function findDebitByWalletAndId($walletId, $id);

    /**
     * Return the Model class name
     *
     * @return string
     */
    public function model();

    /**
     * Return the Model Credit class name
     *
     * @return string
     */
    public function modelCredit();

    /**
     * Return the Model Debit class name
     *
     * @return string
     */
    public function modelDebit();

    /**
     * Return the model object creation.
     *
     * @return \SunnyDayInc\Shop\Models\WalletInterface
     */
    public function createModel();

    /**
     * Return the model credit object creation.
     *
     * @return \SunnyDayInc\Shop\Models\WalletCreditInterface
     */
    public function createModelCredit();

    /**
     * Return the model debit object creation.
     *
     * @return \SunnyDayInc\Shop\Models\WalletDebitInterface
     */
    public function createModelDebit();

}
