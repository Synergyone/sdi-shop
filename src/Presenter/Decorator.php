<?php namespace SunnyDayInc\Shop\Presenter;

use SunnyDayInc\Shop\Contracts\Presentable;
use ArrayAccess, IteratorAggregate;

class Decorator
{

    /*
      * If this variable implements SunnyDayInc\Shop\Contracts\Presentable
      * then turn it into a presenter.
      *
      * @param  mixed $value
      * @return mixed $value
      */
    public function decorate($value)
    {
        if ($value instanceof Presentable) {
            return $value->getPresenter();
        }

        if (is_array($value) or ($value instanceof IteratorAggregate and $value instanceof ArrayAccess)) {
            foreach ($value as $k => $v) {
                $value[$k] = $this->decorate($v);
            }
        }

        return $value;
    }
  
}