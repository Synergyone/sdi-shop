<?php namespace SunnyDayInc\Shop\Invoice;

use SunnyDayInc\Shop\Models\InvoiceInterface;
use SunnyDayInc\Shop\Models\InvoiceItemInterface;

interface FactoryInterface
{

    /**
     * Set the logger instance.
     *
     * @param  mixed $log
     * @return self
     */
    public function setLogger($log);

    /**
     * Set the invoice instance.
     *
     * @param  \SunnyDayInc\Shop\Models\InvoiceInterface $invoice
     * @return self
     */
    public function setInvoice(InvoiceInterface $invoice);

    /**
     * Return the invoice instance.
     *
     * @return \SunnyDayInc\Shop\Models\InvoiceInterface
     */
    public function getInvoice();

    /**
     * Return the items.
     *
     * @return \Collection|\SunnyDayInc\Shop\Models\InvoiceItemInterface[]
     */
    public function getItems();

    /**
     * Get a item by it's ID.
     *
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Models\InvoiceItemInterface|null
     */
    public function getItem($id);

    /**
     * Insert a new item.
     *
     * @param  \SunnyDayInc\Shop\Models\InvoiceItemInterface $item
     * @return self
     */
    public function addItem(InvoiceItemInterface $item);

    /**
     * Create a new invoice item.
     *
     * @return \SunnyDayInc\Shop\Models\InvoiceItemInterface
     */
    public function createItem();

    /**
     * Set comment.
     *
     * @param  string  $comment
     * @param  boolean $notifyCustomer
     * @param  boolean $visibleInFront
     * @return self
     */
    public function setComment($comment, $notifyCustomer = false, $visibleInFront = false);

    /**
     * Save the invoice.
     *
     * @return boolean
     */
    public function save();

}
