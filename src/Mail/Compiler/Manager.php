<?php namespace SunnyDayInc\Shop\Mail\Compiler;

class Manager
{

    /**
     * The data handlers.
     *
     * @var array
     */
    protected $handlers = [];

    /**
     * Register a handler.
     *
     * @param  string $handle
     * @param  mixed  $handler
     * @return \SunnyDayInc\Shop\Mail\Compiler
     */
    public function registerHandler($handle, $handler)
    {
        $this->handlers[$handle] = $handler;

        return $this;
    }

    /**
     * Return specific data compiler.
     *
     * @param  string $handle
     * @return \SunnyDayInc\Shop\Mail\Compiler\HandlerInterface
     */
    public function getHandler($handle)
    {
        if (isset($this->handlers[$handle])) {
            return new $this->handlers[$handle];
        }

        return null;
    }

}
