<?php namespace SunnyDayInc\Shop\Models;

use SunnyDayInc\Shop\Models\InvoiceItemInterface;

trait ShipmentItem
{

    /**
     * Set the invoice item going to use.
     *
     * @param  \SunnyDayInc\Shop\Models\InvoiceItemInterface $invoice
     * @return self
     */
    public function setItem(InvoiceItemInterface $invoice)
    {
        $this->order_item_id = $invoice->order_item_id; 
        $this->product_id    = $invoice->object_id;
        $this->sku           = $invoice->sku;
        $this->title         = $invoice->title;
        $this->options       = $invoice->options;
        $this->description   = $invoice->description;
        $this->price         = $invoice->price;
        $this->qty           = $invoice->qty;
        $this->total         = $invoice->total;

        return $this;
    }

}
