<?php namespace SunnyDayInc\Shop\Currency\Repository;

abstract class Currency implements CurrencyInterface
{

    /**
     * {@inheritdoc}
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * {@inheritdoc}
     */
    public function createModel()
    {
        $model = $this->model();

        return new $model;
    }

    /**
     * {@inheritdoc}
     */
    public function createModelFormat()
    {
        $model = $this->modelFormat();

        return new $model;
    }

}
