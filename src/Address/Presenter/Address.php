<?php namespace SunnyDayInc\Shop\Address\Presenter;

use SunnyDayInc\Shop\Support\Collection;
use SunnyDayInc\Shop\Presenter\Presenter;
use SunnyDayInc\Shop\Models\AddressInterface;

class Address extends Presenter
{

    /**
     * The Address model.
     *
     * @var Address
     */
    protected $_model;

    /**
     * {@inheritdoc}
     */
    protected $publicData = [
    'id'           => 'id', 
    'title'        => 'title', 
    'company'      => 'company', 
    'phone'        => 'phone', 
    'mobile_phone' => 'mobile_phone', 
    'address'      => 'address', 
    'country_code' => 'country_code', 
    'country_name' => 'country_name', 
    'state'        => 'state', 
    'city_id'      => 'city_id', 
    'city_name'    => 'city_name', 
    'postcode'     => 'postcode', 
    'is_default'   => 'is_default', 
    'is_active'    => 'is_active'
    ];

    /**
     * Create the Presenter and store the object we are presenting.
     *
     * @param  AddressInterface $address
     * @param  array            $fields
     * @return void
     */
    public function __construct(AddressInterface $address, Array $fields = ['*'])
    {
        parent::__construct($address->toArray());

        $this->_model = $address;
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentId()
    {
        return (int) $this->_model->id;
    }

    /**
     * Format and return the data.
     *
     * @return integer
     */
    public function presentCityId()
    {
        return (int) $this->_model->city_id;
    }

    /**
     * Format and return the data.
     *
     * @return boolean
     */
    public function presentIsDefault()
    {
        return (bool) $this->_model->is_default;
    }

    /**
     * Format and return the data.
     *
     * @return boolean
     */
    public function presentIsActive()
    {
        return (bool) $this->_model->is_active;
    }

}
