<?php namespace SunnyDayInc\Shop\Order;

use SunnyDayInc\Shop\Support\Collection;

use SunnyDayInc\Shop\Order\Repository\CustomerInterface as CustomerRepositoryContract;
use SunnyDayInc\Shop\Models\OrderInterface;
use SunnyDayInc\Shop\Models\OrderItemInterface;
use SunnyDayInc\Shop\Models\OrderAddressInterface;
use SunnyDayInc\Shop\Models\PromotionInterface;

interface FactoryInterface
{

    /**
     * Set the logger instance.
     *
     * @param  mixed $log
     * @return self
     */
    public function setLogger($log);

    /**
     * Set the order instance.
     *
     * @param  \SunnyDayInc\Shop\Models\OrderInterface $order
     * @return self
     */
    public function setOrder(OrderInterface $order);

    /**
     * Return the order instance.
     *
     * @return \SunnyDayInc\Shop\Models\OrderInterface
     */
    public function getOrder();

    /**
     * Set the customer repository.
     *
     * @param  \SunnyDayInc\Shop\Order\Repository\CustomerInterface $repository
     * @return self
     */
    public function setCustomerRepository(CustomerRepositoryContract $repository);

    /**
     * Return the items.
     *
     * @return \Collection|\SunnyDayInc\Shop\Models\OrderItemInterface[]
     */
    public function getItems();

    /**
     * Get a item by it's ID.
     *
     * @param  integer $id
     * @return \SunnyDayInc\Shop\Models\OrderItemInterface|null
     */
    public function getItem($id);

    /**
     * Insert a new item.
     *
     * @param  \SunnyDayInc\Shop\Models\OrderItemInterface $item
     * @return self
     */
    public function addItem(OrderItemInterface $item);

    /**
     * Create a new order item.
     *
     * @return \SunnyDayInc\Shop\Models\OrderItemInterface
     */
    public function createItem();

    /**
     * Insert a new item promotion.
     *
     * @param  \Collection\\SunnyDayInc\Shop\Models\PromotionInterface[] $promotions
     * @return self
     */
    public function addItemPromotions(Collection $promotions);

    /**
     * Insert a new promotion.
     *
     * @param  \SunnyDayInc\Shop\Models\PromotionInterface $promo
     * @return boolean
     */
    public function addPromotion(PromotionInterface $promo);

    /**
     * Get order billing address by it's type.
     *
     * @param  string $type
     * @return \SunnyDayInc\Shop\Models\OrderAddressInterface|null
     */
    public function getAddress($type);

    /**
     * Insert a new address.
     *
     * @param  \SunnyDayInc\Shop\Models\OrderAddressInterface $address
     * @return self
     */
    public function addAddress(OrderAddressInterface $address);

    /**
     * Create a new address.
     *
     * @return \SunnyDayInc\Shop\Models\OrderAddressInterface
     */
    public function createAddress();

    /**
     * Set status.
     *
     * @param  string  $status
     * @param  string  $entity
     * @param  string  $comment
     * @param  boolean $notifyCustomer
     * @param  boolean $visibleInFront
     * @return self
     */
    public function setStatus($status, $entity, $comment = '', 
        $notifyCustomer = false, $visibleInFront = false
    );

    /**
     * Calculate order pricing.
     *
     * @return self
     */
    public function calculate();

    /**
     * Save the order.
     *
     * @return boolean
     */
    public function save();

}
