<?php namespace SunnyDayInc\Shop\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

use SunnyDayInc\Shop\Models\ShipmentItemInterface;
use SunnyDayInc\Shop\Models\ShipmentItem as ShipmentItemModel;

class ShipmentItem extends Model implements ShipmentItemInterface
{

    use ShipmentItemModel;
  
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shipment_item';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'shipment_track_id', 
    'order_item_id', 
    'product_id', 
    'sku', 
    'title', 
    'options', 
    'description', 
    'price', 
    'qty', 
    'total'
    ];

}
