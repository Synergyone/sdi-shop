<?php namespace SunnyDayInc\Shop\ServiceProvider\Illuminate;

use Illuminate\Support\ServiceProvider;

use SunnyDayInc\Shop\Address\Address as AddressContainer;
use SunnyDayInc\Shop\Address\Repository\EloquentAddress;

class Address extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerRepository();

        $this->app->singleton(
            'SunnyDayInc\Shop\Contracts\Address', function ($app) {
                return new AddressContainer($app['SunnyDayInc\Shop\Address\Repository\AddressInterface']);
            }
        );
    }

    /**
     * Register the repository instance.
     *
     * @return void
     */
    private function registerRepository()
    {
        $this->app->singleton(
            'SunnyDayInc\Shop\Address\Repository\AddressInterface', function ($app) {
                return new EloquentAddress();
            }
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
        'SunnyDayInc\Shop\Address\Repository\AddressInterface', 
        'SunnyDayInc\Shop\Contracts\Address'
        ];
    }
}
