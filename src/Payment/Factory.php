<?php namespace SunnyDayInc\Shop\Payment;

use SunnyDayInc\Shop\Payment\Repository\Payment as PaymentRepository;

use SunnyDayInc\Shop\Support\Collection;

use SunnyDayInc\Shop\Models\PaymentInterface;
use SunnyDayInc\Shop\Models\PaymentTransactionInterface;

class Factory implements FactoryInterface
{

    /**
     * The log context.
     *
     * @var string
     */
    const LOG_CONTEXT = 'payment.factory';

    /**
     * The repository instance.
     *
     * @var \SunnyDayInc\Shop\Payment\Repository\Payment
     */
    protected $repository;

    /**
     * The field formats.
     *
     * @var array
     */
    protected $fieldFormats;

    /**
     * The payment model instance.
     *
     * @var \SunnyDayInc\Shop\Models\PaymentInterface
     */
    protected $payment;

    /**
     * The payment transactions.
     *
     * @var \Collection|\SunnyDayInc\Shop\Models\PaymentTransactionInterface[]
     */
    protected $transactions;

    /**
     * Create a new instance.
     *
     * @param  \SunnyDayInc\Shop\Payment\Repository\Payment $repository
     * @param  array                                        $fieldFormats
     * @param  array                                        $data
     * @return void
     */
    public function __construct(PaymentRepository $repository, Array $fieldFormats, Array $data = [])
    {
        $this->repository = $repository;
        $this->fieldFormats = $fieldFormats;

        $this->transactions = new Collection();
    
        // Create the payment model and fill the default data
        if (! empty($data)) {
            $this->payment = $this->repository->createModel();
            $this->payment->fill($data);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setLogger($log)
    {
        $this->log = $log;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setPayment(PaymentInterface $payment)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * {@inheritdoc}
     */
    public function getTransactions()
    {
        if (is_null($this->payment)) {
            return $this->transactions;
        }

        $paymentId = (isset($this->payment->id)) ? $this->payment->id : 0;

        // Because the payment is not yet saved
        // we cannot identify any item by it's ID
        if (empty($paymentId)) {
            return $this->transactions;
        }

        // Fetch all transactions from repository
        if ($this->transactions->isEmpty()) {
            $transactions = $this->repository->findTransactionsByPayment($paymentId);

            if (! $transactions->isEmpty()) {
                foreach ($transactions as $item) {
                    $this->transactions->put($item->id, $item);
                }
            }

            unset($transactions);
        }

        return $this->transactions;
    }

    /**
     * {@inheritdoc}
     */
    public function getTransaction($id)
    {
        if (is_null($this->payment)) {
            return null;
        }

        $paymentId = (isset($this->payment->id)) ? $this->payment->id : 0;

        // Because the payment is not yet saved
        // we cannot identify any item by it's ID
        if (empty($paymentId)) {
            return null;
        }

        if (! $this->transactions->isEmpty()) {
            $item = $this->transactions->get($id);

            if ($item instanceOf PaymentTransactionInterface) {
                return $item;
            }
        }

        $item = $this->repository->findTransactionByPaymentAndId($paymentId, $id);

        if (! ($item instanceOf PaymentTransactionInterface)) {
            return null;
        }

        $this->transactions->put($item->id, $item);

        return $item;
    }

    /**
     * {@inheritdoc}
     */
    public function addTransaction(PaymentTransactionInterface $transaction)
    {
        // Set the payment ID if available
        if (! empty($this->payment->id)) {
            $transaction->payment_id = $this->payment->id;
        }

        // Add transaction to collection
        $this->transactions->push($transaction);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function createTransaction()
    {
        return $this->repository->createModelTransaction();
    }

    /**
     * {@inheritdoc}
     */
    public function save()
    {
        $connection = $this->payment->getConnection();

        // Initiate transaction
        $connection->beginTransaction();

        $isSaved = true;

        // Save the payment
        // Do field formatters if available only when it's new payment
        if (! $this->payment->exists && ! empty($this->fieldFormats)) {
            foreach ($this->fieldFormats as $field => $format) {
                $handler = new $format['formatter']($format['format'], $this->payment);

                $this->payment->$field = $handler->compile();

                unset($handler);
            }
        }

        try {
            $isSaved = $this->payment->save();
        } catch (\ErrorException $e) {
            $this->createLog('error', $e);

            $isSaved = false;
        }

        if (! $isSaved) {
            // Rollback the transaction
            $connection->rollBack();

            return false;
        }

        // Get the increment payment ID
        $paymentId = $this->payment->id;

        // Save all transactions
        if (! $this->saveTransactions()) {
            // Rollback the transaction
            $connection->rollBack();

            return false;
        }

        // Commit the transaction
        $connection->commit();

        return true;
    }

    /**
     * Save all transactions.
     *
     * @return boolean
     */
    private function saveTransactions()
    {
        $isAllSaved = true;

        if ($this->transactions instanceOf Collection) {
            foreach ($this->transactions as $key => $item) {
                $isSaved = true;

                // Assign payment ID
                $this->transactions[$key]->payment_id = $this->payment->id;

                try {
                    $isSaved = $this->transactions[$key]->save();
                } catch (\ErrorException $e) {
                    $this->createLog('error', $e);

                    $isSaved = false;
                }

                if (! $isSaved) {
                    $isAllSaved = false;
                    break;
                }
            }
        }

        return $isAllSaved;
    }

    /**
     * Return a created presenter.
     *
     * @return \SunnyDayInc\Shop\Presenter\Presenter
     */
    public function getPresenter()
    {
        return $this->payment->getPresenter();
    }

    /**
     * Create a new log.
     *
     * @param  string $level
     * @param  mixed  $message
     * @param  Array  $data
     * @return void
     */
    private function createLog($level, $message, Array $data = [])
    {
        if (! is_null($this->log)) {
            $data['context'] = self::LOG_CONTEXT;

            if ($message instanceOf \ErrorException) {
                $this->log->$level($message->getCode().': '.$message->getMessage()."\n".$message->getTraceAsString(), $data);
            } else {
                $this->log->$level($message, $data);
            }
        }
    }

}
